﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IRUN.API.Functions;
namespace IRUN.API.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : iRunApiController
    {
        private readonly IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        [Route("login-by-accesstoken"), HttpPost]
        public IHttpActionResult LoginByToken([FromBody] LoginModel model)
        {
            model.req_info = GetClientInfo();
            return Ok(_accountService.LoginByToken(model));
        }

        [Route("me-login"), HttpPost]
        public IHttpActionResult Auth([FromBody] LoginModel model)
        {
            model.req_info = function_Client.GetClientInfo();
            return Ok(_accountService.AccountLogin(model));
        }

        [Route("register"), HttpPost]
        public IHttpActionResult Register([FromBody] AccountRegisterModel model)
        {
            return Ok(_accountService.AccountRegister(model));
        }

        [Route("facebook-login"), HttpPost]
        public IHttpActionResult AuthSocial([FromBody] AccountLoginSocialModel model)
        {
            model.req_info = GetClientInfo();
            return Ok(_accountService.FacebookLogin(model));
        }

        [Route("change-password"), HttpPost]
        //    [ApiAuthorizeActionFilter]
        public IHttpActionResult ChangePassword([FromBody] AccountChangePasswordModel model)
        {
            return Ok(_accountService.AccountChangePassword(ACCESS_TOKEN, model));
        }

        [Route("get-gold"), HttpGet]
        public IHttpActionResult GetGoldByUser()
        {
            return Ok(_accountService.GetGoldByUser(ACCESS_TOKEN));
        }
        [Route("get-account-info"), HttpGet]
        public IHttpActionResult GetAccountInfo()
        {
            return Ok(_accountService.GetAccountInfo(ACCESS_TOKEN));
        }

        [Route("check-login-otp"), HttpGet]
        public IHttpActionResult CheckOTP()
        {
            return Ok(_accountService.OnOffOTP());
        }

        [Route("update-account-info"), HttpPost]
        public IHttpActionResult UpdateAccountInfo([FromBody] UpdateAccountViewModel model)
        {
            return Ok(_accountService.UpdateAccountInfo(ACCESS_TOKEN, model));
        }
    }
}
