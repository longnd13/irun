﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Prj.Irun.Utilities;
namespace IRUN.API.Controllers
{
    [RoutePrefix("api/common")]
    public class CommonController : ApiController
    {
        [Route("sync-time"), HttpGet]
        public IHttpActionResult SyncTimes()
        {
            return Ok(DateHandler.DateNow());
        }
    }
}
