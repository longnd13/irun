﻿using Prj.Irun.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IRUN.API.Controllers
{
    [RoutePrefix("api/config")]
    public class ConfigController : iRunApiController
    {

        private readonly IapiClientConfigService _apiClientConfigService;
     
        public ConfigController(IapiClientConfigService apiClientConfigService)
        {
            _apiClientConfigService = apiClientConfigService;
           
        }

        [Route("get-client-config"), HttpGet]
        public IHttpActionResult GetAllClientConfig()
        {
            return Ok(_apiClientConfigService.GetAllClientConfig());
        }
    }
}
