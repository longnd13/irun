﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IRUN.API.Controllers
{
    [RoutePrefix("api/main-core")]
    public class MainCoreController : iRunApiController
    {
        private readonly IAccountService _accountService;
        private readonly IMainCoreService _mainCoreService;
        public MainCoreController(IAccountService accountService, IMainCoreService mainCoreService)
        {
            _mainCoreService = mainCoreService;
            _accountService = accountService;
        }

        [Route("post-steps"), HttpPost]
        public IHttpActionResult PostSteps([FromBody] List<PostStepsModel> models)
        {
            return Ok(_mainCoreService.PostSteps(models, ACCESS_TOKEN));
        }
        [Route("post-distances"), HttpPost]
        public IHttpActionResult PostInstances([FromBody] List<PostStepsModel> models)
        {
            return Ok(_mainCoreService.PostDistance(models, ACCESS_TOKEN));
        }
    }
}
