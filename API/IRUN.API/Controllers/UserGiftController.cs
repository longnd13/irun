﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IRUN.API.Controllers
{
    [RoutePrefix("api/user-gift")]
    public class UserGiftController : iRunApiController
    {
        private readonly IAccountService _accountService;
        private readonly IMainCoreService _mainCoreService;
        private readonly IapiGiftService _apiGiftService;
        public UserGiftController(IAccountService accountService, IMainCoreService mainCoreService, IapiGiftService apiGiftService)
        {
            _mainCoreService = mainCoreService;
            _accountService = accountService;
            _apiGiftService = apiGiftService;
        }

        [Route("open-basic-gift"), HttpPost]
        public IHttpActionResult OpenBasicGift([FromBody]  OpenBasicGiftModel model)
        {
            return Ok(_mainCoreService.OpenBasicGift(ACCESS_TOKEN, model));
        }

        [Route("get-daily-gift"), HttpGet]
        public IHttpActionResult OpenDailyGift()
        {
            return Ok(_mainCoreService.GetDailyGiftByUser(ACCESS_TOKEN));
        }

        [Route("open-daily-gift"), HttpPost]
        public IHttpActionResult OpenDailyGift([FromBody]  OpenDailyGiftModel model)
        {
            return Ok(_mainCoreService.OpenDailyGift(ACCESS_TOKEN, model));
        }

        [Route("get-cumulative-gift"), HttpGet]
        public IHttpActionResult GetCumulativeGift()
        {
            return Ok(_apiGiftService.GetCumulativeGift(ACCESS_TOKEN));
        }

        [Route("open-cumulative-gift"), HttpPost]
        public IHttpActionResult OpenCumulativeGift([FromBody]  OpenCumulativeGiftModel model)
        {
            return Ok(_apiGiftService.OpenCumulativeGift(ACCESS_TOKEN, model));
        }
    }
}
