﻿using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace IRUN.API.Controllers
{
    public class iRunApiController : ApiController
    {
        public iRunApiController()
        {
          
        }
        protected string ACCESS_TOKEN
        {
            get { return Request.Headers.GetValues("access_token").SingleOrDefault(); }
        }
        protected ClientInfoModel GetClientInfo()
        {
            return new ClientInfoModel()
            {
                ip_address = HttpContext.Current.Request.UserHostAddress,
                user_agent = HttpContext.Current.Request.UserAgent
            };
        }


    }
}
