﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.Utilities;
namespace IRUN.API.Controllers
{
    [RoutePrefix("api/test")]
    public class testController : ApiController
    {

        private readonly ItestService _testService;
        public testController(ItestService testService)
        {
            _testService = testService;
        }

        [Route("demo")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return Ok(_testService.getdata());
        }

        [Route("abc")]
        [HttpGet]
        public IHttpActionResult getaaaaa()
        {
            return Ok("OK Done!");
        }

        [Route("rsa-key")]
        [HttpGet]
        public IHttpActionResult rsakey()
        {

            RSAHandler.RSACryptography RSA = new RSAHandler.RSACryptography();


            // ma hoa
            string plaintext = "nguyenlong";
            string encryptedText = RSA.Encrypt(RSAHandler.RSAKey.publicKey, plaintext);

            // giải mã 
            string decryptedText = RSA.Decrypt(RSAHandler.RSAKey.privateKey, encryptedText);

            return Ok("OK Done!");
        }

        [Route("get-token-client")]
        [HttpGet]
        public IHttpActionResult tokenclient()
        {

            RSAHandler.RSACryptography RSA = new RSAHandler.RSACryptography();


            // ma hoa
            string plaintext = "0905181757" + "*" + "5274F814F533C442EC0FC3962BB30ADD";
            string encryptedText = RSA.Encrypt(RSAHandler.RSAKey.publicKey, plaintext);

            // giải mã 
            string decryptedText = RSA.Decrypt(RSAHandler.RSAKey.privateKey, encryptedText);

            return Ok(decryptedText);
        }

        [Route("caculator")]
        [HttpGet]
        public IHttpActionResult caculator()
        {

            int a = 101;
            int b = 3;
            var c = a % b;

            return Ok(c);
        }



    }
}
