﻿using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRUN.API.Functions
{
    public class function_Client
    {
        public static ClientInfoModel GetClientInfo()
        {
            return new ClientInfoModel()
            {
                ip_address = HttpContext.Current.Request.UserHostAddress,
                user_agent = HttpContext.Current.Request.UserAgent
            };
        }
    }
}