﻿using IRUN.API.Infrastructure;
using Ninject;
using Ninject.Modules;
using Prj.Irun.BusinessLogic.AutoMapper;
using Prj.Irun.BusinessLogic.ModuleConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace IRUN.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoMapperConfig.ConfigureMapper();
            //IKernel kernel = new StandardKernel(new ServicesModule());
            //DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));


            NinjectModule registrations = new ServicesModule();
            var kernel = new StandardKernel(registrations);
            var ninjectResolver = new NinjectDependencyResolver(kernel);

       //     DependencyResolver.SetResolver(ninjectResolver); // MVC
            GlobalConfiguration.Configuration.DependencyResolver = ninjectResolver; // Web API
        }
    }
}
