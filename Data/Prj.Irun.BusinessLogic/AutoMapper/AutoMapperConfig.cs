﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.AutoMapper
{
    public class AutoMapperConfig
    {
        public static T MapperModel<T>(object target)
        {
            return Mapper.Map<T>(target);
        }

        public static void ConfigureMapper()
        {
            ConfigureMapping();
        }
        private static void ConfigureMapping()
        {
            Mapper.Initialize(cfg =>
            {
               
                cfg.CreateMap<testModel, testEntity>();
                cfg.CreateMap<testEntity, testModel>();

                cfg.CreateMap<AccountViewModel, AccountInfoEntity>();
                cfg.CreateMap<AccountInfoEntity, AccountViewModel>();

                cfg.CreateMap<MessageModel, MessageEntity>();
                cfg.CreateMap<MessageEntity, MessageModel>();

                cfg.CreateMap<AccountInfoModel, AccountInfoEntity>();
                cfg.CreateMap<AccountInfoEntity, AccountInfoModel>();

                cfg.CreateMap<PrivacySetting_DataEntity, PrivacySetting_DataModel>();
                cfg.CreateMap<PrivacySetting_DataModel, PrivacySetting_DataEntity>();
                cfg.CreateMap<PrivacySettingEntity, PrivacySettingModel>();
                cfg.CreateMap<PrivacySettingModel, PrivacySettingEntity>();

                cfg.CreateMap<Season_DataEntity, Season_DataModel>();
                cfg.CreateMap<Season_DataModel, Season_DataEntity>();
                cfg.CreateMap<SeasonEntity, SeasonModel>();
                cfg.CreateMap<SeasonModel, SeasonEntity>();

                cfg.CreateMap<Post_DataEntity, Post_DataModel>();
                cfg.CreateMap<Post_DataModel, Post_DataEntity>();
                cfg.CreateMap<PostEntity, PostModel>();
                cfg.CreateMap<PostModel, PostEntity>();

                cfg.CreateMap<SeasonRankingType_DataEntity, SeasonRankingType_DataModel>();
                cfg.CreateMap<SeasonRankingType_DataModel, SeasonRankingType_DataEntity>();
                cfg.CreateMap<SeasonRankingTypeEntity, SeasonRankingTypeModel>();
                cfg.CreateMap<SeasonRankingTypeModel, SeasonRankingTypeEntity>();

                cfg.CreateMap<AdminCp_DataEntity, AdminCp_DataModel>();
                cfg.CreateMap<AdminCp_DataModel, AdminCp_DataEntity>();
                cfg.CreateMap<AdminCpEntity, AdminCpModel>();
                cfg.CreateMap<AdminCpModel, AdminCpEntity>();

                cfg.CreateMap<AdminCPFeature_DataEntity, AdminCPFeature_DataModel>();
                cfg.CreateMap<AdminCPFeature_DataModel, AdminCPFeature_DataEntity>();
                cfg.CreateMap<AdminCPFeatureEntity, AdminCPFeatureModel>();
                cfg.CreateMap<AdminCPFeatureModel, AdminCPFeatureEntity>();

                cfg.CreateMap<ApiConfig_DataEntity, ApiConfig_DataModel>();
                cfg.CreateMap<ApiConfig_DataModel, ApiConfig_DataEntity>();
                cfg.CreateMap<ApiConfigEntity, ApiConfigModel>();
                cfg.CreateMap<ApiConfigModel, ApiConfigEntity>();

                cfg.CreateMap<ClientConfig_DataEntity, ClientConfig_DataModel>();
                cfg.CreateMap<ClientConfig_DataModel, ClientConfig_DataEntity>();
                cfg.CreateMap<ClientConfigEntity, ClientConfigModel>();
                cfg.CreateMap<ClientConfigModel, ClientConfigEntity>();

                cfg.CreateMap<SystemGiftType_DataEntity, SystemGiftType_DataModel>();
                cfg.CreateMap<SystemGiftType_DataModel, SystemGiftType_DataEntity>();
                cfg.CreateMap<SystemGiftTypeEntity, SystemGiftTypeModel>();
                cfg.CreateMap<SystemGiftTypeModel, SystemGiftTypeEntity>();

                cfg.CreateMap<GiftCreateCondType_DataEntity, GiftCreateCondType_DataModel>();
                cfg.CreateMap<GiftCreateCondType_DataModel, GiftCreateCondType_DataEntity>();
                cfg.CreateMap<GiftCreateCondTypeEntity, GiftCreateCondTypeModel>();
                cfg.CreateMap<GiftCreateCondTypeModel, GiftCreateCondTypeEntity>();

                cfg.CreateMap<SimilarGiftTypeGiftOpenCondType_DataEntity, SimilarGiftTypeGiftOpenCondType_DataModel>();
                cfg.CreateMap<SimilarGiftTypeGiftOpenCondType_DataModel, SimilarGiftTypeGiftOpenCondType_DataEntity>();
                cfg.CreateMap<SimilarGiftTypeGiftOpenCondTypeEntity, SimilarGiftTypeGiftOpenCondTypeModel>();
                cfg.CreateMap<SimilarGiftTypeGiftOpenCondTypeModel, SimilarGiftTypeGiftOpenCondTypeEntity>();

                cfg.CreateMap<SystemGiftTypeGiftCreateCondType_DataEntity, SystemGiftTypeGiftCreateCondType_DataModel>();
                cfg.CreateMap<SystemGiftTypeGiftCreateCondType_DataModel, SystemGiftTypeGiftCreateCondType_DataEntity>();
                cfg.CreateMap<SystemGiftTypeGiftCreateCondTypeEntity, SystemGiftTypeGiftCreateCondTypeModel>();
                cfg.CreateMap<SystemGiftTypeGiftCreateCondTypeModel, SystemGiftTypeGiftCreateCondTypeEntity>();

                cfg.CreateMap<AccountInfo_DataEntity, AccountInfo_DataModel>();
                cfg.CreateMap<AccountInfo_DataModel, AccountInfo_DataEntity>();
                cfg.CreateMap<AccountInfoEntity, AccountInfoModel>();
                cfg.CreateMap<AccountInfoModel, AccountInfoEntity>();

                cfg.CreateMap<DistanceActivity_DataEntity, DistanceActivity_DataModel>();
                cfg.CreateMap<DistanceActivity_DataModel, DistanceActivity_DataEntity>();
                cfg.CreateMap<DistanceActivityEntity, DistanceActivityModel>();
                cfg.CreateMap<DistanceActivityModel, DistanceActivityEntity>();

                cfg.CreateMap<UserGiftStatus_DataEntity, UserGiftStatus_DataModel>();
                cfg.CreateMap<UserGiftStatus_DataModel, UserGiftStatus_DataEntity>();
                cfg.CreateMap<UserGiftStatusEntity, UserGiftStatusModel>();
                cfg.CreateMap<UserGiftStatusModel, UserGiftStatusEntity>();

                cfg.CreateMap<Category_DataEntity, Category_DataModel>();
                cfg.CreateMap<Category_DataModel, Category_DataEntity>();
                cfg.CreateMap<CategoryEntity, CategoryModel>();
                cfg.CreateMap<CategoryModel, CategoryEntity>();

                cfg.CreateMap<ChallengeType_DataEntity, ChallengeType_DataModel>();
                cfg.CreateMap<ChallengeType_DataModel, ChallengeType_DataEntity>();
                cfg.CreateMap<ChallengeTypeEntity, ChallengeTypeModel>();
                cfg.CreateMap<ChallengeTypeModel, ChallengeTypeEntity>();

                cfg.CreateMap<StepActivity_DataEntity, StepActivity_DataModel>();
                cfg.CreateMap<StepActivity_DataModel, StepActivity_DataEntity>();
                cfg.CreateMap<StepActivityEntity, StepActivityModel>();
                cfg.CreateMap<StepActivityModel, StepActivityEntity>();

                cfg.CreateMap<GiftRewardType_DataEntity, GiftRewardType_DataModel>();
                cfg.CreateMap<GiftRewardType_DataModel, GiftRewardType_DataEntity>();
                cfg.CreateMap<GiftRewardTypeEntity, GiftRewardTypeModel>();
                cfg.CreateMap<GiftRewardTypeModel, GiftRewardTypeEntity>();

                cfg.CreateMap<GiftComponentType_DataEntity, GiftComponentType_DataModel>();
                cfg.CreateMap<GiftComponentType_DataModel, GiftComponentType_DataEntity>();
                cfg.CreateMap<GiftComponentTypeEntity, GiftComponentTypeModel>();
                cfg.CreateMap<GiftComponentTypeModel, GiftComponentTypeEntity>();

                cfg.CreateMap<UserGift_DataEntity, UserGift_DataModel>();
                cfg.CreateMap<UserGift_DataModel, UserGift_DataEntity>();
                cfg.CreateMap<UserGiftEntity, UserGiftModel>();
                cfg.CreateMap<UserGiftModel, UserGiftEntity>();

                cfg.CreateMap<SimilarGiftType_DataEntity, SimilarGiftType_DataModel>();
                cfg.CreateMap<SimilarGiftType_DataModel, SimilarGiftType_DataEntity>();
                cfg.CreateMap<SimilarGiftTypeEntity, SimilarGiftTypeModel>();
                cfg.CreateMap<SimilarGiftTypeModel, SimilarGiftTypeEntity>();

                cfg.CreateMap<RewardUserGift_DataEntity, RewardUserGift_DataModel>();
                cfg.CreateMap<RewardUserGift_DataModel, RewardUserGift_DataEntity>();
                cfg.CreateMap<RewardUserGiftEntity, RewardUserGiftModel>();
                cfg.CreateMap<RewardUserGiftModel, RewardUserGiftEntity>();

                cfg.CreateMap<APIParameter_DataEntity, APIParameter_DataModel>();
                cfg.CreateMap<APIParameter_DataModel, APIParameter_DataEntity>();
                cfg.CreateMap<APIParameterEntity, APIParameterModel>();
                cfg.CreateMap<APIParameterModel, APIParameterEntity>();

                cfg.CreateMap<AccountActivity_DataEntity, AccountActivity_DataModel>();
                cfg.CreateMap<AccountActivity_DataModel, AccountActivity_DataEntity>();
                cfg.CreateMap<AccountActivityEntity, AccountActivityModel>();
                cfg.CreateMap<AccountActivityModel, AccountActivityEntity>();

                cfg.CreateMap<AdminActivity_DataEntity, AdminActivity_DataModel>();
                cfg.CreateMap<AdminActivity_DataModel, AdminActivity_DataEntity>();
                cfg.CreateMap<AdminActivityEntity, AdminActivityModel>();
                cfg.CreateMap<AdminActivityModel, AdminActivityEntity>();

                cfg.CreateMap<AdsActivity_DataEntity, AdsActivity_DataModel>();
                cfg.CreateMap<AdsActivity_DataModel, AdsActivity_DataEntity>();
                cfg.CreateMap<AdsActivityEntity, AdsActivityModel>();
                cfg.CreateMap<AdsActivityModel, AdsActivityEntity>();

                cfg.CreateMap<CommunityActivity_DataEntity, CommunityActivity_DataModel>();
                cfg.CreateMap<CommunityActivity_DataModel, CommunityActivity_DataEntity>();
                cfg.CreateMap<CommunityActivityEntity, CommunityActivityModel>();
                cfg.CreateMap<CommunityActivityModel, CommunityActivityEntity>();

                cfg.CreateMap<ChallengeActivity_DataEntity, ChallengeActivity_DataModel>();
                cfg.CreateMap<ChallengeActivity_DataModel, ChallengeActivity_DataEntity>();
                cfg.CreateMap<ChallengeActivityEntity, ChallengeActivityModel>();
                cfg.CreateMap<ChallengeActivityModel, ChallengeActivityEntity>();

                cfg.CreateMap<GiftActivity_DataEntity, GiftActivity_DataModel>();
                cfg.CreateMap<GiftActivity_DataModel, GiftActivity_DataEntity>();
                cfg.CreateMap<GiftActivityEntity, GiftActivityModel>();
                cfg.CreateMap<GiftActivityModel, GiftActivityEntity>();

                cfg.CreateMap<IgoldActivity_DataEntity, IgoldActivity_DataModel>();
                cfg.CreateMap<IgoldActivity_DataModel, IgoldActivity_DataEntity>();
                cfg.CreateMap<IgoldActivityEntity, IgoldActivityModel>();
                cfg.CreateMap<IgoldActivityModel, IgoldActivityEntity>();

                cfg.CreateMap<JourneyActivity_DataEntity, JourneyActivity_DataModel>();
                cfg.CreateMap<JourneyActivity_DataModel, JourneyActivity_DataEntity>();
                cfg.CreateMap<JourneyActivityEntity, JourneyActivityModel>();
                cfg.CreateMap<JourneyActivityModel, JourneyActivityEntity>();

                cfg.CreateMap<UserRewardItemActivity_DataEntity, UserRewardItemActivity_DataModel>();
                cfg.CreateMap<UserRewardItemActivity_DataModel, UserRewardItemActivity_DataEntity>();
                cfg.CreateMap<UserRewardItemActivityEntity, UserRewardItemActivityModel>();
                cfg.CreateMap<UserRewardItemActivityModel, UserRewardItemActivityEntity>();

                cfg.CreateMap<AdsType_DataEntity, AdsType_DataModel>();
                cfg.CreateMap<AdsType_DataModel, AdsType_DataEntity>();
                cfg.CreateMap<AdsTypeEntity, AdsTypeModel>();
                cfg.CreateMap<AdsTypeModel, AdsTypeEntity>();

                cfg.CreateMap<UserChallenge_DataEntity, UserChallenge_DataModel>();
                cfg.CreateMap<UserChallenge_DataModel, UserChallenge_DataEntity>();
                cfg.CreateMap<UserChallengeEntity, UserChallengeModel>();
                cfg.CreateMap<UserChallengeModel, UserChallengeEntity>();

                cfg.CreateMap<MovementActivity_DataEntity, MovementActivity_DataModel>();
                cfg.CreateMap<MovementActivity_DataModel, MovementActivity_DataEntity>();
                cfg.CreateMap<MovementActivityEntity, MovementActivityModel>();
                cfg.CreateMap<MovementActivityModel, MovementActivityEntity>();

                cfg.CreateMap<RewardItem_DataEntity, RewardItem_DataModel>();
                cfg.CreateMap<RewardItem_DataModel, RewardItem_DataEntity>();
                cfg.CreateMap<RewardItemEntity, RewardItemModel>();
                cfg.CreateMap<RewardItemModel, RewardItemEntity>();

                cfg.CreateMap<UserRewardItemType_DataEntity, UserRewardItemType_DataModel>();
                cfg.CreateMap<UserRewardItemType_DataModel, UserRewardItemType_DataEntity>();
                cfg.CreateMap<UserRewardItemTypeEntity, UserRewardItemTypeModel>();
                cfg.CreateMap<UserRewardItemTypeModel, UserRewardItemTypeEntity>();

                cfg.CreateMap<ChallengeCompleteCondType_DataEntity, ChallengeCompleteCondType_DataModel>();
                cfg.CreateMap<ChallengeCompleteCondType_DataModel, ChallengeCompleteCondType_DataEntity>();
                cfg.CreateMap<ChallengeCompleteCondTypeEntity, ChallengeCompleteCondTypeModel>();
                cfg.CreateMap<ChallengeCompleteCondTypeModel, ChallengeCompleteCondTypeEntity>();

                cfg.CreateMap<GiftOpenCondType_DataEntity, GiftOpenCondType_DataModel>();
                cfg.CreateMap<GiftOpenCondType_DataModel, GiftOpenCondType_DataEntity>();
                cfg.CreateMap<GiftOpenCondTypeEntity, GiftOpenCondTypeModel>();
                cfg.CreateMap<GiftOpenCondTypeModel, GiftOpenCondTypeEntity>();

                cfg.CreateMap<AdminCPPermission_DataEntity, AdminCPPermission_DataModel>();
                cfg.CreateMap<AdminCPPermission_DataModel, AdminCPPermission_DataEntity>();
                cfg.CreateMap<AdminCPPermissionEntity, AdminCPPermissionModel>();
                cfg.CreateMap<AdminCPPermissionModel, AdminCPPermissionEntity>();

                cfg.CreateMap<RewardItemCategory_DataEntity, RewardItemCategory_DataModel>();
                cfg.CreateMap<RewardItemCategory_DataModel, RewardItemCategory_DataEntity>();
                cfg.CreateMap<RewardItemCategoryEntity, RewardItemCategoryModel>();
                cfg.CreateMap<RewardItemCategoryModel, RewardItemCategoryEntity>();

                cfg.CreateMap<Hashtag_DataEntity, Hashtag_DataModel>();
                cfg.CreateMap<Hashtag_DataModel, Hashtag_DataEntity>();
                cfg.CreateMap<HashtagEntity, HashtagModel>();
                cfg.CreateMap<HashtagModel, HashtagEntity>();

                cfg.CreateMap<PostHashtag_DataEntity, PostHashtag_DataModel>();
                cfg.CreateMap<PostHashtag_DataModel, PostHashtag_DataEntity>();
                cfg.CreateMap<PostHashtagEntity, PostHashtagModel>();
                cfg.CreateMap<PostHashtagModel, PostHashtagEntity>();

                cfg.CreateMap<PostLike_DataEntity, PostLike_DataModel>();
                cfg.CreateMap<PostLike_DataModel, PostLike_DataEntity>();
                cfg.CreateMap<PostLikeEntity, PostLikeModel>();
                cfg.CreateMap<PostLikeModel, PostLikeEntity>();

                cfg.CreateMap<PostPic_DataEntity, PostPic_DataModel>();
                cfg.CreateMap<PostPic_DataModel, PostPic_DataEntity>();
                cfg.CreateMap<PostPicEntity, PostPicModel>();
                cfg.CreateMap<PostPicModel, PostPicEntity>();

                cfg.CreateMap<PostVideo_DataEntity, PostVideo_DataModel>();
                cfg.CreateMap<PostVideo_DataModel, PostVideo_DataEntity>();
                cfg.CreateMap<PostVideoEntity, PostVideoModel>();
                cfg.CreateMap<PostVideoModel, PostVideoEntity>();

                cfg.CreateMap<UserFollow_DataEntity, UserFollow_DataModel>();
                cfg.CreateMap<UserFollow_DataModel, UserFollow_DataEntity>();
                cfg.CreateMap<UserFollowEntity, UserFollowModel>();
                cfg.CreateMap<UserFollowModel, UserFollowEntity>();

                cfg.CreateMap<SimilarGiftTypeGiftComponentType_DataEntity, SimilarGiftTypeGiftComponentType_DataModel>();
                cfg.CreateMap<SimilarGiftTypeGiftComponentType_DataModel, SimilarGiftTypeGiftComponentType_DataEntity>();
                cfg.CreateMap<SimilarGiftTypeGiftComponentTypeEntity, SimilarGiftTypeGiftComponentTypeModel>();
                cfg.CreateMap<SimilarGiftTypeGiftComponentTypeModel, SimilarGiftTypeGiftComponentTypeEntity>();
            });
        }
    }
}
