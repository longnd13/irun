﻿using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prj.Irun.Utilities;
using Newtonsoft.Json;

namespace Prj.Irun.BusinessLogic.Functions
{
    public static class AccessTokenResponse
    {
        public static AccessTokenModel DecryptionAccessToken(string access_token)
        {

            // closed 22/8/3018
            //var model = new AccessTokenModel();
            //string hashStr = DecryptionData.Base64Decode(access_token);
            //string json = DecryptionData.DecryptString(hashStr, "irun");
            //model = JsonConvert.DeserializeObject<AccessTokenModel>(json);
            //if (model != null)
            //{
            //    return model;
            //}
            var model = new AccessTokenModel();
            if (access_token != null && access_token != "")
            {
                RSAHandler.RSACryptography RSA = new RSAHandler.RSACryptography();
             
                string decryptedText = RSA.Decrypt(RSAHandler.RSAKey.privateKey, access_token);


                if (model != null)
                {
                    model.UserName = decryptedText.Split('*')[0];
                    model.AccessToken = decryptedText.Split('*')[1];
                 //   model.UserID = (string)decryptedText.Split('*')[2];


                    return model;
                }
            }
            return model;
          
            throw new Exception("Accesstoken Invalid");
        }

        public static string EncryptedAccessToken(string user_name, string access_token, Guid user_id)
        {
            var encryptedAccessToken = "";
            string plaintext = user_name + "*" + access_token + "*" + user_id;
            RSAHandler.RSACryptography RSA = new RSAHandler.RSACryptography();
            encryptedAccessToken = RSA.Encrypt(RSAHandler.RSAKey.publicKey, plaintext);

            return encryptedAccessToken;
        }

        public static string AccessTokenUser(string user_name, string password, Guid user_id)
        {
            return Globals.MD5FromString(user_name + password + user_id);
        }



    }
}
