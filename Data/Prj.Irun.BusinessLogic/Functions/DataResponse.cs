﻿using Prj.Irun.BusinessLogic.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Functions
{

    public abstract class DataResponseBase
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int code { get; set; }
    }

    public class DataResponse : DataResponseBase
    {
        public DataResponse()
        {
            code = -999;
            message = "Undefined";
            success = false;
            data = null;
        }

        public object data { get; set; }

        public static DataResponse<D> ConvertData<D, T>(DataResponse<T> data)
        {
            var result = new DataResponse<D>()
            {
                code = data.code,
                message = data.message,
                success = data.success,
                data = AutoMapperConfig.MapperModel<D>(data.data)
            };
            if (data.success)
            {
                result.data = AutoMapperConfig.MapperModel<D>(data.data);
            }
            return result;
        }
    }

    public class DataResponse<T> : DataResponseBase
    {
        public DataResponse() : base()
        {

        }
        public DataResponse(Exception ex)
        {
            message = ex.Message;
            success = false;
            code = -1;
        }
        public T data { get; set; }
    }



    public static class DataServiceBase
    {
        public static DataResponse<T> TrueData<T>(T data, string message = "RequestOk!")
        {
            return new DataResponse<T>()
            {
                success = true,
                code = 0,
                message = message,
                data = data
            };
        }

        public static DataResponse FailData(int code, string message = "RequestFail!", object data = null)
        {
            return new DataResponse()
            {
                code = code,
                message = message,
                data = data
            };
        }

        public static DataResponse<T> FailData<T>(T data, string message = "Fail")
        {
            return new DataResponse<T>()
            {
                code = -401,
                message = message,
                data = data
            };
        }

        public static DataResponse<T> NotFoundData<T>(T data, string message = "Data not found")
        {
            return new DataResponse<T>()
            {
                code = -404,
                message = message,
                data = data
            };
        }

        public static DataResponse<T> EditorFailData<T>(T data, string message = "Editor fail")
        {
            return new DataResponse<T>()
            {
                code = -500,
                message = message,
                data = data
            };
        }

        public static DataResponse<T> FailData<T>(int code, string message, T data)
        {
            return new DataResponse<T>()
            {
                data = data,
                code = code,
                message = message,
                success = false
            };
        }

        public static DataResponse<T> ExceptionData<T>(Exception ex, T data)
        {
            return new DataResponse<T>()
            {
                code = -1,
                data = data,
                message = $"Exception: {ex.Message}"
            };
        }

        public static DataResponse ExceptionData(Exception ex, object data = null)
        {
            return new DataResponse()
            {
                code = -1,
                data = data,
                message = $"Exception: {ex.Message}"
            };
        }
    }
}
