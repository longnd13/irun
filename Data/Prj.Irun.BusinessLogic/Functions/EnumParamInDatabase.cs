﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Functions
{
  public class EnumParamInDatabase
    {
        //public enum APIParameter
        //{
        //    StepToMeter = 1,           
        //}

        
        public class SystemGiftTypeValue
        {
            public static string BASIC_GIFT = "fgtyh";
            public static string SAVING_POINT_GIFT = "r567y";
            public static string DAILY_GIFT = "hu890";
            public static string CHALLENGE_GIFT = "21dfr";
        }
        //public enum SystemGiftType
        //{
        //    BasicGift = 5,

        //}
        public enum GiftCreateCondType
        {
            StepsToDistance_DistanceToGift = 3,
            DistanceToGift = 4,
            MaximumDailyGift = 5
        }
        public class APIParameter
        {
            public static string StepToMeter = "rth65";
        }
    }
}
