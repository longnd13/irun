﻿using Newtonsoft.Json;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Functions
{
   public class FacebookService
    {
        public static string GetLoginLink(FacebookAppConfigurationModel config, string response_type = "code")
        {
            return $"https://www.facebook.com/dialog/oauth?response_type={response_type}&client_id={config.client_id}&redirect_uri={config.redirect_uri}&scope={config.scopes}";
        }

        public static SocialUserInfoModel GetUserInfoFromAccesstoken(FacebookAppConfigurationModel config, string access_token)
        {
            try
            {
                var result = new SocialUserInfoModel();
                NameValueCollection values = new NameValueCollection();
                values.Add("access_token", access_token);
                values.Add("fields", "id,name,first_name,last_name,email,gender");
                FacebookUserModel fbUser = Get<FacebookUserModel>("me", values);
                if (fbUser != null)
                {
                    result = new SocialUserInfoModel(fbUser);
                    result.access_token = ExchangeToken(config, access_token);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static T Get<T>(string controller, NameValueCollection values)
        {
            try
            {
                using (WebClient wb = new WebClient()
                {
                    Encoding = Encoding.UTF8
                })
                {
                    wb.Headers["Content-Type"] = "application/json;charset=UTF-8";
                    string query = string.Join("&", values.AllKeys.Select(a => $"{a}={WebUtility.UrlEncode(values[a])}"));
                    string api_call = $"{ConfigurationManager.AppSettings["FACEBOOK_GRAPH_URI"]}/{controller}?{query}";
                    string json = wb.DownloadString(api_call);
                    return JsonConvert.DeserializeObject<T>(json);
                }
            }
            catch (WebException ex)
            {
                var fbException = new FacebookExceptionModel();
                if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                {
                    var resp = (HttpWebResponse)ex.Response;

                    if (resp.StatusCode == HttpStatusCode.BadRequest) // HTTP 400
                    {
                        using (var reader = new System.IO.StreamReader(ex.Response.GetResponseStream(), Encoding.UTF8))
                        {
                            fbException = JsonConvert.DeserializeObject<FacebookExceptionModel>(reader.ReadToEnd());
                        }
                    }
                }
                throw new Exception(fbException.error.message);
            }

        }

        public static string ExchangeToken(FacebookAppConfigurationModel config, string current_access_token)
        {
            try
            {
                NameValueCollection values = new NameValueCollection();
                values.Add("client_id", config.client_id);
                values.Add("client_secret", config.secret_key);
                values.Add("grant_type", "fb_exchange_token");
                values.Add("fb_exchange_token", current_access_token);
                var result = Get<FacebookExchangeAccessTokenModel>("oauth/access_token", values);
                return result.access_token;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static FacebookAppConfigurationModel FacebookConfig
        {
            get
            {
                return new FacebookAppConfigurationModel()
                {
                    app_token = ConfigurationManager.AppSettings["FACEBOOK_APP_TOKEN"],
                    client_id = ConfigurationManager.AppSettings["FACEBOOK_APP_ID"],
                    secret_key = ConfigurationManager.AppSettings["FACEBOOK_APP_SECRET"],
                    scopes = ConfigurationManager.AppSettings["FACEBOOK_SCOPES"],
                    graph_version = ConfigurationManager.AppSettings["FACEBOOK_GRAPH_URI"],
                    redirect_uri = ConfigurationManager.AppSettings["FACEBOOK_REDIRECT_URI"]
                };
            }
        }
    }
}
