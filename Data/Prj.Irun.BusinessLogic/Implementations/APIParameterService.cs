﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
  public class APIParameterService : IAPIParameterService
    {
        private readonly IAPIParameterRepository _apiParameterRepository;
        public APIParameterService(IAPIParameterRepository apiParameterRepository)
        {
            _apiParameterRepository = apiParameterRepository;
        }

        public DataResponse<List<APIParameterModel>> GetAllAPIParameter()
        {
            var list_model = new List<APIParameterModel>();
            try
            {
                var result = _apiParameterRepository.GetAllAPIParameter();
                list_model = Mapper.Map<List<APIParameterEntity>, List<APIParameterModel>>(result);
                return DataServiceBase.TrueData<List<APIParameterModel>>(list_model);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<List<APIParameterModel>>(ex, list_model);
            }
        }

        //public DataResponse<APIParameterModel> GetAPIParameterByCodeID(string  code_id)
        //{
        //    var model = new APIParameterModel();
        //    try
        //    {
        //        var result = _apiParameterRepository.GetAPIParameterByCodeID(code_id);
        //        model = Mapper.Map<APIParameterEntity, APIParameterModel>(result);
        //        return DataServiceBase.TrueData<APIParameterModel>(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        return DataServiceBase.ExceptionData<APIParameterModel>(ex, model);
        //    }
        //}
    }
}
