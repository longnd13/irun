﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Prj.Irun.Utilities.EnumHandler;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class AccountService : IAccountService
    {

        private readonly IAccountRepository _accountRepository;
        private readonly ICommonService _commonService;
        private readonly IAccountActivityRepository _accountActivityRepository;
        public AccountService(IAccountActivityRepository accountActivityRepository, IAccountRepository accountRepository, ICommonService commonService)
        {
            _accountActivityRepository = accountActivityRepository;
            _accountRepository = accountRepository;
            _commonService = commonService;


        }

        public DataResponse<string> LoginByToken(LoginModel model)
        {

            var account_activity_entity = new AccountActivityEntity();
            account_activity_entity.CreatedDate = DateHandler.DateNow();
            account_activity_entity.IP = Globals.GetIP();
            string data = "";
            try
            {

                var account_info_entity = _commonService.GetAccountInfo(model.AccessToken);
                if (account_info_entity != null)
                {
                    data = model.AccessToken;

                    // logs

                    account_activity_entity.Code = "UserID:" + account_info_entity.UserName + "|Function:" + "DataResponse<string> LoginByToken(LoginModel model)" + "|AccessToken:" + model.AccessToken;
                }
                else
                {
                    return DataServiceBase.FailData("error", "Tài khoản không hợp lệ.");
                }

                _accountActivityRepository.WriteLogAccountActivity(account_activity_entity);
                return DataServiceBase.TrueData(data);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData(ex, data);
            }
        }

        public DataResponse<string> AccountLogin(LoginModel login_model) // client mã hóa pass MD5 rồi
        {

            var account_info_entity = new AccountInfoEntity();
            string encryptedAccessToken = string.Empty;
            try
            {

                var result_login = _accountRepository.CheckAccountLogin(login_model.UserName, login_model.Password);

                if (result_login != null)
                {
                    if (result_login.AccessTokenExpired < DateHandler.DateNow()) // token expied
                    {
                        account_info_entity.UserID = Globals.RandomGuid();
                        account_info_entity.UserName = result_login.UserName;
                        account_info_entity.AccessTokenExpired = DateHandler.AddMonths(12);
                        account_info_entity.AccessToken = AccessTokenResponse.AccessTokenUser(result_login.UserName, result_login.Password, account_info_entity.UserID);

                        var result = _accountRepository.UpdatedAccessTokenExpired(account_info_entity);
                        if (result.Equals(true))
                        {
                            encryptedAccessToken = AccessTokenResponse.EncryptedAccessToken(result_login.UserName.Trim(), result_login.AccessToken, account_info_entity.UserID);
                        }
                        else
                        {
                            encryptedAccessToken = "";
                        }
                    }
                    else
                    {
                        encryptedAccessToken = AccessTokenResponse.EncryptedAccessToken(result_login.UserName.Trim(), result_login.AccessToken, result_login.UserID);
                    }
                    return DataServiceBase.TrueData(encryptedAccessToken);
                }
                else
                {
                    return DataServiceBase.FailData(encryptedAccessToken);
                }



            }
            catch (Exception ex)
            {
                return DataServiceBase.FailData(encryptedAccessToken, ex.ToString());
            }


        }

        public DataResponse<string> AccountRegister(AccountRegisterModel account_register_model)
        {
            string encryptedAccessToken = string.Empty;
            var account_info_model = new AccountInfoModel();
            try
            {
                var result = _accountRepository.CheckAccountExistByUserName(account_register_model.UserName);
                if (result == -1 || result == -2)
                {
                    return DataServiceBase.FailData("Có lỗi xảy ra trong quá trình xử lý.");
                }
                else if (result > 0)
                {
                    return DataServiceBase.FailData("Tài khoản đã tồn tại trong hệ thống");
                }
                else
                {
                    account_info_model.UserID = Globals.RandomGuid();
                    account_info_model.UserName = account_register_model.UserName.Trim();
                    account_info_model.Password = account_register_model.Password; // da duoc ma hoa tu client
                    account_info_model.Email = account_register_model.Email;
                    account_info_model.AccessToken = AccessTokenResponse.AccessTokenUser(account_register_model.UserName.Trim(), account_register_model.Password, account_info_model.UserID);
                    account_info_model.AccessTokenExpired = DateTime.Now.AddMonths(12);
                    var account_entity = Mapper.Map<AccountInfoModel, AccountInfoEntity>(account_info_model);
                    var data = _accountRepository.Register(account_entity);
                    if (data != null)
                    {
                        encryptedAccessToken = AccessTokenResponse.EncryptedAccessToken(account_info_model.UserName.Trim(), account_info_model.AccessToken, account_info_model.UserID);
                        return DataServiceBase.TrueData(encryptedAccessToken);
                    }
                    else
                    {
                        return DataServiceBase.FailData(encryptedAccessToken);
                    }
                }
            }
            catch (Exception ex)
            {

                return DataServiceBase.ExceptionData(ex, "Error");
            }

        }

        public DataResponse<string> AccountChangePassword(string access_token_client, AccountChangePasswordModel account_changepass_model)
        {
            string encryptedAccessToken = string.Empty;
            var account_info_model = new AccountInfoModel();
            try
            {
                var account_info_entity = _commonService.GetAccountInfo(access_token_client);

                if (account_info_entity != null)
                {
                    var account_info = _accountRepository.CheckUserNameAndPassword(account_info_entity.UserName, account_changepass_model.old_password, account_info_entity.AccessToken);
                    if (account_info != null)
                    {
                        string new_access_token_user = Globals.MD5FromString(account_info.UserName.Trim() + account_changepass_model.new_password);

                        var updated_password = _accountRepository.UpdatePassword(account_info.UserName, account_changepass_model.new_password, new_access_token_user);

                        if (updated_password > 0)
                        {
                            string plaintext = account_info.UserName.Trim() + "-" + new_access_token_user;
                            RSAHandler.RSACryptography RSA = new RSAHandler.RSACryptography();
                            encryptedAccessToken = RSA.Encrypt(RSAHandler.RSAKey.publicKey, plaintext);
                            return DataServiceBase.TrueData(encryptedAccessToken);
                        }
                    }
                    else
                    {
                        return DataServiceBase.FailData(encryptedAccessToken);
                    }

                }
                else
                {
                    return DataServiceBase.FailData(encryptedAccessToken);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DataServiceBase.FailData(encryptedAccessToken);
        }

        public DataResponse<string> FacebookLogin(AccountLoginSocialModel login_social_model)
        {
            string encryptedAccessToken = string.Empty;

            try
            {
                var fbUser = FacebookService.GetUserInfoFromAccesstoken(FacebookService.FacebookConfig, login_social_model.access_token);
                if (fbUser != null)
                {
                    var accSocial = _accountRepository.GetExisted(fbUser.social_id, LoginProviderType.FACEBOOK);

                    AccountInfoEntity account_info_entity = null;

                    if (accSocial != null)
                    {
                        account_info_entity = _accountRepository.GetByUserName(accSocial.UserName);

                        string plaintext = account_info_entity.UserName.Trim() + "-" + account_info_entity.AccessToken;
                        RSAHandler.RSACryptography RSA = new RSAHandler.RSACryptography();
                        encryptedAccessToken = RSA.Encrypt(RSAHandler.RSAKey.publicKey, plaintext);
                        return DataServiceBase.TrueData(encryptedAccessToken);
                    }
                    else
                    {
                        return DataServiceBase.TrueData(encryptedAccessToken);
                    }
                }
                else
                {
                    return DataServiceBase.FailData<string>(3, "Mã truy cập Facebook không hợp lệ hoặc đã hết hạn sử dụng.", null);
                }

            }
            catch (Exception ex)
            {
                return DataServiceBase.FailData<string>(4, ex.Message, null);
            }
        }

        public DataResponse<long> GetGoldByUser(string access_token_client)
        {
            long gold_user = 0;
            try
            {


                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity != null)
                {
                    gold_user = account_info_entity.Gold;
                    return DataServiceBase.TrueData(gold_user);
                }
                else
                {
                    return DataServiceBase.FailData<long>(0, "Tài khoản không tồn tại trong hệ thống.");
                }
            }
            catch (Exception ex)
            {

                return DataServiceBase.FailData<long>(0, ex.Message.ToString());
            }
        }

        public DataResponse<AccountViewModel> GetAccountInfo(string access_token_client)
        {
            var account_view_model = new AccountViewModel();
            try
            {
                var aToken = AccessTokenResponse.DecryptionAccessToken(access_token_client);
                if (aToken != null)
                {
                    var account_info_entity = _accountRepository.GetByAccessToken(aToken.AccessToken);


                    account_view_model = Mapper.Map<AccountInfoEntity, AccountViewModel>(account_info_entity);
                    return DataServiceBase.TrueData(account_view_model);
                }
                else
                {
                    return DataServiceBase.FailData<AccountViewModel>(account_view_model);
                }
            }
            catch (Exception ex)
            {

                return DataServiceBase.FailData<AccountViewModel>(account_view_model, ex.Message.ToString());
            }
        }

        public DataResponse<int> OnOffOTP()
        {
            try
            {
                return DataServiceBase.TrueData(_accountRepository.OnOffOTP((int)EnumHandler.Config.OTPAccount));
            }
            catch (Exception ex)
            {
                return DataServiceBase.FailData<int>(0, ex.Message.ToString());
            }
        }

        public DataResponse<UpdateAccountViewModel> UpdateAccountInfo(string access_token_client, UpdateAccountViewModel model)
        {
            var account_view_model = new UpdateAccountViewModel();
            try
            {
                var aToken = AccessTokenResponse.DecryptionAccessToken(access_token_client);
                if (aToken != null)
                {
                    var account_entity = Mapper.Map<UpdateAccountViewModel, AccountInfoEntity>(model);
                    var account_info_entity = _accountRepository.UpdateAccountInfo(account_entity, aToken.UserName);

                    account_view_model = Mapper.Map<AccountInfoEntity, UpdateAccountViewModel>(account_info_entity);
                    return DataServiceBase.TrueData(account_view_model);
                }
                else
                {
                    return DataServiceBase.FailData<UpdateAccountViewModel>(account_view_model);
                }
            }
            catch (Exception ex)
            {

                return DataServiceBase.FailData<UpdateAccountViewModel>(account_view_model, ex.Message.ToString());
            }
        }

    }
}
