﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class CommonService : ICommonService
    {
        private readonly IMainCoreRepository _mainCoreRepository;
        private readonly IAccountRepository _accountRepository;
        public CommonService(IMainCoreRepository mainCoreRepository, IAccountRepository accountRepository)
        {
            _mainCoreRepository = mainCoreRepository;
            _accountRepository = accountRepository;
        }

        public AccountInfoEntity GetAccountInfo(string access_token_client)
        {
            var account_info_entity = new AccountInfoEntity();
            try
            {
                var aToken = AccessTokenResponse.DecryptionAccessToken(access_token_client);
                if (aToken != null)
                {
                    account_info_entity = _accountRepository.GetByAccessToken(aToken.AccessToken);
                }
            }
            catch (Exception ex)
            {

                Logger.ErrorLog(ConstantsHandler.Common, "Service_GetAccountInfo(string access_token_client): ", ex.ToString());
            }


            return account_info_entity;
        }
    }
}
