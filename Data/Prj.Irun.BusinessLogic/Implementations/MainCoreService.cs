﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class MainCoreService : IMainCoreService
    {
        private readonly IMainCoreRepository _mainCoreRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IapiUserGiftRepository _apiUserGiftRepository;
        private readonly ICommonService _commonService;
        private readonly IAPIParameterRepository _apiParameterRepository;
        private readonly IGiftCreateCondTypeRepository _giftCreateCondTypeRepository;
        private readonly ISystemGiftTypeRepository _systemGiftTypeRepository;
        private readonly ISimilarGiftTypeRepository _similarGiftTypeRepository;
        private readonly IGiftComponentTypeRepository _giftComponentTypeRepository;
        public MainCoreService(ISystemGiftTypeRepository systemGiftTypeRepository,
                               IGiftCreateCondTypeRepository giftCreateCondTypeRepository,
                               IAPIParameterRepository apiParameterRepository,
                               ICommonService commonService,
                               IMainCoreRepository mainCoreRepository,
                               IAccountRepository accountRepository,
                               ISimilarGiftTypeRepository similarGiftTypeRepository,
                               IGiftComponentTypeRepository giftComponentTypeRepository,
                               IapiUserGiftRepository apiUserGiftRepository)
        {
            _mainCoreRepository = mainCoreRepository;
            _accountRepository = accountRepository;
            _commonService = commonService;

            _apiParameterRepository = apiParameterRepository;
            _systemGiftTypeRepository = systemGiftTypeRepository;
            _giftCreateCondTypeRepository = giftCreateCondTypeRepository;
            _similarGiftTypeRepository = similarGiftTypeRepository;
            _giftComponentTypeRepository = giftComponentTypeRepository;
            _apiUserGiftRepository = apiUserGiftRepository;

        }
        public DataResponse<StepInfoModel> PostSteps(List<PostStepsModel> model, string access_token_client)
        {
            var step_info_model = new StepInfoModel();
            var user_id_num = 0;
            // tổng số bước chân user post
            int total_step_user_post = model.Sum(x => x.steps);
            try
            {
                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity.UserName != null)
                {
                    // lấy cấu hình  đổi step ra mét
                    var steptoMeter = _apiParameterRepository.GetAPIParameterByCodeID(EnumParamInDatabase.APIParameter.StepToMeter);
                    if (steptoMeter != null)
                    {
                        // lấy gói quà basic gift cho user
                        var getIdSystemGiftType = _systemGiftTypeRepository.GetSystemGiftTypeByCodeID(EnumParamInDatabase.SystemGiftTypeValue.BASIC_GIFT);
                        if (getIdSystemGiftType != null)
                        {
                            // lấy tỷ lệ số met duoc quy đổi từ step
                            var getGiftCreateCondType = _giftCreateCondTypeRepository.GetGiftCreateCondTypeByCodeID(getIdSystemGiftType.GiftCreateCondTypeCodeID);
                            if (getGiftCreateCondType != null)
                            {
                                // tổng số mét được qui đổi từ steps
                                var distance_from_step = total_step_user_post * steptoMeter.Ratio;
                                var caculator_steps = Globals.caculatorSteps(distance_from_step, getGiftCreateCondType.CreateCondTypeValue);

                                // lấy thành phần gói quà
                                var similarGiftType = _similarGiftTypeRepository.GetSimilarGiftType(true, getIdSystemGiftType.CodeID);
                                if (similarGiftType.Count > 0)
                                {
                                    var list_User_Gift = new List<UserGiftEntity>();
                                    var list_RewardUserGift = new List<RewardUserGiftEntity>();

                                    // check đủ 100% gói quà duoc chọn thì mới hợp lệ
                                    var countSimilarGiftType = similarGiftType.Sum(x => x.CreatedRate);
                                    if (countSimilarGiftType == 100)
                                    {
                                        // đi qua từng gói quà
                                        for (int i = 0; i < caculator_steps.gift_receive; i++)
                                        {
                                            // bắt đầu random để trả thành phần quà cho từng gói quà
                                            var ran = new Random();
                                            int position = ran.Next(0, 100);
                                            foreach (var item in similarGiftType)
                                            {
                                                position = position - item.CreatedRate;
                                                // thành phần quà 
                                                if (position < 0)
                                                {
                                                    var list_giftComponentType = _giftComponentTypeRepository.GetGiftComponentTypeBySimilarGiftTypeID(item.CodeID);
                                                    if (list_giftComponentType.Count > 0)
                                                    {
                                                        foreach (var item_2 in list_giftComponentType)
                                                        {
                                                            int rd = ran.Next(0, 100);
                                                            if (rd <= item_2.ObtainingRate)
                                                            {
                                                                user_id_num++;
                                                                list_User_Gift.Add(new UserGiftEntity
                                                                {
                                                                    Id = user_id_num,
                                                                    UserID = account_info_entity.UserName,
                                                                    SimilarGiftTypeCodeID = item.CodeID,
                                                                    TotalSavingValue = 0,
                                                                    SystemGiftTypeCodeID = getIdSystemGiftType.CodeID,
                                                                    UserGiftStatusCodeID = ValueHandler.UserGiftStatusValue.CheckingOpenCond,
                                                                    CreatedDate = DateHandler.DateNow(),
                                                                    CreatedDateString = DateHandler.DateNow().ToString("dd-MM-yyyy"),
                                                                });
                                                                list_RewardUserGift.Add(new RewardUserGiftEntity
                                                                {
                                                                    Reward = item_2.Reward,
                                                                    UserGiftID = user_id_num,
                                                                    GiftRewardTypeCodeID = item_2.GiftRewardTypeCodeID,
                                                                });


                                                                step_info_model.DataGiftOfUser.Add(new GiftOfUserModel
                                                                {
                                                                    GiftRewardTypeID = item_2.GiftRewardTypeCodeID,
                                                                    SystemGiftTypeCodeID = getIdSystemGiftType.CodeID,
                                                                    Quantity = item_2.Reward, // số lượng thành phần trong gói quà
                                                                    UserID = account_info_entity.UserName,
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // add all database
                                        var result = _mainCoreRepository.SaveGiftBySteps(list_User_Gift, list_RewardUserGift, account_info_entity.UserName, total_step_user_post);
                                        if (result.Equals(true)) // sau khi add thành công thì trả về thông tin cho client
                                        {
                                            step_info_model.SurplusDistance = caculator_steps.distance_surplus;
                                            step_info_model.TotalGift = Protector.Int(caculator_steps.gift_receive);
                                            step_info_model.TotalSteps = total_step_user_post;
                                            step_info_model.Gold = 0;
                                            return DataServiceBase.TrueData(step_info_model);
                                        }
                                        else
                                        {
                                            step_info_model.DataGiftOfUser = null;
                                            return DataServiceBase.FailData(step_info_model, "Có lỗi xảy ra trong quá trình xử lý.");
                                        }

                                    }

                                }
                            }

                        }
                    }
                }
                else
                {
                    return DataServiceBase.FailData(step_info_model, "Tài khoản không tồn tại trong hệ thống.");
                }
                return DataServiceBase.TrueData(step_info_model);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData(ex, step_info_model);
            }
        }

        public DataResponse<DistanceInfoModel> PostDistance(List<PostStepsModel> model, string access_token_client)
        {
            var user_id_num = 0;
            var distance_info_model = new DistanceInfoModel();
            var list_distance_entity = new List<DistanceEntity>();
            try
            {

                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity != null)
                {

                    int distance_user_post = model.Sum(x => x.distance);


                    // lấy gói quà basic gift cho user
                    var getIdSystemGiftType = _systemGiftTypeRepository.GetSystemGiftTypeByCodeID(EnumParamInDatabase.SystemGiftTypeValue.BASIC_GIFT);
                    if (getIdSystemGiftType != null)
                    {
                        // lấy tỷ lệ số met duoc quy đổi từ step
                        var getGiftCreateCondType = _giftCreateCondTypeRepository.GetGiftCreateCondTypeByCodeID(getIdSystemGiftType.GiftCreateCondTypeCodeID);
                        if (getGiftCreateCondType != null)
                        {

                            var total_gift = Globals.Chia_lay_so_nguyen(distance_user_post, getGiftCreateCondType.CreateCondTypeValue);
                            var distance_so_du = Globals.Chia_lay_so_du(distance_user_post, getGiftCreateCondType.CreateCondTypeValue);

                            // nếu ko đủ distance đổi quà thì return 
                            if (total_gift < 1)
                            {
                                return DataServiceBase.FailData(distance_info_model, "Tổng số distance không đủ đổi quà.");
                            }
                            else //  to be continue
                            {
                                // lấy thành phần gói quà
                                var similarGiftType = _similarGiftTypeRepository.GetSimilarGiftType(true, getIdSystemGiftType.CodeID);
                                if (similarGiftType.Count > 0)
                                {
                                    var list_User_Gift = new List<UserGiftEntity>();
                                    var list_RewardUserGift = new List<RewardUserGiftEntity>();
                                    // check đủ 100% gói quà duoc chọn thì mới hợp lệ
                                    var countSimilarGiftType = similarGiftType.Sum(x => x.CreatedRate);
                                    if (countSimilarGiftType == 100)
                                    {
                                        // đi qua từng gói quà
                                        for (int i = 0; i < total_gift; i++)
                                        {
                                            // bắt đầu random để trả thành phần quà cho từng gói quà
                                            var ran = new Random();
                                            int position = ran.Next(0, 100);
                                            foreach (var item in similarGiftType)
                                            {
                                                position = position - item.CreatedRate;
                                                // thành phần quà 
                                                if (position < 0)
                                                {
                                                    var list_giftComponentType = _giftComponentTypeRepository.GetGiftComponentTypeBySimilarGiftTypeID(item.CodeID);
                                                    if (list_giftComponentType.Count > 0)
                                                    {
                                                        foreach (var item_2 in list_giftComponentType)
                                                        {
                                                            int rd = ran.Next(0, 100);
                                                            if (rd <= item_2.ObtainingRate)
                                                            {
                                                                user_id_num++;
                                                                list_User_Gift.Add(new UserGiftEntity
                                                                {
                                                                    Id = user_id_num,
                                                                    UserID = account_info_entity.UserName,
                                                                    SimilarGiftTypeCodeID = item.CodeID,
                                                                    TotalSavingValue = 0,
                                                                    SystemGiftTypeCodeID = getIdSystemGiftType.CodeID,
                                                                    UserGiftStatusCodeID = ValueHandler.UserGiftStatusValue.CheckingOpenCond,
                                                                    CreatedDate = DateHandler.DateNow(),
                                                                    CreatedDateString = DateHandler.DateNow().ToString("dd-MM-yyyy"),
                                                                });
                                                                list_RewardUserGift.Add(new RewardUserGiftEntity
                                                                {
                                                                    Reward = item_2.Reward,
                                                                    UserGiftID = user_id_num,
                                                                    GiftRewardTypeCodeID = item_2.GiftRewardTypeCodeID,
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // insert database
                                        var result = _mainCoreRepository.SaveGiftByDistance(list_User_Gift, list_RewardUserGift, account_info_entity.UserName, distance_user_post);
                                        if (result.Equals(true))
                                        {
                                            distance_info_model.SurplusDistance = distance_so_du;
                                            distance_info_model.TotalGift = Protector.Int(total_gift);
                                            distance_info_model.TotalDistance = distance_user_post;
                                            distance_info_model.Gold = 0;
                                            return DataServiceBase.TrueData(distance_info_model);
                                        }
                                        else
                                        {
                                            return DataServiceBase.FailData(distance_info_model, "Có lỗi xảy ra trong quá trình xử lý.");
                                        }

                                    }

                                }
                            }


                        }
                    }
                    return DataServiceBase.TrueData(distance_info_model);
                }
                else
                {
                    return DataServiceBase.FailData(distance_info_model, "Tài khoản không hợp lệ.");
                }

            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData(ex, distance_info_model);
            }
        }

        public DataResponse<bool> OpenBasicGift(string access_token_client, OpenBasicGiftModel open_basic_gift_model)
        {
            try
            {
                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity.UserName != null)
                {
                    bool result = _mainCoreRepository.OpenBasicGift(account_info_entity.UserName, open_basic_gift_model.UserGiftID);
                    if (result == true)
                    {
                        return DataServiceBase.TrueData(result);
                    }
                    else
                    {
                        return DataServiceBase.FailData(false, "Có lỗi xảy ra trong quá trình xử lý.");
                    }
                }
                else
                {
                    return DataServiceBase.FailData(false, "Tài khoản không hợp lệ.");
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.FailData(false, ex.ToString());
            }
        }

        public DataResponse<ResponseDailyGiftUserModel> GetDailyGiftByUser(string access_token_client)
        {
            var user_id_num = 0;
            var list_User_Gift = new List<UserGiftEntity>();
            var list_RewardUserGift = new List<RewardUserGiftEntity>();
            var data_response = new ResponseDailyGiftUserModel();
            try
            {
                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity.UserName != null)
                {
                    // xem ngày hôm nay có bao nhiêu dailygift, max là 3 
                    var count_daily_gift_user = _mainCoreRepository.CheckDailyGiftByUserID(account_info_entity.UserName, DateHandler.DateNow().ToString("dd-MM-yyyy"));


                    // lấy max daily gift
                    // lấy gói quà basic gift cho user
                    var getIdSystemGiftType = _systemGiftTypeRepository.GetSystemGiftTypeByCodeID(EnumParamInDatabase.SystemGiftTypeValue.DAILY_GIFT);
                    var getGiftCreateCondType = _giftCreateCondTypeRepository.GetGiftCreateCondTypeByCodeID(getIdSystemGiftType.GiftCreateCondTypeCodeID);
                    if (getGiftCreateCondType.Id > 0)
                    {
                        // = 0 thì bắt đầu ngày mới tạo ra 3 gói quà cho user
                        if (count_daily_gift_user == 0)
                        {
                            // lấy thành phần gói quà
                            var similarGiftType = _similarGiftTypeRepository.GetSimilarGiftType(true, getIdSystemGiftType.CodeID);
                            if (similarGiftType.Count > 0)
                            {
                                var countSimilarGiftType = similarGiftType.Sum(x => x.CreatedRate);
                                if (countSimilarGiftType == 100)
                                {
                                    // tạo từng gói quà daily gift cho user với max theo CreateCondTypeValue
                                    for (int i = 0; i < getGiftCreateCondType.CreateCondTypeValue; i++)
                                    {
                                        // bắt đầu random để trả thành phần quà cho từng gói quà

                                        foreach (var item in similarGiftType)
                                        {
                                            if (item.CreatedRate == 100)
                                            {
                                                var list_giftComponentType = _giftComponentTypeRepository.GetGiftComponentTypeBySimilarGiftTypeID(item.CodeID);
                                                if (list_giftComponentType.Count > 0)
                                                {
                                                    user_id_num++;
                                                    list_User_Gift.Add(new UserGiftEntity
                                                    {
                                                        Id = user_id_num,
                                                        UserID = account_info_entity.UserName,
                                                        SimilarGiftTypeCodeID = item.CodeID,
                                                        TotalSavingValue = 0,
                                                        SystemGiftTypeCodeID = getIdSystemGiftType.CodeID, // daily gift
                                                        UserGiftStatusCodeID = ValueHandler.UserGiftStatusValue.WaitingToOpen,
                                                        CreatedDate = DateHandler.DateNow(),
                                                        CreatedDateString = DateHandler.DateNow().ToString("dd-MM-yyyy"),
                                                    });
                                                    foreach (var item_2 in list_giftComponentType)
                                                    {
                                                        var ran = new Random();
                                                        int rd = ran.Next(0, 100);
                                                        if (rd <= item_2.ObtainingRate)
                                                        {
                                                            list_RewardUserGift.Add(new RewardUserGiftEntity
                                                            {
                                                                Reward = item_2.Reward,
                                                                UserGiftID = user_id_num,
                                                                GiftRewardTypeCodeID = item_2.GiftRewardTypeCodeID,
                                                            });
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {

                                            }
                                        }
                                    }
                                }
                                else // nếu CreatedRate k là 100% thì phải random từng thành phần 
                                {

                                }
                                // insert database
                                var result = _mainCoreRepository.SaveDailyGift(list_User_Gift, list_RewardUserGift, account_info_entity.UserName);
                                if (result.Equals(true))
                                {
                                    var list_user_gift_entity = _apiUserGiftRepository.GetDailyGiftByUserID(account_info_entity.UserName,
                                                                                       ValueHandler.SystemGiftTypeValue.DAILY_GIFT,
                                                                                       ValueHandler.UserGiftStatusValue.WaitingToOpen,
                                                                                       DateHandler.DateNow().ToString("dd-MM-yyyy"));
                                    var mapper = Mapper.Map<List<UserGiftEntity>, List<UserGiftModel>>(list_user_gift_entity);
                                    data_response.UserGift = mapper;
                                    data_response.Count = mapper.Count;
                                    return DataServiceBase.TrueData(data_response, "Số quà DailyGift còn lại trong ngày");
                                }
                                else
                                {
                                    return DataServiceBase.FailData(data_response, "Có lỗi xảy ra trong quá trình xử lý.");
                                }
                            }
                        }
                        else // trả số gói quà còn lại trong ngày của user
                        {

                            var list_user_gift_entity = _apiUserGiftRepository.GetDailyGiftByUserID(account_info_entity.UserName,
                                                                                      ValueHandler.SystemGiftTypeValue.DAILY_GIFT,
                                                                                      ValueHandler.UserGiftStatusValue.WaitingToOpen,
                                                                                      DateHandler.DateNow().ToString("dd-MM-yyyy"));
                            var mapper = Mapper.Map<List<UserGiftEntity>, List<UserGiftModel>>(list_user_gift_entity);
                            data_response.UserGift = mapper;
                            data_response.Count = mapper.Count;
                            return DataServiceBase.TrueData(data_response, "Số quà DailyGift còn lại trong ngày");
                        }
                    }

                    return DataServiceBase.FailData(data_response, "Bạn đã nhận đủ số quà ngày hôm nay.");
                }
                else
                {
                    return DataServiceBase.FailData(data_response, "Tài khoản không hợp lệ.");
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.FailData(data_response, ex.ToString());
            }
        }

        public DataResponse<bool> OpenDailyGift(string access_token_client, OpenDailyGiftModel open_daily_gift_model)
        {
            var result = false;
            try
            {
                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity.UserName != null)
                {
                    var data_user_gift = _apiUserGiftRepository.GetUserGiftByID(open_daily_gift_model.UserGiftID, account_info_entity.UserName);

                    if (data_user_gift.Id > 0)
                    {
                        result = _apiUserGiftRepository.OpenDailyGift(account_info_entity.UserName, data_user_gift.Id);
                        if (result == true)
                        {
                            return DataServiceBase.TrueData(result);
                        }
                        return DataServiceBase.FailData(false, "Có lỗi xảy ra trong quá trình xử lý!");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Service_OpenDailyGift:", ex.ToString());
            }

            return DataServiceBase.FailData(false, "Có lỗi xảy ra trong quá trình xử lý.");
        }

       
    }
}
