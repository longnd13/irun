﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
  public class SimilarGiftTypeService : ISimilarGiftTypeService
    {
        private readonly ISimilarGiftTypeRepository _systemGiftTypeRepository;
        public SimilarGiftTypeService(ISimilarGiftTypeRepository systemGiftTypeRepository)
        {
            _systemGiftTypeRepository = systemGiftTypeRepository;
        }

        public DataResponse<List<SimilarGiftTypeModel>> GetAllSystemGiftType(bool is_selected, string system_gift_type_id)
        {
            var list_model = new List<SimilarGiftTypeModel>();
            try
            {
                var result = _systemGiftTypeRepository.GetSimilarGiftType(is_selected, system_gift_type_id);
                list_model = Mapper.Map<List<SimilarGiftTypeEntity>, List<SimilarGiftTypeModel>>(result);
                return DataServiceBase.TrueData<List<SimilarGiftTypeModel>>(list_model);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<List<SimilarGiftTypeModel>>(ex, list_model);
            }
        }

       
    }
}
