﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class SystemGiftTypeService : ISystemGiftTypeService
    {
        private readonly ISystemGiftTypeRepository _systemGiftTypeRepository;
        public SystemGiftTypeService(ISystemGiftTypeRepository systemGiftTypeRepository)
        {
            _systemGiftTypeRepository = systemGiftTypeRepository;
        }

        public DataResponse<List<SystemGiftTypeModel>> GetAllSystemGiftType()
        {
            var list_model = new List<SystemGiftTypeModel>();
            try
            {
                var result = _systemGiftTypeRepository.GetAllSystemGiftType();
                list_model = Mapper.Map<List<SystemGiftTypeEntity>, List<SystemGiftTypeModel>>(result);
                return DataServiceBase.TrueData<List<SystemGiftTypeModel>>(list_model);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<List<SystemGiftTypeModel>>(ex, list_model);
            }
        }

        public DataResponse<SystemGiftTypeModel> GetSystemGiftTypeByCodeID(string code_id)
        {
            var model = new SystemGiftTypeModel();
            try
            {
                var result = _systemGiftTypeRepository.GetSystemGiftTypeByCodeID(code_id);
                model = Mapper.Map<SystemGiftTypeEntity, SystemGiftTypeModel>(result);
                return DataServiceBase.TrueData<SystemGiftTypeModel>(model);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeModel>(ex, model);
            }
        }
    }
}
