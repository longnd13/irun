﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
  public  class apiClientConfigService : IapiClientConfigService
    {
        private readonly IapiClientConfigRepository _apiClientConfigRepository;
        public apiClientConfigService(IapiClientConfigRepository apiClientConfigRepository)
        {
            _apiClientConfigRepository = apiClientConfigRepository;
        }

        public DataResponse<List<ClientConfigModel>> GetAllClientConfig()
        {
            var list_model = new List<ClientConfigModel>();
            try
            {
                var result = _apiClientConfigRepository.GetAllClientConfig();
                list_model = Mapper.Map<List<ClientConfigEntity>, List<ClientConfigModel>>(result);
                return DataServiceBase.TrueData<List<ClientConfigModel>>(list_model);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<List<ClientConfigModel>>(ex, list_model);
            }
        }
    }
}
