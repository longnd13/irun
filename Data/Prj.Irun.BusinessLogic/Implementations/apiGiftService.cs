﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class apiGiftService : IapiGiftService
    {

        private readonly IapiUserGiftRepository _apiUserGiftRepository;
        private readonly ICommonService _commonService;
        private readonly ISimilarGiftTypeRepository _similarGiftTypeRepository;
        private readonly IGiftComponentTypeRepository _giftComponentTypeRepository;
        public apiGiftService(IapiUserGiftRepository apiUserGiftRepository,
                              ICommonService commonService,
                              ISimilarGiftTypeRepository similarGiftTypeRepository,
                              IGiftComponentTypeRepository giftComponentTypeRepository)
        {
            _apiUserGiftRepository = apiUserGiftRepository;
            _commonService = commonService;
            _similarGiftTypeRepository = similarGiftTypeRepository;
            _giftComponentTypeRepository = giftComponentTypeRepository;
        }

        public DataResponse<ResponseCumulativeGiftUserModel> GetCumulativeGift(string access_token_client)
        {
            var user_id_num = 0;
            var data_response = new ResponseCumulativeGiftUserModel();
            try
            {
                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity.UserName != null)
                {
                    var count_Cumulative = _apiUserGiftRepository.CountUserGiftBySystemGiftTypeCodeID(ValueHandler.SystemGiftTypeValue.SAVING_POINT_GIFT);
                    if (count_Cumulative < 0)
                    {
                        return DataServiceBase.FailData(data_response, "Có lỗi xảy ra trong quá trình xử lý.");
                    }


                    if (count_Cumulative == 0) // trả về gói quà đầu tiên khi user chưa có gói quà nào
                    {
                        // lấy thành phần gói quà
                        var similarGiftType = _similarGiftTypeRepository.GetSimilarGiftType(true, ValueHandler.SystemGiftTypeValue.SAVING_POINT_GIFT);
                        if (similarGiftType.Count > 0)
                        {
                            var list_User_Gift = new List<UserGiftEntity>();
                            var list_RewardUserGift = new List<RewardUserGiftEntity>();
                            // check đủ 100% gói quà duoc chọn thì mới hợp lệ
                            var countSimilarGiftType = similarGiftType.Sum(x => x.CreatedRate);
                            if (countSimilarGiftType == 100)
                            {
                                // tạo 1 gói quà tích lũy đầu tiên cho user
                                for (int i = 0; i < 1; i++)
                                {
                                    // bắt đầu random để trả thành phần quà cho từng gói quà
                                    var ran = new Random();
                                    int position = ran.Next(0, 100);
                                    foreach (var item in similarGiftType)
                                    {
                                        position = position - item.CreatedRate;
                                        // thành phần quà 
                                        if (position < 0)
                                        {
                                            var list_giftComponentType = _giftComponentTypeRepository.GetGiftComponentTypeBySimilarGiftTypeID(item.CodeID);
                                            if (list_giftComponentType.Count > 0)
                                            {

                                                user_id_num++;
                                                list_User_Gift.Add(new UserGiftEntity
                                                {
                                                    Id = user_id_num,
                                                    UserID = account_info_entity.UserName,
                                                    SimilarGiftTypeCodeID = item.CodeID,
                                                    TotalSavingValue = 0,
                                                    SystemGiftTypeCodeID = ValueHandler.SystemGiftTypeValue.SAVING_POINT_GIFT,
                                                    UserGiftStatusCodeID = ValueHandler.UserGiftStatusValue.CheckingOpenCond,
                                                    CreatedDate = DateHandler.DateNow(),
                                                    CreatedDateString = DateHandler.DateNow().ToString("dd-MM-yyyy"),
                                                });

                                                foreach (var item_2 in list_giftComponentType)
                                                {
                                                    int rd = ran.Next(0, 100);
                                                    if (rd <= item_2.ObtainingRate)
                                                    {
                                                        list_RewardUserGift.Add(new RewardUserGiftEntity
                                                        {
                                                            Reward = item_2.Reward,
                                                            UserGiftID = user_id_num,
                                                            GiftRewardTypeCodeID = item_2.GiftRewardTypeCodeID,
                                                        });
                                                    }
                                                }

                                                // inser db
                                            }
                                        }
                                    }
                                }
                            }
                            var result = _apiUserGiftRepository.SaveUserGift(list_User_Gift, list_RewardUserGift, account_info_entity.UserName);
                            if (result.Equals(true))
                            {
                                var list_user_gift_entity = _apiUserGiftRepository.GetUserGift(account_info_entity.UserName,
                                                                                   ValueHandler.SystemGiftTypeValue.SAVING_POINT_GIFT,
                                                                                   ValueHandler.UserGiftStatusValue.WaitingToOpen);
                                var mapper = Mapper.Map<UserGiftEntity, UserGiftModel>(list_user_gift_entity);
                                data_response.Count = 1;
                                return DataServiceBase.TrueData(data_response, "Gói quà Saving Point");
                            }

                        }                    
                    }
                    else // gói quà tiếp theo
                    {
                        var user_gift = _apiUserGiftRepository.GetUserGift(account_info_entity.UserName, ValueHandler.SystemGiftTypeValue.SAVING_POINT_GIFT, ValueHandler.UserGiftStatusValue.CheckingOpenCond);
                        if (user_gift.Id > 0)
                        {
                            data_response.ID = user_gift.Id;
                            data_response.CreatedDate = DateHandler.DateNow();
                            data_response.Count = 1;
                            data_response.Description = "Gói quà tích lũy tiếp theo của User.";
                            return DataServiceBase.TrueData(data_response);
                        }
                        else
                        {
                            return DataServiceBase.TrueData(data_response, "Lỗi xảy ra trong quá trình tạo gói quà.");
                        }
                    }
                }
                else
                {
                    return DataServiceBase.FailData(data_response, "Tài khoản không hợp lệ.");
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.FailData(data_response, ex.ToString());
            }

            return DataServiceBase.FailData(data_response, "");
        }
        public DataResponse<bool> OpenCumulativeGift(string access_token_client, OpenCumulativeGiftModel open_cumulative_gift_model)
        {
            try
            {
                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity.UserName != null)
                {
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return DataServiceBase.TrueData(false, "Gói quà tiếp theo.");
        }



    }
}
