﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
  public class apiStepsService : IapiStepsService
    {
        private readonly IMainCoreRepository _mainCoreRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IapiUserGiftRepository _apiUserGiftRepository;
        private readonly ICommonService _commonService;
        private readonly IAPIParameterRepository _apiParameterRepository;
        private readonly IGiftCreateCondTypeRepository _giftCreateCondTypeRepository;
        private readonly ISystemGiftTypeRepository _systemGiftTypeRepository;
        private readonly ISimilarGiftTypeRepository _similarGiftTypeRepository;
        private readonly IGiftComponentTypeRepository _giftComponentTypeRepository;
        public apiStepsService(ISystemGiftTypeRepository systemGiftTypeRepository,
                               IGiftCreateCondTypeRepository giftCreateCondTypeRepository,
                               IAPIParameterRepository apiParameterRepository,
                               ICommonService commonService,
                               IMainCoreRepository mainCoreRepository,
                               IAccountRepository accountRepository,
                               ISimilarGiftTypeRepository similarGiftTypeRepository,
                               IGiftComponentTypeRepository giftComponentTypeRepository,
                               IapiUserGiftRepository apiUserGiftRepository)
        {
            _mainCoreRepository = mainCoreRepository;
            _accountRepository = accountRepository;
            _commonService = commonService;

            _apiParameterRepository = apiParameterRepository;
            _systemGiftTypeRepository = systemGiftTypeRepository;
            _giftCreateCondTypeRepository = giftCreateCondTypeRepository;
            _similarGiftTypeRepository = similarGiftTypeRepository;
            _giftComponentTypeRepository = giftComponentTypeRepository;
            _apiUserGiftRepository = apiUserGiftRepository;

        }

        public DataResponse<StepInfoModel> UserPostSteps(List<PostStepsModel> model, string access_token_client)
        {
            var step_info_model = new StepInfoModel();
            var user_id_num = 0;
            // tổng số bước chân user post
            int total_step_user_post = model.Sum(x => x.steps);
            try
            {
                var account_info_entity = _commonService.GetAccountInfo(access_token_client);
                if (account_info_entity.UserName != null)
                {
                    // lấy cấu hình  đổi step ra mét
                    var steptoMeter = _apiParameterRepository.GetAPIParameterByCodeID(EnumParamInDatabase.APIParameter.StepToMeter);
                    if (steptoMeter != null)
                    {
                        // lấy gói quà basic gift cho user
                        var getIdSystemGiftType = _systemGiftTypeRepository.GetSystemGiftTypeByCodeID(EnumParamInDatabase.SystemGiftTypeValue.BASIC_GIFT);
                        if (getIdSystemGiftType != null)
                        {
                            // lấy tỷ lệ số met duoc quy đổi từ step
                            var getGiftCreateCondType = _giftCreateCondTypeRepository.GetGiftCreateCondTypeByCodeID(getIdSystemGiftType.GiftCreateCondTypeCodeID);
                            if (getGiftCreateCondType != null)
                            {
                                // tổng số mét được qui đổi từ steps
                                var distance_from_step = total_step_user_post * steptoMeter.Ratio;
                                var caculator_steps = Globals.caculatorSteps(distance_from_step, getGiftCreateCondType.CreateCondTypeValue);

                                // lấy thành phần gói quà
                                var similarGiftType = _similarGiftTypeRepository.GetSimilarGiftType(true, getIdSystemGiftType.CodeID);
                                if (similarGiftType.Count > 0)
                                {
                                    var list_User_Gift = new List<UserGiftEntity>();
                                    var list_RewardUserGift = new List<RewardUserGiftEntity>();

                                    // check đủ 100% gói quà duoc chọn thì mới hợp lệ
                                    var countSimilarGiftType = similarGiftType.Sum(x => x.CreatedRate);
                                    if (countSimilarGiftType == 100)
                                    {
                                        // đi qua từng gói quà
                                        for (int i = 0; i < caculator_steps.gift_receive; i++)
                                        {
                                            // bắt đầu random để trả thành phần quà cho từng gói quà
                                            var ran = new Random();
                                            int position = ran.Next(0, 100);
                                            foreach (var item in similarGiftType)
                                            {
                                                position = position - item.CreatedRate;
                                                // thành phần quà 
                                                if (position < 0)
                                                {
                                                    var list_giftComponentType = _giftComponentTypeRepository.GetGiftComponentTypeBySimilarGiftTypeID(item.CodeID);
                                                    if (list_giftComponentType.Count > 0)
                                                    {
                                                        foreach (var item_2 in list_giftComponentType)
                                                        {
                                                            int rd = ran.Next(0, 100);
                                                            if (rd <= item_2.ObtainingRate)
                                                            {
                                                                user_id_num++;
                                                                list_User_Gift.Add(new UserGiftEntity
                                                                {
                                                                    Id = user_id_num,
                                                                    UserID = account_info_entity.UserName,
                                                                    SimilarGiftTypeCodeID = item.CodeID,
                                                                    TotalSavingValue = 0,
                                                                    SystemGiftTypeCodeID = getIdSystemGiftType.CodeID,
                                                                    UserGiftStatusCodeID = ValueHandler.UserGiftStatusValue.CheckingOpenCond,
                                                                    CreatedDate = DateHandler.DateNow(),
                                                                    CreatedDateString = DateHandler.DateNow().ToString("dd-MM-yyyy"),
                                                                });
                                                                list_RewardUserGift.Add(new RewardUserGiftEntity
                                                                {
                                                                    Reward = item_2.Reward,
                                                                    UserGiftID = user_id_num,
                                                                    GiftRewardTypeCodeID = item_2.GiftRewardTypeCodeID,
                                                                });


                                                                step_info_model.DataGiftOfUser.Add(new GiftOfUserModel
                                                                {
                                                                    GiftRewardTypeID = item_2.GiftRewardTypeCodeID,
                                                                    SystemGiftTypeCodeID = getIdSystemGiftType.CodeID,
                                                                    Quantity = item_2.Reward, // số lượng thành phần trong gói quà
                                                                    UserID = account_info_entity.UserName,
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // add all database
                                        var result = _mainCoreRepository.SaveGiftBySteps(list_User_Gift, list_RewardUserGift, account_info_entity.UserName, total_step_user_post);
                                        if (result.Equals(true)) // sau khi add thành công thì trả về thông tin cho client
                                        {
                                            step_info_model.SurplusDistance = caculator_steps.distance_surplus;
                                            step_info_model.TotalGift = Protector.Int(caculator_steps.gift_receive);
                                            step_info_model.TotalSteps = total_step_user_post;
                                            step_info_model.Gold = 0;
                                            return DataServiceBase.TrueData(step_info_model);
                                        }
                                        else
                                        {
                                            step_info_model.DataGiftOfUser = null;
                                            return DataServiceBase.FailData(step_info_model, "Có lỗi xảy ra trong quá trình xử lý.");
                                        }

                                    }

                                }
                            }

                        }
                    }
                }
                else
                {
                    return DataServiceBase.FailData(step_info_model, "Tài khoản không tồn tại trong hệ thống.");
                }
                return DataServiceBase.TrueData(step_info_model);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData(ex, step_info_model);
            }
        }
    }
}
