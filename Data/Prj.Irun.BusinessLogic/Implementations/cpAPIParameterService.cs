﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAPIParameterService : IcpAPIParameterService
    {
        private readonly IcpAPIParameterRepository _apiParameterRepository;
        public cpAPIParameterService(IcpAPIParameterRepository APIParameterRepository)
        {
            _apiParameterRepository = APIParameterRepository;
        }

        public DataResponse<APIParameterModel> Add(APIParameterModel model)
        {
            try
            {
                var entity = Mapper.Map<APIParameterModel, APIParameterEntity>(model);
                var result = _apiParameterRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<APIParameterEntity, APIParameterModel>(result);
                    return DataServiceBase.TrueData<APIParameterModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<APIParameterModel>(ex, model);
            }

            return DataServiceBase.FailData<APIParameterModel>(model);
        }

        public DataResponse<bool> Delete(string CodeID)
        {
            try
            {
                var result = _apiParameterRepository.Delete(CodeID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<APIParameter_DataModel> GetAll(APIParameter_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<APIParameter_DataModel, APIParameter_DataEntity>(model);
                var result = _apiParameterRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<APIParameter_DataEntity, APIParameter_DataModel>(result);
                    return DataServiceBase.TrueData<APIParameter_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<APIParameter_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<APIParameter_DataModel>(model);
        }

        public DataResponse<APIParameterModel> GetByID(string CodeID)
        {
            var model = new APIParameterModel();
            try
            {
                var result = _apiParameterRepository.GetById(CodeID);
                if (result != null)
                {
                    model = Mapper.Map<APIParameterEntity, APIParameterModel>(result);
                    return DataServiceBase.TrueData<APIParameterModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<APIParameterModel>(ex, model);
            }

            return DataServiceBase.FailData<APIParameterModel>(model);
        }

        public DataResponse<APIParameterModel> Updated(APIParameterModel model)
        {
            try
            {
                var entity = Mapper.Map<APIParameterModel, APIParameterEntity>(model);
                var result = _apiParameterRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<APIParameterEntity, APIParameterModel>(result);
                    return DataServiceBase.TrueData<APIParameterModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<APIParameterModel>(ex, model);
            }

            return DataServiceBase.FailData<APIParameterModel>(model);
        }
    }
}
