﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAccountActivityService : IcpAccountActivityService
    {
        private readonly IcpAccountActivityRepository _accountActivityService;
        public cpAccountActivityService(IcpAccountActivityRepository AccountActivityService)
        {
            _accountActivityService = AccountActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _accountActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<AccountActivity_DataModel> GetAll(AccountActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<AccountActivity_DataModel, AccountActivity_DataEntity>(model);
                var result = _accountActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<AccountActivity_DataEntity, AccountActivity_DataModel>(result);
                    return DataServiceBase.TrueData<AccountActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AccountActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AccountActivity_DataModel>(model);
        }

        public DataResponse<AccountActivity_DataModel> SearchByUserID(AccountActivity_DataModel model, string userID)
        {
            try
            {
                var entity = Mapper.Map<AccountActivity_DataModel, AccountActivity_DataEntity>(model);
                var result = _accountActivityService.SearchByUserID(entity, userID);
                if (result != null)
                {
                    model = Mapper.Map<AccountActivity_DataEntity, AccountActivity_DataModel>(result);
                    return DataServiceBase.TrueData<AccountActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AccountActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AccountActivity_DataModel>(model);
        }
    }
}
