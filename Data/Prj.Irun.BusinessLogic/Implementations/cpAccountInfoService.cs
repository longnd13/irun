﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAccountInfoService : IcpAccountInfoService
    {
        private readonly IcpAccountInfoRepository _accountInfoRepository;
        private readonly IAccountRepository _accountRepository;
        public cpAccountInfoService(IAccountRepository accountRepository, IcpAccountInfoRepository accountInfoRepository)
        {
            _accountInfoRepository = accountInfoRepository;
            _accountRepository = accountRepository;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _accountInfoRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<AccountInfo_DataModel> GetAll(AccountInfo_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<AccountInfo_DataModel, AccountInfo_DataEntity>(model);
                var result = _accountInfoRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<AccountInfo_DataEntity, AccountInfo_DataModel>(result);
                    return DataServiceBase.TrueData<AccountInfo_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AccountInfo_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AccountInfo_DataModel>(model);
        }

        public DataResponse<AccountInfo_DataModel> SearchByUsername(AccountInfo_DataModel model, string username, string fullname)
        {
            try
            {
                var entity = Mapper.Map<AccountInfo_DataModel, AccountInfo_DataEntity>(model);
                var result = _accountInfoRepository.SearchByUsername(entity, username, fullname);
                if (result != null)
                {
                    model = Mapper.Map<AccountInfo_DataEntity, AccountInfo_DataModel>(result);
                    return DataServiceBase.TrueData<AccountInfo_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AccountInfo_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AccountInfo_DataModel>(model);
        }

        public DataResponse<AccountInfo_DataModel> GetAllBySort(AccountInfo_DataModel model, string goldSort, string stepSort, string distanceSort)
        {
            try
            {
                var entity = Mapper.Map<AccountInfo_DataModel, AccountInfo_DataEntity>(model);
                var result = _accountInfoRepository.GetAllBySort(entity, goldSort, stepSort, distanceSort);
                if (result != null)
                {
                    model = Mapper.Map<AccountInfo_DataEntity, AccountInfo_DataModel>(result);
                    return DataServiceBase.TrueData<AccountInfo_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AccountInfo_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AccountInfo_DataModel>(model);
        }

        public DataResponse<AccountInfoModel> Add(AccountInfoModel model)
        {
            string encryptedAccessToken = string.Empty;
            try
            {
                var checkAccountExist = _accountRepository.CheckAccountExistByUserName(model.UserName);
                if (checkAccountExist == -1 || checkAccountExist == -2)
                {
                    return DataServiceBase.FailData<AccountInfoModel>(model, "Có lỗi xảy ra trong quá trình xử lý.");
                }
                else if (checkAccountExist > 0)
                {
                    return DataServiceBase.FailData<AccountInfoModel>(model, "Tài khoản đã tồn tại trong hệ thống");
                }
                else
                {
                    model.Password = "irun123@"; // Globals.RandomCode(6); // random password
                    model.UserID = Globals.RandomGuid(); // randomGuid
                    model.AccessToken = AccessTokenResponse.AccessTokenUser(model.UserName.Trim(), model.Password, model.UserID);
                    model.AccessTokenExpired = DateTime.Now.AddMonths(12);
                    var account_entity = Mapper.Map<AccountInfoModel, AccountInfoEntity>(model);
                    var entity = Mapper.Map<AccountInfoModel, AccountInfoEntity>(model);
                    var data = _accountRepository.Register(account_entity);
                    if (data != null)
                    {
                        encryptedAccessToken = AccessTokenResponse.EncryptedAccessToken(model.UserName.Trim(), model.AccessToken, model.UserID);
                        return DataServiceBase.TrueData<AccountInfoModel>(model, encryptedAccessToken);
                    }
                    else
                    {
                        return DataServiceBase.FailData<AccountInfoModel>(model, encryptedAccessToken);
                    }
                }


            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AccountInfoModel>(ex, model);
            }
        }

        public DataResponse<AccountInfoModel> GetByID(int id)
        {
            var model = new AccountInfoModel();
            try
            {
                var result = _accountInfoRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<AccountInfoEntity, AccountInfoModel>(result);
                    return DataServiceBase.TrueData<AccountInfoModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AccountInfoModel>(ex, model);
            }

            return DataServiceBase.FailData<AccountInfoModel>(model);
        }

        public DataResponse<AccountInfoModel> Updated(AccountInfoModel model)
        {
            try
            {
                var entity = Mapper.Map<AccountInfoModel, AccountInfoEntity>(model);
                var result = _accountInfoRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<AccountInfoEntity, AccountInfoModel>(result);
                    return DataServiceBase.TrueData<AccountInfoModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AccountInfoModel>(ex, model);
            }

            return DataServiceBase.FailData<AccountInfoModel>(model);
        }
    }
}
