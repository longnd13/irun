﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAdminActivityService : IcpAdminActivityService
    {
        private readonly IcpAdminActivityRepository _adminActivityService;
        public cpAdminActivityService(IcpAdminActivityRepository AdminActivityService)
        {
            _adminActivityService = AdminActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _adminActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<AdminActivity_DataModel> GetAll(AdminActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminActivity_DataModel, AdminActivity_DataEntity>(model);
                var result = _adminActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminActivity_DataEntity, AdminActivity_DataModel>(result);
                    return DataServiceBase.TrueData<AdminActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminActivity_DataModel>(model);
        }

        public DataResponse<AdminActivity_DataModel> SearchByAdminId(AdminActivity_DataModel model, int adminID)
        {
            try
            {
                var entity = Mapper.Map<AdminActivity_DataModel, AdminActivity_DataEntity>(model);
                var result = _adminActivityService.SearchByAdminId(entity, adminID);
                if (result != null)
                {
                    model = Mapper.Map<AdminActivity_DataEntity, AdminActivity_DataModel>(result);
                    return DataServiceBase.TrueData<AdminActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminActivity_DataModel>(model);
        }
    }
}
