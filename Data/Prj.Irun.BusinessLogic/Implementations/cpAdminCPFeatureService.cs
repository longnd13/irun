﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAdminCPFeatureService : IAdminCPFeatureService
    {
        private readonly IcpAccountFeatureRepository _accountFeatureRepository;
        public cpAdminCPFeatureService(IcpAccountFeatureRepository accountFeatureRepository)
        {
            _accountFeatureRepository = accountFeatureRepository;
        }

        public DataResponse<AdminCPFeatureModel> Add(AdminCPFeatureModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminCPFeatureModel, AdminCPFeatureEntity>(model);
                var result = _accountFeatureRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPFeatureEntity, AdminCPFeatureModel>(result);
                    return DataServiceBase.TrueData<AdminCPFeatureModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPFeatureModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPFeatureModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _accountFeatureRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<AdminCPFeature_DataModel> GetAll(AdminCPFeature_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminCPFeature_DataModel, AdminCPFeature_DataEntity>(model);
                var result = _accountFeatureRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPFeature_DataEntity, AdminCPFeature_DataModel>(result);
                    return DataServiceBase.TrueData<AdminCPFeature_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPFeature_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPFeature_DataModel>(model);
        }

        public DataResponse<AdminCPFeatureModel> GetByID(int id)
        {
            var model = new AdminCPFeatureModel();
            try
            {
                var result = _accountFeatureRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPFeatureEntity, AdminCPFeatureModel>(result);
                    return DataServiceBase.TrueData<AdminCPFeatureModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPFeatureModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPFeatureModel>(model);
        }

        public DataResponse<AdminCPFeatureModel> Updated(AdminCPFeatureModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminCPFeatureModel, AdminCPFeatureEntity>(model);
                var result = _accountFeatureRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPFeatureEntity, AdminCPFeatureModel>(result);
                    return DataServiceBase.TrueData<AdminCPFeatureModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPFeatureModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPFeatureModel>(model);
        }

        public DataResponse<AdminCPFeature_DataModel> GetByParentID(AdminCPFeature_DataModel model, int parentID)
        {
            try
            {
                var entity = Mapper.Map<AdminCPFeature_DataModel, AdminCPFeature_DataEntity>(model);
                var result = _accountFeatureRepository.GetByParentID(entity, parentID);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPFeature_DataEntity, AdminCPFeature_DataModel>(result);
                    return DataServiceBase.TrueData<AdminCPFeature_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPFeature_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPFeature_DataModel>(model);
        }

        public DataResponse<AdminCPFeatureModel> GetNodeByParentID(int parentID, long id)
        {
            var model = new AdminCPFeatureModel();
            try
            {
                var result = _accountFeatureRepository.GetNodeByParentID(parentID, id);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPFeatureEntity, AdminCPFeatureModel>(result);
                    return DataServiceBase.TrueData<AdminCPFeatureModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPFeatureModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPFeatureModel>(model);
        }

        public DataResponse<bool> DeleteNode(int parentID)
        {
            try
            {
                var result = _accountFeatureRepository.DeleteNode(parentID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public List<AdminCPFeatureModel> GetFeatureByGroupId(int groupId)
        {
            var list = new List<AdminCPFeatureModel>();
            try
            {
                var data = _accountFeatureRepository.GetFeatureByGroupId(groupId);
                list = Mapper.Map<List<AdminCPFeatureEntity>, List<AdminCPFeatureModel>>(data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }
    }
}
