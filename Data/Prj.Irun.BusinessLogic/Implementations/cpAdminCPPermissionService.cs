﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAdminCPPermissionService : IcpAdminCPPermissionService
    {
        private readonly IcpAdminCPPermissionRepository _adminCPPermissionRepository;
        public cpAdminCPPermissionService(IcpAdminCPPermissionRepository AdminCPPermissionRepository)
        {
            _adminCPPermissionRepository = AdminCPPermissionRepository;
        }

        public DataResponse<AdminCPPermissionModel> Add(AdminCPPermissionModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminCPPermissionModel, AdminCPPermissionEntity>(model);
                var result = _adminCPPermissionRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPPermissionEntity, AdminCPPermissionModel>(result);
                    return DataServiceBase.TrueData<AdminCPPermissionModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPPermissionModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPPermissionModel>(model);
        }

        public DataResponse<bool> Delete(string userName)
        {
            try
            {
                var result = _adminCPPermissionRepository.Delete(userName);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<AdminCPPermission_DataModel> GetAll(AdminCPPermission_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminCPPermission_DataModel, AdminCPPermission_DataEntity>(model);
                var result = _adminCPPermissionRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPPermission_DataEntity, AdminCPPermission_DataModel>(result);
                    return DataServiceBase.TrueData<AdminCPPermission_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPPermission_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPPermission_DataModel>(model);
        }

        public DataResponse<AdminCPPermissionModel> GetByID(int id)
        {
            var model = new AdminCPPermissionModel();
            try
            {
                var result = _adminCPPermissionRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPPermissionEntity, AdminCPPermissionModel>(result);
                    return DataServiceBase.TrueData<AdminCPPermissionModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPPermissionModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPPermissionModel>(model);
        }

        public DataResponse<AdminCPPermissionModel> Updated(AdminCPPermissionModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminCPPermissionModel, AdminCPPermissionEntity>(model);
                var result = _adminCPPermissionRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCPPermissionEntity, AdminCPPermissionModel>(result);
                    return DataServiceBase.TrueData<AdminCPPermissionModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCPPermissionModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCPPermissionModel>(model);
        }

        public MessageModel GetPermission(string userName)
        {
            var msg = new MessageModel();
            try
            {
                var data = _adminCPPermissionRepository.GetPermission(userName);
                msg = Mapper.Map<MessageEntity, MessageModel>(data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return msg;
        }
    }
}
