﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAdminCpService : IcpAdminCpService
    {
        private readonly IcpAdminCpRepository _adminCpRepository;
        public cpAdminCpService(IcpAdminCpRepository adminCpRepository)
        {
            _adminCpRepository = adminCpRepository;
        }

        public DataResponse<AdminCpModel> Add(AdminCpModel model)
        {
            try
            {
                model.Password = Globals.MD5FromString(model.UserName.Trim() + model.Password.Trim());
                var entity = Mapper.Map<AdminCpModel, AdminCpEntity>(model);
                var result = _adminCpRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCpEntity, AdminCpModel>(result);
                    return DataServiceBase.TrueData<AdminCpModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCpModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCpModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _adminCpRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<AdminCp_DataModel> GetAll(AdminCp_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminCp_DataModel, AdminCp_DataEntity>(model);
                var result = _adminCpRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCp_DataEntity, AdminCp_DataModel>(result);
                    return DataServiceBase.TrueData<AdminCp_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCp_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCp_DataModel>(model);
        }

        public DataResponse<AdminCpModel> GetByID(int id)
        {
            var model = new AdminCpModel();
            try
            {
                var result = _adminCpRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<AdminCpEntity, AdminCpModel>(result);
                    return DataServiceBase.TrueData<AdminCpModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCpModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCpModel>(model);
        }

        public DataResponse<AdminCpModel> Updated(AdminCpModel model)
        {
            try
            {
                var entity = Mapper.Map<AdminCpModel, AdminCpEntity>(model);
                var result = _adminCpRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdminCpEntity, AdminCpModel>(result);
                    return DataServiceBase.TrueData<AdminCpModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCpModel>(ex, model);
            }

            return DataServiceBase.FailData<AdminCpModel>(model);
        }

        public DataResponse<AdminCpModel> Authenticate(AuthenticateModel.LoginModel model)
        {
            var admincp_model = new AdminCpModel();
            try
            {
                model.Password = Globals.MD5FromString(model.UserID.Trim() + model.Password.Trim());

                var result = _adminCpRepository.Authenticate(model.UserID, model.Password);
                if (result != null)
                {
                    admincp_model = Mapper.Map<AdminCpEntity, AdminCpModel>(result);
                    return DataServiceBase.TrueData<AdminCpModel>(admincp_model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdminCpModel>(ex, admincp_model);
            }

            return DataServiceBase.FailData<AdminCpModel>(-1, null, admincp_model);
        }
    }
}
