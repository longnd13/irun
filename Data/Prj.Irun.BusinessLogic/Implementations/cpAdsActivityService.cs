﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAdsActivityService : IcpAdsActivityService
    {
        private readonly IcpAdsActivityRepository _adsActivityService;
        public cpAdsActivityService(IcpAdsActivityRepository AdsActivityService)
        {
            _adsActivityService = AdsActivityService;
        }

        public DataResponse<bool> Delete(string adsCodeID)
        {
            try
            {
                var result = _adsActivityService.Delete(adsCodeID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<AdsActivity_DataModel> GetAll(AdsActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<AdsActivity_DataModel, AdsActivity_DataEntity>(model);
                var result = _adsActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdsActivity_DataEntity, AdsActivity_DataModel>(result);
                    return DataServiceBase.TrueData<AdsActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdsActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdsActivity_DataModel>(model);
        }

        public DataResponse<AdsActivity_DataModel> SearchByAdsCodeID(AdsActivity_DataModel model, string adsCodeID)
        {
            try
            {
                var entity = Mapper.Map<AdsActivity_DataModel, AdsActivity_DataEntity>(model);
                var result = _adsActivityService.SearchByAdsCodeID(entity, adsCodeID);
                if (result != null)
                {
                    model = Mapper.Map<AdsActivity_DataEntity, AdsActivity_DataModel>(result);
                    return DataServiceBase.TrueData<AdsActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdsActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdsActivity_DataModel>(model);
        }
    }
}
