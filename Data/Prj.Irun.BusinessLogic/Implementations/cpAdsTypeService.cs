﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpAdsTypeService : IcpAdsTypeService
    {
        private readonly IcpAdsTypeRepository _adsTypeRepository;
        public cpAdsTypeService(IcpAdsTypeRepository adsTypeRepository)
        {
            _adsTypeRepository = adsTypeRepository;
        }

        public DataResponse<AdsTypeModel> Add(AdsTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<AdsTypeModel, AdsTypeEntity>(model);
                var result = _adsTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdsTypeEntity, AdsTypeModel>(result);
                    return DataServiceBase.TrueData<AdsTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdsTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<AdsTypeModel>(model);
        }

        public DataResponse<bool> Delete(string codeID)
        {
            try
            {
                var result = _adsTypeRepository.Delete(codeID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<AdsType_DataModel> GetAll(AdsType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<AdsType_DataModel, AdsType_DataEntity>(model);
                var result = _adsTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdsType_DataEntity, AdsType_DataModel>(result);
                    return DataServiceBase.TrueData<AdsType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdsType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<AdsType_DataModel>(model);
        }

        public DataResponse<AdsTypeModel> GetByID(string codeID)
        {
            var model = new AdsTypeModel();
            try
            {
                var result = _adsTypeRepository.GetById(codeID);
                if (result != null)
                {
                    model = Mapper.Map<AdsTypeEntity, AdsTypeModel>(result);
                    return DataServiceBase.TrueData<AdsTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdsTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<AdsTypeModel>(model);
        }

        public DataResponse<AdsTypeModel> Updated(AdsTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<AdsTypeModel, AdsTypeEntity>(model);
                var result = _adsTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<AdsTypeEntity, AdsTypeModel>(result);
                    return DataServiceBase.TrueData<AdsTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<AdsTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<AdsTypeModel>(model);
        }
    }
}
