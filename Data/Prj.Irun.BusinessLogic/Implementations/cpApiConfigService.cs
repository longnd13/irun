﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpApiConfigService : IcpApiConfigService
    {
        private readonly IcpApiConfigRepository _apiConfigRepository;
        public cpApiConfigService(IcpApiConfigRepository apiConfigRepository)
        {
            _apiConfigRepository = apiConfigRepository;
        }

        public DataResponse<ApiConfigModel> Add(ApiConfigModel model)
        {
            try
            {
                var entity = Mapper.Map<ApiConfigModel, ApiConfigEntity>(model);
                var result = _apiConfigRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<ApiConfigEntity, ApiConfigModel>(result);
                    return DataServiceBase.TrueData<ApiConfigModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ApiConfigModel>(ex, model);
            }

            return DataServiceBase.FailData<ApiConfigModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _apiConfigRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<ApiConfig_DataModel> GetAll(ApiConfig_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<ApiConfig_DataModel, ApiConfig_DataEntity>(model);
                var result = _apiConfigRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<ApiConfig_DataEntity, ApiConfig_DataModel>(result);
                    return DataServiceBase.TrueData<ApiConfig_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ApiConfig_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<ApiConfig_DataModel>(model);
        }

        public DataResponse<ApiConfigModel> GetByID(int id)
        {
            var model = new ApiConfigModel();
            try
            {
                var result = _apiConfigRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<ApiConfigEntity, ApiConfigModel>(result);
                    return DataServiceBase.TrueData<ApiConfigModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ApiConfigModel>(ex, model);
            }

            return DataServiceBase.FailData<ApiConfigModel>(model);
        }

        public DataResponse<ApiConfigModel> Updated(ApiConfigModel model)
        {
            try
            {
                var entity = Mapper.Map<ApiConfigModel, ApiConfigEntity>(model);
                var result = _apiConfigRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<ApiConfigEntity, ApiConfigModel>(result);
                    return DataServiceBase.TrueData<ApiConfigModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ApiConfigModel>(ex, model);
            }

            return DataServiceBase.FailData<ApiConfigModel>(model);
        }
    }
}
