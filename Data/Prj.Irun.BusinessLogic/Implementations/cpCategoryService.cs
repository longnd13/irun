﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpCategoryService : IcpCategoryService
    {
        private readonly IcpCategoryRepository _categoryRepository;
        public cpCategoryService(IcpCategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public DataResponse<CategoryModel> Add(CategoryModel model)
        {
            try
            {
                var entity = Mapper.Map<CategoryModel, CategoryEntity>(model);
                var result = _categoryRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<CategoryEntity, CategoryModel>(result);
                    return DataServiceBase.TrueData<CategoryModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<CategoryModel>(ex, model);
            }

            return DataServiceBase.FailData<CategoryModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _categoryRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<Category_DataModel> GetAll(Category_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<Category_DataModel, Category_DataEntity>(model);
                var result = _categoryRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<Category_DataEntity, Category_DataModel>(result);
                    return DataServiceBase.TrueData<Category_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<Category_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<Category_DataModel>(model);
        }

        public DataResponse<CategoryModel> GetByID(int id)
        {
            var model = new CategoryModel();
            try
            {
                var result = _categoryRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<CategoryEntity, CategoryModel>(result);
                    return DataServiceBase.TrueData<CategoryModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<CategoryModel>(ex, model);
            }

            return DataServiceBase.FailData<CategoryModel>(model);
        }

        public DataResponse<CategoryModel> Updated(CategoryModel model)
        {
            try
            {
                var entity = Mapper.Map<CategoryModel, CategoryEntity>(model);
                var result = _categoryRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<CategoryEntity, CategoryModel>(result);
                    return DataServiceBase.TrueData<CategoryModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<CategoryModel>(ex, model);
            }

            return DataServiceBase.FailData<CategoryModel>(model);
        }
    }
}
