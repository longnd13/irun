﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpChallengeActivityService : IcpChallengeActivityService
    {
        private readonly IcpChallengeActivityRepository _challengeActivityService;
        public cpChallengeActivityService(IcpChallengeActivityRepository ChallengeActivityService)
        {
            _challengeActivityService = ChallengeActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _challengeActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<ChallengeActivity_DataModel> GetAll(ChallengeActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<ChallengeActivity_DataModel, ChallengeActivity_DataEntity>(model);
                var result = _challengeActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeActivity_DataEntity, ChallengeActivity_DataModel>(result);
                    return DataServiceBase.TrueData<ChallengeActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeActivity_DataModel>(model);
        }

        public DataResponse<ChallengeActivity_DataModel> SearchByUserChallengeID(ChallengeActivity_DataModel model, int userChallengeID)
        {
            try
            {
                var entity = Mapper.Map<ChallengeActivity_DataModel, ChallengeActivity_DataEntity>(model);
                var result = _challengeActivityService.SearchByUserChallengeID(entity, userChallengeID);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeActivity_DataEntity, ChallengeActivity_DataModel>(result);
                    return DataServiceBase.TrueData<ChallengeActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeActivity_DataModel>(model);
        }
    }
}
