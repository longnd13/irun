﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpChallengeCompleteCondTypeService : IcpChallengeCompleteCondTypeService
    {
        private readonly IcpChallengeCompleteCondTypeRepository _ChallengeCompleteCondTypeRepository;
        public cpChallengeCompleteCondTypeService(IcpChallengeCompleteCondTypeRepository ChallengeCompleteCondTypeRepository)
        {
            _ChallengeCompleteCondTypeRepository = ChallengeCompleteCondTypeRepository;
        }

        public DataResponse<ChallengeCompleteCondTypeModel> Add(ChallengeCompleteCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<ChallengeCompleteCondTypeModel, ChallengeCompleteCondTypeEntity>(model);
                var result = _ChallengeCompleteCondTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeCompleteCondTypeEntity, ChallengeCompleteCondTypeModel>(result);
                    return DataServiceBase.TrueData<ChallengeCompleteCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeCompleteCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeCompleteCondTypeModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _ChallengeCompleteCondTypeRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<ChallengeCompleteCondType_DataModel> GetAll(ChallengeCompleteCondType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<ChallengeCompleteCondType_DataModel, ChallengeCompleteCondType_DataEntity>(model);
                var result = _ChallengeCompleteCondTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeCompleteCondType_DataEntity, ChallengeCompleteCondType_DataModel>(result);
                    return DataServiceBase.TrueData<ChallengeCompleteCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeCompleteCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeCompleteCondType_DataModel>(model);
        }

        public DataResponse<ChallengeCompleteCondTypeModel> GetByID(int id)
        {
            var model = new ChallengeCompleteCondTypeModel();
            try
            {
                var result = _ChallengeCompleteCondTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeCompleteCondTypeEntity, ChallengeCompleteCondTypeModel>(result);
                    return DataServiceBase.TrueData<ChallengeCompleteCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeCompleteCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeCompleteCondTypeModel>(model);
        }

        public DataResponse<ChallengeCompleteCondTypeModel> Updated(ChallengeCompleteCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<ChallengeCompleteCondTypeModel, ChallengeCompleteCondTypeEntity>(model);
                var result = _ChallengeCompleteCondTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeCompleteCondTypeEntity, ChallengeCompleteCondTypeModel>(result);
                    return DataServiceBase.TrueData<ChallengeCompleteCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeCompleteCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeCompleteCondTypeModel>(model);
        }
    }
}
