﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpChallengeTypeService : IcpChallengeTypeService
    {
        private readonly IcpChallengeTypeRepository _challengeTypeRepository;
        public cpChallengeTypeService(IcpChallengeTypeRepository ChallengeTypeRepository)
        {
            _challengeTypeRepository = ChallengeTypeRepository;
        }

        public DataResponse<ChallengeTypeModel> Add(ChallengeTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<ChallengeTypeModel, ChallengeTypeEntity>(model);
                var result = _challengeTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeTypeEntity, ChallengeTypeModel>(result);
                    return DataServiceBase.TrueData<ChallengeTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeTypeModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _challengeTypeRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<ChallengeType_DataModel> GetAll(ChallengeType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<ChallengeType_DataModel, ChallengeType_DataEntity>(model);
                var result = _challengeTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeType_DataEntity, ChallengeType_DataModel>(result);
                    return DataServiceBase.TrueData<ChallengeType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeType_DataModel>(model);
        }

        public DataResponse<ChallengeTypeModel> GetByID(int id)
        {
            var model = new ChallengeTypeModel();
            try
            {
                var result = _challengeTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeTypeEntity, ChallengeTypeModel>(result);
                    return DataServiceBase.TrueData<ChallengeTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeTypeModel>(model);
        }

        public DataResponse<ChallengeTypeModel> Updated(ChallengeTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<ChallengeTypeModel, ChallengeTypeEntity>(model);
                var result = _challengeTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<ChallengeTypeEntity, ChallengeTypeModel>(result);
                    return DataServiceBase.TrueData<ChallengeTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ChallengeTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<ChallengeTypeModel>(model);
        }
    }
}
