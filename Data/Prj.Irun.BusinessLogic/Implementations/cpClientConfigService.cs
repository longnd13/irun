﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpClientConfigService : IcpClientConfigService
    {
        private readonly IcpClientConfigRepository _clientConfigRepository;
        public cpClientConfigService(IcpClientConfigRepository clientConfigRepository)
        {
            _clientConfigRepository = clientConfigRepository;
        }

        public DataResponse<ClientConfigModel> Add(ClientConfigModel model)
        {
            try
            {
                var entity = Mapper.Map<ClientConfigModel, ClientConfigEntity>(model);
                var result = _clientConfigRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<ClientConfigEntity, ClientConfigModel>(result);
                    return DataServiceBase.TrueData<ClientConfigModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ClientConfigModel>(ex, model);
            }

            return DataServiceBase.FailData<ClientConfigModel>(model);
        }

        public DataResponse<bool> Delete(string CodeID)
        {
            try
            {
                var result = _clientConfigRepository.Delete(CodeID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<ClientConfig_DataModel> GetAll(ClientConfig_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<ClientConfig_DataModel, ClientConfig_DataEntity>(model);
                var result = _clientConfigRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<ClientConfig_DataEntity, ClientConfig_DataModel>(result);
                    return DataServiceBase.TrueData<ClientConfig_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ClientConfig_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<ClientConfig_DataModel>(model);
        }

        public DataResponse<ClientConfigModel> GetByID(string CodeID)
        {
            var model = new ClientConfigModel();
            try
            {
                var result = _clientConfigRepository.GetById(CodeID);
                if (result != null)
                {
                    model = Mapper.Map<ClientConfigEntity, ClientConfigModel>(result);
                    return DataServiceBase.TrueData<ClientConfigModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ClientConfigModel>(ex, model);
            }

            return DataServiceBase.FailData<ClientConfigModel>(model);
        }

        public DataResponse<ClientConfigModel> Updated(ClientConfigModel model)
        {
            try
            {
                var entity = Mapper.Map<ClientConfigModel, ClientConfigEntity>(model);
                var result = _clientConfigRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<ClientConfigEntity, ClientConfigModel>(result);
                    return DataServiceBase.TrueData<ClientConfigModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<ClientConfigModel>(ex, model);
            }

            return DataServiceBase.FailData<ClientConfigModel>(model);
        }
    }
}
