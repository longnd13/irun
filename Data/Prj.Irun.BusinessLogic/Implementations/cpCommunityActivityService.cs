﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpCommunityActivityService : IcpCommunityActivityService
    {
        private readonly IcpCommunityActivityRepository _communityActivityService;
        public cpCommunityActivityService(IcpCommunityActivityRepository CommunityActivityService)
        {
            _communityActivityService = CommunityActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _communityActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<CommunityActivity_DataModel> GetAll(CommunityActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<CommunityActivity_DataModel, CommunityActivity_DataEntity>(model);
                var result = _communityActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<CommunityActivity_DataEntity, CommunityActivity_DataModel>(result);
                    return DataServiceBase.TrueData<CommunityActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<CommunityActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<CommunityActivity_DataModel>(model);
        }

        public DataResponse<CommunityActivity_DataModel> SearchByUserID(CommunityActivity_DataModel model, string userID)
        {
            try
            {
                var entity = Mapper.Map<CommunityActivity_DataModel, CommunityActivity_DataEntity>(model);
                var result = _communityActivityService.SearchByUserID(entity, userID);
                if (result != null)
                {
                    model = Mapper.Map<CommunityActivity_DataEntity, CommunityActivity_DataModel>(result);
                    return DataServiceBase.TrueData<CommunityActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<CommunityActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<CommunityActivity_DataModel>(model);
        }
    }
}
