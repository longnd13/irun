﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpDistanceActivityService : IcpDistanceActivityService
    {
        private readonly IcpDistanceActivityRepository _distanceActivityRepository;
        public cpDistanceActivityService(IcpDistanceActivityRepository distanceActivityRepository)
        {
            _distanceActivityRepository = distanceActivityRepository;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _distanceActivityRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<DistanceActivity_DataModel> GetAll(DistanceActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<DistanceActivity_DataModel, DistanceActivity_DataEntity>(model);
                var result = _distanceActivityRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<DistanceActivity_DataEntity, DistanceActivity_DataModel>(result);
                    return DataServiceBase.TrueData<DistanceActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<DistanceActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<DistanceActivity_DataModel>(model);
        }

        public DataResponse<DistanceActivity_DataModel> SearchByUserID(DistanceActivity_DataModel model, string userID)
        {
            try
            {
                var entity = Mapper.Map<DistanceActivity_DataModel, DistanceActivity_DataEntity>(model);
                var result = _distanceActivityRepository.SearchByUserID(entity, userID);
                if (result != null)
                {
                    model = Mapper.Map<DistanceActivity_DataEntity, DistanceActivity_DataModel>(result);
                    return DataServiceBase.TrueData<DistanceActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<DistanceActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<DistanceActivity_DataModel>(model);
        }

        public DataResponse<DistanceActivityModel> Add(DistanceActivityModel model)
        {
            try
            {
                var entity = Mapper.Map<DistanceActivityModel, DistanceActivityEntity>(model);
                var result = _distanceActivityRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<DistanceActivityEntity, DistanceActivityModel>(result);
                    return DataServiceBase.TrueData<DistanceActivityModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<DistanceActivityModel>(ex, model);
            }

            return DataServiceBase.FailData<DistanceActivityModel>(model);
        }

        public DataResponse<DistanceActivityModel> GetByID(int id)
        {
            var model = new DistanceActivityModel();
            try
            {
                var result = _distanceActivityRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<DistanceActivityEntity, DistanceActivityModel>(result);
                    return DataServiceBase.TrueData<DistanceActivityModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<DistanceActivityModel>(ex, model);
            }

            return DataServiceBase.FailData<DistanceActivityModel>(model);
        }

        public DataResponse<DistanceActivityModel> Updated(DistanceActivityModel model)
        {
            try
            {
                var entity = Mapper.Map<DistanceActivityModel, DistanceActivityEntity>(model);
                var result = _distanceActivityRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<DistanceActivityEntity, DistanceActivityModel>(result);
                    return DataServiceBase.TrueData<DistanceActivityModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<DistanceActivityModel>(ex, model);
            }

            return DataServiceBase.FailData<DistanceActivityModel>(model);
        }
    }
}
