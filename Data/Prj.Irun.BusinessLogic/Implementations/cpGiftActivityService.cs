﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpGiftActivityService : IcpGiftActivityService
    {
        private readonly IcpGiftActivityRepository _giftActivityService;
        public cpGiftActivityService(IcpGiftActivityRepository GiftActivityService)
        {
            _giftActivityService = GiftActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _giftActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<GiftActivity_DataModel> GetAll(GiftActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftActivity_DataModel, GiftActivity_DataEntity>(model);
                var result = _giftActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftActivity_DataEntity, GiftActivity_DataModel>(result);
                    return DataServiceBase.TrueData<GiftActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftActivity_DataModel>(model);
        }

        public DataResponse<GiftActivity_DataModel> SearchByUserGiftID(GiftActivity_DataModel model, int userGiftID)
        {
            try
            {
                var entity = Mapper.Map<GiftActivity_DataModel, GiftActivity_DataEntity>(model);
                var result = _giftActivityService.SearchByUserGiftID(entity, userGiftID);
                if (result != null)
                {
                    model = Mapper.Map<GiftActivity_DataEntity, GiftActivity_DataModel>(result);
                    return DataServiceBase.TrueData<GiftActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftActivity_DataModel>(model);
        }
    }
}
