﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpGiftComponentTypeService : IcpGiftComponentTypeService
    {
        private readonly IcpGiftComponentTypeRepository _giftComponentTypeRepository;
        public cpGiftComponentTypeService(IcpGiftComponentTypeRepository GiftComponentTypeRepository)
        {
            _giftComponentTypeRepository = GiftComponentTypeRepository;
        }

        public DataResponse<GiftComponentTypeModel> Add(GiftComponentTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftComponentTypeModel, GiftComponentTypeEntity>(model);
                entity.CodeID = Globals.RandomCode(5);
                var result = _giftComponentTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftComponentTypeEntity, GiftComponentTypeModel>(result);
                    return DataServiceBase.TrueData<GiftComponentTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftComponentTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftComponentTypeModel>(model);
        }

        public DataResponse<bool> Delete(string CodeID)
        {
            try
            {
                var result = _giftComponentTypeRepository.Delete(CodeID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<GiftComponentType_DataModel> GetAll(GiftComponentType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftComponentType_DataModel, GiftComponentType_DataEntity>(model);
                var result = _giftComponentTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftComponentType_DataEntity, GiftComponentType_DataModel>(result);
                    return DataServiceBase.TrueData<GiftComponentType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftComponentType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftComponentType_DataModel>(model);
        }

        public DataResponse<GiftComponentTypeModel> GetByID(string CodeID)
        {
            var model = new GiftComponentTypeModel();
            try
            {
                var result = _giftComponentTypeRepository.GetById(CodeID);
                if (result != null)
                {
                    model = Mapper.Map<GiftComponentTypeEntity, GiftComponentTypeModel>(result);
                    return DataServiceBase.TrueData<GiftComponentTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftComponentTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftComponentTypeModel>(model);
        }

        public DataResponse<GiftComponentTypeModel> Updated(GiftComponentTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftComponentTypeModel, GiftComponentTypeEntity>(model);
                var result = _giftComponentTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftComponentTypeEntity, GiftComponentTypeModel>(result);
                    return DataServiceBase.TrueData<GiftComponentTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftComponentTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftComponentTypeModel>(model);
        }

        public DataResponse<GiftComponentType_DataModel> MultipleSelectedGift(GiftComponentType_DataModel model, string code_id)
        {
            try
            {
                var entity = Mapper.Map<GiftComponentType_DataModel, GiftComponentType_DataEntity>(model);
                var result = _giftComponentTypeRepository.MultipleSelectedGift(entity, code_id);
                if (result != null)
                {
                    model = Mapper.Map<GiftComponentType_DataEntity, GiftComponentType_DataModel>(result);
                    return DataServiceBase.TrueData<GiftComponentType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftComponentType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftComponentType_DataModel>(model);
        }
    }
}
