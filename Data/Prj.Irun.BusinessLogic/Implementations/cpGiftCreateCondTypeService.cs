﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpGiftCreateCondTypeService : IcpGiftCreateCondTypeService
    {
        private readonly IcpGiftCreateCondTypeRepository _giftCreateCondTypeRepository;
        public cpGiftCreateCondTypeService(IcpGiftCreateCondTypeRepository giftCreateCondTypeRepository)
        {
            _giftCreateCondTypeRepository = giftCreateCondTypeRepository;
        }

        public DataResponse<GiftCreateCondTypeModel> Add(GiftCreateCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftCreateCondTypeModel, GiftCreateCondTypeEntity>(model);
                var result = _giftCreateCondTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftCreateCondTypeEntity, GiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<GiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftCreateCondTypeModel>(model);
        }

        public DataResponse<bool> Delete(string CodeID)
        {
            try
            {
                var result = _giftCreateCondTypeRepository.Delete(CodeID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<GiftCreateCondType_DataModel> GetAll(GiftCreateCondType_DataModel model)
        {
            try
            {

                var entity = Mapper.Map<GiftCreateCondType_DataModel, GiftCreateCondType_DataEntity>(model);
            
                var result = _giftCreateCondTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftCreateCondType_DataEntity, GiftCreateCondType_DataModel>(result);
                    return DataServiceBase.TrueData<GiftCreateCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftCreateCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftCreateCondType_DataModel>(model);
        }

        public DataResponse<GiftCreateCondTypeModel> GetByID(string CodeID)
        {
            var model = new GiftCreateCondTypeModel();
            try
            {
                var result = _giftCreateCondTypeRepository.GetById(CodeID);
                if (result != null)
                {
                    model = Mapper.Map<GiftCreateCondTypeEntity, GiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<GiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftCreateCondTypeModel>(model);
        }

        public DataResponse<GiftCreateCondTypeModel> Updated(GiftCreateCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftCreateCondTypeModel, GiftCreateCondTypeEntity>(model);
                var result = _giftCreateCondTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftCreateCondTypeEntity, GiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<GiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftCreateCondTypeModel>(model);
        }

        public DataResponse<GiftCreateCondType_DataModel> MultipleSelectedGift(GiftCreateCondType_DataModel model, string code_id)
        {
            try
            {
                var entity = Mapper.Map<GiftCreateCondType_DataModel, GiftCreateCondType_DataEntity>(model);
                var result = _giftCreateCondTypeRepository.MultipleSelectedGift(entity, code_id);
                if (result != null)
                {
                    model = Mapper.Map<GiftCreateCondType_DataEntity, GiftCreateCondType_DataModel>(result);
                    return DataServiceBase.TrueData<GiftCreateCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftCreateCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftCreateCondType_DataModel>(model);
        }
    }
}
