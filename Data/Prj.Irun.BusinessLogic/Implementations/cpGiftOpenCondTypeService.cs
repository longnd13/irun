﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpGiftOpenCondTypeService : IcpGiftOpenCondTypeService
    {
        private readonly IcpGiftOpenCondTypeRepository _giftOpenCondTypeRepository;
        public cpGiftOpenCondTypeService(IcpGiftOpenCondTypeRepository giftOpenCondTypeRepository)
        {
            _giftOpenCondTypeRepository = giftOpenCondTypeRepository;
        }

        public DataResponse<GiftOpenCondTypeModel> Add(GiftOpenCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftOpenCondTypeModel, GiftOpenCondTypeEntity>(model);
                var result = _giftOpenCondTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftOpenCondTypeEntity, GiftOpenCondTypeModel>(result);
                    return DataServiceBase.TrueData<GiftOpenCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftOpenCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftOpenCondTypeModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _giftOpenCondTypeRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<GiftOpenCondType_DataModel> GetAll(GiftOpenCondType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftOpenCondType_DataModel, GiftOpenCondType_DataEntity>(model);
                var result = _giftOpenCondTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftOpenCondType_DataEntity, GiftOpenCondType_DataModel>(result);
                    return DataServiceBase.TrueData<GiftOpenCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftOpenCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftOpenCondType_DataModel>(model);
        }

        public DataResponse<GiftOpenCondTypeModel> GetByID(int id)
        {
            var model = new GiftOpenCondTypeModel();
            try
            {
                var result = _giftOpenCondTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<GiftOpenCondTypeEntity, GiftOpenCondTypeModel>(result);
                    return DataServiceBase.TrueData<GiftOpenCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftOpenCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftOpenCondTypeModel>(model);
        }

        public DataResponse<GiftOpenCondTypeModel> Updated(GiftOpenCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftOpenCondTypeModel, GiftOpenCondTypeEntity>(model);
                var result = _giftOpenCondTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftOpenCondTypeEntity, GiftOpenCondTypeModel>(result);
                    return DataServiceBase.TrueData<GiftOpenCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftOpenCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftOpenCondTypeModel>(model);
        }

        public DataResponse<GiftOpenCondTypeModel> GetByCodeId(string code_id)
        {
            var model = new GiftOpenCondTypeModel();
            try
            {
                var result = _giftOpenCondTypeRepository.GetByCodeId(code_id);
                if (result != null)
                {
                    model = Mapper.Map<GiftOpenCondTypeEntity, GiftOpenCondTypeModel>(result);
                    return DataServiceBase.TrueData<GiftOpenCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftOpenCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftOpenCondTypeModel>(model);
        }
    }
}
