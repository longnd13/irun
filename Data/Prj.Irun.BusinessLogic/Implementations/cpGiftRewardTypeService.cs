﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpGiftRewardTypeService : IcpGiftRewardTypeService
    {
        private readonly IcpGiftRewardTypeRepository _giftRewardTypeRepository;
        public cpGiftRewardTypeService(IcpGiftRewardTypeRepository GiftRewardTypeRepository)
        {
            _giftRewardTypeRepository = GiftRewardTypeRepository;
        }

        public DataResponse<GiftRewardTypeModel> Add(GiftRewardTypeModel model)
        {
            try
            {
                model.CodeID = Globals.RandomCode(5);
                var entity = Mapper.Map<GiftRewardTypeModel, GiftRewardTypeEntity>(model);
                var result = _giftRewardTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftRewardTypeEntity, GiftRewardTypeModel>(result);
                    return DataServiceBase.TrueData<GiftRewardTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftRewardTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftRewardTypeModel>(model);
        }

        public DataResponse<bool> Delete(string Code)
        {
            try
            {
                var result = _giftRewardTypeRepository.Delete(Code);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<GiftRewardType_DataModel> GetAll(GiftRewardType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftRewardType_DataModel, GiftRewardType_DataEntity>(model);
                var result = _giftRewardTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftRewardType_DataEntity, GiftRewardType_DataModel>(result);
                    return DataServiceBase.TrueData<GiftRewardType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftRewardType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftRewardType_DataModel>(model);
        }

        public DataResponse<GiftRewardTypeModel> GetByID(string Code)
        {
            var model = new GiftRewardTypeModel();
            try
            {
                var result = _giftRewardTypeRepository.GetById(Code);
                if (result != null)
                {
                    model = Mapper.Map<GiftRewardTypeEntity, GiftRewardTypeModel>(result);
                    return DataServiceBase.TrueData<GiftRewardTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftRewardTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftRewardTypeModel>(model);
        }

        public DataResponse<GiftRewardTypeModel> Updated(GiftRewardTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<GiftRewardTypeModel, GiftRewardTypeEntity>(model);
                var result = _giftRewardTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<GiftRewardTypeEntity, GiftRewardTypeModel>(result);
                    return DataServiceBase.TrueData<GiftRewardTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<GiftRewardTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<GiftRewardTypeModel>(model);
        }
    }
}
