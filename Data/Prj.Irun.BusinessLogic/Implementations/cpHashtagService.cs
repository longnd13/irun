﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpHashtagService : IcpHashtagService
    {
        private readonly IcpHashtagRepository _hashtagRepository;
        public cpHashtagService(IcpHashtagRepository hashtagRepository)
        {
            _hashtagRepository = hashtagRepository;
        }

        public DataResponse<HashtagModel> Add(HashtagModel model)
        {
            try
            {
                var entity = Mapper.Map<HashtagModel, HashtagEntity>(model);
                var result = _hashtagRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<HashtagEntity, HashtagModel>(result);
                    return DataServiceBase.TrueData<HashtagModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<HashtagModel>(ex, model);
            }

            return DataServiceBase.FailData<HashtagModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _hashtagRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<Hashtag_DataModel> GetAll(Hashtag_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<Hashtag_DataModel, Hashtag_DataEntity>(model);
                var result = _hashtagRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<Hashtag_DataEntity, Hashtag_DataModel>(result);
                    return DataServiceBase.TrueData<Hashtag_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<Hashtag_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<Hashtag_DataModel>(model);
        }

        public DataResponse<HashtagModel> GetByID(int id)
        {
            var model = new HashtagModel();
            try
            {
                var result = _hashtagRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<HashtagEntity, HashtagModel>(result);
                    return DataServiceBase.TrueData<HashtagModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<HashtagModel>(ex, model);
            }

            return DataServiceBase.FailData<HashtagModel>(model);
        }

        public DataResponse<HashtagModel> Updated(HashtagModel model)
        {
            try
            {
                var entity = Mapper.Map<HashtagModel, HashtagEntity>(model);
                var result = _hashtagRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<HashtagEntity, HashtagModel>(result);
                    return DataServiceBase.TrueData<HashtagModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<HashtagModel>(ex, model);
            }

            return DataServiceBase.FailData<HashtagModel>(model);
        }
    }
}
