﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpIgoldActivityService : IcpIgoldActivityService
    {
        private readonly IcpIgoldActivityRepository _goldActivityService;
        public cpIgoldActivityService(IcpIgoldActivityRepository IgoldActivityService)
        {
            _goldActivityService = IgoldActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _goldActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<IgoldActivity_DataModel> GetAll(IgoldActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<IgoldActivity_DataModel, IgoldActivity_DataEntity>(model);
                var result = _goldActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<IgoldActivity_DataEntity, IgoldActivity_DataModel>(result);
                    return DataServiceBase.TrueData<IgoldActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<IgoldActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<IgoldActivity_DataModel>(model);
        }

        public DataResponse<IgoldActivity_DataModel> SearchByUserID(IgoldActivity_DataModel model, string userID)
        {
            try
            {
                var entity = Mapper.Map<IgoldActivity_DataModel, IgoldActivity_DataEntity>(model);
                var result = _goldActivityService.SearchByUserID(entity, userID);
                if (result != null)
                {
                    model = Mapper.Map<IgoldActivity_DataEntity, IgoldActivity_DataModel>(result);
                    return DataServiceBase.TrueData<IgoldActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<IgoldActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<IgoldActivity_DataModel>(model);
        }
    }
}
