﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpJourneyActivityService : IcpJourneyActivityService
    {
        private readonly IcpJourneyActivityRepository _journeyActivityService;
        public cpJourneyActivityService(IcpJourneyActivityRepository JourneyActivityService)
        {
            _journeyActivityService = JourneyActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _journeyActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<JourneyActivity_DataModel> GetAll(JourneyActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<JourneyActivity_DataModel, JourneyActivity_DataEntity>(model);
                var result = _journeyActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<JourneyActivity_DataEntity, JourneyActivity_DataModel>(result);
                    return DataServiceBase.TrueData<JourneyActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<JourneyActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<JourneyActivity_DataModel>(model);
        }

        public DataResponse<JourneyActivity_DataModel> SearchByUserID(JourneyActivity_DataModel model, string userID)
        {
            try
            {
                var entity = Mapper.Map<JourneyActivity_DataModel, JourneyActivity_DataEntity>(model);
                var result = _journeyActivityService.SearchByUserID(entity, userID);
                if (result != null)
                {
                    model = Mapper.Map<JourneyActivity_DataEntity, JourneyActivity_DataModel>(result);
                    return DataServiceBase.TrueData<JourneyActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<JourneyActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<JourneyActivity_DataModel>(model);
        }
    }
}
