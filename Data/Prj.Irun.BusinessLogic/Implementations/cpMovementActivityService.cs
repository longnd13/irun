﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpMovementActivityService : IcpMovementActivityService
    {
        private readonly IcpMovementActivityRepository _movementActivityService;
        public cpMovementActivityService(IcpMovementActivityRepository movementActivityService)
        {
            _movementActivityService = movementActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _movementActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<MovementActivity_DataModel> GetAll(MovementActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<MovementActivity_DataModel, MovementActivity_DataEntity>(model);
                var result = _movementActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<MovementActivity_DataEntity, MovementActivity_DataModel>(result);
                    return DataServiceBase.TrueData<MovementActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<MovementActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<MovementActivity_DataModel>(model);
        }

        public DataResponse<MovementActivity_DataModel> SearchByUserID(MovementActivity_DataModel model, string userID)
        {
            try
            {
                var entity = Mapper.Map<MovementActivity_DataModel, MovementActivity_DataEntity>(model);
                var result = _movementActivityService.SearchByUserID(entity, userID);
                if (result != null)
                {
                    model = Mapper.Map<MovementActivity_DataEntity, MovementActivity_DataModel>(result);
                    return DataServiceBase.TrueData<MovementActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<MovementActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<MovementActivity_DataModel>(model);
        }
    }
}
