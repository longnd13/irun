﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpPostHashtagService : IcpPostHashtagService
    {
        private readonly IcpPostHashtagRepository _postHashtagRepository;
        public cpPostHashtagService(IcpPostHashtagRepository postHashtagRepository)
        {
            _postHashtagRepository = postHashtagRepository;
        }

        public DataResponse<PostHashtagModel> Add(PostHashtagModel model)
        {
            try
            {
                var entity = Mapper.Map<PostHashtagModel, PostHashtagEntity>(model);
                var result = _postHashtagRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostHashtagEntity, PostHashtagModel>(result);
                    return DataServiceBase.TrueData<PostHashtagModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostHashtagModel>(ex, model);
            }

            return DataServiceBase.FailData<PostHashtagModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _postHashtagRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<PostHashtag_DataModel> GetAll(PostHashtag_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<PostHashtag_DataModel, PostHashtag_DataEntity>(model);
                var result = _postHashtagRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostHashtag_DataEntity, PostHashtag_DataModel>(result);
                    return DataServiceBase.TrueData<PostHashtag_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostHashtag_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<PostHashtag_DataModel>(model);
        }

        public DataResponse<PostHashtagModel> GetByID(int id)
        {
            var model = new PostHashtagModel();
            try
            {
                var result = _postHashtagRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<PostHashtagEntity, PostHashtagModel>(result);
                    return DataServiceBase.TrueData<PostHashtagModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostHashtagModel>(ex, model);
            }

            return DataServiceBase.FailData<PostHashtagModel>(model);
        }

        public DataResponse<PostHashtagModel> Updated(PostHashtagModel model)
        {
            try
            {
                var entity = Mapper.Map<PostHashtagModel, PostHashtagEntity>(model);
                var result = _postHashtagRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostHashtagEntity, PostHashtagModel>(result);
                    return DataServiceBase.TrueData<PostHashtagModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostHashtagModel>(ex, model);
            }

            return DataServiceBase.FailData<PostHashtagModel>(model);
        }
    }
}
