﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpPostLikeService : IcpPostLikeService
    {
        private readonly IcpPostLikeRepository _postLikeRepository;
        public cpPostLikeService(IcpPostLikeRepository postLikeRepository)
        {
            _postLikeRepository = postLikeRepository;
        }

        public DataResponse<PostLikeModel> Add(PostLikeModel model)
        {
            try
            {
                var entity = Mapper.Map<PostLikeModel, PostLikeEntity>(model);
                var result = _postLikeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostLikeEntity, PostLikeModel>(result);
                    return DataServiceBase.TrueData<PostLikeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostLikeModel>(ex, model);
            }

            return DataServiceBase.FailData<PostLikeModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _postLikeRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<PostLike_DataModel> GetAll(PostLike_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<PostLike_DataModel, PostLike_DataEntity>(model);
                var result = _postLikeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostLike_DataEntity, PostLike_DataModel>(result);
                    return DataServiceBase.TrueData<PostLike_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostLike_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<PostLike_DataModel>(model);
        }

        public DataResponse<PostLikeModel> GetByID(int id)
        {
            var model = new PostLikeModel();
            try
            {
                var result = _postLikeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<PostLikeEntity, PostLikeModel>(result);
                    return DataServiceBase.TrueData<PostLikeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostLikeModel>(ex, model);
            }

            return DataServiceBase.FailData<PostLikeModel>(model);
        }

        public DataResponse<PostLikeModel> Updated(PostLikeModel model)
        {
            try
            {
                var entity = Mapper.Map<PostLikeModel, PostLikeEntity>(model);
                var result = _postLikeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostLikeEntity, PostLikeModel>(result);
                    return DataServiceBase.TrueData<PostLikeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostLikeModel>(ex, model);
            }

            return DataServiceBase.FailData<PostLikeModel>(model);
        }
    }
}
