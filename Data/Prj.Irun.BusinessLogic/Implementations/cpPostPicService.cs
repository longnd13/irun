﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpPostPicService : IcpPostPicService
    {
        private readonly IcpPostPicRepository _postPicRepository;
        public cpPostPicService(IcpPostPicRepository postPicRepository)
        {
            _postPicRepository = postPicRepository;
        }

        public DataResponse<PostPicModel> Add(PostPicModel model)
        {
            try
            {
                var entity = Mapper.Map<PostPicModel, PostPicEntity>(model);
                var result = _postPicRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostPicEntity, PostPicModel>(result);
                    return DataServiceBase.TrueData<PostPicModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostPicModel>(ex, model);
            }

            return DataServiceBase.FailData<PostPicModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _postPicRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<PostPic_DataModel> GetAll(PostPic_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<PostPic_DataModel, PostPic_DataEntity>(model);
                var result = _postPicRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostPic_DataEntity, PostPic_DataModel>(result);
                    return DataServiceBase.TrueData<PostPic_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostPic_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<PostPic_DataModel>(model);
        }

        public DataResponse<PostPicModel> GetByID(int id)
        {
            var model = new PostPicModel();
            try
            {
                var result = _postPicRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<PostPicEntity, PostPicModel>(result);
                    return DataServiceBase.TrueData<PostPicModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostPicModel>(ex, model);
            }

            return DataServiceBase.FailData<PostPicModel>(model);
        }

        public DataResponse<PostPicModel> Updated(PostPicModel model)
        {
            try
            {
                var entity = Mapper.Map<PostPicModel, PostPicEntity>(model);
                var result = _postPicRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostPicEntity, PostPicModel>(result);
                    return DataServiceBase.TrueData<PostPicModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostPicModel>(ex, model);
            }

            return DataServiceBase.FailData<PostPicModel>(model);
        }

        public DataResponse<PostPic_DataModel> SearchById(PostPic_DataModel model, int id)
        {
            try
            {
                var entity = Mapper.Map<PostPic_DataModel, PostPic_DataEntity>(model);
                var result = _postPicRepository.SearchById(entity, id);
                if (result != null)
                {
                    model = Mapper.Map<PostPic_DataEntity, PostPic_DataModel>(result);
                    return DataServiceBase.TrueData<PostPic_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostPic_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<PostPic_DataModel>(model);
        }
    }
}
