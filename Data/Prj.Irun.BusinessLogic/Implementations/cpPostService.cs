﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpPostService : IcpPostService
    {
        private readonly IcpPostRepository _postService;
        public cpPostService(IcpPostRepository postService)
        {
            _postService = postService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _postService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<bool> ChangeStatus(int id)
        {
            try
            {
                var result = _postService.ChangeStatus(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<Post_DataModel> GetAll(Post_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<Post_DataModel, Post_DataEntity>(model);
                var result = _postService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<Post_DataEntity, Post_DataModel>(result);
                    return DataServiceBase.TrueData<Post_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<Post_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<Post_DataModel>(model);
        }

        public DataResponse<PostModel> GetByID(int id)
        {
            var model = new PostModel();
            try
            {
                var result = _postService.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<PostEntity, PostModel>(result);
                    return DataServiceBase.TrueData<PostModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostModel>(ex, model);
            }

            return DataServiceBase.FailData<PostModel>(model);
        }

        public DataResponse<Post_DataModel> SearchByUsername(Post_DataModel model, string username)
        {
            try
            {
                var entity = Mapper.Map<Post_DataModel, Post_DataEntity>(model);
                var result = _postService.SearchByUsername(entity, username);
                if (result != null)
                {
                    model = Mapper.Map<Post_DataEntity, Post_DataModel>(result);
                    return DataServiceBase.TrueData<Post_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<Post_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<Post_DataModel>(model);
        }
    }
}
