﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpPostVideoService : IcpPostVideoService
    {
        private readonly IcpPostVideoRepository _postVideoRepository;
        public cpPostVideoService(IcpPostVideoRepository postVideoRepository)
        {
            _postVideoRepository = postVideoRepository;
        }

        public DataResponse<PostVideoModel> Add(PostVideoModel model)
        {
            try
            {
                var entity = Mapper.Map<PostVideoModel, PostVideoEntity>(model);
                var result = _postVideoRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostVideoEntity, PostVideoModel>(result);
                    return DataServiceBase.TrueData<PostVideoModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostVideoModel>(ex, model);
            }

            return DataServiceBase.FailData<PostVideoModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _postVideoRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<PostVideo_DataModel> GetAll(PostVideo_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<PostVideo_DataModel, PostVideo_DataEntity>(model);
                var result = _postVideoRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostVideo_DataEntity, PostVideo_DataModel>(result);
                    return DataServiceBase.TrueData<PostVideo_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostVideo_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<PostVideo_DataModel>(model);
        }

        public DataResponse<PostVideoModel> GetByID(int id)
        {
            var model = new PostVideoModel();
            try
            {
                var result = _postVideoRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<PostVideoEntity, PostVideoModel>(result);
                    return DataServiceBase.TrueData<PostVideoModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostVideoModel>(ex, model);
            }

            return DataServiceBase.FailData<PostVideoModel>(model);
        }

        public DataResponse<PostVideoModel> Updated(PostVideoModel model)
        {
            try
            {
                var entity = Mapper.Map<PostVideoModel, PostVideoEntity>(model);
                var result = _postVideoRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<PostVideoEntity, PostVideoModel>(result);
                    return DataServiceBase.TrueData<PostVideoModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostVideoModel>(ex, model);
            }

            return DataServiceBase.FailData<PostVideoModel>(model);
        }

        public DataResponse<PostVideo_DataModel> SearchById(PostVideo_DataModel model, int id)
        {
            try
            {
                var entity = Mapper.Map<PostVideo_DataModel, PostVideo_DataEntity>(model);
                var result = _postVideoRepository.SearchById(entity, id);
                if (result != null)
                {
                    model = Mapper.Map<PostVideo_DataEntity, PostVideo_DataModel>(result);
                    return DataServiceBase.TrueData<PostVideo_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PostVideo_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<PostVideo_DataModel>(model);
        }
    }
}
