﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpPrivacySettingService : IcpPrivacySettingService
    {
        private readonly IcpPrivacySettingRepository _privacySettingRepository;
        public cpPrivacySettingService(IcpPrivacySettingRepository privacySettingRepository)
        {
            _privacySettingRepository = privacySettingRepository;
        }

        public DataResponse<PrivacySettingModel> Add(PrivacySettingModel model)
        {
            try
            {
                var entity = Mapper.Map<PrivacySettingModel, PrivacySettingEntity>(model);
                var result = _privacySettingRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<PrivacySettingEntity, PrivacySettingModel>(result);
                    return DataServiceBase.TrueData<PrivacySettingModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PrivacySettingModel>(ex, model);
            }

            return DataServiceBase.FailData<PrivacySettingModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _privacySettingRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<PrivacySetting_DataModel> GetAll(PrivacySetting_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<PrivacySetting_DataModel, PrivacySetting_DataEntity>(model);
                var result = _privacySettingRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<PrivacySetting_DataEntity, PrivacySetting_DataModel>(result);
                    return DataServiceBase.TrueData<PrivacySetting_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PrivacySetting_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<PrivacySetting_DataModel>(model);
        }

        public DataResponse<PrivacySettingModel> GetByID(int id)
        {
            var model = new PrivacySettingModel();
            try
            {
                var result = _privacySettingRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<PrivacySettingEntity, PrivacySettingModel>(result);
                    return DataServiceBase.TrueData<PrivacySettingModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PrivacySettingModel>(ex, model);
            }

            return DataServiceBase.FailData<PrivacySettingModel>(model);
        }

        public DataResponse<PrivacySettingModel> Updated(PrivacySettingModel model)
        {
            try
            {
                var entity = Mapper.Map<PrivacySettingModel, PrivacySettingEntity>(model);
                var result = _privacySettingRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<PrivacySettingEntity, PrivacySettingModel>(result);
                    return DataServiceBase.TrueData<PrivacySettingModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<PrivacySettingModel>(ex, model);
            }

            return DataServiceBase.FailData<PrivacySettingModel>(model);
        }
    }
}
