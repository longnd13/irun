﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpRewardItemCategoryService : IcpRewardItemCategoryService
    {
        private readonly IcpRewardItemCategoryRepository _rewardItemCategoryRepository;
        public cpRewardItemCategoryService(IcpRewardItemCategoryRepository rewardItemCategoryRepository)
        {
            _rewardItemCategoryRepository = rewardItemCategoryRepository;
        }

        public DataResponse<RewardItemCategoryModel> Add(RewardItemCategoryModel model)
        {
            try
            {
                var entity = Mapper.Map<RewardItemCategoryModel, RewardItemCategoryEntity>(model);
                var result = _rewardItemCategoryRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<RewardItemCategoryEntity, RewardItemCategoryModel>(result);
                    return DataServiceBase.TrueData<RewardItemCategoryModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardItemCategoryModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardItemCategoryModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _rewardItemCategoryRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<RewardItemCategory_DataModel> GetAll(RewardItemCategory_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<RewardItemCategory_DataModel, RewardItemCategory_DataEntity>(model);
                var result = _rewardItemCategoryRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<RewardItemCategory_DataEntity, RewardItemCategory_DataModel>(result);
                    return DataServiceBase.TrueData<RewardItemCategory_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardItemCategory_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardItemCategory_DataModel>(model);
        }

        public DataResponse<RewardItemCategoryModel> GetByID(int id)
        {
            var model = new RewardItemCategoryModel();
            try
            {
                var result = _rewardItemCategoryRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<RewardItemCategoryEntity, RewardItemCategoryModel>(result);
                    return DataServiceBase.TrueData<RewardItemCategoryModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardItemCategoryModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardItemCategoryModel>(model);
        }

        public DataResponse<RewardItemCategoryModel> Updated(RewardItemCategoryModel model)
        {
            try
            {
                var entity = Mapper.Map<RewardItemCategoryModel, RewardItemCategoryEntity>(model);
                var result = _rewardItemCategoryRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<RewardItemCategoryEntity, RewardItemCategoryModel>(result);
                    return DataServiceBase.TrueData<RewardItemCategoryModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardItemCategoryModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardItemCategoryModel>(model);
        }
    }
}
