﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpRewardItemService : IcpRewardItemService
    {
        private readonly IcpRewardItemRepository _rewardItemRepository;
        public cpRewardItemService(IcpRewardItemRepository rewardItemRepository)
        {
            _rewardItemRepository = rewardItemRepository;
        }

        public DataResponse<RewardItemModel> Add(RewardItemModel model)
        {
            try
            {
                var entity = Mapper.Map<RewardItemModel, RewardItemEntity>(model);
                var result = _rewardItemRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<RewardItemEntity, RewardItemModel>(result);
                    return DataServiceBase.TrueData<RewardItemModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardItemModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardItemModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _rewardItemRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<RewardItem_DataModel> GetAll(RewardItem_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<RewardItem_DataModel, RewardItem_DataEntity>(model);
                var result = _rewardItemRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<RewardItem_DataEntity, RewardItem_DataModel>(result);
                    return DataServiceBase.TrueData<RewardItem_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardItem_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardItem_DataModel>(model);
        }

        public DataResponse<RewardItemModel> GetByID(int id)
        {
            var model = new RewardItemModel();
            try
            {
                var result = _rewardItemRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<RewardItemEntity, RewardItemModel>(result);
                    return DataServiceBase.TrueData<RewardItemModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardItemModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardItemModel>(model);
        }

        public DataResponse<RewardItemModel> Updated(RewardItemModel model)
        {
            try
            {
                var entity = Mapper.Map<RewardItemModel, RewardItemEntity>(model);
                var result = _rewardItemRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<RewardItemEntity, RewardItemModel>(result);
                    return DataServiceBase.TrueData<RewardItemModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardItemModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardItemModel>(model);
        }
    }
}
