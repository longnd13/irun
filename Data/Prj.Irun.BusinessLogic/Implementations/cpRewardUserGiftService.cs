﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpRewardUserGiftService : IcpRewardUserGiftService
    {
        private readonly IcpRewardUserGiftRepository _rewardUserGiftService;
        public cpRewardUserGiftService(IcpRewardUserGiftRepository RewardUserGiftService)
        {
            _rewardUserGiftService = RewardUserGiftService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _rewardUserGiftService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<RewardUserGift_DataModel> GetAll(RewardUserGift_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<RewardUserGift_DataModel, RewardUserGift_DataEntity>(model);
                var result = _rewardUserGiftService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<RewardUserGift_DataEntity, RewardUserGift_DataModel>(result);
                    return DataServiceBase.TrueData<RewardUserGift_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardUserGift_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardUserGift_DataModel>(model);
        }

        public DataResponse<RewardUserGift_DataModel> Search(RewardUserGift_DataModel model, int userGiftID, string giftRewardTypeCodeID)
        {
            try
            {
                var entity = Mapper.Map<RewardUserGift_DataModel, RewardUserGift_DataEntity>(model);
                var result = _rewardUserGiftService.Search(entity, userGiftID, giftRewardTypeCodeID);
                if (result != null)
                {
                    model = Mapper.Map<RewardUserGift_DataEntity, RewardUserGift_DataModel>(result);
                    return DataServiceBase.TrueData<RewardUserGift_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<RewardUserGift_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<RewardUserGift_DataModel>(model);
        }
    }
}
