﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpSeasonRankingTypeService : IcpSeasonRankingTypeService
    {
        private readonly IcpSeasonRankingTypeRepository _seasonRankingTypeRepository;
        public cpSeasonRankingTypeService(IcpSeasonRankingTypeRepository seasonRankingTypeRepository)
        {
            _seasonRankingTypeRepository = seasonRankingTypeRepository;
        }

        public DataResponse<SeasonRankingTypeModel> Add(SeasonRankingTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SeasonRankingTypeModel, SeasonRankingTypeEntity>(model);
                var result = _seasonRankingTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<SeasonRankingTypeEntity, SeasonRankingTypeModel>(result);
                    return DataServiceBase.TrueData<SeasonRankingTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SeasonRankingTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SeasonRankingTypeModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _seasonRankingTypeRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<SeasonRankingType_DataModel> GetAll(SeasonRankingType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<SeasonRankingType_DataModel, SeasonRankingType_DataEntity>(model);
                var result = _seasonRankingTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<SeasonRankingType_DataEntity, SeasonRankingType_DataModel>(result);
                    return DataServiceBase.TrueData<SeasonRankingType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SeasonRankingType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SeasonRankingType_DataModel>(model);
        }

        public DataResponse<SeasonRankingTypeModel> GetByID(int id)
        {
            var model = new SeasonRankingTypeModel();
            try
            {
                var result = _seasonRankingTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<SeasonRankingTypeEntity, SeasonRankingTypeModel>(result);
                    return DataServiceBase.TrueData<SeasonRankingTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SeasonRankingTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SeasonRankingTypeModel>(model);
        }

        public DataResponse<SeasonRankingTypeModel> Updated(SeasonRankingTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SeasonRankingTypeModel, SeasonRankingTypeEntity>(model);
                var result = _seasonRankingTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<SeasonRankingTypeEntity, SeasonRankingTypeModel>(result);
                    return DataServiceBase.TrueData<SeasonRankingTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SeasonRankingTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SeasonRankingTypeModel>(model);
        }

        public DataResponse<SeasonRankingType_DataModel> SearchByName(SeasonRankingType_DataModel model, string name)
        {
            try
            {
                var entity = Mapper.Map<SeasonRankingType_DataModel, SeasonRankingType_DataEntity>(model);
                var result = _seasonRankingTypeRepository.SearchByName(entity, name);
                if (result != null)
                {
                    model = Mapper.Map<SeasonRankingType_DataEntity, SeasonRankingType_DataModel>(result);
                    return DataServiceBase.TrueData<SeasonRankingType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SeasonRankingType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SeasonRankingType_DataModel>(model);
        }
    }
}
