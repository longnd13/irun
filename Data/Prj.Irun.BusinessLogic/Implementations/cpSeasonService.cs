﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpSeasonService : IcpSeasonService
    {
        private readonly IcpSeasonRepository _seasonRepository;
        public cpSeasonService(IcpSeasonRepository seasonRepository)
        {
            _seasonRepository = seasonRepository;
        }

        public DataResponse<SeasonModel> Add(SeasonModel model)
        {
            try
            {
                var entity = Mapper.Map<SeasonModel, SeasonEntity>(model);
                var result = _seasonRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<SeasonEntity, SeasonModel>(result);
                    return DataServiceBase.TrueData<SeasonModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SeasonModel>(ex, model);
            }

            return DataServiceBase.FailData<SeasonModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _seasonRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<Season_DataModel> GetAll(Season_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<Season_DataModel, Season_DataEntity>(model);
                var result = _seasonRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<Season_DataEntity, Season_DataModel>(result);
                    return DataServiceBase.TrueData<Season_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<Season_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<Season_DataModel>(model);
        }

        public DataResponse<SeasonModel> GetByID(int id)
        {
            var model = new SeasonModel();
            try
            {
                var result = _seasonRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<SeasonEntity, SeasonModel>(result);
                    return DataServiceBase.TrueData<SeasonModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SeasonModel>(ex, model);
            }

            return DataServiceBase.FailData<SeasonModel>(model);
        }

        public DataResponse<SeasonModel> Updated(SeasonModel model)
        {
            try
            {
                var entity = Mapper.Map<SeasonModel, SeasonEntity>(model);
                var result = _seasonRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<SeasonEntity, SeasonModel>(result);
                    return DataServiceBase.TrueData<SeasonModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SeasonModel>(ex, model);
            }

            return DataServiceBase.FailData<SeasonModel>(model);
        }
    }
}
