﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpSimilarGiftTypeGiftComponentTypeService : IcpSimilarGiftTypeGiftComponentTypeService
    {
        private readonly IcpSimilarGiftTypeGiftComponentTypeRepository _similarGiftTypeGiftComponentTypeRepository;
        public cpSimilarGiftTypeGiftComponentTypeService(IcpSimilarGiftTypeGiftComponentTypeRepository similarGiftTypeGiftComponentTypeRepository)
        {
            _similarGiftTypeGiftComponentTypeRepository = similarGiftTypeGiftComponentTypeRepository;
        }

        public DataResponse<SimilarGiftTypeGiftComponentTypeModel> Add(SimilarGiftTypeGiftComponentTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftComponentTypeModel, SimilarGiftTypeGiftComponentTypeEntity>(model);
                var result = _similarGiftTypeGiftComponentTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftComponentTypeEntity, SimilarGiftTypeGiftComponentTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftComponentTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftComponentTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftComponentTypeModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _similarGiftTypeGiftComponentTypeRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<SimilarGiftTypeGiftComponentType_DataModel> GetAll(SimilarGiftTypeGiftComponentType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftComponentType_DataModel, SimilarGiftTypeGiftComponentType_DataEntity>(model);
                var result = _similarGiftTypeGiftComponentTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftComponentType_DataEntity, SimilarGiftTypeGiftComponentType_DataModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftComponentType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftComponentType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftComponentType_DataModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftComponentTypeModel> GetByID(int id)
        {
            var model = new SimilarGiftTypeGiftComponentTypeModel();
            try
            {
                var result = _similarGiftTypeGiftComponentTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftComponentTypeEntity, SimilarGiftTypeGiftComponentTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftComponentTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftComponentTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftComponentTypeModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftComponentTypeModel> Updated(SimilarGiftTypeGiftComponentTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftComponentTypeModel, SimilarGiftTypeGiftComponentTypeEntity>(model);
                var result = _similarGiftTypeGiftComponentTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftComponentTypeEntity, SimilarGiftTypeGiftComponentTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftComponentTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftComponentTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftComponentTypeModel>(model);
        }

        public DataResponse<bool> DeleteBySimilarGiftId(string code_id)
        {
            try
            {
                var result = _similarGiftTypeGiftComponentTypeRepository.DeleteBySimilarGiftId(code_id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }
    }
}
