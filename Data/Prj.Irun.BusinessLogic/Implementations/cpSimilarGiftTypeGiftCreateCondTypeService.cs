﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpSimilarGiftTypeGiftCreateCondTypeService : IcpSimilarGiftTypeGiftCreateCondTypeService
    {
        private readonly IcpSimilarGiftTypeGiftCreateCondTypeRepository _SimilarGiftTypeGiftCreateCondTypeRepository;
        public cpSimilarGiftTypeGiftCreateCondTypeService(IcpSimilarGiftTypeGiftCreateCondTypeRepository SimilarGiftTypeGiftCreateCondTypeRepository)
        {
            _SimilarGiftTypeGiftCreateCondTypeRepository = SimilarGiftTypeGiftCreateCondTypeRepository;
        }

        public DataResponse<SimilarGiftTypeGiftCreateCondTypeModel> Add(SimilarGiftTypeGiftCreateCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftCreateCondTypeModel, SimilarGiftTypeGiftCreateCondTypeEntity>(model);
                var result = _SimilarGiftTypeGiftCreateCondTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftCreateCondTypeEntity, SimilarGiftTypeGiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftCreateCondTypeModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftCreateCondType_DataModel> GetAll(SimilarGiftTypeGiftCreateCondType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftCreateCondType_DataModel, SimilarGiftTypeGiftCreateCondType_DataEntity>(model);
                var result = _SimilarGiftTypeGiftCreateCondTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftCreateCondType_DataEntity, SimilarGiftTypeGiftCreateCondType_DataModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftCreateCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftCreateCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftCreateCondType_DataModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftCreateCondTypeModel> GetByID(int id)
        {
            var model = new SimilarGiftTypeGiftCreateCondTypeModel();
            try
            {
                var result = _SimilarGiftTypeGiftCreateCondTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftCreateCondTypeEntity, SimilarGiftTypeGiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftCreateCondTypeModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftCreateCondTypeModel> Updated(SimilarGiftTypeGiftCreateCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftCreateCondTypeModel, SimilarGiftTypeGiftCreateCondTypeEntity>(model);
                var result = _SimilarGiftTypeGiftCreateCondTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftCreateCondTypeEntity, SimilarGiftTypeGiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftCreateCondTypeModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftCreateCondType_DataModel> SearchBySimilarGiftId(SimilarGiftTypeGiftCreateCondType_DataModel model, int id)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftCreateCondType_DataModel, SimilarGiftTypeGiftCreateCondType_DataEntity>(model);
                var result = _SimilarGiftTypeGiftCreateCondTypeRepository.SearchBySimilarGiftId(entity, id);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftCreateCondType_DataEntity, SimilarGiftTypeGiftCreateCondType_DataModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftCreateCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftCreateCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftCreateCondType_DataModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftCreateCondType_DataModel> SearchByGiftCreateCondTypeId(SimilarGiftTypeGiftCreateCondType_DataModel model, int id)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftCreateCondType_DataModel, SimilarGiftTypeGiftCreateCondType_DataEntity>(model);
                var result = _SimilarGiftTypeGiftCreateCondTypeRepository.SearchByGiftCreateCondTypeId(entity, id);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftCreateCondType_DataEntity, SimilarGiftTypeGiftCreateCondType_DataModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftCreateCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftCreateCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftCreateCondType_DataModel>(model);
        }
    }
}
