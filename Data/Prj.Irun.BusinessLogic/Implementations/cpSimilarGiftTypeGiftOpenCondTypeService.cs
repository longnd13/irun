﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpSimilarGiftTypeGiftOpenCondTypeService : IcpSimilarGiftTypeGiftOpenCondTypeService
    {
        private readonly IcpSimilarGiftTypeGiftOpenCondTypeRepository _similarGiftTypeGiftOpenCondTypeRepository;
        public cpSimilarGiftTypeGiftOpenCondTypeService(IcpSimilarGiftTypeGiftOpenCondTypeRepository similarGiftTypeGiftOpenCondTypeRepository)
        {
            _similarGiftTypeGiftOpenCondTypeRepository = similarGiftTypeGiftOpenCondTypeRepository;
        }

        public DataResponse<SimilarGiftTypeGiftOpenCondTypeModel> Add(SimilarGiftTypeGiftOpenCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftOpenCondTypeModel, SimilarGiftTypeGiftOpenCondTypeEntity>(model);
                var result = _similarGiftTypeGiftOpenCondTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftOpenCondTypeEntity, SimilarGiftTypeGiftOpenCondTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftOpenCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftOpenCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftOpenCondTypeModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftOpenCondType_DataModel> GetAll(SimilarGiftTypeGiftOpenCondType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftOpenCondType_DataModel, SimilarGiftTypeGiftOpenCondType_DataEntity>(model);
                var result = _similarGiftTypeGiftOpenCondTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftOpenCondType_DataEntity, SimilarGiftTypeGiftOpenCondType_DataModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftOpenCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftOpenCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftOpenCondType_DataModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftOpenCondTypeModel> GetByID(int id)
        {
            var model = new SimilarGiftTypeGiftOpenCondTypeModel();
            try
            {
                var result = _similarGiftTypeGiftOpenCondTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftOpenCondTypeEntity, SimilarGiftTypeGiftOpenCondTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftOpenCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftOpenCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftOpenCondTypeModel>(model);
        }

        public DataResponse<SimilarGiftTypeGiftOpenCondTypeModel> Updated(SimilarGiftTypeGiftOpenCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeGiftOpenCondTypeModel, SimilarGiftTypeGiftOpenCondTypeEntity>(model);
                var result = _similarGiftTypeGiftOpenCondTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeGiftOpenCondTypeEntity, SimilarGiftTypeGiftOpenCondTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeGiftOpenCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeGiftOpenCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeGiftOpenCondTypeModel>(model);
        }
    }
}
