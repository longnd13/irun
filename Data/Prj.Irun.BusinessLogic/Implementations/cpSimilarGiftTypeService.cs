﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpSimilarGiftTypeService : IcpSimilarGiftTypeService
    {
        private readonly IcpSimilarGiftTypeRepository _similarGiftTypeRepository;
        public cpSimilarGiftTypeService(IcpSimilarGiftTypeRepository SimilarGiftTypeRepository)
        {
            _similarGiftTypeRepository = SimilarGiftTypeRepository;
        }

        public DataResponse<SimilarGiftTypeModel> Add(SimilarGiftTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeModel, SimilarGiftTypeEntity>(model);
                entity.CodeID = Globals.RandomCode(5);
                var result = _similarGiftTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeEntity, SimilarGiftTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeModel>(model);
        }

        public DataResponse<bool> Delete(string CodeID)
        {
            try
            {
                var result = _similarGiftTypeRepository.Delete(CodeID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<bool> UpdateAll()
        {
            try
            {
                var result = _similarGiftTypeRepository.UpdateAll();
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<SimilarGiftType_DataModel> GetAll(SimilarGiftType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftType_DataModel, SimilarGiftType_DataEntity>(model);
                var result = _similarGiftTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftType_DataEntity, SimilarGiftType_DataModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftType_DataModel>(model);
        }

        public DataResponse<SimilarGiftTypeModel> GetByID(string CodeID)
        {
            var model = new SimilarGiftTypeModel();
            try
            {
                var result = _similarGiftTypeRepository.GetById(CodeID);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeEntity, SimilarGiftTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeModel>(model);
        }

        public DataResponse<SimilarGiftTypeModel> GetByCreatedRate(int createdRate, string SystemGiftTypeCode)
        {
            var model = new SimilarGiftTypeModel();
            try
            {
                var result = _similarGiftTypeRepository.GetByCreatedRate(createdRate, SystemGiftTypeCode);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeEntity, SimilarGiftTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeModel>(model);
        }

        public DataResponse<bool> UpdateCreateRate(string code_id, int createdRate)
        {
            try
            {
                var result = _similarGiftTypeRepository.UpdateCreateRate(code_id, createdRate);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<SimilarGiftTypeModel> Updated(SimilarGiftTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SimilarGiftTypeModel, SimilarGiftTypeEntity>(model);
                var result = _similarGiftTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<SimilarGiftTypeEntity, SimilarGiftTypeModel>(result);
                    return DataServiceBase.TrueData<SimilarGiftTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SimilarGiftTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SimilarGiftTypeModel>(model);
        }

        public List<SimilarGiftTypeModel> SearchBySystemGiftType(string systemGiftTypeCodeID)
        {
            var list = new List<SimilarGiftTypeModel>();
            try
            {
                var data = _similarGiftTypeRepository.SearchBySystemGiftType(systemGiftTypeCodeID);
                list = Mapper.Map<List<SimilarGiftTypeEntity>, List<SimilarGiftTypeModel>>(data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }

        public List<SimilarGiftTypeModel> GetAllSystemGiftType()
        {
            var list = new List<SimilarGiftTypeModel>();
            try
            {
                var data = _similarGiftTypeRepository.GetAllSystemGiftType();
                list = Mapper.Map<List<SimilarGiftTypeEntity>, List<SimilarGiftTypeModel>>(data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return list;
        }
    }
}
