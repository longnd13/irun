﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpStepActivityService : IcpStepActivityService
    {
        private readonly IcpStepActivityRepository _stepActivityRepository;
        public cpStepActivityService(IcpStepActivityRepository stepActivityRepository)
        {
            _stepActivityRepository = stepActivityRepository;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _stepActivityRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<StepActivity_DataModel> GetAll(StepActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<StepActivity_DataModel, StepActivity_DataEntity>(model);
                var result = _stepActivityRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<StepActivity_DataEntity, StepActivity_DataModel>(result);
                    return DataServiceBase.TrueData<StepActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<StepActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<StepActivity_DataModel>(model);
        }

        public DataResponse<StepActivity_DataModel> SearchByUserID(StepActivity_DataModel model, string userID)
        {
            try
            {
                var entity = Mapper.Map<StepActivity_DataModel, StepActivity_DataEntity>(model);
                var result = _stepActivityRepository.SearchByUserID(entity, userID);
                if (result != null)
                {
                    model = Mapper.Map<StepActivity_DataEntity, StepActivity_DataModel>(result);
                    return DataServiceBase.TrueData<StepActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<StepActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<StepActivity_DataModel>(model);
        }

        public DataResponse<StepActivityModel> Add(StepActivityModel model)
        {
            try
            {
                var entity = Mapper.Map<StepActivityModel, StepActivityEntity>(model);
                var result = _stepActivityRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<StepActivityEntity, StepActivityModel>(result);
                    return DataServiceBase.TrueData<StepActivityModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<StepActivityModel>(ex, model);
            }

            return DataServiceBase.FailData<StepActivityModel>(model);
        }

        public DataResponse<StepActivityModel> GetByID(int id)
        {
            var model = new StepActivityModel();
            try
            {
                var result = _stepActivityRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<StepActivityEntity, StepActivityModel>(result);
                    return DataServiceBase.TrueData<StepActivityModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<StepActivityModel>(ex, model);
            }

            return DataServiceBase.FailData<StepActivityModel>(model);
        }

        public DataResponse<StepActivityModel> Updated(StepActivityModel model)
        {
            try
            {
                var entity = Mapper.Map<StepActivityModel, StepActivityEntity>(model);
                var result = _stepActivityRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<StepActivityEntity, StepActivityModel>(result);
                    return DataServiceBase.TrueData<StepActivityModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<StepActivityModel>(ex, model);
            }

            return DataServiceBase.FailData<StepActivityModel>(model);
        }
    }
}
