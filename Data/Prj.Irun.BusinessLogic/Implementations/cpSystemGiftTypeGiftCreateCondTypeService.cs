﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpSystemGiftTypeGiftCreateCondTypeService : IcpSystemGiftTypeGiftCreateCondTypeService
    {
        private readonly IcpSystemGiftTypeGiftCreateCondTypeRepository _systemGiftTypeGiftCreateCondTypeRepository;
        public cpSystemGiftTypeGiftCreateCondTypeService(IcpSystemGiftTypeGiftCreateCondTypeRepository systemGiftTypeGiftCreateCondTypeRepository)
        {
            _systemGiftTypeGiftCreateCondTypeRepository = systemGiftTypeGiftCreateCondTypeRepository;
        }

        public DataResponse<SystemGiftTypeGiftCreateCondTypeModel> Add(SystemGiftTypeGiftCreateCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftTypeGiftCreateCondTypeModel, SystemGiftTypeGiftCreateCondTypeEntity>(model);
                var result = _systemGiftTypeGiftCreateCondTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeGiftCreateCondTypeEntity, SystemGiftTypeGiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeGiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeGiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeGiftCreateCondTypeModel>(model);
        }

        public DataResponse<SystemGiftTypeGiftCreateCondType_DataModel> GetAll(SystemGiftTypeGiftCreateCondType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftTypeGiftCreateCondType_DataModel, SystemGiftTypeGiftCreateCondType_DataEntity>(model);
                var result = _systemGiftTypeGiftCreateCondTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeGiftCreateCondType_DataEntity, SystemGiftTypeGiftCreateCondType_DataModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeGiftCreateCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeGiftCreateCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeGiftCreateCondType_DataModel>(model);
        }

        public DataResponse<SystemGiftTypeGiftCreateCondTypeModel> GetByID(int id)
        {
            var model = new SystemGiftTypeGiftCreateCondTypeModel();
            try
            {
                var result = _systemGiftTypeGiftCreateCondTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeGiftCreateCondTypeEntity, SystemGiftTypeGiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeGiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeGiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeGiftCreateCondTypeModel>(model);
        }

        public DataResponse<SystemGiftTypeGiftCreateCondTypeModel> Updated(SystemGiftTypeGiftCreateCondTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftTypeGiftCreateCondTypeModel, SystemGiftTypeGiftCreateCondTypeEntity>(model);
                var result = _systemGiftTypeGiftCreateCondTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeGiftCreateCondTypeEntity, SystemGiftTypeGiftCreateCondTypeModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeGiftCreateCondTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeGiftCreateCondTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeGiftCreateCondTypeModel>(model);
        }

        public DataResponse<SystemGiftTypeGiftCreateCondType_DataModel> SearchBySimilarGiftId(SystemGiftTypeGiftCreateCondType_DataModel model, int id)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftTypeGiftCreateCondType_DataModel, SystemGiftTypeGiftCreateCondType_DataEntity>(model);
                var result = _systemGiftTypeGiftCreateCondTypeRepository.SearchBySimilarGiftId(entity, id);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeGiftCreateCondType_DataEntity, SystemGiftTypeGiftCreateCondType_DataModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeGiftCreateCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeGiftCreateCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeGiftCreateCondType_DataModel>(model);
        }

        public DataResponse<SystemGiftTypeGiftCreateCondType_DataModel> SearchByGiftCreateCondTypeId(SystemGiftTypeGiftCreateCondType_DataModel model, int id)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftTypeGiftCreateCondType_DataModel, SystemGiftTypeGiftCreateCondType_DataEntity>(model);
                var result = _systemGiftTypeGiftCreateCondTypeRepository.SearchByGiftCreateCondTypeId(entity, id);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeGiftCreateCondType_DataEntity, SystemGiftTypeGiftCreateCondType_DataModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeGiftCreateCondType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeGiftCreateCondType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeGiftCreateCondType_DataModel>(model);
        }

        public DataResponse<bool> DeleteBySystemGiftId(string code_id)
        {
            try
            {
                var result = _systemGiftTypeGiftCreateCondTypeRepository.DeleteBySystemGiftId(code_id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<bool> DeleteByGiftCreateCondTypeId(string code_id)
        {
            try
            {
                var result = _systemGiftTypeGiftCreateCondTypeRepository.DeleteByGiftCreateCondTypeId(code_id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }
    }
}
