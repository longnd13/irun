﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpSystemGiftTypeService : IcpSystemGiftTypeService
    {
        private readonly IcpSystemGiftTypeRepository _systemGiftTypeRepository;
        public cpSystemGiftTypeService(IcpSystemGiftTypeRepository systemGiftTypeRepository)
        {
            _systemGiftTypeRepository = systemGiftTypeRepository;
        }

        public DataResponse<SystemGiftTypeModel> Add(SystemGiftTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftTypeModel, SystemGiftTypeEntity>(model);
                entity.CodeID = Globals.RandomCode(5);
                var result = _systemGiftTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeEntity, SystemGiftTypeModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeModel>(model);
        }

        public DataResponse<bool> Delete(string CodeID)
        {
            try
            {
                var result = _systemGiftTypeRepository.Delete(CodeID);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<SystemGiftType_DataModel> GetAll(SystemGiftType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftType_DataModel, SystemGiftType_DataEntity>(model);
                var result = _systemGiftTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftType_DataEntity, SystemGiftType_DataModel>(result);
                    return DataServiceBase.TrueData<SystemGiftType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftType_DataModel>(model);
        }
        public DataResponse<SystemGiftTypeModel> GetByCodeId(string code_id)
        {
            var model = new SystemGiftTypeModel();
            try
            {
                var result = _systemGiftTypeRepository.GetByCodeId(code_id);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeEntity, SystemGiftTypeModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeModel>(model);
        }

        public DataResponse<SystemGiftTypeModel> GetByID(int id)
        {
            var model = new SystemGiftTypeModel();
            try
            {
                var result = _systemGiftTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeEntity, SystemGiftTypeModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeModel>(model);
        }

        public DataResponse<SystemGiftTypeModel> Updated(SystemGiftTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftTypeModel, SystemGiftTypeEntity>(model);
                var result = _systemGiftTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftTypeEntity, SystemGiftTypeModel>(result);
                    return DataServiceBase.TrueData<SystemGiftTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftTypeModel>(model);
        }

        public DataResponse<SystemGiftType_DataModel> MultipleSelectedGift(SystemGiftType_DataModel model, int id)
        {
            try
            {
                var entity = Mapper.Map<SystemGiftType_DataModel, SystemGiftType_DataEntity>(model);
                var result = _systemGiftTypeRepository.MultipleSelectedGift(entity, id);
                if (result != null)
                {
                    model = Mapper.Map<SystemGiftType_DataEntity, SystemGiftType_DataModel>(result);
                    return DataServiceBase.TrueData<SystemGiftType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<SystemGiftType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<SystemGiftType_DataModel>(model);
        }
    }
}
