﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpUserChallengeService : IcpUserChallengeService
    {
        private readonly IcpUserChallengeRepository _userChallengeRepository;
        public cpUserChallengeService(IcpUserChallengeRepository userChallengeRepository)
        {
            _userChallengeRepository = userChallengeRepository;
        }

        public DataResponse<UserChallengeModel> Add(UserChallengeModel model)
        {
            try
            {
                var entity = Mapper.Map<UserChallengeModel, UserChallengeEntity>(model);
                var result = _userChallengeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserChallengeEntity, UserChallengeModel>(result);
                    return DataServiceBase.TrueData<UserChallengeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserChallengeModel>(ex, model);
            }

            return DataServiceBase.FailData<UserChallengeModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _userChallengeRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<UserChallenge_DataModel> GetAll(UserChallenge_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<UserChallenge_DataModel, UserChallenge_DataEntity>(model);
                var result = _userChallengeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserChallenge_DataEntity, UserChallenge_DataModel>(result);
                    return DataServiceBase.TrueData<UserChallenge_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserChallenge_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserChallenge_DataModel>(model);
        }

        public DataResponse<UserChallengeModel> GetByID(int id)
        {
            var model = new UserChallengeModel();
            try
            {
                var result = _userChallengeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<UserChallengeEntity, UserChallengeModel>(result);
                    return DataServiceBase.TrueData<UserChallengeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserChallengeModel>(ex, model);
            }

            return DataServiceBase.FailData<UserChallengeModel>(model);
        }

        public DataResponse<UserChallengeModel> Updated(UserChallengeModel model)
        {
            try
            {
                var entity = Mapper.Map<UserChallengeModel, UserChallengeEntity>(model);
                var result = _userChallengeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserChallengeEntity, UserChallengeModel>(result);
                    return DataServiceBase.TrueData<UserChallengeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserChallengeModel>(ex, model);
            }

            return DataServiceBase.FailData<UserChallengeModel>(model);
        }
    }
}
