﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpUserFollowService : IcpUserFollowService
    {
        private readonly IcpUserFollowRepository _userFollowRepository;
        public cpUserFollowService(IcpUserFollowRepository userFollowRepository)
        {
            _userFollowRepository = userFollowRepository;
        }

        public DataResponse<UserFollowModel> Add(UserFollowModel model)
        {
            try
            {
                var entity = Mapper.Map<UserFollowModel, UserFollowEntity>(model);
                var result = _userFollowRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserFollowEntity, UserFollowModel>(result);
                    return DataServiceBase.TrueData<UserFollowModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserFollowModel>(ex, model);
            }

            return DataServiceBase.FailData<UserFollowModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _userFollowRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<UserFollow_DataModel> GetAll(UserFollow_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<UserFollow_DataModel, UserFollow_DataEntity>(model);
                var result = _userFollowRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserFollow_DataEntity, UserFollow_DataModel>(result);
                    return DataServiceBase.TrueData<UserFollow_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserFollow_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserFollow_DataModel>(model);
        }

        public DataResponse<UserFollowModel> GetByID(int id)
        {
            var model = new UserFollowModel();
            try
            {
                var result = _userFollowRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<UserFollowEntity, UserFollowModel>(result);
                    return DataServiceBase.TrueData<UserFollowModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserFollowModel>(ex, model);
            }

            return DataServiceBase.FailData<UserFollowModel>(model);
        }

        public DataResponse<UserFollowModel> Updated(UserFollowModel model)
        {
            try
            {
                var entity = Mapper.Map<UserFollowModel, UserFollowEntity>(model);
                var result = _userFollowRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserFollowEntity, UserFollowModel>(result);
                    return DataServiceBase.TrueData<UserFollowModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserFollowModel>(ex, model);
            }

            return DataServiceBase.FailData<UserFollowModel>(model);
        }

        public DataResponse<UserFollow_DataModel> SearchByUsername(UserFollow_DataModel model, string username)
        {
            try
            {
                var entity = Mapper.Map<UserFollow_DataModel, UserFollow_DataEntity>(model);
                var result = _userFollowRepository.SearchByUsername(entity, username);
                if (result != null)
                {
                    model = Mapper.Map<UserFollow_DataEntity, UserFollow_DataModel>(result);
                    return DataServiceBase.TrueData<UserFollow_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserFollow_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserFollow_DataModel>(model);
        }
    }
}
