﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpUserGiftService : IcpUserGiftService
    {
        private readonly IcpUserGiftRepository _userGiftService;
        public cpUserGiftService(IcpUserGiftRepository UserGiftService)
        {
            _userGiftService = UserGiftService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _userGiftService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<UserGift_DataModel> GetAll(UserGift_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<UserGift_DataModel, UserGift_DataEntity>(model);
                var result = _userGiftService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserGift_DataEntity, UserGift_DataModel>(result);
                    return DataServiceBase.TrueData<UserGift_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserGift_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserGift_DataModel>(model);
        }

        public DataResponse<UserGift_DataModel> Search(UserGift_DataModel model, string SystemGiftTypeCodeID, string SimilarGiftTypeID)
        {
            try
            {
                var entity = Mapper.Map<UserGift_DataModel, UserGift_DataEntity>(model);
                var result = _userGiftService.Search(entity, SystemGiftTypeCodeID, SimilarGiftTypeID);
                if (result != null)
                {
                    model = Mapper.Map<UserGift_DataEntity, UserGift_DataModel>(result);
                    return DataServiceBase.TrueData<UserGift_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserGift_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserGift_DataModel>(model);
        }

        public DataResponse<UserGiftModel> GetByID(int id)
        {
            var model = new UserGiftModel();
            try
            {
                var result = _userGiftService.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<UserGiftEntity, UserGiftModel>(result);
                    return DataServiceBase.TrueData<UserGiftModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserGiftModel>(ex, model);
            }

            return DataServiceBase.FailData<UserGiftModel>(model);
        }
    }
}
