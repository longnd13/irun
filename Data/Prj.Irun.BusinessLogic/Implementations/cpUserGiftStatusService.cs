﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpUserGiftStatusService : IcpUserGiftStatusService
    {
        private readonly IcpUserGiftStatusRepository _userGiftStatusRepository;
        public cpUserGiftStatusService(IcpUserGiftStatusRepository userGiftStatusRepository)
        {
            _userGiftStatusRepository = userGiftStatusRepository;
        }

        public DataResponse<UserGiftStatusModel> Add(UserGiftStatusModel model)
        {
            try
            {
                var entity = Mapper.Map<UserGiftStatusModel, UserGiftStatusEntity>(model);
                var result = _userGiftStatusRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserGiftStatusEntity, UserGiftStatusModel>(result);
                    return DataServiceBase.TrueData<UserGiftStatusModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserGiftStatusModel>(ex, model);
            }

            return DataServiceBase.FailData<UserGiftStatusModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _userGiftStatusRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<UserGiftStatus_DataModel> GetAll(UserGiftStatus_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<UserGiftStatus_DataModel, UserGiftStatus_DataEntity>(model);
                var result = _userGiftStatusRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserGiftStatus_DataEntity, UserGiftStatus_DataModel>(result);
                    return DataServiceBase.TrueData<UserGiftStatus_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserGiftStatus_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserGiftStatus_DataModel>(model);
        }

        public DataResponse<UserGiftStatusModel> GetByID(int id)
        {
            var model = new UserGiftStatusModel();
            try
            {
                var result = _userGiftStatusRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<UserGiftStatusEntity, UserGiftStatusModel>(result);
                    return DataServiceBase.TrueData<UserGiftStatusModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserGiftStatusModel>(ex, model);
            }

            return DataServiceBase.FailData<UserGiftStatusModel>(model);
        }

        public DataResponse<UserGiftStatusModel> Updated(UserGiftStatusModel model)
        {
            try
            {
                var entity = Mapper.Map<UserGiftStatusModel, UserGiftStatusEntity>(model);
                var result = _userGiftStatusRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserGiftStatusEntity, UserGiftStatusModel>(result);
                    return DataServiceBase.TrueData<UserGiftStatusModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserGiftStatusModel>(ex, model);
            }

            return DataServiceBase.FailData<UserGiftStatusModel>(model);
        }
    }
}
