﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpUserRewardItemActivityService : IcpUserRewardItemActivityService
    {
        private readonly IcpUserRewardItemActivityRepository _userRewardItemActivityService;
        public cpUserRewardItemActivityService(IcpUserRewardItemActivityRepository UserRewardItemActivityService)
        {
            _userRewardItemActivityService = UserRewardItemActivityService;
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _userRewardItemActivityService.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<UserRewardItemActivity_DataModel> GetAll(UserRewardItemActivity_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<UserRewardItemActivity_DataModel, UserRewardItemActivity_DataEntity>(model);
                var result = _userRewardItemActivityService.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserRewardItemActivity_DataEntity, UserRewardItemActivity_DataModel>(result);
                    return DataServiceBase.TrueData<UserRewardItemActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserRewardItemActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserRewardItemActivity_DataModel>(model);
        }

        public DataResponse<UserRewardItemActivity_DataModel> SearchByUsername(UserRewardItemActivity_DataModel model, string username)
        {
            try
            {
                var entity = Mapper.Map<UserRewardItemActivity_DataModel, UserRewardItemActivity_DataEntity>(model);
                var result = _userRewardItemActivityService.SearchByUsername(entity, username);
                if (result != null)
                {
                    model = Mapper.Map<UserRewardItemActivity_DataEntity, UserRewardItemActivity_DataModel>(result);
                    return DataServiceBase.TrueData<UserRewardItemActivity_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserRewardItemActivity_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserRewardItemActivity_DataModel>(model);
        }
    }
}
