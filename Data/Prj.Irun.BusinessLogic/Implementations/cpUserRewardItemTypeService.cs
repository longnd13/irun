﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class cpUserRewardItemTypeService : IcpUserRewardItemTypeService
    {
        private readonly IcpUserRewardItemTypeRepository _userRewardItemTypeRepository;
        public cpUserRewardItemTypeService(IcpUserRewardItemTypeRepository userRewardItemTypeRepository)
        {
            _userRewardItemTypeRepository = userRewardItemTypeRepository;
        }

        public DataResponse<UserRewardItemTypeModel> Add(UserRewardItemTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<UserRewardItemTypeModel, UserRewardItemTypeEntity>(model);
                var result = _userRewardItemTypeRepository.Add(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserRewardItemTypeEntity, UserRewardItemTypeModel>(result);
                    return DataServiceBase.TrueData<UserRewardItemTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserRewardItemTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<UserRewardItemTypeModel>(model);
        }

        public DataResponse<bool> Delete(int id)
        {
            try
            {
                var result = _userRewardItemTypeRepository.Delete(id);
                return DataServiceBase.TrueData<bool>(result);
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<bool>(ex, false);
            }
        }

        public DataResponse<UserRewardItemType_DataModel> GetAll(UserRewardItemType_DataModel model)
        {
            try
            {
                var entity = Mapper.Map<UserRewardItemType_DataModel, UserRewardItemType_DataEntity>(model);
                var result = _userRewardItemTypeRepository.GetAll(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserRewardItemType_DataEntity, UserRewardItemType_DataModel>(result);
                    return DataServiceBase.TrueData<UserRewardItemType_DataModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserRewardItemType_DataModel>(ex, model);
            }

            return DataServiceBase.FailData<UserRewardItemType_DataModel>(model);
        }

        public DataResponse<UserRewardItemTypeModel> GetByID(int id)
        {
            var model = new UserRewardItemTypeModel();
            try
            {
                var result = _userRewardItemTypeRepository.GetById(id);
                if (result != null)
                {
                    model = Mapper.Map<UserRewardItemTypeEntity, UserRewardItemTypeModel>(result);
                    return DataServiceBase.TrueData<UserRewardItemTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserRewardItemTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<UserRewardItemTypeModel>(model);
        }

        public DataResponse<UserRewardItemTypeModel> Updated(UserRewardItemTypeModel model)
        {
            try
            {
                var entity = Mapper.Map<UserRewardItemTypeModel, UserRewardItemTypeEntity>(model);
                var result = _userRewardItemTypeRepository.Update(entity);
                if (result != null)
                {
                    model = Mapper.Map<UserRewardItemTypeEntity, UserRewardItemTypeModel>(result);
                    return DataServiceBase.TrueData<UserRewardItemTypeModel>(model);
                }
            }
            catch (Exception ex)
            {
                return DataServiceBase.ExceptionData<UserRewardItemTypeModel>(ex, model);
            }

            return DataServiceBase.FailData<UserRewardItemTypeModel>(model);
        }
    }
}
