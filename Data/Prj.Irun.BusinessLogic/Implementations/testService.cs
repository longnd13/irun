﻿using AutoMapper;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Implementations
{
    public class testService : ItestService
    {
        private readonly ItestRepository _testRepository;
        public testService(ItestRepository testRepository)
        {
            _testRepository = testRepository;
        }


        public List<testModel> getdata()
        {
            try
            {
                var data = _testRepository.getData();
                var result = Mapper.Map<List<testEntity>, List<testModel>>(data);

                return result;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return new List<testModel>();
        }
    }
}
