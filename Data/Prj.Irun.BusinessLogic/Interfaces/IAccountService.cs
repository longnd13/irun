﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
public  interface IAccountService
    {
        DataResponse<string> LoginByToken(LoginModel model);
        DataResponse<string> AccountRegister(AccountRegisterModel account_register_model);
        DataResponse<string> AccountChangePassword(string access_token_client, AccountChangePasswordModel account_changepass_model);
        DataResponse<string> AccountLogin(LoginModel account_login_model);
        DataResponse<string> FacebookLogin(AccountLoginSocialModel login_social_model);
        DataResponse<long> GetGoldByUser(string access_token_client);
        DataResponse<AccountViewModel> GetAccountInfo(string access_token_client);
        DataResponse<int> OnOffOTP();
        DataResponse<UpdateAccountViewModel> UpdateAccountInfo(string access_token_client, UpdateAccountViewModel model);
    }
}
