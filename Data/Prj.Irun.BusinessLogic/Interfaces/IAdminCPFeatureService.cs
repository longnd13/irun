﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IAdminCPFeatureService
    {
        DataResponse<AdminCPFeatureModel> Add(AdminCPFeatureModel model);

        DataResponse<AdminCPFeatureModel> Updated(AdminCPFeatureModel model);

        DataResponse<AdminCPFeature_DataModel> GetAll(AdminCPFeature_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<AdminCPFeatureModel> GetByID(int id);

        DataResponse<AdminCPFeature_DataModel> GetByParentID(AdminCPFeature_DataModel model, int parentID);

        DataResponse<AdminCPFeatureModel> GetNodeByParentID(int parentID, long id);

        DataResponse<bool> DeleteNode(int parentID);

        List<AdminCPFeatureModel> GetFeatureByGroupId(int groupId);
    }
}
