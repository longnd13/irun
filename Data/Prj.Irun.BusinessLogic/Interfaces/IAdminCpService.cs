﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IAdminCpService
    {
        DataResponse<AdminCpModel> Add(AdminCpModel model);

        DataResponse<AdminCpModel> Updated(AdminCpModel model);

        DataResponse<AdminCp_DataModel> GetAll(AdminCp_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<AdminCpModel> GetByID(int id);
    }
}
