﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IApiConfigService
    {
        DataResponse<ApiConfigModel> Add(ApiConfigModel model);

        DataResponse<ApiConfigModel> Updated(ApiConfigModel model);

        DataResponse<ApiConfig_DataModel> GetAll(ApiConfig_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<ApiConfigModel> GetByID(int id);
    }
}
