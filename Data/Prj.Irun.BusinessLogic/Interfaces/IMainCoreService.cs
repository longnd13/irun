﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
   public interface IMainCoreService
    {
        DataResponse<StepInfoModel> PostSteps(List<PostStepsModel> model, string access_token_client);
        DataResponse<DistanceInfoModel> PostDistance(List<PostStepsModel> model, string access_token_client);

        DataResponse<bool> OpenBasicGift(string access_token_client, OpenBasicGiftModel open_basic_gift_model);
        DataResponse<bool> OpenDailyGift(string access_token_client, OpenDailyGiftModel open_daily_gift_model);

        DataResponse<ResponseDailyGiftUserModel> GetDailyGiftByUser(string access_token_client);


    }
}
