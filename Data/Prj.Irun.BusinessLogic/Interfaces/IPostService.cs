﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IPostService
    {
        DataResponse<Post_DataModel> GetAll(Post_DataModel model);

        DataResponse<Post_DataModel> SearchByUsername(Post_DataModel model, string username);

        DataResponse<bool> Delete(int id);

    }
}
