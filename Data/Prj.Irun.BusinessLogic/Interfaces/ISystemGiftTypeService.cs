
using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{

    public interface ISystemGiftTypeService
    {
        DataResponse<List<SystemGiftTypeModel>> GetAllSystemGiftType();

        DataResponse<SystemGiftTypeModel> GetSystemGiftTypeByCodeID(string code_id);
    }
}
