﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
   public interface IapiGiftService
    {
        DataResponse<ResponseCumulativeGiftUserModel> GetCumulativeGift(string access_token_client);
        DataResponse<bool> OpenCumulativeGift(string access_token_client, OpenCumulativeGiftModel open_cumulative_gift_model);
    }
}
