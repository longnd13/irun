﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpAPIParameterService
    {
        DataResponse<APIParameterModel> Add(APIParameterModel model);

        DataResponse<APIParameterModel> Updated(APIParameterModel model);

        DataResponse<APIParameter_DataModel> GetAll(APIParameter_DataModel model);

        DataResponse<bool> Delete(string CodeID);

        DataResponse<APIParameterModel> GetByID(string CodeID);
    }
}
