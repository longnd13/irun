﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpAccountActivityService
    {
        DataResponse<AccountActivity_DataModel> GetAll(AccountActivity_DataModel model);

        DataResponse<AccountActivity_DataModel> SearchByUserID(AccountActivity_DataModel model, string userID);

        DataResponse<bool> Delete(int id);
    }
}
