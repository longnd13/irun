﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpAccountInfoService
    {
        DataResponse<AccountInfo_DataModel> GetAll(AccountInfo_DataModel model);

        DataResponse<AccountInfo_DataModel> SearchByUsername(AccountInfo_DataModel model, string username, string fullname);

        DataResponse<AccountInfo_DataModel> GetAllBySort(AccountInfo_DataModel model, string goldSort, string stepSort, string distanceSort);

        DataResponse<bool> Delete(int id);

        DataResponse<AccountInfoModel> Add(AccountInfoModel model);

        DataResponse<AccountInfoModel> GetByID(int id);

        DataResponse<AccountInfoModel> Updated(AccountInfoModel model);
    }
}
