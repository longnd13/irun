﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpAdminActivityService
    {
        DataResponse<AdminActivity_DataModel> GetAll(AdminActivity_DataModel model);

        DataResponse<AdminActivity_DataModel> SearchByAdminId(AdminActivity_DataModel model, int adminID);

        DataResponse<bool> Delete(int id);

    }
}
