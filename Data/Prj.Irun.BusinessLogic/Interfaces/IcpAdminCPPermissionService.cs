﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpAdminCPPermissionService
    {
        DataResponse<AdminCPPermissionModel> Add(AdminCPPermissionModel model);

        DataResponse<AdminCPPermissionModel> Updated(AdminCPPermissionModel model);

        DataResponse<AdminCPPermission_DataModel> GetAll(AdminCPPermission_DataModel model);

        DataResponse<bool> Delete(string userName);

        DataResponse<AdminCPPermissionModel> GetByID(int id);

        MessageModel GetPermission(string userName);
    }
}
