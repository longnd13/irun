﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpAdsActivityService
    {
        DataResponse<AdsActivity_DataModel> GetAll(AdsActivity_DataModel model);

        DataResponse<AdsActivity_DataModel> SearchByAdsCodeID(AdsActivity_DataModel model, string adsCodeID);

        DataResponse<bool> Delete(string adsCodeID);

    }
}
