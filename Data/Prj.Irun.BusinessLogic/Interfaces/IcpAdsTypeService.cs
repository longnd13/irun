﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpAdsTypeService
    {
        DataResponse<AdsTypeModel> Add(AdsTypeModel model);

        DataResponse<AdsTypeModel> Updated(AdsTypeModel model);

        DataResponse<AdsType_DataModel> GetAll(AdsType_DataModel model);

        DataResponse<bool> Delete(string codeID);

        DataResponse<AdsTypeModel> GetByID(string codeID);
    }
}
