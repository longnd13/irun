﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpCategoryService
    {
        DataResponse<CategoryModel> Add(CategoryModel model);

        DataResponse<CategoryModel> Updated(CategoryModel model);

        DataResponse<Category_DataModel> GetAll(Category_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<CategoryModel> GetByID(int id);
    }
}
