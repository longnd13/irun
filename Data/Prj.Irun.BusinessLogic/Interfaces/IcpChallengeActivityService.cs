﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpChallengeActivityService
    {
        DataResponse<ChallengeActivity_DataModel> GetAll(ChallengeActivity_DataModel model);

        DataResponse<ChallengeActivity_DataModel> SearchByUserChallengeID(ChallengeActivity_DataModel model, int userChallengeID);

        DataResponse<bool> Delete(int id);

    }
}
