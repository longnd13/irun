﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpChallengeCompleteCondTypeService
    {
        DataResponse<ChallengeCompleteCondTypeModel> Add(ChallengeCompleteCondTypeModel model);

        DataResponse<ChallengeCompleteCondTypeModel> Updated(ChallengeCompleteCondTypeModel model);

        DataResponse<ChallengeCompleteCondType_DataModel> GetAll(ChallengeCompleteCondType_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<ChallengeCompleteCondTypeModel> GetByID(int id);
    }
}
