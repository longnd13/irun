﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpChallengeTypeService
    {
        DataResponse<ChallengeTypeModel> Add(ChallengeTypeModel model);

        DataResponse<ChallengeTypeModel> Updated(ChallengeTypeModel model);

        DataResponse<ChallengeType_DataModel> GetAll(ChallengeType_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<ChallengeTypeModel> GetByID(int id);
    }
}
