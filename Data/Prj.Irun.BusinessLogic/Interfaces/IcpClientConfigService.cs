﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpClientConfigService
    {
        DataResponse<ClientConfigModel> Add(ClientConfigModel model);

        DataResponse<ClientConfigModel> Updated(ClientConfigModel model);

        DataResponse<ClientConfig_DataModel> GetAll(ClientConfig_DataModel model);

        DataResponse<bool> Delete(string CodeID);

        DataResponse<ClientConfigModel> GetByID(string CodeID);
    }
}
