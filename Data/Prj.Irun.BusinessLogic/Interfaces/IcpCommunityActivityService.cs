﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpCommunityActivityService
    {
        DataResponse<CommunityActivity_DataModel> GetAll(CommunityActivity_DataModel model);

        DataResponse<CommunityActivity_DataModel> SearchByUserID(CommunityActivity_DataModel model, string userID);

        DataResponse<bool> Delete(int id);

    }
}
