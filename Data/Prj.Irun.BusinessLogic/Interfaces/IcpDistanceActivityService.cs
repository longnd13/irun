﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpDistanceActivityService
    {
        DataResponse<DistanceActivity_DataModel> GetAll(DistanceActivity_DataModel model);

        DataResponse<DistanceActivity_DataModel> SearchByUserID(DistanceActivity_DataModel model, string userID);

        DataResponse<bool> Delete(int id);

        DataResponse<DistanceActivityModel> Add(DistanceActivityModel model);

        DataResponse<DistanceActivityModel> GetByID(int id);

        DataResponse<DistanceActivityModel> Updated(DistanceActivityModel model);
    }
}
