﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpGiftActivityService
    {
        DataResponse<GiftActivity_DataModel> GetAll(GiftActivity_DataModel model);

        DataResponse<GiftActivity_DataModel> SearchByUserGiftID(GiftActivity_DataModel model, int userGiftID);

        DataResponse<bool> Delete(int id);

    }
}
