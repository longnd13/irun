﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpGiftComponentTypeService
    {
        DataResponse<GiftComponentTypeModel> Add(GiftComponentTypeModel model);

        DataResponse<GiftComponentTypeModel> Updated(GiftComponentTypeModel model);

        DataResponse<GiftComponentType_DataModel> GetAll(GiftComponentType_DataModel model);

        DataResponse<bool> Delete(string CodeID);

        DataResponse<GiftComponentTypeModel> GetByID(string CodeID);

        DataResponse<GiftComponentType_DataModel> MultipleSelectedGift(GiftComponentType_DataModel model, string CodeID);

    }
}
