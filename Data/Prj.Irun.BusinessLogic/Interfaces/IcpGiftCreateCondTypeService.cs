﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpGiftCreateCondTypeService
    {
        DataResponse<GiftCreateCondTypeModel> Add(GiftCreateCondTypeModel model);

        DataResponse<GiftCreateCondTypeModel> Updated(GiftCreateCondTypeModel model);

        DataResponse<GiftCreateCondType_DataModel> GetAll(GiftCreateCondType_DataModel model);

        DataResponse<bool> Delete(string CodeID);

        DataResponse<GiftCreateCondTypeModel> GetByID(string CodeID);

        DataResponse<GiftCreateCondType_DataModel> MultipleSelectedGift(GiftCreateCondType_DataModel model, string id);


    }
}
