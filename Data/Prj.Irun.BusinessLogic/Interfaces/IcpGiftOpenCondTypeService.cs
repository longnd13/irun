﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpGiftOpenCondTypeService
    {
        DataResponse<GiftOpenCondTypeModel> Add(GiftOpenCondTypeModel model);

        DataResponse<GiftOpenCondTypeModel> Updated(GiftOpenCondTypeModel model);

        DataResponse<GiftOpenCondType_DataModel> GetAll(GiftOpenCondType_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<GiftOpenCondTypeModel> GetByID(int id);

        DataResponse<GiftOpenCondTypeModel> GetByCodeId(string code_id);
    }
}
