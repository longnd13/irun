﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpGiftRewardTypeService
    {
        DataResponse<GiftRewardTypeModel> Add(GiftRewardTypeModel model);

        DataResponse<GiftRewardTypeModel> Updated(GiftRewardTypeModel model);

        DataResponse<GiftRewardType_DataModel> GetAll(GiftRewardType_DataModel model);

        DataResponse<bool> Delete(string Code);

        DataResponse<GiftRewardTypeModel> GetByID(string Code);
    }
}
