﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpHashtagService
    {
        DataResponse<HashtagModel> Add(HashtagModel model);

        DataResponse<HashtagModel> Updated(HashtagModel model);

        DataResponse<Hashtag_DataModel> GetAll(Hashtag_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<HashtagModel> GetByID(int id);
    }
}
