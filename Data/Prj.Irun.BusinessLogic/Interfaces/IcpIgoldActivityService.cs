﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpIgoldActivityService
    {
        DataResponse<IgoldActivity_DataModel> GetAll(IgoldActivity_DataModel model);

        DataResponse<IgoldActivity_DataModel> SearchByUserID(IgoldActivity_DataModel model, string userID);

        DataResponse<bool> Delete(int id);

    }
}
