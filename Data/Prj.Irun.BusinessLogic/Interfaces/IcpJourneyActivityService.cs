﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpJourneyActivityService
    {
        DataResponse<JourneyActivity_DataModel> GetAll(JourneyActivity_DataModel model);

        DataResponse<JourneyActivity_DataModel> SearchByUserID(JourneyActivity_DataModel model, string userID);

        DataResponse<bool> Delete(int id);

    }
}
