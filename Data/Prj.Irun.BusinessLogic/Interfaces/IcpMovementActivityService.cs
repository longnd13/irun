﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpMovementActivityService
    {
        DataResponse<MovementActivity_DataModel> GetAll(MovementActivity_DataModel model);

        DataResponse<MovementActivity_DataModel> SearchByUserID(MovementActivity_DataModel model, string userID);

        DataResponse<bool> Delete(int id);
    }
}
