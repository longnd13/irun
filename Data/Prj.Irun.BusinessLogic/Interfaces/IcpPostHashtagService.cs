﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpPostHashtagService
    {
        DataResponse<PostHashtagModel> Add(PostHashtagModel model);

        DataResponse<PostHashtagModel> Updated(PostHashtagModel model);

        DataResponse<PostHashtag_DataModel> GetAll(PostHashtag_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<PostHashtagModel> GetByID(int id);
    }
}
