﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpPostLikeService
    {
        DataResponse<PostLikeModel> Add(PostLikeModel model);

        DataResponse<PostLikeModel> Updated(PostLikeModel model);

        DataResponse<PostLike_DataModel> GetAll(PostLike_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<PostLikeModel> GetByID(int id);
    }
}
