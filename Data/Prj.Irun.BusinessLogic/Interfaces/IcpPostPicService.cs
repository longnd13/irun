﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpPostPicService
    {
        DataResponse<PostPicModel> Add(PostPicModel model);

        DataResponse<PostPicModel> Updated(PostPicModel model);

        DataResponse<PostPic_DataModel> GetAll(PostPic_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<PostPicModel> GetByID(int id);

        DataResponse<PostPic_DataModel> SearchById(PostPic_DataModel model, int id);
    }
}
