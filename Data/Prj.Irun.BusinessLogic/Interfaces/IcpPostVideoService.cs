﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpPostVideoService
    {
        DataResponse<PostVideoModel> Add(PostVideoModel model);

        DataResponse<PostVideoModel> Updated(PostVideoModel model);

        DataResponse<PostVideo_DataModel> GetAll(PostVideo_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<PostVideoModel> GetByID(int id);

        DataResponse<PostVideo_DataModel> SearchById(PostVideo_DataModel model, int id);
    }
}
