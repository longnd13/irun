﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpPrivacySettingService
    {
        DataResponse<PrivacySettingModel> Add(PrivacySettingModel model);

        DataResponse<PrivacySettingModel> Updated(PrivacySettingModel model);

        DataResponse<PrivacySetting_DataModel> GetAll(PrivacySetting_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<PrivacySettingModel> GetByID(int id);
    }
}
