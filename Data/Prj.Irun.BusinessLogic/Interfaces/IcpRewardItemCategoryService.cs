﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpRewardItemCategoryService
    {
        DataResponse<RewardItemCategoryModel> Add(RewardItemCategoryModel model);

        DataResponse<RewardItemCategoryModel> Updated(RewardItemCategoryModel model);

        DataResponse<RewardItemCategory_DataModel> GetAll(RewardItemCategory_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<RewardItemCategoryModel> GetByID(int id);
    }
}
