﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpRewardItemService
    {
        DataResponse<RewardItemModel> Add(RewardItemModel model);

        DataResponse<RewardItemModel> Updated(RewardItemModel model);

        DataResponse<RewardItem_DataModel> GetAll(RewardItem_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<RewardItemModel> GetByID(int id);
    }
}
