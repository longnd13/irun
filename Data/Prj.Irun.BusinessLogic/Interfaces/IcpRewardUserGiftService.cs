﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpRewardUserGiftService
    {
        DataResponse<RewardUserGift_DataModel> GetAll(RewardUserGift_DataModel model);

        DataResponse<RewardUserGift_DataModel> Search(RewardUserGift_DataModel model, int userGiftID, string giftRewardTypeCodeID);

        DataResponse<bool> Delete(int id);
    }
}
