﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpSeasonRankingTypeService
    {
        DataResponse<SeasonRankingTypeModel> Add(SeasonRankingTypeModel model);

        DataResponse<SeasonRankingTypeModel> Updated(SeasonRankingTypeModel model);

        DataResponse<SeasonRankingType_DataModel> GetAll(SeasonRankingType_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<SeasonRankingTypeModel> GetByID(int id);

        DataResponse<SeasonRankingType_DataModel> SearchByName(SeasonRankingType_DataModel model, string name);

    }
}
