﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpSeasonService
    {
        DataResponse<SeasonModel> Add(SeasonModel model);

        DataResponse<SeasonModel> Updated(SeasonModel model);

        DataResponse<Season_DataModel> GetAll(Season_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<SeasonModel> GetByID(int id);
    }
}
