﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpSimilarGiftTypeGiftComponentTypeService
    {
        DataResponse<SimilarGiftTypeGiftComponentTypeModel> Add(SimilarGiftTypeGiftComponentTypeModel model);

        DataResponse<SimilarGiftTypeGiftComponentTypeModel> Updated(SimilarGiftTypeGiftComponentTypeModel model);

        DataResponse<SimilarGiftTypeGiftComponentType_DataModel> GetAll(SimilarGiftTypeGiftComponentType_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<SimilarGiftTypeGiftComponentTypeModel> GetByID(int id);

        DataResponse<bool> DeleteBySimilarGiftId(string code_id);
    }
}
