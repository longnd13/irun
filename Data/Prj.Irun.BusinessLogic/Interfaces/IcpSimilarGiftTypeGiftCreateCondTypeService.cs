﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpSimilarGiftTypeGiftCreateCondTypeService
    {
        DataResponse<SimilarGiftTypeGiftCreateCondTypeModel> Add(SimilarGiftTypeGiftCreateCondTypeModel model);

        DataResponse<SimilarGiftTypeGiftCreateCondTypeModel> Updated(SimilarGiftTypeGiftCreateCondTypeModel model);

        DataResponse<SimilarGiftTypeGiftCreateCondType_DataModel> GetAll(SimilarGiftTypeGiftCreateCondType_DataModel model);

        DataResponse<SimilarGiftTypeGiftCreateCondTypeModel> GetByID(int id);

        DataResponse<SimilarGiftTypeGiftCreateCondType_DataModel> SearchBySimilarGiftId(SimilarGiftTypeGiftCreateCondType_DataModel model, int id);

        DataResponse<SimilarGiftTypeGiftCreateCondType_DataModel> SearchByGiftCreateCondTypeId(SimilarGiftTypeGiftCreateCondType_DataModel model, int id);
    }
}
