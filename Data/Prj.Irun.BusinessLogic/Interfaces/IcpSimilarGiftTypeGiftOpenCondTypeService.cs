﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpSimilarGiftTypeGiftOpenCondTypeService
    {
        DataResponse<SimilarGiftTypeGiftOpenCondTypeModel> Add(SimilarGiftTypeGiftOpenCondTypeModel model);

        DataResponse<SimilarGiftTypeGiftOpenCondTypeModel> Updated(SimilarGiftTypeGiftOpenCondTypeModel model);

        DataResponse<SimilarGiftTypeGiftOpenCondType_DataModel> GetAll(SimilarGiftTypeGiftOpenCondType_DataModel model);

        DataResponse<SimilarGiftTypeGiftOpenCondTypeModel> GetByID(int id);
    }
}
