﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpSimilarGiftTypeService
    {
        DataResponse<SimilarGiftTypeModel> Add(SimilarGiftTypeModel model);

        DataResponse<SimilarGiftTypeModel> Updated(SimilarGiftTypeModel model);

        DataResponse<SimilarGiftType_DataModel> GetAll(SimilarGiftType_DataModel model);

        DataResponse<bool> Delete(string CodeID);

        DataResponse<bool> UpdateAll();

        DataResponse<bool> UpdateCreateRate(string code_id, int createdRate);

        DataResponse<SimilarGiftTypeModel> GetByID(string CodeID);

        DataResponse<SimilarGiftTypeModel> GetByCreatedRate(int createdRate, string SystemGiftTypeCode);

        List<SimilarGiftTypeModel> SearchBySystemGiftType(string systemGiftTypeCodeID);

        List<SimilarGiftTypeModel> GetAllSystemGiftType();
    }
}
