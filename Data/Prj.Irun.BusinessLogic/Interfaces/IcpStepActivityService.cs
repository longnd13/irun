﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpStepActivityService
    {
        DataResponse<StepActivity_DataModel> GetAll(StepActivity_DataModel model);

        DataResponse<StepActivity_DataModel> SearchByUserID(StepActivity_DataModel model, string userID);

        DataResponse<bool> Delete(int id);

        DataResponse<StepActivityModel> Add(StepActivityModel model);

        DataResponse<StepActivityModel> GetByID(int id);

        DataResponse<StepActivityModel> Updated(StepActivityModel model);
    }
}
