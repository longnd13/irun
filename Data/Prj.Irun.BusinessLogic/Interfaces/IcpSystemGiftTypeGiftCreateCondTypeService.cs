﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpSystemGiftTypeGiftCreateCondTypeService
    {
        DataResponse<SystemGiftTypeGiftCreateCondTypeModel> Add(SystemGiftTypeGiftCreateCondTypeModel model);

        DataResponse<SystemGiftTypeGiftCreateCondTypeModel> Updated(SystemGiftTypeGiftCreateCondTypeModel model);

        DataResponse<SystemGiftTypeGiftCreateCondType_DataModel> GetAll(SystemGiftTypeGiftCreateCondType_DataModel model);

        DataResponse<SystemGiftTypeGiftCreateCondTypeModel> GetByID(int id);

        DataResponse<SystemGiftTypeGiftCreateCondType_DataModel> SearchBySimilarGiftId(SystemGiftTypeGiftCreateCondType_DataModel model, int id);

        DataResponse<SystemGiftTypeGiftCreateCondType_DataModel> SearchByGiftCreateCondTypeId(SystemGiftTypeGiftCreateCondType_DataModel model, int id);

        DataResponse<bool> DeleteBySystemGiftId(string code_id);

        DataResponse<bool> DeleteByGiftCreateCondTypeId(string code_id);
    }
}
