﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
  public interface IcpSystemGiftTypeService
    {
        DataResponse<SystemGiftTypeModel> Add(SystemGiftTypeModel model);

        DataResponse<bool> Delete(string CodeID);

        DataResponse<SystemGiftType_DataModel> GetAll(SystemGiftType_DataModel model);

        DataResponse<SystemGiftTypeModel> GetByCodeId(string code_id);

        DataResponse<SystemGiftTypeModel> GetByID(int id);

        DataResponse<SystemGiftTypeModel> Updated(SystemGiftTypeModel model);

        DataResponse<SystemGiftType_DataModel> MultipleSelectedGift(SystemGiftType_DataModel model, int id);
    }
}
