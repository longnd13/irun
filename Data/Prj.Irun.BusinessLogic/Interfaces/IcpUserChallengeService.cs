﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpUserChallengeService
    {
        DataResponse<UserChallengeModel> Add(UserChallengeModel model);

        DataResponse<UserChallengeModel> Updated(UserChallengeModel model);

        DataResponse<UserChallenge_DataModel> GetAll(UserChallenge_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<UserChallengeModel> GetByID(int id);
    }
}
