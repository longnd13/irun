﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpUserFollowService
    {
        DataResponse<UserFollowModel> Add(UserFollowModel model);

        DataResponse<UserFollowModel> Updated(UserFollowModel model);

        DataResponse<UserFollow_DataModel> GetAll(UserFollow_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<UserFollowModel> GetByID(int id);

        DataResponse<UserFollow_DataModel> SearchByUsername(UserFollow_DataModel model, string username);
    }
}
