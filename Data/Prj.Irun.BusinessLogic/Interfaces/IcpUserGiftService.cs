﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpUserGiftService
    {
        DataResponse<UserGift_DataModel> GetAll(UserGift_DataModel model);

        DataResponse<UserGift_DataModel> Search(UserGift_DataModel model, string SystemGiftTypeCodeID, string SimilarGiftTypeID);

        DataResponse<bool> Delete(int id);

        DataResponse<UserGiftModel> GetByID(int id);
    }
}
