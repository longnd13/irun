﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpUserGiftStatusService
    {
        DataResponse<UserGiftStatusModel> Add(UserGiftStatusModel model);

        DataResponse<UserGiftStatusModel> Updated(UserGiftStatusModel model);

        DataResponse<UserGiftStatus_DataModel> GetAll(UserGiftStatus_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<UserGiftStatusModel> GetByID(int id);
    }
}
