﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpUserRewardItemActivityService
    {
        DataResponse<UserRewardItemActivity_DataModel> GetAll(UserRewardItemActivity_DataModel model);

        DataResponse<UserRewardItemActivity_DataModel> SearchByUsername(UserRewardItemActivity_DataModel model, string username);

        DataResponse<bool> Delete(int id);

    }
}
