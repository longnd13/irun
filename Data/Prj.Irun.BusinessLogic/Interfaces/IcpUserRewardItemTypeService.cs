﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Interfaces
{
    public interface IcpUserRewardItemTypeService
    {
        DataResponse<UserRewardItemTypeModel> Add(UserRewardItemTypeModel model);

        DataResponse<UserRewardItemTypeModel> Updated(UserRewardItemTypeModel model);

        DataResponse<UserRewardItemType_DataModel> GetAll(UserRewardItemType_DataModel model);

        DataResponse<bool> Delete(int id);

        DataResponse<UserRewardItemTypeModel> GetByID(int id);
    }
}
