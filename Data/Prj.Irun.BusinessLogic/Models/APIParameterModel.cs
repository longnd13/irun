﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
   public class APIParameterModel
    {
        public string CodeID { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Ratio { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class APIParameter_DataModel : CommonModel
    {
        public APIParameter_DataModel()
        {
            List = new List<APIParameterModel>();
            Model = new APIParameterModel();
        }
        public List<APIParameterModel> List { get; set; }
        public APIParameterModel Model { get; set; }
    }
}
