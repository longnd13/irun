﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class AccountActivityModel
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string Code { get; set; }
        public string IP { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class AccountActivity_DataModel : CommonModel
    {
        public AccountActivity_DataModel()
        {
            List = new List<AccountActivityModel>();
            Model = new AccountActivityModel();
        }
        public List<AccountActivityModel> List { get; set; }
        public AccountActivityModel Model { get; set; }
    }
}
