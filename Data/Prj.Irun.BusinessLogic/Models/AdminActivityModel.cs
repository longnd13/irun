﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class AdminActivityModel
    {
        public long Id { get; set; }
        public int AdminId { get; set; }
        public string Code { get; set; }
        public int ObjectID { get; set; }
        public string Value { get; set; }
    }

    public class AdminActivity_DataModel : CommonModel
    {
        public AdminActivity_DataModel()
        {
            List = new List<AdminActivityModel>();
            Model = new AdminActivityModel();
        }
        public List<AdminActivityModel> List { get; set; }
        public AdminActivityModel Model { get; set; }
    }
}
