﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class AdminCPFeatureModel : CommonTypeModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Url { get; set; }
        public int ParentId { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class AdminCPFeature_DataModel : CommonModel
    {
        public AdminCPFeature_DataModel()
        {
            List = new List<AdminCPFeatureModel>();
            Model = new AdminCPFeatureModel();
        }
        public List<AdminCPFeatureModel> List { get; set; }
        public AdminCPFeatureModel Model { get; set; }
    }
}
