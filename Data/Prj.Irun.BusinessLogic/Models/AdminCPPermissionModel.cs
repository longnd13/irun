﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class AdminCPPermissionModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FeatureCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public List<string> Code { get; set; }
    }

    public class AdminCPPermission_DataModel : CommonModel
    {
        public AdminCPPermission_DataModel()
        {
            List = new List<AdminCPPermissionModel>();
            Model = new AdminCPPermissionModel();
        }
        public List<AdminCPPermissionModel> List { get; set; }
        public AdminCPPermissionModel Model { get; set; }
    }
}
