﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class AdminCpModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "The 'UserName' field is required")]
        [StringLength(20, ErrorMessage = "Field 'UserName' must be between 5 and 20 characters", MinimumLength = 5)]
        public string UserName { get; set; }
        public string Password { get; set; }
        [Required(ErrorMessage = "The 'FullName' field is required")]
        [StringLength(100, ErrorMessage = "Field 'FullName' must be between 3 and 100 characters", MinimumLength = 3)]
        public string FullName { get; set; }
        [Required(ErrorMessage = "The 'Email' field is required")]
        [StringLength(30, ErrorMessage = "Field 'Email' must be between 10 and 30 characters", MinimumLength = 10)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        public int IsLevel { get; set; }
        public bool bActived { get; set; }
        public bool bBlocked { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.Password)]
        public string OldPwd { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.Password)]
        public string NewPwd { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Compare(nameof(NewPwd), ErrorMessage = "Passwords don't match.")]
        [DataType(DataType.Password)]
        public string ReTypePwd { get; set; }

    }

    public class AdminCp_DataModel : CommonModel
    {
        public AdminCp_DataModel()
        {
            List = new List<AdminCpModel>();
            Model = new AdminCpModel();
        }
        public List<AdminCpModel> List { get; set; }
        public AdminCpModel Model { get; set; }
    }
}
