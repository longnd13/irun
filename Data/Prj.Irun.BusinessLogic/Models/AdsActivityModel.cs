﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class AdsActivityModel
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string AdsCodeID { get; set; }
        public int Quantity { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class AdsActivity_DataModel : CommonModel
    {
        public AdsActivity_DataModel()
        {
            List = new List<AdsActivityModel>();
            Model = new AdsActivityModel();
        }
        public List<AdsActivityModel> List { get; set; }
        public AdsActivityModel Model { get; set; }
    }
}
