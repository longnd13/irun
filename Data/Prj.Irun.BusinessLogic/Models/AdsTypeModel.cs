﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class AdsTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CodeID { get; set; }
    }

    public class AdsType_DataModel : CommonModel
    {
        public AdsType_DataModel()
        {
            List = new List<AdsTypeModel>();
            Model = new AdsTypeModel();
        }
        public List<AdsTypeModel> List { get; set; }
        public AdsTypeModel Model { get; set; }
    }
}
