﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class ApiConfigModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
    }

    public class ApiConfig_DataModel : CommonModel
    {
        public ApiConfig_DataModel()
        {
            List = new List<ApiConfigModel>();
            Model = new ApiConfigModel();
        }
        public List<ApiConfigModel> List { get; set; }
        public ApiConfigModel Model { get; set; }
    }
}
