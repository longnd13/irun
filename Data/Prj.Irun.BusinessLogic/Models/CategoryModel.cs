﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Category_DataModel : CommonModel
    {
        public Category_DataModel()
        {
            List = new List<CategoryModel>();
            Model = new CategoryModel();
        }
        public List<CategoryModel> List { get; set; }
        public CategoryModel Model { get; set; }
    }
}
