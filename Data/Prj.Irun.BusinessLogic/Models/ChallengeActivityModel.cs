﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class ChallengeActivityModel
    {
        public long Id { get; set; }
        public int UserChallengeID { get; set; }
        public string SavingValue { get; set; }
        public string UserChallengeStatus { get; set; }
    }

    public class ChallengeActivity_DataModel : CommonModel
    {
        public ChallengeActivity_DataModel()
        {
            List = new List<ChallengeActivityModel>();
            Model = new ChallengeActivityModel();
        }
        public List<ChallengeActivityModel> List { get; set; }
        public ChallengeActivityModel Model { get; set; }
    }
}
