﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class ChallengeCompleteCondTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FunctionName { get; set; }
        public string CompleteCondValue { get; set; }
    }

    public class ChallengeCompleteCondType_DataModel : CommonModel
    {
        public ChallengeCompleteCondType_DataModel()
        {
            List = new List<ChallengeCompleteCondTypeModel>();
            Model = new ChallengeCompleteCondTypeModel();
        }
        public List<ChallengeCompleteCondTypeModel> List { get; set; }
        public ChallengeCompleteCondTypeModel Model { get; set; }
    }
}
