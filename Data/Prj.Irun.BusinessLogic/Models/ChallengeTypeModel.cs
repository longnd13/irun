﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class ChallengeTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfCompletion { get; set; }
    }

    public class ChallengeType_DataModel : CommonModel
    {
        public ChallengeType_DataModel()
        {
            List = new List<ChallengeTypeModel>();
            Model = new ChallengeTypeModel();
        }
        public List<ChallengeTypeModel> List { get; set; }
        public ChallengeTypeModel Model { get; set; }
    }
}
