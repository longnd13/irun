﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class ClientConfigModel
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
    }

    public class ClientConfig_DataModel : CommonModel
    {
        public ClientConfig_DataModel()
        {
            List = new List<ClientConfigModel>();
            Model = new ClientConfigModel();
        }
        public List<ClientConfigModel> List { get; set; }
        public ClientConfigModel Model { get; set; }
    }
}
