﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
  public  class ClientInfoModel
    {
        public string ip_address { get; set; }
        public string user_agent { get; set; }
    }
}
