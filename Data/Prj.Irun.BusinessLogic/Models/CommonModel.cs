﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class CommonModel
    {
        public int Count { get; set; }
        public long PageIndex { get; set; }
        public long PageSize { get; set; }
        public long PageTotal { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    public class CommonDateModel
    {
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

    }

    public class CommonTypeModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DeletedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string DeletedBy { get; set; }
    }

    public class GiftOfUserModel
    {
        public string UserID { get; set; }
        public string SystemGiftTypeCodeID { get; set; }
        public string GiftRewardTypeID { get; set; }
        public int Quantity { get; set; }

    }


    public class OpenBasicGiftModel
    {
        public int UserGiftID { get; set; }
    }

    public class DailyGiftUserModel
    {
       


    }

    public class ResponseDailyGiftUserModel
    {
        public ResponseDailyGiftUserModel()
        {
            UserGift = new List<UserGiftModel>();
        }

        public string CreatedDateString { get; set; }
        public string DescDailyGift { get; set; }
        public int Count { get; set; }
        public int DailyGiftID { get; set; }
        public List<UserGiftModel> UserGift { get; set; }
    }

    public class OpenDailyGiftModel
    {
       public int UserGiftID { get; set; }


    }

    // tích lũy

    public class CumulativeGiftUserModel
    {



    }

    public class ResponseCumulativeGiftUserModel
    {
        public ResponseCumulativeGiftUserModel()
        {
            
        }
        public int ID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public int Count { get; set; }
      
    }

    public class OpenCumulativeGiftModel
    {
        public int UserGiftID { get; set; }
    }
}
