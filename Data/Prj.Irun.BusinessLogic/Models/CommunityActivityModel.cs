﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class CommunityActivityModel
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string Code { get; set; }
        public int ObjectID { get; set; }
    }

    public class CommunityActivity_DataModel : CommonModel
    {
        public CommunityActivity_DataModel()
        {
            List = new List<CommunityActivityModel>();
            Model = new CommunityActivityModel();
        }
        public List<CommunityActivityModel> List { get; set; }
        public CommunityActivityModel Model { get; set; }
    }
}
