﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class DistanceActivityModel
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int Distance { get; set; }
    }

    public class DistanceActivity_DataModel : CommonModel
    {
        public DistanceActivity_DataModel()
        {
            List = new List<DistanceActivityModel>();
            Model = new DistanceActivityModel();
        }
        public List<DistanceActivityModel> List { get; set; }
        public DistanceActivityModel Model { get; set; }
    }
}
