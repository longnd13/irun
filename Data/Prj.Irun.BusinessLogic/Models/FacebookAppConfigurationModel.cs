﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
 public   class FacebookAppConfigurationModel
    {
        public string graph_version { get; set; }
        public string client_id { get; set; }
        public string secret_key { get; set; }
        public string app_token { get; set; }
        public string scopes { get; set; }
        public string redirect_uri { get; set; }
    }
}
