﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class GiftActivityModel
    {
        public long Id { get; set; }
        public int UserGiftID { get; set; }
        public string SavingValue { get; set; }
        public int UserGiftStatus { get; set; }
    }

    public class GiftActivity_DataModel : CommonModel
    {
        public GiftActivity_DataModel()
        {
            List = new List<GiftActivityModel>();
            Model = new GiftActivityModel();
        }
        public List<GiftActivityModel> List { get; set; }
        public GiftActivityModel Model { get; set; }
    }
}
