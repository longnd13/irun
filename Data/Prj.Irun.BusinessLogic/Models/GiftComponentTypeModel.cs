﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
   public class GiftComponentTypeModel
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string LogicDesc { get; set; }
        public int Reward { get; set; }
        public string GiftRewardTypeCodeID { get; set; }
        public int FunctionName { get; set; }
        public int ObtainingRate { get; set; }
    }

    public class GiftComponentType_DataModel : CommonModel
    {
        public GiftComponentType_DataModel()
        {
            List = new List<GiftComponentTypeModel>();
            Model = new GiftComponentTypeModel();
        }
        public List<GiftComponentTypeModel> List { get; set; }
        public GiftComponentTypeModel Model { get; set; }
    }
}
