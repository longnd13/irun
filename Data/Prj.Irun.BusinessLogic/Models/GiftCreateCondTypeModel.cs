﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class GiftCreateCondTypeModel
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string LogicDesc { get; set; }
        public string FunctionName { get; set; }
        public string CreateCondTypeValue { get; set; }
    }

    public class GiftCreateCondType_DataModel : CommonModel
    {
        public GiftCreateCondType_DataModel()
        {
            List = new List<GiftCreateCondTypeModel>();
            Model = new GiftCreateCondTypeModel();
        }
        public List<GiftCreateCondTypeModel> List { get; set; }
        public GiftCreateCondTypeModel Model { get; set; }
    }
}
