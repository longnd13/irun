﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
   public class GiftOpenCondTypeModel
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string LogicDesc { get; set; }
        public string FunctionName { get; set; }
        public string OpenCondTypeValue { get; set; }
    }

    public class GiftOpenCondType_DataModel : CommonModel
    {
        public GiftOpenCondType_DataModel()
        {
            List = new List<GiftOpenCondTypeModel>();
            Model = new GiftOpenCondTypeModel();
        }
        public List<GiftOpenCondTypeModel> List { get; set; }
        public GiftOpenCondTypeModel Model { get; set; }
    }
}
