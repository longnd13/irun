﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class GiftRewardTypeModel
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class GiftRewardType_DataModel : CommonModel
    {
        public GiftRewardType_DataModel()
        {
            List = new List<GiftRewardTypeModel>();
            Model = new GiftRewardTypeModel();
        }
        public List<GiftRewardTypeModel> List { get; set; }
        public GiftRewardTypeModel Model { get; set; }
    }
}
