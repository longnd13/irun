﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class HashtagModel : CommonTypeModel
    {
        public int Id { get; set; }
        public int UserName { get; set; }
        public string Hashtag { get; set; }
    }

    public class Hashtag_DataModel : CommonModel
    {
        public Hashtag_DataModel()
        {
            List = new List<HashtagModel>();
            Model = new HashtagModel();
        }
        public List<HashtagModel> List { get; set; }
        public HashtagModel Model { get; set; }
    }
}
