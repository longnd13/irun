﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class IgoldActivityModel
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string Code { get; set; }
        public int ObjectID { get; set; }
        public int IGold { get; set; }
    }

    public class IgoldActivity_DataModel : CommonModel
    {
        public IgoldActivity_DataModel()
        {
            List = new List<IgoldActivityModel>();
            Model = new IgoldActivityModel();
        }
        public List<IgoldActivityModel> List { get; set; }
        public IgoldActivityModel Model { get; set; }
    }
}
