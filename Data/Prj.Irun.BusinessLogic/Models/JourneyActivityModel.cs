﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class JourneyActivityModel
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public int JourneyID { get; set; }
        public string Code { get; set; }
    }

    public class JourneyActivity_DataModel : CommonModel
    {
        public JourneyActivity_DataModel()
        {
            List = new List<JourneyActivityModel>();
            Model = new JourneyActivityModel();
        }
        public List<JourneyActivityModel> List { get; set; }
        public JourneyActivityModel Model { get; set; }
    }
}
