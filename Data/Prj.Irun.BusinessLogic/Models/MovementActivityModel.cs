﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class MovementActivityModel
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string Movement { get; set; }
    }

    public class MovementActivity_DataModel : CommonModel
    {
        public MovementActivity_DataModel()
        {
            List = new List<MovementActivityModel>();
            Model = new MovementActivityModel();
        }
        public List<MovementActivityModel> List { get; set; }
        public MovementActivityModel Model { get; set; }
    }
}
