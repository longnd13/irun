﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class PostHashtagModel
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int HashtagId { get; set; }
    }

    public class PostHashtag_DataModel : CommonModel
    {
        public PostHashtag_DataModel()
        {
            List = new List<PostHashtagModel>();
            Model = new PostHashtagModel();
        }
        public List<PostHashtagModel> List { get; set; }
        public PostHashtagModel Model { get; set; }
    }
}
