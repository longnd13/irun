﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class PostLikeModel
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int PostId { get; set; }
    }

    public class PostLike_DataModel : CommonModel
    {
        public PostLike_DataModel()
        {
            List = new List<PostLikeModel>();
            Model = new PostLikeModel();
        }
        public List<PostLikeModel> List { get; set; }
        public PostLikeModel Model { get; set; }
    }
}
