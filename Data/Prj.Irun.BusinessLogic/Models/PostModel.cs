﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class PostModel : CommonTypeModel
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public string Content { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class Post_DataModel : CommonModel
    {
        public Post_DataModel()
        {
            List = new List<PostModel>();
            Model = new PostModel();
        }
        public List<PostModel> List { get; set; }
        public PostModel Model { get; set; }
    }
}
