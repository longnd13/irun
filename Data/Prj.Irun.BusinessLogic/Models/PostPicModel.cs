﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class PostPicModel
    {
        public int Id { get; set; }
        public int PostID { get; set; }
        public string LinkImages { get; set; }
        public string LinkImagesList { get; set; }
    }

    public class PostPic_DataModel : CommonModel
    {
        public PostPic_DataModel()
        {
            List = new List<PostPicModel>();
            Model = new PostPicModel();
        }
        public List<PostPicModel> List { get; set; }
        public PostPicModel Model { get; set; }
    }
}
