﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class PostVideoModel
    {
        public int Id { get; set; }
        public int PostID { get; set; }
        public string LinkImages { get; set; }
        public string LinkVideosList { get; set; }
    }

    public class PostVideo_DataModel : CommonModel
    {
        public PostVideo_DataModel()
        {
            List = new List<PostVideoModel>();
            Model = new PostVideoModel();
        }
        public List<PostVideoModel> List { get; set; }
        public PostVideoModel Model { get; set; }
    }
}
