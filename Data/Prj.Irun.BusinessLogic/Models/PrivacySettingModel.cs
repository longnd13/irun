﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class PrivacySettingModel : CommonTypeModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "The 'Name' field is required")]
        [StringLength(30, ErrorMessage = "Field 'Name' must be between 3 and 30 characters", MinimumLength = 3)]
        public string Name { get; set; }
        [Required(ErrorMessage = "The 'Description' field is required")]
        [StringLength(50, ErrorMessage = "Field 'Description' must be between 3 and 50 characters", MinimumLength = 3)]
        public string Description { get; set; }
        public bool Status { get; set; }        
    }

    public class PrivacySetting_DataModel : CommonModel
    {
        public PrivacySetting_DataModel()
        {
            List = new List<PrivacySettingModel>();
            Model = new PrivacySettingModel();
        }
        public List<PrivacySettingModel> List { get; set; }
        public PrivacySettingModel Model { get; set; }
    }
}
