﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
   

    public enum OsType
    {
        NONE = 0,
        ANDROID = 1,
        IOS = 2,
        WEB_BROWSER = 3,
        WEB_CLIENT = 4
    }

    public class RequestBaseModel
    {
        public OsType os_type { get; set; } = OsType.NONE;
        public string device_id { get; set; } = "";
        public ClientInfoModel req_info { get; set; } = new ClientInfoModel();
    }
}
