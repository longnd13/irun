﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class RewardItemCategoryModel
    {
        public int Id { get; set; }
        public int ParentCategoryID { get; set; }
        public string CategoryName { get; set; }
        public bool Status { get; set; }
    }

    public class RewardItemCategory_DataModel : CommonModel
    {
        public RewardItemCategory_DataModel()
        {
            List = new List<RewardItemCategoryModel>();
            Model = new RewardItemCategoryModel();
        }
        public List<RewardItemCategoryModel> List { get; set; }
        public RewardItemCategoryModel Model { get; set; }
    }
}
