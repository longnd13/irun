﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class RewardItemModel : CommonTypeModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string ImgURL { get; set; }
        public string Description { get; set; }
        public string QRCode { get; set; }
        public string BARCode { get; set; }
        public string SerialNumber { get; set; }
        public int Gold { get; set; }
        public int GoldFactorForGame { get; set; }
    }

    public class RewardItem_DataModel : CommonModel
    {
        public RewardItem_DataModel()
        {
            List = new List<RewardItemModel>();
            Model = new RewardItemModel();
        }
        public List<RewardItemModel> List { get; set; }
        public RewardItemModel Model { get; set; }
    }
}
