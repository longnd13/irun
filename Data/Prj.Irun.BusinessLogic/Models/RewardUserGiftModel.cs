﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class RewardUserGiftModel
    {
        public int Id { get; set; }
        public int UserGiftID { get; set; }
        public string GiftRewardTypeCodeID { get; set; }
        public int Reward { get; set; }
    }

    public class RewardUserGift_DataModel : CommonModel
    {
        public RewardUserGift_DataModel()
        {
            List = new List<RewardUserGiftModel>();
            Model = new RewardUserGiftModel();
        }
        public List<RewardUserGiftModel> List { get; set; }
        public RewardUserGiftModel Model { get; set; }
    }
}
