﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class SeasonModel : CommonTypeModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "The 'Name' field is required")]
        [StringLength(30, ErrorMessage = "Field 'Name' must be between 3 and 30 characters", MinimumLength = 3)]
        public string Name { get; set; }
        public int RankingTypeID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "The 'StartDate' field is required")]
        public DateTime? StartDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "The 'EndDate' field is required")]
        public DateTime? EndDate { get; set; }
        public bool Status { get; set; }
    }

    public class Season_DataModel : CommonModel
    {
        public Season_DataModel()
        {
            List = new List<SeasonModel>();
            Model = new SeasonModel();
        }
        public List<SeasonModel> List { get; set; }
        public SeasonModel Model { get; set; }
    }
}
