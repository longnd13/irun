﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class SeasonRankingTypeModel : CommonTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required(ErrorMessage = "The 'Description' field is required")]
        [StringLength(100, ErrorMessage = "Field 'Description' must be between 3 and 100 characters", MinimumLength = 3)]
        public string Description { get; set; }
        [Required(ErrorMessage = "The 'FunctionName' field is required")]
        [StringLength(30, ErrorMessage = "Field 'FunctionName' must be between 3 and 30 characters", MinimumLength = 3)]
        public string FunctionName { get; set; }
    }

    public class SeasonRankingType_DataModel : CommonModel
    {
        public SeasonRankingType_DataModel()
        {
            List = new List<SeasonRankingTypeModel>();
            Model = new SeasonRankingTypeModel();
        }
        public List<SeasonRankingTypeModel> List { get; set; }
        public SeasonRankingTypeModel Model { get; set; }
    }
}
