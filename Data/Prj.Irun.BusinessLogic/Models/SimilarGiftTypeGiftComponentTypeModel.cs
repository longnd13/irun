﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class SimilarGiftTypeGiftComponentTypeModel
    {
        public int Id { get; set; }
        public string SimilarGiftTypeCodeID { get; set; }
        public string GiftComponentTypeCodeID { get; set; }
    }

    public class SimilarGiftTypeGiftComponentType_DataModel : CommonModel
    {
        public SimilarGiftTypeGiftComponentType_DataModel()
        {
            List = new List<SimilarGiftTypeGiftComponentTypeModel>();
            Model = new SimilarGiftTypeGiftComponentTypeModel();
        }
        public List<SimilarGiftTypeGiftComponentTypeModel> List { get; set; }
        public SimilarGiftTypeGiftComponentTypeModel Model { get; set; }
    }
}
