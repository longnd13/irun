﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class SimilarGiftTypeGiftCreateCondTypeModel
    {
        public int Id { get; set; }
        public int SimilarGiftTypeID { get; set; }
        public int GiftCreateCondTypeID { get; set; }
    }

    public class SimilarGiftTypeGiftCreateCondType_DataModel : CommonModel
    {
        public SimilarGiftTypeGiftCreateCondType_DataModel()
        {
            List = new List<SimilarGiftTypeGiftCreateCondTypeModel>();
            Model = new SimilarGiftTypeGiftCreateCondTypeModel();
        }
        public List<SimilarGiftTypeGiftCreateCondTypeModel> List { get; set; }
        public SimilarGiftTypeGiftCreateCondTypeModel Model { get; set; }
    }
}
