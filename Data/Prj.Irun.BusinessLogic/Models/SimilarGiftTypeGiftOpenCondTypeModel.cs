﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class SimilarGiftTypeGiftOpenCondTypeModel
    {
        public int Id { get; set; }
        public int SimilarGiftTypeID { get; set; }
        public int GiftOpenCondTypeID { get; set; }
    }

    public class SimilarGiftTypeGiftOpenCondType_DataModel : CommonModel
    {
        public SimilarGiftTypeGiftOpenCondType_DataModel()
        {
            List = new List<SimilarGiftTypeGiftOpenCondTypeModel>();
            Model = new SimilarGiftTypeGiftOpenCondTypeModel();
        }
        public List<SimilarGiftTypeGiftOpenCondTypeModel> List { get; set; }
        public SimilarGiftTypeGiftOpenCondTypeModel Model { get; set; }
    }
}
