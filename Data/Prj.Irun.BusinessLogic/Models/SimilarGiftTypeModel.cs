﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
  public class SimilarGiftTypeModel
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string SystemGiftTypeCodeID { get; set; }
        public string GiftOpenCondTypeCodeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        [Range(1, 100, ErrorMessage = "CreatedRate from 1 to maximum is 100.")]
        public int CreatedRate { get; set; }
        public string SystemGiftTypeCode { get; set; }
        public string GiftOpenCondTypeCode { get; set; }
        public List<string> GiftComponentType { get; set; }
    }

    public class SimilarGiftType_DataModel : CommonModel
    {
        public SimilarGiftType_DataModel()
        {
            List = new List<SimilarGiftTypeModel>();
            Model = new SimilarGiftTypeModel();
        }
        public List<SimilarGiftTypeModel> List { get; set; }
        public SimilarGiftTypeModel Model { get; set; }
    }
}
