﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
  public  class SocialUserInfoModel
    {
        public SocialUserInfoModel()
        {

        }
        public SocialUserInfoModel(FacebookUserModel fbUser)
        {

            provider_name = "FACEBOOK";
            social_id = fbUser.id;
            name = fbUser.name;
            first_name = fbUser.first_name;
            last_name = fbUser.last_name;
            email = fbUser.email;
            picture_url = $"https://graph.facebook.com/{fbUser.id}/picture?type=large&redirect=true&width=320&height=320";
        }

        public string access_token { get; set; }
        public string provider_name { get; set; }
        public string social_id { get; set; }
        public string name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string picture_url { get; set; }
    }
}
