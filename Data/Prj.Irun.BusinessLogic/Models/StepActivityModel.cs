﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class StepActivityModel
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int Step { get; set; }
    }

    public class StepActivity_DataModel : CommonModel
    {
        public StepActivity_DataModel()
        {
            List = new List<StepActivityModel>();
            Model = new StepActivityModel();
        }
        public List<StepActivityModel> List { get; set; }
        public StepActivityModel Model { get; set; }
    }
}
