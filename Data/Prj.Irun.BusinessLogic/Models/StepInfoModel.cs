﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class StepInfoModel
    {
        public StepInfoModel()
        { 
            DataGiftOfUser = new List<GiftOfUserModel>();
        }

        public long Gold { get; set; }
        public int TotalSteps { get; set; }
        public int TotalGift { get; set; }
        public decimal SurplusDistance { get; set; }
        public List<GiftOfUserModel> DataGiftOfUser { get; set; }
    }




}
