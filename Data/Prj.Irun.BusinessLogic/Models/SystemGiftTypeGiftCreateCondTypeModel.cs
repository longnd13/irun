﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class SystemGiftTypeGiftCreateCondTypeModel
    {
        public int Id { get; set; }
        public string SystemGiftTypeID { get; set; }
        public int GiftCreateCondTypeID { get; set; }
    }

    public class SystemGiftTypeGiftCreateCondType_DataModel : CommonModel
    {
        public SystemGiftTypeGiftCreateCondType_DataModel()
        {
            List = new List<SystemGiftTypeGiftCreateCondTypeModel>();
            Model = new SystemGiftTypeGiftCreateCondTypeModel();
        }
        public List<SystemGiftTypeGiftCreateCondTypeModel> List { get; set; }
        public SystemGiftTypeGiftCreateCondTypeModel Model { get; set; }
    }
}
