﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{


    public class SystemGiftTypeModel
    {
        public int Id { get; set; }
        public string GiftCreateCondTypeCodeID { get; set; }
        public string Name { get; set; }
        public string CodeID { get; set; }
        public List<string> GiftCreateCondTypeId { get; set; }
    }

    public class SystemGiftType_DataModel : CommonModel
    {
        public SystemGiftType_DataModel()
        {
            List = new List<SystemGiftTypeModel>();
            Model = new SystemGiftTypeModel();
        }
        public List<SystemGiftTypeModel> List { get; set; }
        public SystemGiftTypeModel Model { get; set; }

    }
}
