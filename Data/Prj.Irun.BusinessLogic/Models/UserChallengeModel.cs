﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class UserChallengeModel
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int ChallengeTypeID { get; set; }
        public string TotalSavingValue { get; set; }
        public int Status { get; set; }
    }

    public class UserChallenge_DataModel : CommonModel
    {
        public UserChallenge_DataModel()
        {
            List = new List<UserChallengeModel>();
            Model = new UserChallengeModel();
        }
        public List<UserChallengeModel> List { get; set; }
        public UserChallengeModel Model { get; set; }
    }
}
