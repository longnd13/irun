﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class UserFollowModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FollowedUserID { get; set; }
    }

    public class UserFollow_DataModel : CommonModel
    {
        public UserFollow_DataModel()
        {
            List = new List<UserFollowModel>();
            Model = new UserFollowModel();
        }
        public List<UserFollowModel> List { get; set; }
        public UserFollowModel Model { get; set; }
    }
}
