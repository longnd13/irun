﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class UserGiftModel
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public string SystemGiftTypeCodeID { get; set; }
        public string SimilarGiftTypeID { get; set; }
        public string TotalSavingValue { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateString { get; set; }
    }

    public class UserGift_DataModel : CommonModel
    {
        public UserGift_DataModel()
        {
            List = new List<UserGiftModel>();
            Model = new UserGiftModel();
        }
        public List<UserGiftModel> List { get; set; }
        public UserGiftModel Model { get; set; }
    }
}
