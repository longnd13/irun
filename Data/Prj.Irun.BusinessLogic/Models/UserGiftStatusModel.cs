﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class UserGiftStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string StatusName { get; set; }
        public string Description { get; set; }
    }

    public class UserGiftStatus_DataModel : CommonModel
    {
        public UserGiftStatus_DataModel()
        {
            List = new List<UserGiftStatusModel>();
            Model = new UserGiftStatusModel();
        }
        public List<UserGiftStatusModel> List { get; set; }
        public UserGiftStatusModel Model { get; set; }
    }
}
