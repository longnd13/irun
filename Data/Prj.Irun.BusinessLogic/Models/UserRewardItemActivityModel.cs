﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class UserRewardItemActivityModel
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public int RewardItemID { get; set; }
        public int UserRewardItemTypeID { get; set; }
        public DateTime? ObtainedDateTime { get; set; }
        public DateTime? ExpiredDateTime { get; set; }
        public string QRCode { get; set; }
        public string BarCode { get; set; }
        public string SerialCode { get; set; }
    }

    public class UserRewardItemActivity_DataModel : CommonModel
    {
        public UserRewardItemActivity_DataModel()
        {
            List = new List<UserRewardItemActivityModel>();
            Model = new UserRewardItemActivityModel();
        }
        public List<UserRewardItemActivityModel> List { get; set; }
        public UserRewardItemActivityModel Model { get; set; }
    }
}
