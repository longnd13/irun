﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.Models
{
    public class UserRewardItemTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }

    public class UserRewardItemType_DataModel : CommonModel
    {
        public UserRewardItemType_DataModel()
        {
            List = new List<UserRewardItemTypeModel>();
            Model = new UserRewardItemTypeModel();
        }
        public List<UserRewardItemTypeModel> List { get; set; }
        public UserRewardItemTypeModel Model { get; set; }
    }
}
