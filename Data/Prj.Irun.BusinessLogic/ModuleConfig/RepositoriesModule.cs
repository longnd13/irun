﻿using Ninject.Modules;
using Prj.Irun.Repositories.Implementations;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.ModuleConfig
{
   public class RepositoriesModule : NinjectModule
    {
        public override void Load()
        {
          
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<IUnitOfWorkProvider>().To<UnitOfWorkProvider>();

            Bind<IAccountActivityRepository>().To<AccountActivityRepository>();
            Bind<ItestRepository>().To<testRepository>();
            Bind<IcpPrivacySettingRepository>().To<cpPrivacySettingRepository>();       
            Bind<IcpSeasonRepository>().To<cpSeasonRepository>();
            Bind<IcpPostRepository>().To<cpPostRepository>();
            Bind<IcpSeasonRankingTypeRepository>().To<cpSeasonRankingTypeRepository>();
            Bind<IcpAdminCpRepository>().To<cpAdminCpRepository>();
            Bind<IcpAccountFeatureRepository>().To<cpAdminCPFeatureRepository>();
            Bind<IcpApiConfigRepository>().To<cpApiConfigRepository>();        
            Bind<IcpSystemGiftTypeRepository>().To<cpSystemGiftTypeRepository>();        
            Bind<IcpGiftCreateCondTypeRepository>().To<cpGiftCreateCondTypeRepository>();
            Bind<IcpSimilarGiftTypeGiftOpenCondTypeRepository>().To<cpSimilarGiftTypeGiftOpenCondTypeRepository>();               
            Bind<IcpAccountInfoRepository>().To<cpAccountInfoRepository>();
            Bind<IcpDistanceActivityRepository>().To<cpDistanceActivityRepository>();
            Bind<IcpUserGiftStatusRepository>().To<cpUserGiftStatusRepository>();
            Bind<IcpCategoryRepository>().To<cpCategoryRepository>();
            Bind<IcpChallengeTypeRepository>().To<cpChallengeTypeRepository>();
            Bind<IcpStepActivityRepository>().To<cpStepActivityRepository>();
            Bind<IcpSystemGiftTypeGiftCreateCondTypeRepository>().To<cpSystemGiftTypeGiftCreateCondTypeRepository>();
            Bind<IcpClientConfigRepository>().To<cpClientConfigRepository>();


            Bind<IAPIParameterRepository>().To<APIParameterRepository>();       
            Bind<IAccountRepository>().To<AccountRepository>();
            Bind<IMainCoreRepository>().To<MainCoreRepository>();
            Bind<ISystemGiftTypeRepository>().To<SystemGiftTypeRepository>();          
            Bind<IGiftCreateCondTypeRepository>().To<GiftCreateCondTypeRepository>();
            Bind<ISimilarGiftTypeRepository>().To<SimilarGiftTypeRepository>();
            Bind<IGiftComponentTypeRepository>().To<GiftComponentTypeRepository>();
            Bind<IcpGiftRewardTypeRepository>().To<cpGiftRewardTypeRepository>();
            Bind<IcpGiftComponentTypeRepository>().To<cpGiftComponentTypeRepository>();
            Bind<IcpUserGiftRepository>().To<cpUserGiftRepository>();
            Bind<IcpSimilarGiftTypeRepository>().To<cpSimilarGiftTypeRepository>();
            Bind<IcpRewardUserGiftRepository>().To<cpRewardUserGiftRepository>();
            Bind<IcpAPIParameterRepository>().To<cpAPIParameterRepository>();
            Bind<IcpAccountActivityRepository>().To<cpAccountActivityRepository>();

            Bind<IcpAdminActivityRepository>().To<cpAdminActivityRepository>();
            Bind<IcpAdsActivityRepository>().To<cpAdsActivityRepository>();
            Bind<IcpCommunityActivityRepository>().To<cpCommunityActivityRepository>();
            Bind<IcpChallengeActivityRepository>().To<cpChallengeActivityRepository>();
            Bind<IcpGiftActivityRepository>().To<cpGiftActivityRepository>();
            Bind<IcpIgoldActivityRepository>().To<cpIgoldActivityRepository>();
            Bind<IcpJourneyActivityRepository>().To<cpJourneyActivityRepository>();
            Bind<IcpUserRewardItemActivityRepository>().To<cpUserRewardItemActivityRepository>();
            Bind<IcpAdsTypeRepository>().To<cpAdsTypeRepository>();
            Bind<IcpUserChallengeRepository>().To<cpUserChallengeRepository>();
            Bind<IcpMovementActivityRepository>().To<cpMovementActivityRepository>();
            Bind<IcpRewardItemRepository>().To<cpRewardItemRepository>();
            Bind<IcpUserRewardItemTypeRepository>().To<cpUserRewardItemTypeRepository>();
            Bind<IcpChallengeCompleteCondTypeRepository>().To<cpChallengeCompleteCondTypeRepository>();
            Bind<IcpGiftOpenCondTypeRepository>().To<cpGiftOpenCondTypeRepository>();
            Bind<IcpAdminCPPermissionRepository>().To<cpAdminCPPermissionRepository>();
            Bind<IcpRewardItemCategoryRepository>().To<cpRewardItemCategoryRepository>();
            Bind<IcpHashtagRepository>().To<cpHashtagRepository>();
            Bind<IcpPostHashtagRepository>().To<cpPostHashtagRepository>();
            Bind<IcpPostLikeRepository>().To<cpPostLikeRepository>();
            Bind<IcpPostPicRepository>().To<cpPostPicRepository>();
            Bind<IcpPostVideoRepository>().To<cpPostVideoRepository>();
            Bind<IcpUserFollowRepository>().To<cpUserFollowRepository>();
            Bind<IcpSimilarGiftTypeGiftComponentTypeRepository>().To<cpSimilarGiftTypeGiftComponentTypeRepository>();

            // api
            Bind<IapiClientConfigRepository>().To<apiClientConfigRepository>();
            Bind<IapiUserGiftRepository>().To<apiUserGiftRepository>();
            Bind<IapiStepsRepository>().To<apiStepsRepository>();
            Bind<IapiDistanceRepository>().To<apiDistanceRepository>();



        }
    }
}
