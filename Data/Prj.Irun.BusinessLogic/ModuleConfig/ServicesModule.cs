﻿using Ninject.Modules;
using Prj.Irun.BusinessLogic.Implementations;
using Prj.Irun.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.BusinessLogic.ModuleConfig
{
  public  class ServicesModule : NinjectModule
    {
        public override void Load()
        {
          
            Bind<ItestService>().To<testService>();         
            Bind<IcpPrivacySettingService>().To<cpPrivacySettingService>();
            Bind<IcpSeasonService>().To<cpSeasonService>();
            Bind<IcpPostService>().To<cpPostService>();
            Bind<IcpSeasonRankingTypeService>().To<cpSeasonRankingTypeService>();
            Bind<IcpAdminCpService>().To<cpAdminCpService>();
            Bind<IAdminCPFeatureService>().To<cpAdminCPFeatureService>();
            Bind<IcpApiConfigService>().To<cpApiConfigService>();
            Bind<IcpClientConfigService>().To<cpClientConfigService>();
            Bind<IcpSystemGiftTypeService>().To<cpSystemGiftTypeService>();
            Bind<ISystemGiftTypeService>().To<SystemGiftTypeService>();
            Bind<IcpGiftCreateCondTypeService>().To<cpGiftCreateCondTypeService>();
            Bind<IcpSimilarGiftTypeGiftOpenCondTypeService>().To<cpSimilarGiftTypeGiftOpenCondTypeService>();
            Bind<IcpSystemGiftTypeGiftCreateCondTypeService>().To<cpSystemGiftTypeGiftCreateCondTypeService>();
          
            Bind<IcpAccountInfoService>().To<cpAccountInfoService>();
            Bind<IcpDistanceActivityService>().To<cpDistanceActivityService>();
            Bind<IcpUserGiftStatusService>().To<cpUserGiftStatusService>();
            Bind<IcpCategoryService>().To<cpCategoryService>();
            Bind<IcpChallengeTypeService>().To<cpChallengeTypeService>();
            Bind<IcpStepActivityService>().To<cpStepActivityService>();

            Bind<IAPIParameterService>().To<APIParameterService>();     
            Bind<IAccountService>().To<AccountService>();
            Bind<IMainCoreService>().To<MainCoreService>();
            Bind<ICommonService>().To<CommonService>();
            Bind<IGiftCreateCondTypeService>().To<GiftCreateCondTypeService>(); 
            Bind<ISimilarGiftTypeService>().To<SimilarGiftTypeService>();
            Bind<ISystemGiftTypeService>().To<SystemGiftTypeService>();
            Bind<IcpGiftRewardTypeService>().To<cpGiftRewardTypeService>();
            Bind<IcpGiftComponentTypeService>().To<cpGiftComponentTypeService>();
            Bind<IcpUserGiftService>().To<cpUserGiftService>();
            Bind<IcpSimilarGiftTypeService>().To<cpSimilarGiftTypeService>();
            Bind<IcpRewardUserGiftService>().To<cpRewardUserGiftService>();
            Bind<IcpAPIParameterService>().To<cpAPIParameterService>();
            Bind<IcpAccountActivityService>().To<cpAccountActivityService>();

            Bind<IcpAdminActivityService>().To<cpAdminActivityService>();
            Bind<IcpAdsActivityService>().To<cpAdsActivityService>();
            Bind<IcpCommunityActivityService>().To<cpCommunityActivityService>();
            Bind<IcpChallengeActivityService>().To<cpChallengeActivityService>();
            Bind<IcpGiftActivityService>().To<cpGiftActivityService>();
            Bind<IcpIgoldActivityService>().To<cpIgoldActivityService>();
            Bind<IcpJourneyActivityService>().To<cpJourneyActivityService>();
            Bind<IcpUserRewardItemActivityService>().To<cpUserRewardItemActivityService>();
            Bind<IcpAdsTypeService>().To<cpAdsTypeService>();
            Bind<IcpUserChallengeService>().To<cpUserChallengeService>();
            Bind<IcpMovementActivityService>().To<cpMovementActivityService>();
            Bind<IcpRewardItemService>().To<cpRewardItemService>();
            Bind<IcpUserRewardItemTypeService>().To<cpUserRewardItemTypeService>();
            Bind<IcpChallengeCompleteCondTypeService>().To<cpChallengeCompleteCondTypeService>();
            Bind<IcpGiftOpenCondTypeService>().To<cpGiftOpenCondTypeService>();
            Bind<IcpAdminCPPermissionService>().To<cpAdminCPPermissionService>();
            Bind<IcpRewardItemCategoryService>().To<cpRewardItemCategoryService>();
            Bind<IcpHashtagService>().To<cpHashtagService>();
            Bind<IcpPostHashtagService>().To<cpPostHashtagService>();
            Bind<IcpPostLikeService>().To<cpPostLikeService>();
            Bind<IcpPostPicService>().To<cpPostPicService>();
            Bind<IcpPostVideoService>().To<cpPostVideoService>();
            Bind<IcpUserFollowService>().To<cpUserFollowService>();
            Bind<IcpSimilarGiftTypeGiftComponentTypeService>().To<cpSimilarGiftTypeGiftComponentTypeService>();

            // api
            Bind<IapiClientConfigService>().To<apiClientConfigService>();
            Bind<IapiGiftService>().To<apiGiftService>();

            Bind<IapiStepsService>().To<apiStepsService>();
            Bind<IapiDistanceService>().To<apiDistanceService>();

        }
    }
}
