﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("APIParameter")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class APIParameterEntity
    {
        public string CodeID { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Ratio { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class APIParameter_DataEntity : CommonEntity
    {
        public APIParameter_DataEntity()
        {
            List = new List<APIParameterEntity>();
            Entity = new APIParameterEntity();
        }

        public List<APIParameterEntity> List { get; set; }
        public APIParameterEntity Entity { get; set; }
    }
}
