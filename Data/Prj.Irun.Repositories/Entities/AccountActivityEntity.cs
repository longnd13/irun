﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("AccountActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public  class AccountActivityEntity
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string Code { get; set; }
        public string IP { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class AccountActivity_DataEntity : CommonEntity
    {
        public AccountActivity_DataEntity()
        {
            List = new List<AccountActivityEntity>();
            Entity = new AccountActivityEntity();
        }

        public List<AccountActivityEntity> List { get; set; }
        public AccountActivityEntity Entity { get; set; }
    }
}
