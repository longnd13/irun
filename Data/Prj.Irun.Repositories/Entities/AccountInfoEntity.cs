﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("AccountInfo")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class AccountInfoEntity : CommonDateEntity
    {
        public long Id { get; set; }
        public long Gold { get; set; }
        public long TotalDistance { get; set; }
        public long TotalSteps { get; set; }
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public string FullName { get; set; }
        public string FullNameSign { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public DateTime? Birthday { get; set; }
        public int Gender { get; set; }
        public int Age { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public string AvatarUrl { get; set; }
        public string AccessToken { get; set; }
        public DateTime AccessTokenExpired { get; set; }
        public bool IsVerified { get; set; }
        public bool IsLocked { get; set; }
        public int IsType { get; set; }
        public bool IsOTP { get; set; }
        public string CurrentLocation { get; set; }
    }

    public class AccountInfo_DataEntity : CommonEntity
    {
        public AccountInfo_DataEntity()
        {
            List = new List<AccountInfoEntity>();
            Entity = new AccountInfoEntity();
        }

        public List<AccountInfoEntity> List { get; set; }
        public AccountInfoEntity Entity { get; set; }
    }
}
