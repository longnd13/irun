﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("AdminActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class AdminActivityEntity
    {
        public long Id { get; set; }
        public int AdminId { get; set; }
        public string Code { get; set; }
        public int ObjectID { get; set; }
        public string Value { get; set; }
    }

    public class AdminActivity_DataEntity : CommonEntity
    {
        public AdminActivity_DataEntity()
        {
            List = new List<AdminActivityEntity>();
            Entity = new AdminActivityEntity();
        }

        public List<AdminActivityEntity> List { get; set; }
        public AdminActivityEntity Entity { get; set; }
    }
}
