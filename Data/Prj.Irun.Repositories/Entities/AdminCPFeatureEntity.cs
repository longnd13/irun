﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("AdminCPFeature")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class AdminCPFeatureEntity : CommonTypeEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Url { get; set; }
        public int ParentId { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class AdminCPFeature_DataEntity : CommonEntity
    {
        public AdminCPFeature_DataEntity()
        {
            List = new List<AdminCPFeatureEntity>();
            Entity = new AdminCPFeatureEntity();
        }

        public List<AdminCPFeatureEntity> List { get; set; }
        public AdminCPFeatureEntity Entity { get; set; }
    }
}
