﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("AdminCPPermission")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class AdminCPPermissionEntity
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FeatureCode { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class AdminCPPermission_DataEntity : CommonEntity
    {
        public AdminCPPermission_DataEntity()
        {
            List = new List<AdminCPPermissionEntity>();
            Entity = new AdminCPPermissionEntity();
        }

        public List<AdminCPPermissionEntity> List { get; set; }
        public AdminCPPermissionEntity Entity { get; set; }
    }
}
