﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("AdminCp")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class AdminCpEntity
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int IsLevel { get; set; }
        public bool bActived { get; set; }
        public bool bBlocked { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }

    public class AdminCp_DataEntity : CommonEntity
    {
        public AdminCp_DataEntity()
        {
            List = new List<AdminCpEntity>();
            Entity = new AdminCpEntity();
        }

        public List<AdminCpEntity> List { get; set; }
        public AdminCpEntity Entity { get; set; }
    }
}
