﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("AdsActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class AdsActivityEntity
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string AdsCodeID { get; set; }
        public int Quantity { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class AdsActivity_DataEntity : CommonEntity
    {
        public AdsActivity_DataEntity()
        {
            List = new List<AdsActivityEntity>();
            Entity = new AdsActivityEntity();
        }

        public List<AdsActivityEntity> List { get; set; }
        public AdsActivityEntity Entity { get; set; }
    }
}
