﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("AdsType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class AdsTypeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CodeID { get; set; }
    }

    public class AdsType_DataEntity : CommonEntity
    {
        public AdsType_DataEntity()
        {
            List = new List<AdsTypeEntity>();
            Entity = new AdsTypeEntity();
        }

        public List<AdsTypeEntity> List { get; set; }
        public AdsTypeEntity Entity { get; set; }
    }
}
