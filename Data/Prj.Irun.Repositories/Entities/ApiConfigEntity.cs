﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("APIConfig")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class ApiConfigEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
    }

    public class ApiConfig_DataEntity : CommonEntity
    {
        public ApiConfig_DataEntity()
        {
            List = new List<ApiConfigEntity>();
            Entity = new ApiConfigEntity();
        }

        public List<ApiConfigEntity> List { get; set; }
        public ApiConfigEntity Entity { get; set; }
    }
}
