﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("Category")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class CategoryEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Category_DataEntity : CommonEntity
    {
        public Category_DataEntity()
        {
            List = new List<CategoryEntity>();
            Entity = new CategoryEntity();
        }

        public List<CategoryEntity> List { get; set; }
        public CategoryEntity Entity { get; set; }
    }
}
