﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("ChallengeActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class ChallengeActivityEntity
    {
        public long Id { get; set; }
        public int UserChallengeID { get; set; }
        public string SavingValue { get; set; }
        public string UserChallengeStatus { get; set; }
    }

    public class ChallengeActivity_DataEntity : CommonEntity
    {
        public ChallengeActivity_DataEntity()
        {
            List = new List<ChallengeActivityEntity>();
            Entity = new ChallengeActivityEntity();
        }

        public List<ChallengeActivityEntity> List { get; set; }
        public ChallengeActivityEntity Entity { get; set; }
    }
}
