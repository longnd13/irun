﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("ChallengeCompleteCondType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class ChallengeCompleteCondTypeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FunctionName { get; set; }
        public string CompleteCondValue { get; set; }
    }

    public class ChallengeCompleteCondType_DataEntity : CommonEntity
    {
        public ChallengeCompleteCondType_DataEntity()
        {
            List = new List<ChallengeCompleteCondTypeEntity>();
            Entity = new ChallengeCompleteCondTypeEntity();
        }

        public List<ChallengeCompleteCondTypeEntity> List { get; set; }
        public ChallengeCompleteCondTypeEntity Entity { get; set; }
    }
}
