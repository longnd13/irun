﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("ChallengeType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class ChallengeTypeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfCompletion { get; set; }
    }

    public class ChallengeType_DataEntity : CommonEntity
    {
        public ChallengeType_DataEntity()
        {
            List = new List<ChallengeTypeEntity>();
            Entity = new ChallengeTypeEntity();
        }

        public List<ChallengeTypeEntity> List { get; set; }
        public ChallengeTypeEntity Entity { get; set; }
    }
}
