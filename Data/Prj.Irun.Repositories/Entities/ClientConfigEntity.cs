﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("ClientConfig")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class ClientConfigEntity
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
    }

    public class ClientConfig_DataEntity : CommonEntity
    {
        public ClientConfig_DataEntity()
        {
            List = new List<ClientConfigEntity>();
            Entity = new ClientConfigEntity();
        }

        public List<ClientConfigEntity> List { get; set; }
        public ClientConfigEntity Entity { get; set; }
    }
}
