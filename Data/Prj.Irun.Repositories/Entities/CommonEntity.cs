﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    public class CommonEntity
    {
        public int Count { get; set; }
        public long PageIndex { get; set; }
        public long PageSize { get; set; }
        public long PageTotal { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    public class CommonDateEntity
    {
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class CommonTypeEntity
    {
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string DeletedBy { get; set; }
    }
}
