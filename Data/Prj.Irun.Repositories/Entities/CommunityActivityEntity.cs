﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("CommunityActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class CommunityActivityEntity
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string Code { get; set; }
        public int ObjectID { get; set; }
    }

    public class CommunityActivity_DataEntity : CommonEntity
    {
        public CommunityActivity_DataEntity()
        {
            List = new List<CommunityActivityEntity>();
            Entity = new CommunityActivityEntity();
        }

        public List<CommunityActivityEntity> List { get; set; }
        public CommunityActivityEntity Entity { get; set; }
    }
}
