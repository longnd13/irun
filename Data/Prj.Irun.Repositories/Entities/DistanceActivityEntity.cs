﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("DistanceActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class DistanceActivityEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int Distance { get; set; }
    }

    public class DistanceActivity_DataEntity : CommonEntity
    {
        public DistanceActivity_DataEntity()
        {
            List = new List<DistanceActivityEntity>();
            Entity = new DistanceActivityEntity();
        }

        public List<DistanceActivityEntity> List { get; set; }
        public DistanceActivityEntity Entity { get; set; }
    }
}
