﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("GiftActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class GiftActivityEntity
    {
        public long Id { get; set; }
        public int UserGiftID { get; set; }
        public string SavingValue { get; set; }
        public int UserGiftStatus { get; set; }
    }

    public class GiftActivity_DataEntity : CommonEntity
    {
        public GiftActivity_DataEntity()
        {
            List = new List<GiftActivityEntity>();
            Entity = new GiftActivityEntity();
        }

        public List<GiftActivityEntity> List { get; set; }
        public GiftActivityEntity Entity { get; set; }
    }
}
