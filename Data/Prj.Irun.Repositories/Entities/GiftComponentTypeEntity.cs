﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("GiftComponentType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class GiftComponentTypeEntity
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string LogicDesc { get; set; }
        public int Reward { get; set; }
        public string GiftRewardTypeCodeID { get; set; }
        public int FunctionName { get; set; }
        public int ObtainingRate { get; set; }
    }

    public class GiftComponentType_DataEntity : CommonEntity
    {
        public GiftComponentType_DataEntity()
        {
            List = new List<GiftComponentTypeEntity>();
            Entity = new GiftComponentTypeEntity();
        }

        public List<GiftComponentTypeEntity> List { get; set; }
        public GiftComponentTypeEntity Entity { get; set; }
    }
}
