﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("GiftCreateCondType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class GiftCreateCondTypeEntity
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string LogicDesc { get; set; }
        public string FunctionName { get; set; }
        public int CreateCondTypeValue { get; set; }
    }

    public class GiftCreateCondType_DataEntity : CommonEntity
    {
        public GiftCreateCondType_DataEntity()
        {
            List = new List<GiftCreateCondTypeEntity>();
            Entity = new GiftCreateCondTypeEntity();
        }

        public List<GiftCreateCondTypeEntity> List { get; set; }
        public GiftCreateCondTypeEntity Entity { get; set; }
    }
}
