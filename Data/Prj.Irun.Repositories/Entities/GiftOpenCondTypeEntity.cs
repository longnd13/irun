﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("GiftOpenCondType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public  class GiftOpenCondTypeEntity
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string LogicDesc { get; set; }
        public string FunctionName { get; set; }
        public int OpenCondTypeValue { get; set; }
    }

    public class GiftOpenCondType_DataEntity : CommonEntity
    {
        public GiftOpenCondType_DataEntity()
        {
            List = new List<GiftOpenCondTypeEntity>();
            Entity = new GiftOpenCondTypeEntity();
        }

        public List<GiftOpenCondTypeEntity> List { get; set; }
        public GiftOpenCondTypeEntity Entity { get; set; }
    }
}
