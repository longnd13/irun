﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("GiftRewardType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class GiftRewardTypeEntity
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class GiftRewardType_DataEntity : CommonEntity
    {
        public GiftRewardType_DataEntity()
        {
            List = new List<GiftRewardTypeEntity>();
            Entity = new GiftRewardTypeEntity();
        }

        public List<GiftRewardTypeEntity> List { get; set; }
        public GiftRewardTypeEntity Entity { get; set; }
    }
}
