﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("Hashtag")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class HashtagEntity : CommonTypeEntity
    {
        public int Id { get; set; }
        public int UserName { get; set; }
        public string Hashtag { get; set; }
    }

    public class Hashtag_DataEntity : CommonEntity
    {
        public Hashtag_DataEntity()
        {
            List = new List<HashtagEntity>();
            Entity = new HashtagEntity();
        }

        public List<HashtagEntity> List { get; set; }
        public HashtagEntity Entity { get; set; }
    }
}
