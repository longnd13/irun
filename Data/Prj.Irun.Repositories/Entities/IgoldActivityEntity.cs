﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("IgoldActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class IgoldActivityEntity
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public string Code { get; set; }
        public int ObjectID { get; set; }
        public int IGold { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class IgoldActivity_DataEntity : CommonEntity
    {
        public IgoldActivity_DataEntity()
        {
            List = new List<IgoldActivityEntity>();
            Entity = new IgoldActivityEntity();
        }

        public List<IgoldActivityEntity> List { get; set; }
        public IgoldActivityEntity Entity { get; set; }
    }
}
