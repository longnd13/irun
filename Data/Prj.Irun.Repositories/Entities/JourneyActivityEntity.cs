﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("JourneyActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class JourneyActivityEntity
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public int JourneyID { get; set; }
        public string Code { get; set; }
    }

    public class JourneyActivity_DataEntity : CommonEntity
    {
        public JourneyActivity_DataEntity()
        {
            List = new List<JourneyActivityEntity>();
            Entity = new JourneyActivityEntity();
        }

        public List<JourneyActivityEntity> List { get; set; }
        public JourneyActivityEntity Entity { get; set; }
    }
}
