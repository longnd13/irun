﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("LogsGoldAccount")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class LogsGoldAccountEntity
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Action { get; set; }
        public int Type { get; set; }
        public int EventId { get; set; }
        public DateTime? CreatedDate { get; set; }


    }
}
