﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
 public class MessageEntity
    {
        public int code { get; set; }
        public bool success { get; set; }
        public string message { get; set; }
    }
}
