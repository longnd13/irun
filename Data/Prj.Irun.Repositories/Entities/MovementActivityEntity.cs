﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("MovementActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class MovementActivityEntity
    {        
        public long Id { get; set; }
        public string UserID { get; set; }
        public string Movement { get; set; }
    }

    public class MovementActivity_DataEntity : CommonEntity
    {
        public MovementActivity_DataEntity()
        {
            List = new List<MovementActivityEntity>();
            Entity = new MovementActivityEntity();
        }

        public List<MovementActivityEntity> List { get; set; }
        public MovementActivityEntity Entity { get; set; }
    }
}
