﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
   public class OpenCondUserGiftEntity
    {
        public int Id { get; set; }
        public int UserGiftID { get; set; }
        public int OpenCondValue { get; set; }
    }
}
