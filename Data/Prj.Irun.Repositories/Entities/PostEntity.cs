﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("Post")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class PostEntity : CommonTypeEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public string Content { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class Post_DataEntity : CommonEntity
    {
        public Post_DataEntity()
        {
            List = new List<PostEntity>();
            Entity = new PostEntity();
        }

        public List<PostEntity> List { get; set; }
        public PostEntity Entity { get; set; }
    }
}
