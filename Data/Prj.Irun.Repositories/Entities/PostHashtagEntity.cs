﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("PostHashtag")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class PostHashtagEntity
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int HashtagId { get; set; }
    }

    public class PostHashtag_DataEntity : CommonEntity
    {
        public PostHashtag_DataEntity()
        {
            List = new List<PostHashtagEntity>();
            Entity = new PostHashtagEntity();
        }

        public List<PostHashtagEntity> List { get; set; }
        public PostHashtagEntity Entity { get; set; }
    }
}
