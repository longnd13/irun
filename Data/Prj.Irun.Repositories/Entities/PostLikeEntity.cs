﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("PostLike")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class PostLikeEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int PostId { get; set; }
    }

    public class PostLike_DataEntity : CommonEntity
    {
        public PostLike_DataEntity()
        {
            List = new List<PostLikeEntity>();
            Entity = new PostLikeEntity();
        }

        public List<PostLikeEntity> List { get; set; }
        public PostLikeEntity Entity { get; set; }
    }
}
