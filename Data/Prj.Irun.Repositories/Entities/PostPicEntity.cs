﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("PostPic")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class PostPicEntity
    {
        public int Id { get; set; }
        public int PostID { get; set; }
        public string LinkImages { get; set; }
    }

    public class PostPic_DataEntity : CommonEntity
    {
        public PostPic_DataEntity()
        {
            List = new List<PostPicEntity>();
            Entity = new PostPicEntity();
        }

        public List<PostPicEntity> List { get; set; }
        public PostPicEntity Entity { get; set; }
    }
}
