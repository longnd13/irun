﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("PostVideo")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class PostVideoEntity
    {
        public int Id { get; set; }
        public int PostID { get; set; }
        public string LinkImages { get; set; }
    }

    public class PostVideo_DataEntity : CommonEntity
    {
        public PostVideo_DataEntity()
        {
            List = new List<PostVideoEntity>();
            Entity = new PostVideoEntity();
        }

        public List<PostVideoEntity> List { get; set; }
        public PostVideoEntity Entity { get; set; }
    }
}
