﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("PrivacySetting")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class PrivacySettingEntity : CommonTypeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
    }

    public class PrivacySetting_DataEntity : CommonEntity
    {
        public PrivacySetting_DataEntity()
        {
            List = new List<PrivacySettingEntity>();
            Entity = new PrivacySettingEntity();
        }

        public List<PrivacySettingEntity> List { get; set; }
        public PrivacySettingEntity Entity { get; set; }
    }
}
