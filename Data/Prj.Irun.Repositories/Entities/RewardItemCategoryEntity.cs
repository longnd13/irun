﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("RewardItemCategory")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class RewardItemCategoryEntity
    {
        public int Id { get; set; }
        public int ParentCategoryID { get; set; }
        public string CategoryName { get; set; }
        public bool Status { get; set; }
    }

    public class RewardItemCategory_DataEntity : CommonEntity
    {
        public RewardItemCategory_DataEntity()
        {
            List = new List<RewardItemCategoryEntity>();
            Entity = new RewardItemCategoryEntity();
        }

        public List<RewardItemCategoryEntity> List { get; set; }
        public RewardItemCategoryEntity Entity { get; set; }
    }
}
