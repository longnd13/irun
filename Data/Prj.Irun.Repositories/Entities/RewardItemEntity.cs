﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("RewardItem")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class RewardItemEntity : CommonTypeEntity
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string ImgURL { get; set; }
        public string Description { get; set; }
        public string QRCode { get; set; }
        public string BARCode { get; set; }
        public string SerialNumber { get; set; }
        public int Gold { get; set; }
        public int GoldFactorForGame { get; set; }
    }

    public class RewardItem_DataEntity : CommonEntity
    {
        public RewardItem_DataEntity()
        {
            List = new List<RewardItemEntity>();
            Entity = new RewardItemEntity();
        }

        public List<RewardItemEntity> List { get; set; }
        public RewardItemEntity Entity { get; set; }
    }
}
