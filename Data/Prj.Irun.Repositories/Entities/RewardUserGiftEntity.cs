﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("RewardUserGift")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class RewardUserGiftEntity
    {
        public int Id { get; set; }
        public int UserGiftID { get; set; }
        public string GiftRewardTypeCodeID { get; set; }
        public int Reward { get; set; }
    }

    public class RewardUserGift_DataEntity : CommonEntity
    {
        public RewardUserGift_DataEntity()
        {
            List = new List<RewardUserGiftEntity>();
            Entity = new RewardUserGiftEntity();
        }

        public List<RewardUserGiftEntity> List { get; set; }
        public RewardUserGiftEntity Entity { get; set; }
    }
}
