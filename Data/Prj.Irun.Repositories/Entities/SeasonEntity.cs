﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("Season")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SeasonEntity : CommonTypeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int RankingTypeID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Status { get; set; }
    }

    public class Season_DataEntity : CommonEntity
    {
        public Season_DataEntity()
        {
            List = new List<SeasonEntity>();
            Entity = new SeasonEntity();
        }

        public List<SeasonEntity> List { get; set; }
        public SeasonEntity Entity { get; set; }
    }
}
