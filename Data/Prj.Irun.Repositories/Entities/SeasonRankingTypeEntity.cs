﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("SeasonRankingType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SeasonRankingTypeEntity: CommonTypeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FunctionName { get; set; }
    }

    public class SeasonRankingType_DataEntity : CommonEntity
    {
        public SeasonRankingType_DataEntity()
        {
            List = new List<SeasonRankingTypeEntity>();
            Entity = new SeasonRankingTypeEntity();
        }

        public List<SeasonRankingTypeEntity> List { get; set; }
        public SeasonRankingTypeEntity Entity { get; set; }
    }
}
