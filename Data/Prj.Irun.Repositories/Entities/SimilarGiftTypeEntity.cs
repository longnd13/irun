﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("SimilarGiftType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SimilarGiftTypeEntity
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string SystemGiftTypeCodeID { get; set; }
        public string GiftOpenCondTypeCodeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        public int CreatedRate { get; set; }
    }

    public class SimilarGiftType_DataEntity : CommonEntity
    {
        public SimilarGiftType_DataEntity()
        {
            List = new List<SimilarGiftTypeEntity>();
            Entity = new SimilarGiftTypeEntity();
        }

        public List<SimilarGiftTypeEntity> List { get; set; }
        public SimilarGiftTypeEntity Entity { get; set; }
    }
}
