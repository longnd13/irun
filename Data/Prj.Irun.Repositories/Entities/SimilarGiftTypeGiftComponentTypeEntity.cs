﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("SimilarGiftTypeGiftComponentType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SimilarGiftTypeGiftComponentTypeEntity
    {
        public int Id { get; set; }
        public string SimilarGiftTypeCodeID { get; set; }
        public string GiftComponentTypeCodeID { get; set; }
    }

    public class SimilarGiftTypeGiftComponentType_DataEntity : CommonEntity
    {
        public SimilarGiftTypeGiftComponentType_DataEntity()
        {
            List = new List<SimilarGiftTypeGiftComponentTypeEntity>();
            Entity = new SimilarGiftTypeGiftComponentTypeEntity();
        }

        public List<SimilarGiftTypeGiftComponentTypeEntity> List { get; set; }
        public SimilarGiftTypeGiftComponentTypeEntity Entity { get; set; }
    }
}
