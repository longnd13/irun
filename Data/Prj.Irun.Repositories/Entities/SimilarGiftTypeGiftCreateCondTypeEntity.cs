﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("SimilarGiftTypeGiftCreateCondType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SimilarGiftTypeGiftCreateCondTypeEntity
    {
        public int Id { get; set; }
        public int SimilarGiftTypeID { get; set; }
        public int GiftCreateCondTypeID { get; set; }
    }

    public class SimilarGiftTypeGiftCreateCondType_DataEntity : CommonEntity
    {
        public SimilarGiftTypeGiftCreateCondType_DataEntity()
        {
            List = new List<SimilarGiftTypeGiftCreateCondTypeEntity>();
            Entity = new SimilarGiftTypeGiftCreateCondTypeEntity();
        }

        public List<SimilarGiftTypeGiftCreateCondTypeEntity> List { get; set; }
        public SimilarGiftTypeGiftCreateCondTypeEntity Entity { get; set; }
    }
}
