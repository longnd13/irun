﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("SimilarGiftTypeGiftOpenCondType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SimilarGiftTypeGiftOpenCondTypeEntity
    {
        public int Id { get; set; }
        public int SimilarGiftTypeID { get; set; }
        public int GiftOpenCondTypeID { get; set; }
    }

    public class SimilarGiftTypeGiftOpenCondType_DataEntity : CommonEntity
    {
        public SimilarGiftTypeGiftOpenCondType_DataEntity()
        {
            List = new List<SimilarGiftTypeGiftOpenCondTypeEntity>();
            Entity = new SimilarGiftTypeGiftOpenCondTypeEntity();
        }

        public List<SimilarGiftTypeGiftOpenCondTypeEntity> List { get; set; }
        public SimilarGiftTypeGiftOpenCondTypeEntity Entity { get; set; }
    }
}
