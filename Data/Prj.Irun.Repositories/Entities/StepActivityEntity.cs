﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("StepActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class StepActivityEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int Step { get; set; }
    }

    public class StepActivity_DataEntity : CommonEntity
    {
        public StepActivity_DataEntity()
        {
            List = new List<StepActivityEntity>();
            Entity = new StepActivityEntity();
        }

        public List<StepActivityEntity> List { get; set; }
        public StepActivityEntity Entity { get; set; }
    }
}
