﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
  public class StepInfoEntity
    {
        public long Gold { get; set; }
        public int TotalSteps { get; set; }
        public int Distances { get; set; }
    }
}
