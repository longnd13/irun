﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("Steps")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class StepsEntity
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Steps { get; set; }
        public int Day { get; set; }
        public int Week { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}
