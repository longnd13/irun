﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("SurplusStepsDistance")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SurplusStepsDistanceEntity
    {
        
        public long Id { get; set; }
        public string UserName { get; set; }
        public decimal SurplusStep { get; set; }
        public decimal SurplusDistance { get; set; }
        public decimal SurplusDistanceFromStep { get; set; }
    }
}
