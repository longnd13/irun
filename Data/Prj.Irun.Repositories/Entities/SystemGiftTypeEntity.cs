﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("SystemGiftType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SystemGiftTypeEntity
    {
        public int Id { get; set; }
        public string GiftCreateCondTypeCodeID { get; set; }
        public string Name { get; set; }
        public string CodeID { get; set; }
    }

    public class SystemGiftType_DataEntity : CommonEntity
    {
        public SystemGiftType_DataEntity()
        {
            List = new List<SystemGiftTypeEntity>();
            Entity = new SystemGiftTypeEntity();
        }

        public List<SystemGiftTypeEntity> List { get; set; }
        public SystemGiftTypeEntity Entity { get; set; }
    }
}
