﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("SystemGiftTypeGiftCreateCondType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class SystemGiftTypeGiftCreateCondTypeEntity
    {
        public int Id { get; set; }
        public int SystemGiftTypeID { get; set; }
        public int GiftCreateCondTypeID { get; set; }
    }

    public class SystemGiftTypeGiftCreateCondType_DataEntity : CommonEntity
    {
        public SystemGiftTypeGiftCreateCondType_DataEntity()
        {
            List = new List<SystemGiftTypeGiftCreateCondTypeEntity>();
            Entity = new SystemGiftTypeGiftCreateCondTypeEntity();
        }

        public List<SystemGiftTypeGiftCreateCondTypeEntity> List { get; set; }
        public SystemGiftTypeGiftCreateCondTypeEntity Entity { get; set; }
    }
}
