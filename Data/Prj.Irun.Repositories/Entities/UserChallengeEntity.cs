﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("UserChallenge")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class UserChallengeEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int ChallengeTypeID { get; set; }
        public string TotalSavingValue { get; set; }
        public int Status { get; set; }
    }

    public class UserChallenge_DataEntity : CommonEntity
    {
        public UserChallenge_DataEntity()
        {
            List = new List<UserChallengeEntity>();
            Entity = new UserChallengeEntity();
        }

        public List<UserChallengeEntity> List { get; set; }
        public UserChallengeEntity Entity { get; set; }
    }
}
