﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("UserFollow")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class UserFollowEntity
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FollowedUserID { get; set; }
    }

    public class UserFollow_DataEntity : CommonEntity
    {
        public UserFollow_DataEntity()
        {
            List = new List<UserFollowEntity>();
            Entity = new UserFollowEntity();
        }

        public List<UserFollowEntity> List { get; set; }
        public UserFollowEntity Entity { get; set; }
    }
}
