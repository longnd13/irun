﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("UserGift")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class UserGiftEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public string SystemGiftTypeCodeID { get; set; }
        public string SimilarGiftTypeCodeID { get; set; }
        public int TotalSavingValue { get; set; }
        public string UserGiftStatusCodeID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedDateString { get; set; }

    }
    public class UserGift_DataEntity : CommonEntity
    {
        public UserGift_DataEntity()
        {
            List = new List<UserGiftEntity>();
            Entity = new UserGiftEntity();
        }

        public List<UserGiftEntity> List { get; set; }
        public UserGiftEntity Entity { get; set; }
    }
}
