﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("UserGiftStatus")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class UserGiftStatusEntity
    {
        public int Id { get; set; }
        public string CodeID { get; set; }
        public string Name { get; set; }
        public string StatusName { get; set; }
        public string Description { get; set; }
    }

    public class UserGiftStatus_DataEntity : CommonEntity
    {
        public UserGiftStatus_DataEntity()
        {
            List = new List<UserGiftStatusEntity>();
            Entity = new UserGiftStatusEntity();
        }

        public List<UserGiftStatusEntity> List { get; set; }
        public UserGiftStatusEntity Entity { get; set; }
    }
}
