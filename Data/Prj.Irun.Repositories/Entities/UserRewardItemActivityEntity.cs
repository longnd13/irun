﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("UserRewardItemActivity")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class UserRewardItemActivityEntity
    {
        public long Id { get; set; }
        public string UserID { get; set; }
        public int RewardItemID { get; set; }
        public int UserRewardItemTypeID { get; set; }
        public DateTime? ObtainedDateTime { get; set; }
        public DateTime? ExpiredDateTime { get; set; }
        public string QRCode { get; set; }
        public string BarCode { get; set; }
        public string SerialCode { get; set; }
    }

    public class UserRewardItemActivity_DataEntity : CommonEntity
    {
        public UserRewardItemActivity_DataEntity()
        {
            List = new List<UserRewardItemActivityEntity>();
            Entity = new UserRewardItemActivityEntity();
        }

        public List<UserRewardItemActivityEntity> List { get; set; }
        public UserRewardItemActivityEntity Entity { get; set; }
    }
}
