﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("UserRewardItemType")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class UserRewardItemTypeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }

    public class UserRewardItemType_DataEntity : CommonEntity
    {
        public UserRewardItemType_DataEntity()
        {
            List = new List<UserRewardItemTypeEntity>();
            Entity = new UserRewardItemTypeEntity();
        }

        public List<UserRewardItemTypeEntity> List { get; set; }
        public UserRewardItemTypeEntity Entity { get; set; }
    }
}
