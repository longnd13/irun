﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Entities
{
    [TableName("test")]
    [PrimaryKey("Id", autoIncrement = true)] // thêm khóa chính để câu lệnh insert trả về ID hiện có của User
    public  class testEntity
    {
        public long Id { get; set; }
        public string stringdata { get; set; }
    }
}
