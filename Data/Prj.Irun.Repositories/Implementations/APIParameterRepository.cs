﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
  public class APIParameterRepository : IAPIParameterRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public APIParameterRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }
        public List<APIParameterEntity> GetAllAPIParameter()
        {
            var result = new List<APIParameterEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM APIParameter");
                    // query.Append("ORDER BY MODIFIEDDATE DESC");
                    result = _uow.Db.Fetch<APIParameterEntity>(query);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.SystemGiftType, "Repo_List<APIParameterEntity> GetAllAPIParameter(): ", ex.ToString());
            }

            return result;
        }

        public APIParameterEntity GetAPIParameterByCodeID(string code_id)
        {
            var entity = new APIParameterEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<APIParameterEntity>("SELECT * FROM APIParameter WHERE CodeID=@0", code_id);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.SystemGiftType, "Repo_APIParameterEntity GetAPIParameterById(int id): ", ex.ToString());
            }

            return entity;
        }
    }
}
