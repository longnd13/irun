﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
   public class AccountActivityRepository : IAccountActivityRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public AccountActivityRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public AccountActivityEntity WriteLogAccountActivity(AccountActivityEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.Account, "Repo_WriteLogAccountActivity(AccountActivityEntity entity): ", ex.ToString());
            }
            return entity;
        }
    }
}
