﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Prj.Irun.Utilities.EnumHandler;

namespace Prj.Irun.Repositories.Implementations
{
    public class AccountRepository : IAccountRepository
    {

        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public AccountRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public bool UpdatedAccessTokenExpired(AccountInfoEntity account_info_entity)
        {
            bool result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    _uow.Db.Execute(@"UPDATE AccountInfo 
                                      SET AccessToken=@0, AccessTokenExpired=@1, UserID=@2
                                      WHERE UserName@3", account_info_entity.AccessToken, account_info_entity.AccessTokenExpired,account_info_entity.UserID  , account_info_entity.UserName);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.Account, "Repo_UpdatedAccessTokenExpired(AccountInfoEntity account_info_entity): ", ex.ToString());
            }


            return result;
        }


        public AccountInfoEntity GetByAccessToken(string access_token)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM AccountInfo WHERE AccessToken=@0", access_token);
                    var result = _uow.Db.FirstOrDefault<AccountInfoEntity>(query);

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AccountInfoEntity GetByUserName(string user_name)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM AccountInfo WHERE UserName=@0", user_name);
                    var result = _uow.Db.FirstOrDefault<AccountInfoEntity>(query);
                    _uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.Account, "Repo_GetByUserName(string user_name): ", ex.ToString());
            }
            return new AccountInfoEntity();
        }

        public int CheckAccountExistByUserName(string user_name)
        {
            int result = -2;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT COUNT(*) FROM AccountInfo WHERE UserName=@0", user_name);
                    result = _uow.Db.ExecuteScalar<int>(query);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                result = -1;
                ex.ToString();
            }

            return result;
        }

        public AccountInfoEntity Register(AccountInfoEntity account_entity)
        {
            var surplus_steps_distance_entity = new SurplusStepsDistanceEntity();
            surplus_steps_distance_entity.UserName = account_entity.UserName;
            surplus_steps_distance_entity.SurplusDistance = 0;
            surplus_steps_distance_entity.SurplusStep = 0;
            surplus_steps_distance_entity.SurplusDistanceFromStep = 0;

            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var result_account_entity = Protector.Int(_uow.Db.Insert(account_entity));
                    var result_surplus_steps_distance_entity = Protector.Int(_uow.Db.Insert(surplus_steps_distance_entity));
                    if (result_account_entity > 0 && result_surplus_steps_distance_entity > 0)
                    {
                        _uow.Commit();
                    }
                    account_entity.Id = result_account_entity;
                }
            }
            catch (Exception ex)
            {

                ex.ToString();
            }

            return account_entity;
        }

   

        public int UpdatePassword(string user_name, string new_password, string access_token)
        {
            var result = 0;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    result = _uow.Db.Execute(@"UPDATE AccountInfo 
                                               SET AccessToken=@0, Password=@1 
                                               WHERE UserName=@2", access_token, new_password, user_name);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public AccountInfoEntity GetAccountByUserNameAndAccessToken(string user_name, string access_token)
        {

            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT *
                                                              FROM AccountInfo 
                                                              WHERE UserName=@0
                                                              AND AccessToken=@1", user_name, access_token);
                    var result = _uow.Db.FirstOrDefault<AccountInfoEntity>(query);
                    _uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AccountInfoEntity CheckUserNameAndPassword( string user_name, string old_password, string access_token)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT *
                                                              FROM AccountInfo 
                                                              WHERE UserName=@0
                                                              AND Password=@1
                                                              AND AccessToken=@2", user_name, old_password, access_token);
                    var result = _uow.Db.FirstOrDefault<AccountInfoEntity>(query);
                    _uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AccountInfoEntity CheckAccountLogin(string user_name, string password)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT *
                                                              FROM AccountInfo 
                                                              WHERE UserName=@0
                                                              AND Password=@1", user_name, password);
                    var result = _uow.Db.FirstOrDefault<AccountInfoEntity>(query);
                    _uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // facebook

        public AccountSocialEntity GetExisted(string social_id, LoginProviderType provider)
        {
            var entity = new AccountSocialEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT *
                                                              FROM AccountSocial 
                                                              WHERE social_id=@0
                                                              AND provider=@1", social_id, provider);
                    entity = _uow.Db.FirstOrDefault<AccountSocialEntity>(query);
                    _uow.Commit();
                    return entity;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // gold
        public AccountInfoEntity UpdatedGoldByUser(string user_name, long gold_updated)
        {

            var account_info_entity = new AccountInfoEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {

                    account_info_entity = _uow.Db.FirstOrDefault<AccountInfoEntity>(@"SELECT * FROM AccountInfo WHERE UserName=@0", user_name);
                    if (account_info_entity != null)
                    {
                        account_info_entity.Gold = gold_updated;
                        _uow.Db.Update(account_info_entity);


                        account_info_entity.Gold = gold_updated;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.Account, "Repo_UpdatedGoldByUser: ", ex.ToString());
            }
            return account_info_entity;


        }
   
        public LogsGoldAccountEntity WriteLogGoldUser(LogsGoldAccountEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var result = Protector.Int(_uow.Db.Insert(entity));
                    entity.Id = result;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.Account, "Repo_WriteLogGoldUser(LogsGoldAccountEntity entity): ", ex.ToString());
            }

            return entity;
        }

        // config OTP OnOff 

        public int OnOffOTP(int id)
        {
            var result = 0;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT *
                                                              FROM Config 
                                                              WHERE Id=@0", id);
                    var data = _uow.Db.FirstOrDefault<ConfigEntity>(query);
                    _uow.Commit();
                    if (data != null)
                    {
                        result = Protector.Int(data.Key);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.Account, "Repo_OnOffOTP(int id): ", ex.ToString());
            }
            return result;
        }


        public AccountInfoEntity UpdateAccountInfo(AccountInfoEntity account_info_entity, string userID)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {

                    var entity = _uow.Db.FirstOrDefault<AccountInfoEntity>(@"SELECT * FROM AccountInfo WHERE UserName=@0", userID);
                    if (account_info_entity != null)
                    {
                        account_info_entity.Id = entity.Id;
                        _uow.Db.Update(account_info_entity, account_info_entity.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.Account, "Repo_UpdateAccountInfo(AccountInfoEntity entity): ", ex.ToString());
            }

            return account_info_entity;
        }




    }
}
