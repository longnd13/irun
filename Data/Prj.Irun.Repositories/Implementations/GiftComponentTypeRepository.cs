﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
  public  class GiftComponentTypeRepository : IGiftComponentTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public GiftComponentTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }



        public List<GiftComponentTypeEntity> GetGiftComponentTypeBySimilarGiftTypeID(string SimilarGiftTypeCodeID)
        {
            var list_entity = new List<GiftComponentTypeEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    list_entity = _uow.Db.Fetch<GiftComponentTypeEntity>(@"SELECT b.* from SimilarGiftTypeGiftComponentType a, GiftComponentType b
                                                                                WHERE a.GiftComponentTypeCodeID = b.CodeID
                                                                                AND A.SimilarGiftTypeCodeID=@0", SimilarGiftTypeCodeID);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.GiftCreateCondType, "Repo_GetGiftComponentTypeBySimilarGiftTypeID: ", ex.ToString());
            }

            return list_entity;
        }
    }
}
