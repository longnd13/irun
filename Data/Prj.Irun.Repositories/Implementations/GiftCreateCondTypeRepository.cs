﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class GiftCreateCondTypeRepository : IGiftCreateCondTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public GiftCreateCondTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }
        public GiftCreateCondTypeEntity GetGiftCreateCondTypeByCodeID(string code_id)
        {
            var entity = new GiftCreateCondTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<GiftCreateCondTypeEntity>(@"SELECT * FROM GiftCreateCondType WHERE CodeID=@0", code_id);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.GiftCreateCondType, "Repo_GetGiftCreateCondTypeByCodeID(string code_id): ", ex.ToString());
            }

            return entity;
        }


    }
}
