﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Utilities;

namespace Prj.Irun.Repositories.Implementations
{
    public class MainCoreRepository : IMainCoreRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public MainCoreRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public StepsEntity AddSteps(StepsEntity step_entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var result = Protector.Int(_uow.Db.Insert(step_entity));
                    step_entity.Id = result;
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {

                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_AddSteps(StepsEntity step_entity): ", ex.ToString());
            }

            return step_entity;
        }

        public DistanceEntity AddDistance(DistanceEntity distance_entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var result = Protector.Int(_uow.Db.Insert(distance_entity));
                    distance_entity.Id = result;
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {

                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_AddDistance(DistanceEntity distance_entity): ", ex.ToString());
            }

            return distance_entity;
        }

        public SurplusStepsDistanceEntity GetStepsDistanceByUserName(string user_name)
        {
            var result = new SurplusStepsDistanceEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  SurplusStepsDistance WHERE UserName=@0", user_name);
                    result = _uow.Db.FirstOrDefault<SurplusStepsDistanceEntity>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_GetStepsDistanceByUserName: ", ex.ToString());
            }

            return result;
        }

        public ConfigEntity GetConfigById(int id)
        {
            var result = new ConfigEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  Config WHERE Id=@0", id);
                    result = _uow.Db.FirstOrDefault<ConfigEntity>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_GetConfigById: ", ex.ToString());
            }

            return result;
        }

        public List<ConfigEntity> GetConfig()
        {
            var result = new List<ConfigEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  Config");
                    result = _uow.Db.Fetch<ConfigEntity>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_GetConfigById: ", ex.ToString());
            }

            return result;
        }



        public List<StepsEntity> GetStepsByUserName(string user_name)
        {
            var result = new List<StepsEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  Steps WHERE userName", user_name);
                    result = _uow.Db.Fetch<StepsEntity>(query);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_GetStepsByUserName: ", ex.ToString());
            }

            return result;
        }
        public List<StepsEntity> GetStepsByUserName(string user_name, int day, int year)
        {
            var result = new List<StepsEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  Steps WHERE UserName=@0 AND Day=@1 AND Year=@2", user_name, day, year);
                    result = _uow.Db.Fetch<StepsEntity>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_GetStepsUserNameByDayNow: ", ex.ToString());
            }

            return result;
        }

        public List<DistanceEntity> GetDistanceByUserName(string user_name)
        {
            var result = new List<DistanceEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  Distance WHERE UserName=@0", user_name);
                    result = _uow.Db.Fetch<DistanceEntity>(query);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_GetDistanceByUserName: ", ex.ToString());
            }

            return result;
        }
        public List<DistanceEntity> GetDistanceByUserName(string user_name, int day, int year)
        {
            var result = new List<DistanceEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  Distance WHERE UserName=@0 AND Day=@1 AND Year=@2", user_name, day, year);
                    result = _uow.Db.Fetch<DistanceEntity>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_GetDistancesUserName: ", ex.ToString());
            }

            return result;
        }



        public bool SaveGiftBySteps(List<UserGiftEntity> user_gift_entity, List<RewardUserGiftEntity> reward_user_gift_entity, string UserID, int steps)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    foreach (var item in user_gift_entity)
                    {
                        int id_compare = item.Id;
                        int id = Protector.Int(_uow.Db.Insert(item));

                        var select = reward_user_gift_entity.Where(x => x.UserGiftID == id_compare).FirstOrDefault();

                        select.UserGiftID = id;
                        var id_2 = _uow.Db.Insert(select);
                    }

                    // steps activity
                    var stepsActivityEntity = new StepActivityEntity();
                    stepsActivityEntity.UserID = UserID;
                    stepsActivityEntity.Step = steps;
                    _uow.Db.Insert(stepsActivityEntity);

                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {

                ex.ToString();
            }

            return result;
        }

        public bool SaveGiftByDistance(List<UserGiftEntity> user_gift_entity, List<RewardUserGiftEntity> reward_user_gift_entity, string UserID, int distance)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    foreach (var item in user_gift_entity)
                    {
                        int id_compare = item.Id;
                        int id = Protector.Int(_uow.Db.Insert(item));

                        var select = reward_user_gift_entity.Where(x => x.UserGiftID == id_compare).FirstOrDefault();

                        select.UserGiftID = id;
                        var id_2 = _uow.Db.Insert(select);
                    }
                    // distance activity
                    var distanceActivityEntity = new DistanceActivityEntity();
                    distanceActivityEntity.UserID = UserID;
                    distanceActivityEntity.Distance = distance;
                    _uow.Db.Insert(distanceActivityEntity);

                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {

                ex.ToString();
            }

            return result;
        }


        public bool SaveDailyGift(List<UserGiftEntity> user_gift_entity, List<RewardUserGiftEntity> reward_user_gift_entity, string UserID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    foreach (var item in user_gift_entity)
                    {
                        int id_compare = item.Id;
                        int id = Protector.Int(_uow.Db.Insert(item));
                        var select = reward_user_gift_entity.Where(x => x.UserGiftID == id_compare);

                        foreach (var item2 in select)
                        {
                            item2.UserGiftID = id;
                            _uow.Db.Insert(item2);
                        }
                   }
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_SaveDailyGift: ", ex.ToString());
            }

            return result;
        }

        public bool OpenBasicGift(string user_id, int user_gift_id)
        {
            bool result = false;
            // var result = new UserGiftEntity();
            var gold_activity_user = new IgoldActivityEntity();
            var ads_activity_user = new AdsActivityEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var user_gift = _uow.Db.FirstOrDefault<UserGiftEntity>(@"SELECT * FROM  UserGift WHERE Id=@0 AND UserID=@1", user_gift_id, user_id);
                    var reward_user_gift = _uow.Db.FirstOrDefault<RewardUserGiftEntity>(@"SELECT * FROM  RewardUserGift WHERE UserGiftID=@0", user_gift_id);
                    // update usergiftstatus and add gold for user
                    if (user_gift != null && reward_user_gift != null)
                    {
                        // gold activity
                        if (reward_user_gift.GiftRewardTypeCodeID == ValueHandler.GiftRewardTypeValue.IGOLD)
                        {
                            gold_activity_user.UserID = user_id;
                            gold_activity_user.Code = "OPEN_BASIC_GIFT";
                            gold_activity_user.ObjectID = 1;
                            gold_activity_user.CreatedDate = DateHandler.DateNow();
                            gold_activity_user.IGold = reward_user_gift.Reward;
                            _uow.Db.Insert(gold_activity_user);

                            // update gold for account info
                            var account_info_entity = _uow.Db.FirstOrDefault<UserGiftEntity>(@"SELECT * FROM  AccountInfo WHERE UserName=@0", user_id);
                            if (account_info_entity != null)
                            {
                                _uow.Db.Execute(@"UPDATE AccountInfo SET Gold=@0 WHERE UserName=@1", reward_user_gift.Reward, user_id);
                            }
                            result = true;
                        }
                        // ads activity
                        else if (reward_user_gift.GiftRewardTypeCodeID == ValueHandler.GiftRewardTypeValue.QUANG_CAO)
                        {
                            ads_activity_user.UserID = user_id;
                            ads_activity_user.AdsCodeID = ValueHandler.GiftRewardTypeValue.QUANG_CAO;
                            ads_activity_user.Quantity = reward_user_gift.Reward;
                            ads_activity_user.CreatedDate = DateHandler.DateNow();
                            _uow.Db.Insert(ads_activity_user);
                            result = true;
                        }



                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }

       

        // check data

        public int CheckDailyGiftByUserID(string user_id, string created_date_string)
        {
            int result = -1;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    result = _uow.Db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM  UserGift WHERE UserID=@0 AND CreatedDateString=@1", user_id, created_date_string);
                    _uow.Commit();
                }
            }

            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_CheckDailyGiftByUserID:", ex.ToString());
            }

            return result;
        }
    }
}
