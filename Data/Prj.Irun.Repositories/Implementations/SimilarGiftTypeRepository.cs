﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
  public class SimilarGiftTypeRepository : ISimilarGiftTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public SimilarGiftTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public List<SimilarGiftTypeEntity> GetSimilarGiftType(bool IsSelected, string SystemGiftTypeCodeID)
        {
            var result = new List<SimilarGiftTypeEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT *
                                                              FROM SimilarGiftType 
                                                              WHERE IsSelected=@0 
                                                              AND SystemGiftTypeCodeID=@1
                                                              ORDER BY CreatedRate", IsSelected, SystemGiftTypeCodeID);
                    result = _uow.Db.Fetch<SimilarGiftTypeEntity>(query);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.SimilarGiftType, "apiRepo_ List<SimilarGiftTypeEntity> GetSimilarGiftType(bool IsSelected, int SystemGiftTypeCodeID): ", ex.ToString());
            }

            return result;
        }

      
    }
}
