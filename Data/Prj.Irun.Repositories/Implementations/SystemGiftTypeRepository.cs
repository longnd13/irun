﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
   public class SystemGiftTypeRepository : ISystemGiftTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public SystemGiftTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }


        public List<SystemGiftTypeEntity> GetAllSystemGiftType()
        {
            var result = new List<SystemGiftTypeEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  SystemGiftType");
                    // query.Append("ORDER BY MODIFIEDDATE DESC");
                    result = _uow.Db.Fetch<SystemGiftTypeEntity>(query);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.SystemGiftType, "apiRepo_List<SystemGiftTypeEntity> GetAllSystemGiftType(): ", ex.ToString());
            }

            return result;
        }

        public SystemGiftTypeEntity GetSystemGiftTypeByCodeID(string code_id)
        {
            var entity = new SystemGiftTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<SystemGiftTypeEntity>("SELECT * FROM SystemGiftType WHERE CodeID=@0", code_id);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.SystemGiftType, "Repo_GetSystemGiftTypeByCodeID(string code_id): ", ex.ToString());
            }

            return entity;
        }
    }
}
