﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class apiClientConfigRepository : IapiClientConfigRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public apiClientConfigRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public List<ClientConfigEntity> GetAllClientConfig()
        {
            var result = new List<ClientConfigEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM ClientConfig");
                    // query.Append("ORDER BY MODIFIEDDATE DESC");
                    result = _uow.Db.Fetch<ClientConfigEntity>(query);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.ClientConfig, "Repo_GetAllClientConfig(): ", ex.ToString());
            }

            return result;
        }
    }
}
