﻿using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prj.Irun.Repositories.Entities;
using Prj.Irun.Utilities;

namespace Prj.Irun.Repositories.Implementations
{
    public class apiUserGiftRepository : IapiUserGiftRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public apiUserGiftRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public UserGiftEntity GetUserGiftByID(int user_gift_id, string user_id)
        {
            var user_gift_entity = new UserGiftEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM UserGift WHERE Id=@0 AND UserID=@1", user_gift_id, user_id);
                    user_gift_entity = _uow.Db.FirstOrDefault<UserGiftEntity>(query);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.UserGift, "Repo_GetUserGiftByID:", ex.ToString());
            }

            return user_gift_entity;
        }

        public GiftOpenCondTypeEntity GetGiftOpenCondTypeBySimilarGiftTypeCodeID(string similar_gift_type_code_id)
        {
            var entity = new GiftOpenCondTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<GiftOpenCondTypeEntity>(@"SELECT b.* from SimilarGiftTypeGiftOpenCondType a, GiftOpenCondType b
                                                                                WHERE a.GiftOpenCondTypeCodeID = b.CodeId
                                                                                AND A.SimilarGiftTypeCodeID=@0", similar_gift_type_code_id);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.GiftCreateCondType, "Repo_GetGiftOpenCondTypeBySimilarGiftTypeCodeID: ", ex.ToString());
            }

            return entity;
        }

        public List<UserGiftEntity> GetDailyGiftByUserID(string user_id, string system_gift_codeID, string user_gift_status_codeID, string created_date_string)
        {
            var list_user_gift_entity = new List<UserGiftEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM UserGift
                                                              WHERE UserID=@0
                                                              AND SystemGiftTypeCodeID=@1
                                                              AND UserGiftStatusCodeID=@2
                                                              AND CreatedDateString=@3",
                                                              user_id,
                                                              system_gift_codeID,
                                                              user_gift_status_codeID,
                                                              created_date_string);
                    list_user_gift_entity = _uow.Db.Fetch<UserGiftEntity>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.UserGift, "Repo_GetDailyGiftByUserID:", ex.ToString());
            }

            return list_user_gift_entity;
        }

        public bool OpenDailyGift(string user_id, int user_gift_id)
        {

            bool result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var user_gift = _uow.Db.FirstOrDefault<UserGiftEntity>(@"SELECT * FROM UserGift WHERE Id=@0 AND UserID=@1", user_gift_id, user_id);
                    if (user_gift != null && user_gift.Id > 0)
                    {
                        var reward_user_gift = _uow.Db.Fetch<RewardUserGiftEntity>(@"SELECT * FROM RewardUserGift WHERE  UserGiftID=@1", user_gift_id, user_gift_id);

                        foreach (var item in reward_user_gift)
                        {
                            if (reward_user_gift != null && item.Id > 0)
                            {
                                if (item.GiftRewardTypeCodeID == ValueHandler.GiftRewardTypeValue.IGOLD)
                                {
                                    // cập nhật lại trạng thái gói quà
                                    _uow.Db.Execute(@"UPDATE UserGift SET UserGiftStatusCodeID=@0 
                                                      WHERE UserID=@1
                                                      AND Id=@2", ValueHandler.UserGiftStatusValue.Opened, user_id, user_gift.Id);
                                    // gold
                                    if (item.GiftRewardTypeCodeID == ValueHandler.GiftRewardTypeValue.IGOLD)
                                    {
                                        // ghi log gold
                                        var user_gold_activity = new IgoldActivityEntity();
                                        user_gold_activity.Code = "OPEN_DAILY_GIFT";
                                        user_gold_activity.ObjectID = user_gift_id;
                                        user_gold_activity.IGold = item.Reward;

                                        _uow.Db.Insert(user_gold_activity);
                                    }
                                    else if (item.GiftRewardTypeCodeID == ValueHandler.GiftRewardTypeValue.QUANG_CAO) // quảng cáo
                                    {
                                        // ads
                                        var ads_activity = new AdsActivityEntity();
                                        ads_activity.UserID = user_id;
                                        ads_activity.Quantity = item.Reward;
                                        ads_activity.CreatedDate = DateHandler.DateNow();
                                        ads_activity.AdsCodeID = "-";
                                        _uow.Db.Insert(ads_activity);
                                    }
                                }
                            }
                        }


                    }
                    _uow.Commit();
                    result = true;
                }


            }

            catch (Exception ex)
            {

                ex.ToString();
            }

            return result;
        }


        public int CountUserGiftBySystemGiftTypeCodeID(string system_gift_type_codeID)
        {
            int iData_response = 0;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    iData_response = _uow.Db.FirstOrDefault<int>(@"SELECT COUNT(*) FROM UserGift WHERE SystemGiftTypeCodeID=@0", system_gift_type_codeID);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                iData_response = -1;
                Logger.ErrorLog(ConstantsHandler.UserGift, "Repo_CountUserGiftBySystemGiftTypeCodeID:", ex.ToString());
            }

            return iData_response;
        }

        public UserGiftEntity AddEntity(UserGiftEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    int iResult = Protector.Int(_uow.Db.Insert(entity));
                    entity.Id = iResult;
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.UserGift, "Repo_AddEntity:", ex.ToString());
            }
            return entity;
        }

        public UserGiftEntity GetUserGift(string user_id, string system_gift_codeID, string user_gift_status_codeID)
        {

            var user_gift = new UserGiftEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM UserGift
                                                              WHERE UserID=@0
                                                              AND SystemGiftTypeCodeID=@1
                                                              AND UserGiftStatusCodeID=@2",
                                                              user_id,
                                                              system_gift_codeID,
                                                              user_gift_status_codeID);
                    user_gift = _uow.Db.FirstOrDefault<UserGiftEntity>(query);
                }
            }
            catch (Exception ex)
            {

                ex.ToString();
            }

            return user_gift;
        }

        public bool SaveUserGift(List<UserGiftEntity> user_gift_entity, List<RewardUserGiftEntity> reward_user_gift_entity, string UserID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    foreach (var item in user_gift_entity)
                    {
                        int id_compare = item.Id;
                        int id = Protector.Int(_uow.Db.Insert(item));
                        var select = reward_user_gift_entity.Where(x => x.UserGiftID == id_compare);
                        foreach (var item2 in select)
                        {
                            item2.UserGiftID = id;
                            _uow.Db.Insert(item2);
                        }
                    }
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.MainCore, "Repo_SaveDailyGift: ", ex.ToString());
            }

            return result;
        }

    }
}
