﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpAccountInfoRepository : IcpAccountInfoRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpAccountInfoRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public AccountInfo_DataEntity GetAll(AccountInfo_DataEntity entity)
        {
            var result = new AccountInfo_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM AccountInfo WHERE ID>0");
                    query.Append("ORDER BY MODIFIEDDATE DESC");
                    var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public bool Delete(int id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM AccountInfo WHERE ID=@0", id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public AccountInfo_DataEntity SearchByUsername(AccountInfo_DataEntity entity, string username, string fullname)
        {
            var result = new AccountInfo_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullname))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AccountInfo WHERE Username like '%" + username + "%' and FullNameSign like '%" + fullname + "%'");
                        var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else if (string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullname))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AccountInfo WHERE FullNameSign like '%" + fullname + "%'");
                        var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else if (!string.IsNullOrEmpty(username) && string.IsNullOrEmpty(fullname))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AccountInfo WHERE Username like '%" + username + "%'");
                        var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(fullname))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AccountInfo WHERE ID>0");
                        var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public AccountInfo_DataEntity GetAllBySort(AccountInfo_DataEntity entity, string goldSort, string stepSort, string distanceSort)
        {
            var result = new AccountInfo_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(goldSort) && string.IsNullOrEmpty(stepSort) && string.IsNullOrEmpty(distanceSort))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AccountInfo order by Gold " + goldSort);
                        var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    if (string.IsNullOrEmpty(goldSort) && !string.IsNullOrEmpty(stepSort) && string.IsNullOrEmpty(distanceSort))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AccountInfo order by TotalSteps " + stepSort);
                        var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    if (string.IsNullOrEmpty(goldSort) && string.IsNullOrEmpty(stepSort) && !string.IsNullOrEmpty(distanceSort))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AccountInfo order by TotalDistance " + distanceSort);
                        var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else if (string.IsNullOrEmpty(goldSort) && string.IsNullOrEmpty(stepSort) && string.IsNullOrEmpty(distanceSort))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AccountInfo WHERE ID>0");
                        var data = _uow.Db.Page<AccountInfoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public AccountInfoEntity Add(AccountInfoEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public AccountInfoEntity GetById(int id)
        {
            var entity = new AccountInfoEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<AccountInfoEntity>("SELECT * FROM AccountInfo WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public AccountInfoEntity Update(AccountInfoEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<AccountInfoEntity>("SELECT * FROM AccountInfo WHERE ID=@0", entity.Id);
                    if (byId.Id > 0 && byId.Id == entity.Id)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }
    }
}
