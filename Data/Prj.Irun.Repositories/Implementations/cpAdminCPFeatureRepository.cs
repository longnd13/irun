﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpAdminCPFeatureRepository : IcpAccountFeatureRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpAdminCPFeatureRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public AdminCPFeatureEntity Add(AdminCPFeatureEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(int id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM AdminCPFeature WHERE ID=@0", id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public AdminCPFeature_DataEntity GetAll(AdminCPFeature_DataEntity entity)
        {
            var result = new AdminCPFeature_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  AdminCPFeature WHERE ID>0");
                    query.Append("ORDER BY MODIFIEDDATE DESC");
                    var data = _uow.Db.Page<AdminCPFeatureEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public AdminCPFeatureEntity GetById(int id)
        {
            var entity = new AdminCPFeatureEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<AdminCPFeatureEntity>("SELECT * FROM AdminCPFeature WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public AdminCPFeatureEntity Update(AdminCPFeatureEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<AdminCPFeatureEntity>("SELECT * FROM AdminCPFeature WHERE ID=@0", entity.Id);
                    if (byId.Id > 0 && byId.Id == entity.Id)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public AdminCPFeature_DataEntity GetByParentID(AdminCPFeature_DataEntity entity, int parentID)
        {
            var result = new AdminCPFeature_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if(parentID >= 0)
                    {
                        var query = PetaPoco.Sql.Builder.Append(@"select * from AdminCPFeature where ParentId=@0", parentID);
                        var data = _uow.Db.Page<AdminCPFeatureEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                        _uow.Commit();
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  AdminCPFeature WHERE ID>0");
                        query.Append("ORDER BY MODIFIEDDATE DESC");
                        var data = _uow.Db.Page<AdminCPFeatureEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public AdminCPFeatureEntity GetNodeByParentID(int parentID, long id)
        {
            var entity = new AdminCPFeatureEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<AdminCPFeatureEntity>("select * from AdminCPFeature where ParentId=@0 and ID=@1", parentID, id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public bool DeleteNode(int parentID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM AdminCPFeature WHERE ParentId=@0", parentID);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public List<AdminCPFeatureEntity> GetFeatureByGroupId(int groupId)
        {
            List<AdminCPFeatureEntity> result = new List<AdminCPFeatureEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    result = _uow.Db.Fetch<AdminCPFeatureEntity>(@"select * from AdminCPFeature where ParentId=@0", groupId);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
