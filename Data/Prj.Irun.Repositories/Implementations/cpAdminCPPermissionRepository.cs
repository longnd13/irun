﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpAdminCPPermissionRepository : IcpAdminCPPermissionRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpAdminCPPermissionRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public AdminCPPermissionEntity Add(AdminCPPermissionEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(string userName)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM AdminCPPermission WHERE username=@0", userName);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public AdminCPPermission_DataEntity GetAll(AdminCPPermission_DataEntity entity)
        {
            var result = new AdminCPPermission_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  AdminCPPermission WHERE ID>0");
                    query.Append("ORDER BY CreatedDate DESC");
                    var data = _uow.Db.Page<AdminCPPermissionEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public AdminCPPermissionEntity GetById(int id)
        {
            var entity = new AdminCPPermissionEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<AdminCPPermissionEntity>("SELECT * FROM AdminCPPermission WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public AdminCPPermissionEntity Update(AdminCPPermissionEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<AdminCPPermissionEntity>("SELECT * FROM AdminCPPermission WHERE ID=@0", entity.Id);
                    if (byId.Id > 0 && byId.Id == entity.Id)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public MessageEntity GetPermission(string userName)
        {
            var msgEntity = new MessageEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var result = _uow.Db.Fetch<AdminCPPermissionEntity>("SELECT * FROM  AdminCPPermission WHERE UserName=@0", userName);

                    if (result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            msgEntity.message += item.FeatureCode + ",";
                        }
                        msgEntity.message = msgEntity.message.TrimEnd(',');
                        msgEntity.code = 0;
                        msgEntity.success = true;
                    }
                    else
                    {
                        msgEntity.message = "empty";
                        msgEntity.code = -1;
                        msgEntity.success = false;
                    }
                    _uow.Commit();

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return msgEntity;
        }
    }
}
