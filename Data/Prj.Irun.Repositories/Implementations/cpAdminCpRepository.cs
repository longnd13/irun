﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpAdminCpRepository : IcpAdminCpRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpAdminCpRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public AdminCpEntity Add(AdminCpEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.AdminCP, "Repo_AdminCpEntity Add(AdminCpEntity entity): ", ex.ToString());
            }
            return entity;
        }

        public bool Delete(int id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM AdminCp WHERE ID=@0", id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public AdminCp_DataEntity GetAll(AdminCp_DataEntity entity)
        {
            var result = new AdminCp_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  AdminCp WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<AdminCpEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public AdminCpEntity GetById(int id)
        {
            var entity = new AdminCpEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<AdminCpEntity>("SELECT * FROM AdminCp WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.AdminCP, "Repo_AdminCpEntity GetById(int id): ", ex.ToString());
            }

            return entity;
        }

        public AdminCpEntity Update(AdminCpEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<AdminCpEntity>("SELECT * FROM AdminCp WHERE ID=@0", entity.Id);
                    if (byId.Id > 0 && byId.Id == entity.Id)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public AdminCpEntity Authenticate(string user_name, string password)
        {
            var entity = new AdminCpEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<AdminCpEntity>(@"SELECT * FROM AdminCp
                                                                     WHERE UserName=@0 
                                                                     AND Password=@1", user_name, password);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog(ConstantsHandler.AdminCP, "Repo_AdminCpEntity Authenticate(string user_name, string password): ", ex.ToString());
            }

            return entity;
        }
    }
}
