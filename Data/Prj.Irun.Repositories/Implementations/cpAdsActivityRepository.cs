﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpAdsActivityRepository : IcpAdsActivityRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpAdsActivityRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public AdsActivity_DataEntity GetAll(AdsActivity_DataEntity entity)
        {
            var result = new AdsActivity_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM AdsActivity WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<AdsActivityEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public bool Delete(string adsCodeID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM AdsActivity WHERE adsCodeID=@0", adsCodeID);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public AdsActivity_DataEntity SearchByAdsCodeID(AdsActivity_DataEntity entity, string adsCodeID)
        {
            var result = new AdsActivity_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (adsCodeID != "0")
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AdsActivity WHERE adsCodeID = @0", adsCodeID);
                        var data = _uow.Db.Page<AdsActivityEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM AdsActivity WHERE ID>0");
                        var data = _uow.Db.Page<AdsActivityEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
