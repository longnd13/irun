﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpGiftComponentTypeRepository : IcpGiftComponentTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpGiftComponentTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public GiftComponentTypeEntity Add(GiftComponentTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(string CodeID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM GiftComponentType WHERE CodeID=@0", CodeID);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public GiftComponentType_DataEntity GetAll(GiftComponentType_DataEntity entity)
        {
            var result = new GiftComponentType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  GiftComponentType WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<GiftComponentTypeEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public GiftComponentTypeEntity GetById(string CodeID)
        {
            var entity = new GiftComponentTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<GiftComponentTypeEntity>("SELECT * FROM GiftComponentType WHERE CodeID=@0", CodeID);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public GiftComponentTypeEntity Update(GiftComponentTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<GiftComponentTypeEntity>("SELECT * FROM GiftComponentType WHERE CodeID=@0", entity.CodeID);
                    if (byId.Id > 0 && byId.CodeID == entity.CodeID)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public GiftComponentType_DataEntity MultipleSelectedGift(GiftComponentType_DataEntity entity, string code_id)
        {
            var result = new GiftComponentType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (code_id != "" && code_id != null)
                    {
                        var query = PetaPoco.Sql.Builder.Append(@"select c.CodeID, c.Name from SimilarGiftType a, SimilarGiftTypeGiftComponentType b, GiftComponentType c where a.CodeID = b.SimilarGiftTypeCodeID and b.GiftComponentTypeCodeID = c.CodeID and a.CodeID = @0", code_id);

                        var data = _uow.Db.Page<GiftComponentTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
