﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpGiftCreateCondTypeRepository : IcpGiftCreateCondTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpGiftCreateCondTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public GiftCreateCondTypeEntity Add(GiftCreateCondTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(string CodeID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM GiftCreateCondType WHERE CodeID=@0", CodeID);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public GiftCreateCondType_DataEntity GetAll(GiftCreateCondType_DataEntity entity)
        {
            var result = new GiftCreateCondType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  GiftCreateCondType WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<GiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public GiftCreateCondTypeEntity GetById(string CodeID)
        {
            var entity = new GiftCreateCondTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<GiftCreateCondTypeEntity>("SELECT * FROM GiftCreateCondType WHERE CodeID=@0", CodeID);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public GiftCreateCondTypeEntity Update(GiftCreateCondTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<GiftCreateCondTypeEntity>("SELECT * FROM GiftCreateCondType WHERE CodeID=@0", entity.CodeID);
                    if (byId.Id > 0 && byId.CodeID == entity.CodeID)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public GiftCreateCondType_DataEntity MultipleSelectedGift(GiftCreateCondType_DataEntity entity, string code_id)
        {
            var result = new GiftCreateCondType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (code_id != "" && code_id != null)
                    {
                        var query = PetaPoco.Sql.Builder.Append(@"select a.Name, a.CodeID from GiftCreateCondType a, SystemGiftTypeGiftCreateCondType b where b.SystemGiftTypeCodeID=@0
                                                                  and a.CodeID = b.GiftCreateCondTypeCodeID group by a.CodeID, a.Name, a.LogicDesc, a.CreateCondTypeValue, a.FunctionName", code_id);
                                                      
                        var data = _uow.Db.Page<GiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append(@"select a.* from GiftCreateCondType a, SystemGiftTypeGiftCreateCondType b where a.CodeID = b.GiftCreateCondTypeCodeID group by a.Id, a.Name, a.LogicDesc, a.CreateCondTypeValue, a.FunctionName");
                        var data = _uow.Db.Page<GiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
