﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpGiftRewardTypeRepository : IcpGiftRewardTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpGiftRewardTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public GiftRewardTypeEntity Add(GiftRewardTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(string CodeID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM GiftRewardType WHERE CodeID=@0", CodeID);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public GiftRewardType_DataEntity GetAll(GiftRewardType_DataEntity entity)
        {
            var result = new GiftRewardType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  GiftRewardType WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<GiftRewardTypeEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public GiftRewardTypeEntity GetById(string CodeID)
        {
            var entity = new GiftRewardTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<GiftRewardTypeEntity>("SELECT * FROM GiftRewardType WHERE CodeID=@0", CodeID);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public GiftRewardTypeEntity Update(GiftRewardTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<GiftRewardTypeEntity>("SELECT * FROM GiftRewardType WHERE CodeID=@0", entity.CodeID);
                    if (byId.Id > 0 && byId.CodeID == entity.CodeID)
                    {
                        _uow.Db.Update(entity);
                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }
    }
}
