﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpPostVideoRepository : IcpPostVideoRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpPostVideoRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public PostVideoEntity Add(PostVideoEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(int id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM PostVideo WHERE ID=@0", id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public PostVideo_DataEntity GetAll(PostVideo_DataEntity entity)
        {
            var result = new PostVideo_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  PostVideo WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<PostVideoEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public PostVideoEntity GetById(int id)
        {
            var entity = new PostVideoEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<PostVideoEntity>("SELECT * FROM PostVideo WHERE PostId=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public PostVideo_DataEntity SearchById(PostVideo_DataEntity entity, int id)
        {
            var result = new PostVideo_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (id > 0)
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM PostVideo WHERE PostID=@0", id);
                        var data = _uow.Db.Page<PostVideoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM PostVideo WHERE ID>0");
                        var data = _uow.Db.Page<PostVideoEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public PostVideoEntity Update(PostVideoEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<PostVideoEntity>("SELECT * FROM PostVideo WHERE ID=@0", entity.Id);
                    if (byId.Id > 0 && byId.Id == entity.Id)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }
    }
}
