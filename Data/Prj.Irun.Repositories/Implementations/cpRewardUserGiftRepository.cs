﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpRewardUserGiftRepository : IcpRewardUserGiftRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpRewardUserGiftRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public RewardUserGift_DataEntity GetAll(RewardUserGift_DataEntity entity)
        {
            var result = new RewardUserGift_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM RewardUserGift WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<RewardUserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public bool Delete(int id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM RewardUserGift WHERE ID=@0", id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public RewardUserGift_DataEntity Search(RewardUserGift_DataEntity entity, int userGiftID, string giftRewardTypeCodeID)
        {
            var result = new RewardUserGift_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (userGiftID > 0 && giftRewardTypeCodeID == "None")
                    {
                        var query = PetaPoco.Sql.Builder.Append("select a.* from RewardUserGift a, UserGift b where a.UserGiftID = b.Id and a.UserGiftID=@0", userGiftID);
                        var data = _uow.Db.Page<RewardUserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else if (userGiftID == 0 && giftRewardTypeCodeID != "None")
                    {
                        var query = PetaPoco.Sql.Builder.Append("select a.* from RewardUserGift a, GiftRewardType b where a.giftRewardTypeCodeID = b.Code and a.giftRewardTypeCodeID=@0", giftRewardTypeCodeID);
                        var data = _uow.Db.Page<RewardUserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else if (userGiftID > 0 && giftRewardTypeCodeID != "None")
                    {
                        var query = PetaPoco.Sql.Builder.Append("select a.* from RewardUserGift a, UserGift b, GiftRewardType c where a.UserGiftID = b.Id and a.giftRewardTypeCodeID = c.Code and a.UserGiftID=@0 and a.giftRewardTypeCodeID=@1", userGiftID, giftRewardTypeCodeID);
                        var data = _uow.Db.Page<RewardUserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM RewardUserGift WHERE ID>0");
                        var data = _uow.Db.Page<RewardUserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
