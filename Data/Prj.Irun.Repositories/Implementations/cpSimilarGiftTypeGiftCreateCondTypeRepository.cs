﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpSimilarGiftTypeGiftCreateCondTypeRepository : IcpSimilarGiftTypeGiftCreateCondTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpSimilarGiftTypeGiftCreateCondTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public SimilarGiftTypeGiftCreateCondTypeEntity Add(SimilarGiftTypeGiftCreateCondTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public SimilarGiftTypeGiftCreateCondType_DataEntity GetAll(SimilarGiftTypeGiftCreateCondType_DataEntity entity)
        {
            var result = new SimilarGiftTypeGiftCreateCondType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  SimilarGiftTypeGiftCreateCondType WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<SimilarGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public SimilarGiftTypeGiftCreateCondTypeEntity GetById(int id)
        {
            var entity = new SimilarGiftTypeGiftCreateCondTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<SimilarGiftTypeGiftCreateCondTypeEntity>("SELECT * FROM SimilarGiftTypeGiftCreateCondType WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public SimilarGiftTypeGiftCreateCondTypeEntity Update(SimilarGiftTypeGiftCreateCondTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<SimilarGiftTypeGiftCreateCondTypeEntity>("SELECT * FROM SimilarGiftTypeGiftCreateCondType WHERE ID=@0", entity.Id);
                    if (byId.Id > 0 && byId.Id == entity.Id)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public SimilarGiftTypeGiftCreateCondType_DataEntity SearchBySimilarGiftId(SimilarGiftTypeGiftCreateCondType_DataEntity entity, int id)
        {
            var result = new SimilarGiftTypeGiftCreateCondType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(id.ToString()))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM SimilarGiftTypeGiftCreateCondType WHERE SimilarGiftTypeID=@0", id);
                        var data = _uow.Db.Page<SimilarGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM Post SimilarGiftTypeGiftCreateCondType ID>0");
                        var data = _uow.Db.Page<SimilarGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public SimilarGiftTypeGiftCreateCondType_DataEntity SearchByGiftCreateCondTypeId(SimilarGiftTypeGiftCreateCondType_DataEntity entity, int id)
        {
            var result = new SimilarGiftTypeGiftCreateCondType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(id.ToString()))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM SimilarGiftTypeGiftCreateCondType WHERE GiftCreateCondTypeID=@0", id);
                        var data = _uow.Db.Page<SimilarGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM Post SimilarGiftTypeGiftCreateCondType ID>0");
                        var data = _uow.Db.Page<SimilarGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
