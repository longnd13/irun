﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpSimilarGiftTypeRepository : IcpSimilarGiftTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpSimilarGiftTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public SimilarGiftTypeEntity Add(SimilarGiftTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(string CodeID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM SimilarGiftType WHERE CodeID=@0", CodeID);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public SimilarGiftType_DataEntity GetAll(SimilarGiftType_DataEntity entity)
        {
            var result = new SimilarGiftType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  SimilarGiftType WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<SimilarGiftTypeEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public bool UpdateAll()
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("update SimilarGiftType set IsSelected='False'");
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public SimilarGiftTypeEntity GetById(string CodeID)
        {
            var entity = new SimilarGiftTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<SimilarGiftTypeEntity>("SELECT * FROM SimilarGiftType WHERE CodeID=@0", CodeID);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }        

        public List<SimilarGiftTypeEntity> SearchBySystemGiftType(string systemGiftTypeCodeID)
        {
            List<SimilarGiftTypeEntity> result = new List<SimilarGiftTypeEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    result = _uow.Db.Fetch<SimilarGiftTypeEntity>(@"SELECT * FROM SimilarGiftType WHERE systemGiftTypeCodeID =@0", systemGiftTypeCodeID);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public List<SimilarGiftTypeEntity> GetAllSystemGiftType()
        {
            List<SimilarGiftTypeEntity> result = new List<SimilarGiftTypeEntity>();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    result = _uow.Db.Fetch<SimilarGiftTypeEntity>("SELECT * FROM SimilarGiftType");
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public SimilarGiftTypeEntity GetByCreatedRate(int createdRate, string SystemGiftTypeCode)
        {
            var entity = new SimilarGiftTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<SimilarGiftTypeEntity>("SELECT * FROM SimilarGiftType WHERE CreatedRate=@0 and SystemGiftTypeCodeID=@1", createdRate, SystemGiftTypeCode);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public bool UpdateCreateRate(string code_id, int createdRate)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("update SimilarGiftType set CreatedRate = @0 where CodeId = @1", createdRate, code_id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public SimilarGiftTypeEntity Update(SimilarGiftTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<SimilarGiftTypeEntity>("SELECT * FROM SimilarGiftType WHERE CodeID=@0", entity.CodeID);
                    if (byId.Id > 0 && byId.CodeID == entity.CodeID)
                    {
                        entity.CodeID = byId.CodeID;
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }
    }
}
