﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpSystemGiftTypeGiftCreateCondTypeRepository : IcpSystemGiftTypeGiftCreateCondTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpSystemGiftTypeGiftCreateCondTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public SystemGiftTypeGiftCreateCondTypeEntity Add(SystemGiftTypeGiftCreateCondTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public SystemGiftTypeGiftCreateCondType_DataEntity GetAll(SystemGiftTypeGiftCreateCondType_DataEntity entity)
        {
            var result = new SystemGiftTypeGiftCreateCondType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  SystemGiftTypeGiftCreateCondType WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<SystemGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public SystemGiftTypeGiftCreateCondTypeEntity GetById(int id)
        {
            var entity = new SystemGiftTypeGiftCreateCondTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<SystemGiftTypeGiftCreateCondTypeEntity>("SELECT * FROM SystemGiftTypeGiftCreateCondType WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public SystemGiftTypeGiftCreateCondTypeEntity Update(SystemGiftTypeGiftCreateCondTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<SystemGiftTypeGiftCreateCondTypeEntity>("SELECT * FROM SystemGiftTypeGiftCreateCondType WHERE ID=@0", entity.Id);
                    if (byId.Id > 0 && byId.Id == entity.Id)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public SystemGiftTypeGiftCreateCondType_DataEntity SearchBySimilarGiftId(SystemGiftTypeGiftCreateCondType_DataEntity entity, int id)
        {
            var result = new SystemGiftTypeGiftCreateCondType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(id.ToString()))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM SystemGiftTypeGiftCreateCondType WHERE SystemGiftTypeCodeID=@0", id);
                        var data = _uow.Db.Page<SystemGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM Post SystemGiftTypeGiftCreateCondType ID>0");
                        var data = _uow.Db.Page<SystemGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public SystemGiftTypeGiftCreateCondType_DataEntity SearchByGiftCreateCondTypeId(SystemGiftTypeGiftCreateCondType_DataEntity entity, int id)
        {
            var result = new SystemGiftTypeGiftCreateCondType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(id.ToString()))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM SystemGiftTypeGiftCreateCondType WHERE GiftCreateCondTypeID=@0", id);
                        var data = _uow.Db.Page<SystemGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM Post SystemGiftTypeGiftCreateCondType ID>0");
                        var data = _uow.Db.Page<SystemGiftTypeGiftCreateCondTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public bool DeleteBySystemGiftId(string code_id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM SystemGiftTypeGiftCreateCondType WHERE SystemGiftTypeCodeID=@0", code_id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public bool DeleteByGiftCreateCondTypeId(string code_id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM SystemGiftTypeGiftCreateCondType WHERE GiftCreateCondTypeCodeID=@0", code_id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
    }
}
