﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpSystemGiftTypeRepository : IcpSystemGiftTypeRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpSystemGiftTypeRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public SystemGiftTypeEntity Add(SystemGiftTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(string CodeID)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM SystemGiftType WHERE CodeID=@0", CodeID);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public SystemGiftType_DataEntity GetAll(SystemGiftType_DataEntity entity)
        {
            var result = new SystemGiftType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  SystemGiftType WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<SystemGiftTypeEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public SystemGiftTypeEntity GetByCodeId(string code_id)
        {
            var entity = new SystemGiftTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<SystemGiftTypeEntity>("SELECT * FROM SystemGiftType WHERE CodeID=@0", code_id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }
        public SystemGiftTypeEntity GetById(int id)
        {
            var entity = new SystemGiftTypeEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<SystemGiftTypeEntity>("SELECT * FROM SystemGiftType WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public SystemGiftTypeEntity Update(SystemGiftTypeEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<SystemGiftTypeEntity>("SELECT * FROM SystemGiftType WHERE CodeID=@0", entity.CodeID);
                    if (byId.Id > 0 && byId.CodeID == entity.CodeID)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public SystemGiftType_DataEntity MultipleSelectedGift(SystemGiftType_DataEntity entity, int id)
        {
            var result = new SystemGiftType_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(id.ToString()))
                    {
                        var query = PetaPoco.Sql.Builder.Append("select a.Id, a.Name from SystemGiftType a, SystemGiftTypeGiftCreateCondType b where b.GiftCreateCondTypeID=@0", id);
                        query.Append("and a.Id = b.SystemGiftTypeCodeID group by a.Id, a.Name");
                        var data = _uow.Db.Page<SystemGiftTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("select a.* from SystemGiftType a, SystemGiftTypeGiftCreateCondType b where a.Id = b.SystemGiftTypeCodeID group by a.Id, a.Name");
                        var data = _uow.Db.Page<SystemGiftTypeEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
