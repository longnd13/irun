﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpUserFollowRepository : IcpUserFollowRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpUserFollowRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public UserFollowEntity Add(UserFollowEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var data = _uow.Db.Insert(entity);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }
            return entity;
        }

        public bool Delete(int id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM UserFollow WHERE ID=@0", id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public UserFollow_DataEntity GetAll(UserFollow_DataEntity entity)
        {
            var result = new UserFollow_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM  UserFollow WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<UserFollowEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public UserFollowEntity GetById(int id)
        {
            var entity = new UserFollowEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<UserFollowEntity>("SELECT * FROM UserFollow WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public UserFollowEntity Update(UserFollowEntity entity)
        {
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.FirstOrDefault<UserFollowEntity>("SELECT * FROM UserFollow WHERE ID=@0", entity.Id);
                    if (byId.Id > 0 && byId.Id == entity.Id)
                    {
                        _uow.Db.Update(entity);

                        _uow.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }

        public UserFollow_DataEntity SearchByUsername(UserFollow_DataEntity entity, string username)
        {
            var result = new UserFollow_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(username))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM UserFollow WHERE Username like '%" + username + "%'");
                        var data = _uow.Db.Page<UserFollowEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM UserFollow WHERE ID>0");
                        var data = _uow.Db.Page<UserFollowEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
