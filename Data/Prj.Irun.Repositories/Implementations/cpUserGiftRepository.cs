﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpUserGiftRepository : IcpUserGiftRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpUserGiftRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public UserGift_DataEntity GetAll(UserGift_DataEntity entity)
        {
            var result = new UserGift_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM UserGift WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<UserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public bool Delete(int id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM UserGift WHERE ID=@0", id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public UserGift_DataEntity Search(UserGift_DataEntity entity, string SystemGiftTypeCodeID, string SimilarGiftTypeID)
        {
            var result = new UserGift_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if ((SystemGiftTypeCodeID != "None") && (SimilarGiftTypeID == "None"))
                    {
                        var query = PetaPoco.Sql.Builder.Append("select a.* from UserGift a, SystemGiftType b where a.SystemGiftTypeCodeID = b.CodeID and a.SystemGiftTypeCodeID=@0", SystemGiftTypeCodeID);
                        var data = _uow.Db.Page<UserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else if ((SystemGiftTypeCodeID == "None") && (SimilarGiftTypeID != "None"))
                    {
                        var query = PetaPoco.Sql.Builder.Append("select a.* from UserGift a, SimilarGiftType b where a.SimilarGiftTypeID = b.CodeID and a.SimilarGiftTypeID=@0", SimilarGiftTypeID);
                        var data = _uow.Db.Page<UserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else if ((SystemGiftTypeCodeID != "None") && (SimilarGiftTypeID != "None"))
                    {
                        var query = PetaPoco.Sql.Builder.Append("select a.* from UserGift a, SystemGiftType b, SimilarGiftType c where a.SystemGiftTypeCodeID = b.CodeID and a.SimilarGiftTypeID = c.CodeID and a.SystemGiftTypeCodeID=@0 and a.SimilarGiftTypeID=@1", SystemGiftTypeCodeID, SimilarGiftTypeID);
                        var data = _uow.Db.Page<UserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM UserGift WHERE ID>0");
                        var data = _uow.Db.Page<UserGiftEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public UserGiftEntity GetById(int id)
        {
            var entity = new UserGiftEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    entity = _uow.Db.FirstOrDefault<UserGiftEntity>("SELECT * FROM UserGift WHERE ID=@0", id);
                }
            }
            catch (Exception ex)
            {
                entity = null;
                ex.ToString();
            }

            return entity;
        }
    }
}
