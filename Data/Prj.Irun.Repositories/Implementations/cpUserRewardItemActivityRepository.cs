﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
    public class cpUserRewardItemActivityRepository: IcpUserRewardItemActivityRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public cpUserRewardItemActivityRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public UserRewardItemActivity_DataEntity GetAll(UserRewardItemActivity_DataEntity entity)
        {
            var result = new UserRewardItemActivity_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var query = PetaPoco.Sql.Builder.Append(@"SELECT * FROM UserRewardItemActivity WHERE ID>0");
                    query.Append("ORDER BY ID DESC");
                    var data = _uow.Db.Page<UserRewardItemActivityEntity>(entity.PageIndex, entity.PageSize, query);
                    if (data != null && data.TotalItems > 0)
                    {
                        result.List = data.Items;
                        result.PageTotal = data.TotalPages;
                        result.PageIndex = data.CurrentPage;
                        result.Count = Protector.Int(data.TotalItems);
                    }
                    _uow.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public bool Delete(int id)
        {
            var result = false;
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var byId = _uow.Db.Execute("DELETE FROM UserRewardItemActivity WHERE ID=@0", id);
                    _uow.Commit();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public UserRewardItemActivity_DataEntity SearchByUsername(UserRewardItemActivity_DataEntity entity, string username)
        {
            var result = new UserRewardItemActivity_DataEntity();
            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(username))
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM UserRewardItemActivity WHERE UserID like '%" + username + "%'");
                        var data = _uow.Db.Page<UserRewardItemActivityEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }
                    else
                    {
                        var query = PetaPoco.Sql.Builder.Append("SELECT * FROM UserRewardItemActivity WHERE ID>0");
                        var data = _uow.Db.Page<UserRewardItemActivityEntity>(entity.PageIndex, entity.PageSize, query);
                        if (data != null && data.TotalItems > 0)
                        {
                            result.List = data.Items;
                            result.PageTotal = data.TotalPages;
                            result.PageIndex = data.CurrentPage;
                            result.Count = Protector.Int(data.TotalItems);
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
    }
}
