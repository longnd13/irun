﻿using Prj.Irun.Repositories.Entities;
using Prj.Irun.Repositories.Interfaces;
using Prj.Irun.Repositories.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Implementations
{
   public class testRepository : ItestRepository
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        public testRepository(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
        }


        public List<testEntity> getData()
        {

            try
            {
                using (var _uow = _unitOfWorkProvider.GetUnitOfWork())
                {
                    var result = _uow.Db.Fetch<testEntity>(@"SELECT * FROM test");
                    _uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {

                ex.ToString();
            }

            return new List<testEntity>();
        }
    }
}
