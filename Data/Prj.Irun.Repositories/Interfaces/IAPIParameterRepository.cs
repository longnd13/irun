﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
  public interface IAPIParameterRepository
    {
        List<APIParameterEntity> GetAllAPIParameter();

        APIParameterEntity GetAPIParameterByCodeID(string code_id);
    }
}
