﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IAccountFeatureRepository
    {
        AdminCPFeatureEntity Add(AdminCPFeatureEntity entity);
        AdminCPFeatureEntity GetById(int id);
        AdminCPFeatureEntity Update(AdminCPFeatureEntity entity);
        AdminCPFeature_DataEntity GetAll(AdminCPFeature_DataEntity entity);
        bool Delete(int id);
    }
}
