﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Prj.Irun.Utilities.EnumHandler;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IAccountRepository
    {
        // 18/9/2018
        bool UpdatedAccessTokenExpired(AccountInfoEntity account_info_entity);


        AccountInfoEntity GetByAccessToken(string access_token);
        AccountInfoEntity GetByUserName(string user_name);
        int CheckAccountExistByUserName(string user_name);
        AccountInfoEntity Register(AccountInfoEntity account_entity);
      
        AccountInfoEntity GetAccountByUserNameAndAccessToken(string user_name, string access_token);
        int UpdatePassword(string user_name, string new_password, string access_token);
        AccountInfoEntity CheckUserNameAndPassword( string user_name, string old_password, string access_token);
        AccountInfoEntity CheckAccountLogin(string user_name, string password);
        AccountSocialEntity GetExisted(string social_id, LoginProviderType provider);

        // gold
        AccountInfoEntity UpdatedGoldByUser(string user_name, long gold);
        LogsGoldAccountEntity WriteLogGoldUser(LogsGoldAccountEntity entity);

        //OTP
        int OnOffOTP(int id);

        AccountInfoEntity UpdateAccountInfo(AccountInfoEntity account_info_entity, string userID);
    }
}
