﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IApiConfigRepository
    {
        ApiConfigEntity Add(ApiConfigEntity entity);
        ApiConfigEntity GetById(int id);
        ApiConfigEntity Update(ApiConfigEntity entity);
        ApiConfig_DataEntity GetAll(ApiConfig_DataEntity entity);
        bool Delete(int id);
    }
}
