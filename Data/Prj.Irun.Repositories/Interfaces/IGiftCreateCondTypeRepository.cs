﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IGiftCreateCondTypeRepository
    {
        GiftCreateCondTypeEntity GetGiftCreateCondTypeByCodeID(string code_id);
    }
}
