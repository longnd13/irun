﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IMainCoreRepository
    {
        StepsEntity AddSteps(StepsEntity step_entity);

        DistanceEntity AddDistance(DistanceEntity distance_entity);

        ConfigEntity GetConfigById(int id);

        List<ConfigEntity> GetConfig();
     
        SurplusStepsDistanceEntity GetStepsDistanceByUserName(string user_name);

      

        List<StepsEntity> GetStepsByUserName(string user_name);
        List<StepsEntity> GetStepsByUserName(string user_name, int day, int year);

        List<DistanceEntity> GetDistanceByUserName(string user_name);
        List<DistanceEntity> GetDistanceByUserName(string user_name, int day, int year);

        bool SaveGiftBySteps(List<UserGiftEntity> user_gift_entity, List<RewardUserGiftEntity> reward_user_gift_entity, string UserID, int steps);
        bool SaveGiftByDistance(List<UserGiftEntity> user_gift_entity, List<RewardUserGiftEntity> reward_user_gift_entity, string UserID, int distance);

        bool OpenBasicGift(string user_id, int user_gift_id);

        bool SaveDailyGift(List<UserGiftEntity> user_gift_entity, List<RewardUserGiftEntity> reward_user_gift_entity, string UserID);

        int CheckDailyGiftByUserID(string user_id, string created_date_string);
    }
}
