﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IPrivacySettingRepository
    {
        PrivacySettingEntity Add(PrivacySettingEntity entity);
        PrivacySettingEntity GetById(int id);
        PrivacySettingEntity Update(PrivacySettingEntity entity);
        PrivacySetting_DataEntity GetAll(PrivacySetting_DataEntity entity);
        bool Delete(int id);
    }
}
