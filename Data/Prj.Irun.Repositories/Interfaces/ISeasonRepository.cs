﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface ISeasonRepository
    {
        SeasonEntity Add(SeasonEntity entity);
        SeasonEntity GetById(int id);
        SeasonEntity Update(SeasonEntity entity);
        Season_DataEntity GetAll(Season_DataEntity entity);
        bool Delete(int id);
    }
}
