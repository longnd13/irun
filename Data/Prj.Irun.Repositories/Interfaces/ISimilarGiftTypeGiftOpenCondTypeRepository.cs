﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface ISimilarGiftTypeGiftOpenCondTypeRepository
    {
        SimilarGiftTypeGiftOpenCondTypeEntity Add(SimilarGiftTypeGiftOpenCondTypeEntity entity);
        SimilarGiftTypeGiftOpenCondTypeEntity GetById(int id);
        SimilarGiftTypeGiftOpenCondTypeEntity Update(SimilarGiftTypeGiftOpenCondTypeEntity entity);
        SimilarGiftTypeGiftOpenCondType_DataEntity GetAll(SimilarGiftTypeGiftOpenCondType_DataEntity entity);
    }
}
