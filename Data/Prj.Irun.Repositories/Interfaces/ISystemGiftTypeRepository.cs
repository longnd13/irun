﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface ISystemGiftTypeRepository
    {
        List<SystemGiftTypeEntity> GetAllSystemGiftType();
        SystemGiftTypeEntity GetSystemGiftTypeByCodeID(string code_id);

    }
}
