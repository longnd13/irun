﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
   public interface IapiUserGiftRepository
    {
        UserGiftEntity GetUserGiftByID(int user_gift_id, string user_id);
        GiftOpenCondTypeEntity GetGiftOpenCondTypeBySimilarGiftTypeCodeID(string similar_gift_type_code_id);
        List<UserGiftEntity> GetDailyGiftByUserID(string user_id, string system_gift_codeID, string user_gift_status_codeID, string created_date_string);
        bool OpenDailyGift(string user_id, int user_gift_id);

        int CountUserGiftBySystemGiftTypeCodeID(string system_gift_type_codeID);

        UserGiftEntity AddEntity(UserGiftEntity entity);

        UserGiftEntity GetUserGift(string user_id, string system_gift_codeID, string user_gift_status_codeID);

        bool SaveUserGift(List<UserGiftEntity> user_gift_entity, List<RewardUserGiftEntity> reward_user_gift_entity, string UserID);
    }
}
