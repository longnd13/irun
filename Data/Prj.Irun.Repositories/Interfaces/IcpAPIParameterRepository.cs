﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpAPIParameterRepository
    {
        APIParameterEntity Add(APIParameterEntity entity);
        APIParameterEntity GetById(string CodeID);
        APIParameterEntity Update(APIParameterEntity entity);
        APIParameter_DataEntity GetAll(APIParameter_DataEntity entity);
        bool Delete(string CodeID);
    }
}
