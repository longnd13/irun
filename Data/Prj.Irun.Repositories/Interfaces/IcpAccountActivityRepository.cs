﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpAccountActivityRepository
    {
        AccountActivity_DataEntity GetAll(AccountActivity_DataEntity entity);
        AccountActivity_DataEntity SearchByUserID(AccountActivity_DataEntity entity, string userID);
        bool Delete(int id);
    }
}
