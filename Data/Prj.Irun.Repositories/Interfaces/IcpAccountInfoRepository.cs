﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpAccountInfoRepository
    {
        AccountInfo_DataEntity GetAll(AccountInfo_DataEntity entity);
        AccountInfo_DataEntity SearchByUsername(AccountInfo_DataEntity entity, string username, string fullname);
        AccountInfo_DataEntity GetAllBySort(AccountInfo_DataEntity entity, string goldSort, string stepSort, string distanceSort);
        bool Delete(int id);
        AccountInfoEntity Add(AccountInfoEntity entity);
        AccountInfoEntity GetById(int id);
        AccountInfoEntity Update(AccountInfoEntity entity);
    }
}
