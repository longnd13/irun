﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpAdminActivityRepository
    {
        AdminActivity_DataEntity GetAll(AdminActivity_DataEntity entity);
        AdminActivity_DataEntity SearchByAdminId(AdminActivity_DataEntity entity, int adminID);
        bool Delete(int id);
    }
}
