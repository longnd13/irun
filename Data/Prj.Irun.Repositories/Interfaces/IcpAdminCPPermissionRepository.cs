﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpAdminCPPermissionRepository
    {
        AdminCPPermissionEntity Add(AdminCPPermissionEntity entity);
        AdminCPPermissionEntity GetById(int id);
        AdminCPPermissionEntity Update(AdminCPPermissionEntity entity);
        AdminCPPermission_DataEntity GetAll(AdminCPPermission_DataEntity entity);
        bool Delete(string userName);
        MessageEntity GetPermission(string userName);
    }
}
