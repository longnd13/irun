﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpAdminCpRepository
    {
        AdminCpEntity Add(AdminCpEntity entity);

        AdminCpEntity GetById(int id);

        AdminCpEntity Update(AdminCpEntity entity);

        AdminCp_DataEntity GetAll(AdminCp_DataEntity entity);

        bool Delete(int id);

        AdminCpEntity Authenticate(string user_name, string password);
    }
}
