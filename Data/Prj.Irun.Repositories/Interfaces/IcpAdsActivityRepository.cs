﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpAdsActivityRepository
    {
        AdsActivity_DataEntity GetAll(AdsActivity_DataEntity entity);
        AdsActivity_DataEntity SearchByAdsCodeID(AdsActivity_DataEntity entity, string adsCodeID);
        bool Delete(string adsCodeID);
    }
}
