﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpAdsTypeRepository
    {
        AdsTypeEntity Add(AdsTypeEntity entity);
        AdsTypeEntity GetById(string codeID);
        AdsTypeEntity Update(AdsTypeEntity entity);
        AdsType_DataEntity GetAll(AdsType_DataEntity entity);
        bool Delete(string codeID);
    }
}
