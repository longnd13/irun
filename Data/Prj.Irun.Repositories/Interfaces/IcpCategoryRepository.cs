﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpCategoryRepository
    {
        CategoryEntity Add(CategoryEntity entity);
        CategoryEntity GetById(int id);
        CategoryEntity Update(CategoryEntity entity);
        Category_DataEntity GetAll(Category_DataEntity entity);
        bool Delete(int id);
    }
}
