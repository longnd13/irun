﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpChallengeActivityRepository
    {
        ChallengeActivity_DataEntity GetAll(ChallengeActivity_DataEntity entity);
        ChallengeActivity_DataEntity SearchByUserChallengeID(ChallengeActivity_DataEntity entity, int userChallengeID);
        bool Delete(int id);
    }
}
