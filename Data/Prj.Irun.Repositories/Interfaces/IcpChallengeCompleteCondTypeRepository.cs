﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpChallengeCompleteCondTypeRepository
    {
        ChallengeCompleteCondTypeEntity Add(ChallengeCompleteCondTypeEntity entity);
        ChallengeCompleteCondTypeEntity GetById(int id);
        ChallengeCompleteCondTypeEntity Update(ChallengeCompleteCondTypeEntity entity);
        ChallengeCompleteCondType_DataEntity GetAll(ChallengeCompleteCondType_DataEntity entity);
        bool Delete(int id);
    }
}
