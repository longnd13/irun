﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpChallengeTypeRepository
    {
        ChallengeTypeEntity Add(ChallengeTypeEntity entity);
        ChallengeTypeEntity GetById(int id);
        ChallengeTypeEntity Update(ChallengeTypeEntity entity);
        ChallengeType_DataEntity GetAll(ChallengeType_DataEntity entity);
        bool Delete(int id);
    }
}
