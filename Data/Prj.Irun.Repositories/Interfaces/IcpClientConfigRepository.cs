﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpClientConfigRepository
    {
        ClientConfigEntity Add(ClientConfigEntity entity);
        ClientConfigEntity GetById(string CodeID);
        ClientConfigEntity Update(ClientConfigEntity entity);
        ClientConfig_DataEntity GetAll(ClientConfig_DataEntity entity);
        bool Delete(string CodeID);
    }
}
