﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpCommunityActivityRepository
    {
        CommunityActivity_DataEntity GetAll(CommunityActivity_DataEntity entity);
        CommunityActivity_DataEntity SearchByUserID(CommunityActivity_DataEntity entity, string userID);
        bool Delete(int id);
    }
}
