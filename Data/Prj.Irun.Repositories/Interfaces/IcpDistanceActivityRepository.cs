﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpDistanceActivityRepository
    {
        DistanceActivity_DataEntity GetAll(DistanceActivity_DataEntity entity);
        DistanceActivity_DataEntity SearchByUserID(DistanceActivity_DataEntity entity, string userID);
        bool Delete(int id);
        DistanceActivityEntity Add(DistanceActivityEntity entity);
        DistanceActivityEntity GetById(int id);
        DistanceActivityEntity Update(DistanceActivityEntity entity);
    }
}
