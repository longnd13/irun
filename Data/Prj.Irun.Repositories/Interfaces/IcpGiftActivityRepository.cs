﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpGiftActivityRepository
    {
        GiftActivity_DataEntity GetAll(GiftActivity_DataEntity entity);
        GiftActivity_DataEntity SearchByUserGiftID(GiftActivity_DataEntity entity, int userGiftID);
        bool Delete(int id);
    }
}
