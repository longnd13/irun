﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpGiftComponentTypeRepository
    {
        GiftComponentTypeEntity Add(GiftComponentTypeEntity entity);
        GiftComponentTypeEntity GetById(string CodeID);
        GiftComponentTypeEntity Update(GiftComponentTypeEntity entity);
        GiftComponentType_DataEntity GetAll(GiftComponentType_DataEntity entity);
        bool Delete(string CodeID);
        GiftComponentType_DataEntity MultipleSelectedGift(GiftComponentType_DataEntity entity, string code_id);
    }
}
