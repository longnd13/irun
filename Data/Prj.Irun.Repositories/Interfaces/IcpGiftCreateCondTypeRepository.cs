﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpGiftCreateCondTypeRepository
    {
        GiftCreateCondTypeEntity Add(GiftCreateCondTypeEntity entity);
        GiftCreateCondTypeEntity GetById(string CodeID);
        GiftCreateCondTypeEntity Update(GiftCreateCondTypeEntity entity);
        GiftCreateCondType_DataEntity GetAll(GiftCreateCondType_DataEntity entity);
        bool Delete(string CodeID);
        GiftCreateCondType_DataEntity MultipleSelectedGift(GiftCreateCondType_DataEntity entity, string code_id);
    }
}
