﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpGiftOpenCondTypeRepository
    {
        GiftOpenCondTypeEntity Add(GiftOpenCondTypeEntity entity);
        GiftOpenCondTypeEntity GetById(int id);
        GiftOpenCondTypeEntity Update(GiftOpenCondTypeEntity entity);
        GiftOpenCondType_DataEntity GetAll(GiftOpenCondType_DataEntity entity);
        bool Delete(int id);
        GiftOpenCondTypeEntity GetByCodeId(string code_id);
    }
}
