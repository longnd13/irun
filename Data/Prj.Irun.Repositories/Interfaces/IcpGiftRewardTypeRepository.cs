﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpGiftRewardTypeRepository
    {
        GiftRewardTypeEntity Add(GiftRewardTypeEntity entity);
        GiftRewardTypeEntity GetById(string Code);
        GiftRewardTypeEntity Update(GiftRewardTypeEntity entity);
        GiftRewardType_DataEntity GetAll(GiftRewardType_DataEntity entity);
        bool Delete(string Code);
    }
}
