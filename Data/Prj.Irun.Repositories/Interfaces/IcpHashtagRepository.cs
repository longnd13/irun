﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpHashtagRepository
    {
        HashtagEntity Add(HashtagEntity entity);
        HashtagEntity GetById(int id);
        HashtagEntity Update(HashtagEntity entity);
        Hashtag_DataEntity GetAll(Hashtag_DataEntity entity);
        bool Delete(int id);
    }
}
