﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpIgoldActivityRepository
    {
        IgoldActivity_DataEntity GetAll(IgoldActivity_DataEntity entity);
        IgoldActivity_DataEntity SearchByUserID(IgoldActivity_DataEntity entity, string userID);
        bool Delete(int id);
    }
}
