﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpJourneyActivityRepository
    {
        JourneyActivity_DataEntity GetAll(JourneyActivity_DataEntity entity);
        JourneyActivity_DataEntity SearchByUserID(JourneyActivity_DataEntity entity, string userID);
        bool Delete(int id);
    }
}
