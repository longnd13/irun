﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpMovementActivityRepository
    {
        MovementActivity_DataEntity GetAll(MovementActivity_DataEntity entity);
        MovementActivity_DataEntity SearchByUserID(MovementActivity_DataEntity entity, string userID);
        bool Delete(int id);
    }
}
