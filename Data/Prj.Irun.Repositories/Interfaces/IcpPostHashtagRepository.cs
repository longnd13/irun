﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpPostHashtagRepository
    {
        PostHashtagEntity Add(PostHashtagEntity entity);
        PostHashtagEntity GetById(int id);
        PostHashtagEntity Update(PostHashtagEntity entity);
        PostHashtag_DataEntity GetAll(PostHashtag_DataEntity entity);
        bool Delete(int id);
    }
}
