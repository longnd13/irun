﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpPostLikeRepository
    {
        PostLikeEntity Add(PostLikeEntity entity);
        PostLikeEntity GetById(int id);
        PostLikeEntity Update(PostLikeEntity entity);
        PostLike_DataEntity GetAll(PostLike_DataEntity entity);
        bool Delete(int id);
    }
}
