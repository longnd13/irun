﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpPostPicRepository
    {
        PostPicEntity Add(PostPicEntity entity);
        PostPicEntity GetById(int id);
        PostPicEntity Update(PostPicEntity entity);
        PostPic_DataEntity GetAll(PostPic_DataEntity entity);
        bool Delete(int id);
        PostPic_DataEntity SearchById(PostPic_DataEntity entity, int id);
    }
}
