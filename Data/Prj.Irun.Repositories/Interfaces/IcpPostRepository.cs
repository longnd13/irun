﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpPostRepository
    {
        Post_DataEntity GetAll(Post_DataEntity entity);
        Post_DataEntity SearchByUsername(Post_DataEntity entity, string username);
        bool Delete(int id);
        bool ChangeStatus(int id);
        PostEntity GetById(int id);
    }
}
