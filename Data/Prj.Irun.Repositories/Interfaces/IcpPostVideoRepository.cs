﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpPostVideoRepository
    {
        PostVideoEntity Add(PostVideoEntity entity);
        PostVideoEntity GetById(int id);
        PostVideoEntity Update(PostVideoEntity entity);
        PostVideo_DataEntity GetAll(PostVideo_DataEntity entity);
        bool Delete(int id);
        PostVideo_DataEntity SearchById(PostVideo_DataEntity entity, int id);
    }
}
