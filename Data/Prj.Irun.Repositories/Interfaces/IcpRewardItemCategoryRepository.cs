﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpRewardItemCategoryRepository
    {
        RewardItemCategoryEntity Add(RewardItemCategoryEntity entity);
        RewardItemCategoryEntity GetById(int id);
        RewardItemCategoryEntity Update(RewardItemCategoryEntity entity);
        RewardItemCategory_DataEntity GetAll(RewardItemCategory_DataEntity entity);
        bool Delete(int id);
    }
}
