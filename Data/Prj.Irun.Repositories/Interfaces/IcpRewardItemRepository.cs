﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpRewardItemRepository
    {
        RewardItemEntity Add(RewardItemEntity entity);
        RewardItemEntity GetById(int id);
        RewardItemEntity Update(RewardItemEntity entity);
        RewardItem_DataEntity GetAll(RewardItem_DataEntity entity);
        bool Delete(int id);
    }
}
