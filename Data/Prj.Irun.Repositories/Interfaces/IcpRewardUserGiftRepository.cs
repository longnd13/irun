﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpRewardUserGiftRepository
    {
        RewardUserGift_DataEntity GetAll(RewardUserGift_DataEntity entity);
        RewardUserGift_DataEntity Search(RewardUserGift_DataEntity entity, int userGiftID, string giftRewardTypeCodeID);
        bool Delete(int id);
    }
}
