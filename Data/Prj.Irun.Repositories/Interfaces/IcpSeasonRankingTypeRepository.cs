﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpSeasonRankingTypeRepository
    {
        SeasonRankingTypeEntity Add(SeasonRankingTypeEntity entity);
        SeasonRankingTypeEntity GetById(int id);
        SeasonRankingTypeEntity Update(SeasonRankingTypeEntity entity);
        SeasonRankingType_DataEntity GetAll(SeasonRankingType_DataEntity entity);
        SeasonRankingType_DataEntity SearchByName(SeasonRankingType_DataEntity entity, string username);
        bool Delete(int id);
    }
}
