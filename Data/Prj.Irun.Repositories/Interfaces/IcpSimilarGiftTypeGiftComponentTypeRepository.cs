﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpSimilarGiftTypeGiftComponentTypeRepository
    {
        SimilarGiftTypeGiftComponentTypeEntity Add(SimilarGiftTypeGiftComponentTypeEntity entity);
        SimilarGiftTypeGiftComponentTypeEntity GetById(int id);
        SimilarGiftTypeGiftComponentTypeEntity Update(SimilarGiftTypeGiftComponentTypeEntity entity);
        SimilarGiftTypeGiftComponentType_DataEntity GetAll(SimilarGiftTypeGiftComponentType_DataEntity entity);
        bool Delete(int id);

        bool DeleteBySimilarGiftId(string code_id);
    }
}
