﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpSimilarGiftTypeGiftCreateCondTypeRepository
    {
        SimilarGiftTypeGiftCreateCondTypeEntity Add(SimilarGiftTypeGiftCreateCondTypeEntity entity);
        SimilarGiftTypeGiftCreateCondTypeEntity GetById(int id);
        SimilarGiftTypeGiftCreateCondTypeEntity Update(SimilarGiftTypeGiftCreateCondTypeEntity entity);
        SimilarGiftTypeGiftCreateCondType_DataEntity GetAll(SimilarGiftTypeGiftCreateCondType_DataEntity entity);
        SimilarGiftTypeGiftCreateCondType_DataEntity SearchBySimilarGiftId(SimilarGiftTypeGiftCreateCondType_DataEntity entity, int id);
        SimilarGiftTypeGiftCreateCondType_DataEntity SearchByGiftCreateCondTypeId(SimilarGiftTypeGiftCreateCondType_DataEntity entity, int id);
    }
}
