﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpSimilarGiftTypeRepository
    {
        SimilarGiftTypeEntity Add(SimilarGiftTypeEntity entity);
        SimilarGiftTypeEntity GetById(string CodeID);
        SimilarGiftTypeEntity GetByCreatedRate(int createdRate, string SystemGiftTypeCode);
        SimilarGiftTypeEntity Update(SimilarGiftTypeEntity entity);
        SimilarGiftType_DataEntity GetAll(SimilarGiftType_DataEntity entity);
        bool Delete(string CodeID);
        bool UpdateAll();
        bool UpdateCreateRate(string code_id, int createdRate);
        List<SimilarGiftTypeEntity> SearchBySystemGiftType(string systemGiftTypeCodeID);
        List<SimilarGiftTypeEntity> GetAllSystemGiftType();
    }
}
