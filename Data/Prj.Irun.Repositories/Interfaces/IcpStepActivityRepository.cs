﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpStepActivityRepository
    {
        StepActivity_DataEntity GetAll(StepActivity_DataEntity entity);
        StepActivity_DataEntity SearchByUserID(StepActivity_DataEntity entity, string userID);
        bool Delete(int id);
        StepActivityEntity Add(StepActivityEntity entity);
        StepActivityEntity GetById(int id);
        StepActivityEntity Update(StepActivityEntity entity);
    }
}
