﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpSystemGiftTypeGiftCreateCondTypeRepository
    {
        SystemGiftTypeGiftCreateCondTypeEntity Add(SystemGiftTypeGiftCreateCondTypeEntity entity);
        SystemGiftTypeGiftCreateCondTypeEntity GetById(int id);
        SystemGiftTypeGiftCreateCondTypeEntity Update(SystemGiftTypeGiftCreateCondTypeEntity entity);
        SystemGiftTypeGiftCreateCondType_DataEntity GetAll(SystemGiftTypeGiftCreateCondType_DataEntity entity);
        SystemGiftTypeGiftCreateCondType_DataEntity SearchBySimilarGiftId(SystemGiftTypeGiftCreateCondType_DataEntity entity, int id);
        SystemGiftTypeGiftCreateCondType_DataEntity SearchByGiftCreateCondTypeId(SystemGiftTypeGiftCreateCondType_DataEntity entity, int id);
        bool DeleteBySystemGiftId(string code_id);
        bool DeleteByGiftCreateCondTypeId(string code_id);
    }
}
