﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
  public  interface IcpSystemGiftTypeRepository
    {
        SystemGiftTypeEntity Add(SystemGiftTypeEntity entity);

        bool Delete(string CodeID);

        SystemGiftType_DataEntity GetAll(SystemGiftType_DataEntity entity);

        SystemGiftTypeEntity GetByCodeId(string code_id);

        SystemGiftTypeEntity GetById(int id);

        SystemGiftTypeEntity Update(SystemGiftTypeEntity entity);

        SystemGiftType_DataEntity MultipleSelectedGift(SystemGiftType_DataEntity entity, int id);
    }
}
