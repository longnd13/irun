﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpUserChallengeRepository
    {
        UserChallengeEntity Add(UserChallengeEntity entity);
        UserChallengeEntity GetById(int id);
        UserChallengeEntity Update(UserChallengeEntity entity);
        UserChallenge_DataEntity GetAll(UserChallenge_DataEntity entity);
        bool Delete(int id);
    }
}
