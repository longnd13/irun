﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpUserFollowRepository
    {
        UserFollowEntity Add(UserFollowEntity entity);
        UserFollowEntity GetById(int id);
        UserFollowEntity Update(UserFollowEntity entity);
        UserFollow_DataEntity GetAll(UserFollow_DataEntity entity);
        bool Delete(int id);
        UserFollow_DataEntity SearchByUsername(UserFollow_DataEntity entity, string username);
    }
}
