﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpUserGiftRepository
    {
        UserGift_DataEntity GetAll(UserGift_DataEntity entity);
        UserGift_DataEntity Search(UserGift_DataEntity entity, string SystemGiftTypeCodeID, string SimilarGiftTypeID);
        bool Delete(int id);
        UserGiftEntity GetById(int id);
    }
}
