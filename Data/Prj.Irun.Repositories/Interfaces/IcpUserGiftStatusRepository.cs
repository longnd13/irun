﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpUserGiftStatusRepository
    {
        UserGiftStatusEntity Add(UserGiftStatusEntity entity);
        UserGiftStatusEntity GetById(int id);
        UserGiftStatusEntity Update(UserGiftStatusEntity entity);
        UserGiftStatus_DataEntity GetAll(UserGiftStatus_DataEntity entity);
        bool Delete(int id);
    }
}
