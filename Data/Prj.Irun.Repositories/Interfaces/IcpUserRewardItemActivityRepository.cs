﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpUserRewardItemActivityRepository
    {
        UserRewardItemActivity_DataEntity GetAll(UserRewardItemActivity_DataEntity entity);
        UserRewardItemActivity_DataEntity SearchByUsername(UserRewardItemActivity_DataEntity entity, string username);
        bool Delete(int id);
    }
}
