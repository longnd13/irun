﻿using Prj.Irun.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Repositories.Interfaces
{
    public interface IcpUserRewardItemTypeRepository
    {
        UserRewardItemTypeEntity Add(UserRewardItemTypeEntity entity);
        UserRewardItemTypeEntity GetById(int id);
        UserRewardItemTypeEntity Update(UserRewardItemTypeEntity entity);
        UserRewardItemType_DataEntity GetAll(UserRewardItemType_DataEntity entity);
        bool Delete(int id);
    }
}
