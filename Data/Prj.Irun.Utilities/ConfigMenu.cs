﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Utilities
{
    public class ConfigMenu
    {
        public static string ActviedMenu(string Group, string url)
        {
            string active = "";
            // AdminCP
            if (Group == "AdminCP" && (url.ToLower().Contains("/admin/admincp/index") ||
                                       url.ToLower().Contains("/admin/admincp/createdadmin") ||
                                       url.ToLower().Contains("/admin/admincp/changepassword")))
            {
                active = "active";
            }
            else if (Group == "AdminCPIndex" && url.ToLower().Contains("/admin/admincp/index"))
            {
                active = "active";
            }
            else if (Group == "AdminCPCreate" && url.ToLower().Contains("/admin/admincp/createdadmin"))
            {
                active = "active";
            }
            else if (Group == "AdminCPChangePassword" && url.ToLower().Contains("/admin/admincp/changepassword"))
            {
                active = "active";
            }

            // AccountInfo
            if (Group == "AccountInfo" && (url.ToLower().Contains("/admin/accountinfo/index") ||
                                           url.ToLower().Contains("/admin/accountinfo/add") ||
                                           url.ToLower().Contains("/admin/usergift/add")))
            {
                active = "active";
            }
            else if (Group == "AccountInfoAccountIndex" && url.ToLower().Contains("/admin/accountinfo/index"))
            {
                active = "active";
            }
            else if (Group == "AccountInfoAccountIndex" && url.ToLower().Contains("/admin/accountinfo/add"))
            {
                active = "active";
            }
            else if (Group == "UserGiftIndex" && url.ToLower().Contains("/admin/usergift/add"))
            {
                active = "active";
            }

            // Gift  UserGiftStatus
            if (Group == "Gift" && (url.ToLower().Contains("/admin/systemgifttype/index") ||
                                           url.ToLower().Contains("/admin/systemgifttype/add") ||
                                           url.ToLower().Contains("/admin/systemgifttype/update") ||

                                           url.ToLower().Contains("/admin/giftcreatecondtype/index") ||
                                           url.ToLower().Contains("/admin/giftcreatecondtype/add") ||
                                           url.ToLower().Contains("/admin/giftcreatecondtype/update") ||

                                           url.ToLower().Contains("/admin/giftcomponenttype/index") ||
                                           url.ToLower().Contains("/admin/giftcomponenttype/add") ||
                                           url.ToLower().Contains("/admin/giftcomponenttype/update") ||

                                           url.ToLower().Contains("/admin/giftrewardtype/index") ||
                                           url.ToLower().Contains("/admin/giftrewardtype/add") ||
                                           url.ToLower().Contains("/admin/giftrewardtype/update") ||

                                           url.ToLower().Contains("/admin/similargifttype/index") ||
                                           url.ToLower().Contains("/admin/similargifttype/update") ||
                                           url.ToLower().Contains("/admin/similargifttype/add") ||

                                           url.ToLower().Contains("/admin/usergiftstatus/index") ||
                                           url.ToLower().Contains("/admin/usergiftstatus/update") ||
                                           url.ToLower().Contains("/admin/usergiftstatus/add") ||

                                             url.ToLower().Contains("/admin/giftopencondtype/index") ||
                                             url.ToLower().Contains("/admin/giftopencondtype/update") ||
                                             url.ToLower().Contains("/admin/giftopencondtype/add")))


            {
                active = "active";
            }
            else if (Group == "GiftSystemGiftTypeIndex" && url.ToLower().Contains("/admin/systemgifttype/index"))
            {
                active = "active";
            }
            else if (Group == "GiftSystemGiftTypeIndex" && url.ToLower().Contains("/admin/systemgifttype/add"))
            {
                active = "active";
            }
            else if (Group == "GiftSystemGiftTypeIndex" && url.ToLower().Contains("/admin/systemgifttype/update"))
            {
                active = "active";
            }

            else if (Group == "GiftGiftCreateCondTypeIndex" && url.ToLower().Contains("/admin/giftcreatecondtype/index"))
            {
                active = "active";
            }
            else if (Group == "GiftGiftCreateCondTypeIndex" && url.ToLower().Contains("/admin/giftcreatecondtype/add"))
            {
                active = "active";
            }
            else if (Group == "GiftGiftCreateCondTypeIndex" && url.ToLower().Contains("/admin/giftcreatecondtype/update"))
            {
                active = "active";
            }

            else if (Group == "GiftComponentTypeIndex" && url.ToLower().Contains("/admin/giftcomponenttype/index"))
            {
                active = "active";
            }
            else if (Group == "GiftComponentTypeIndex" && url.ToLower().Contains("/admin/giftcomponenttype/add"))
            {
                active = "active";
            }
            else if (Group == "GiftComponentTypeIndex" && url.ToLower().Contains("/admin/giftcomponenttype/update"))
            {
                active = "active";
            }

            else if (Group == "GiftRewardTypeIndex" && url.ToLower().Contains("/admin/giftrewardtype/index"))
            {
                active = "active";
            }
            else if (Group == "GiftRewardTypeIndex" && url.ToLower().Contains("/admin/giftrewardtype/add"))
            {
                active = "active";
            }
            else if (Group == "GiftRewardTypeIndex" && url.ToLower().Contains("/admin/giftrewardtype/update"))
            {
                active = "active";
            }

            else if (Group == "SimilarGiftTypeIndex" && url.ToLower().Contains("/admin/similargifttype/index"))
            {
                active = "active";
            }
            else if (Group == "SimilarGiftTypeIndex" && url.ToLower().Contains("/admin/similargifttype/add"))
            {
                active = "active";
            }
            else if (Group == "SimilarGiftTypeIndex" && url.ToLower().Contains("/admin/similargifttype/update"))
            {
                active = "active";
            }

            else if (Group == "UserGiftStatusIndex" && url.ToLower().Contains("/admin/usergiftstatus/index"))
            {
                active = "active";
            }
            else if (Group == "UserGiftStatusIndex" && url.ToLower().Contains("/admin/usergiftstatus/add"))
            {
                active = "active";
            }
            else if (Group == "UserGiftStatusIndex" && url.ToLower().Contains("/admin/usergiftstatus/update"))
            {
                active = "active";
            }

            else if (Group == "GiftOpenCondTypeIndex" && url.ToLower().Contains("/admin/giftopencondtype/index"))
            {
                active = "active";
            }
            else if (Group == "GiftOpenCondTypeIndex" && url.ToLower().Contains("/admin/giftopencondtype/add"))
            {
                active = "active";
            }
            else if (Group == "GiftOpenCondTypeIndex" && url.ToLower().Contains("/admin/giftopencondtype/update"))
            {
                active = "active";
            }


            // Config
            if (Group == "Config" && (url.ToLower().Contains("/admin/apiconfig/index") ||
                                      url.ToLower().Contains("/admin/apiconfig/add") ||
                                      url.ToLower().Contains("/admin/apiconfig/update") ||
                                      url.ToLower().Contains("/admin/clientconfig/index") ||
                                      url.ToLower().Contains("/admin/clientconfig/add") ||
                                      url.ToLower().Contains("/admin/clientconfig/update") ||
                                      url.ToLower().Contains("/admin/privacysetting/index") ||
                                      url.ToLower().Contains("/admin/privacysetting/add") ||
                                      url.ToLower().Contains("/admin/privacysetting/update")
                                      ||
                                      url.ToLower().Contains("/admin/apiparameter/index")
                                      ||
                                      url.ToLower().Contains("/admin/apiparameter/add")
                                      ||
                                      url.ToLower().Contains("/admin/apiparameter/update")))
            {
                active = "active";
            }
            else if (Group == "ConfigClientConfig" && url.ToLower().Contains("/admin/clientconfig/index"))
            {
                active = "active";
            }
            else if (Group == "ConfigClientConfig" && url.ToLower().Contains("/admin/clientconfig/add"))
            {
                active = "active";
            }
            else if (Group == "ConfigClientConfig" && url.ToLower().Contains("/admin/clientconfig/update"))
            {
                active = "active";
            }

            else if (Group == "ConfigAPIConfig" && url.ToLower().Contains("/admin/apiconfig/index"))
            {
                active = "active";
            }
            else if (Group == "ConfigAPIConfig" && url.ToLower().Contains("/admin/apiconfig/add"))
            {
                active = "active";
            }
            else if (Group == "ConfigAPIConfig" && url.ToLower().Contains("/admin/apiconfig/update"))
            {
                active = "active";
            }

            else if (Group == "PrivacySetting" && url.ToLower().Contains("/admin/privacysetting/index"))
            {
                active = "active";
            }
            else if (Group == "PrivacySetting" && url.ToLower().Contains("/admin/privacysetting/addd"))
            {
                active = "active";
            }
            else if (Group == "PrivacySetting" && url.ToLower().Contains("/admin/privacysetting/update"))
            {
                active = "active";
            }

            else if (Group == "APIParameter" && url.ToLower().Contains("/admin/apiparameter/index"))
            {
                active = "active";
            }
            else if (Group == "APIParameter" && url.ToLower().Contains("/admin/apiparameter/addd"))
            {
                active = "active";
            }
            else if (Group == "APIParameter" && url.ToLower().Contains("/admin/apiparameter/update"))
            {
                active = "active";
            }

            // Season
            if (Group == "Season" && (url.ToLower().Contains("/admin/season/index") ||
                                           url.ToLower().Contains("/admin/season/add") ||
                                           url.ToLower().Contains("/admin/season/update") ||
                                           url.ToLower().Contains("/admin/seasonrankingtypeindex/index") ||
                                            url.ToLower().Contains("/admin/seasonrankingtypeindex/add") ||
                                             url.ToLower().Contains("/admin/seasonrankingtypeindex/update")))
            {
                active = "active";
            }
            else if (Group == "SeasonIndex" && url.ToLower().Contains("/admin/season/index"))
            {
                active = "active";
            }
            else if (Group == "SeasonIndex" && url.ToLower().Contains("/admin/season/add"))
            {
                active = "active";
            }
            else if (Group == "SeasonIndex" && url.ToLower().Contains("/admin/season/update"))
            {
                active = "active";
            }

            else if (Group == "SeasonRankingTypeIndex" && url.ToLower().Contains("/admin/seasonrankingtype/index"))
            {
                active = "active";
            }
            else if (Group == "SeasonRankingTypeIndex" && url.ToLower().Contains("/admin/seasonrankingtype/add"))
            {
                active = "active";
            }
            else if (Group == "SeasonRankingTypeIndex" && url.ToLower().Contains("/admin/seasonrankingtype/update"))
            {
                active = "active";
            }

            // Challenge
            if (Group == "Challenge" && (url.ToLower().Contains("/admin/challengetype/index") ||
                                           url.ToLower().Contains("/admin/challengetype/add") ||
                                           url.ToLower().Contains("/admin/challengetype/update")
                                           ||
                                           url.ToLower().Contains("/admin/challengecompletecondtype/index")
                                           ||
                                           url.ToLower().Contains("/admin/challengecompletecondtype/add")
                                           ||
                                           url.ToLower().Contains("/admin/challengecompletecondtype/update")))
            {
                active = "active";
            }

            else if (Group == "ChallengeTypeIndex" && url.ToLower().Contains("/admin/challengetype/index"))
            {
                active = "active";
            }
            else if (Group == "ChallengeTypeIndex" && url.ToLower().Contains("/admin/challengetype/add"))
            {
                active = "active";
            }
            else if (Group == "ChallengeTypeIndex" && url.ToLower().Contains("/admin/challengetype/update"))
            {
                active = "active";
            }

            else if (Group == "ChallengeCompleteCondTypeIndex" && url.ToLower().Contains("/admin/challengecompletecondtype/index"))
            {
                active = "active";
            }
            else if (Group == "ChallengeCompleteCondTypeIndex" && url.ToLower().Contains("/admin/challengecompletecondtype/add"))
            {
                active = "active";
            }
            else if (Group == "ChallengeCompleteCondTypeIndex" && url.ToLower().Contains("/admin/challengecompletecondtype/update"))
            {
                active = "active";
            }
            // Activity
            if (Group == "Acitvity" && (url.ToLower().Contains("/admin/accountactivity/index") ||
                                       url.ToLower().Contains("/admin/adsactivity/index") ||
                                       url.ToLower().Contains("/admin/challengeactivity/index") ||
                                       url.ToLower().Contains("/admin/communityactivity/index") ||
                                       url.ToLower().Contains("/admin/distanceactivity/index") ||
                                       url.ToLower().Contains("/admin/stepactivity/index") ||
                                       url.ToLower().Contains("/admin/igoldactivity/index") ||
                                       url.ToLower().Contains("/admin/journeyactivity/index") ||
                                       url.ToLower().Contains("/admin/giftactivity/index")))
            {
                active = "active";
            }
            else if (Group == "AccountActivityIndex" && url.ToLower().Contains("/admin/accountactivity/index"))
            {
                active = "active";
            }
            else if (Group == "AdsActivityIndex" && url.ToLower().Contains("/admin/adsactivity/index"))
            {
                active = "active";
            }
            else if (Group == "ChallengeActivityIndex" && url.ToLower().Contains("/admin/challengeactivity/index"))
            {
                active = "active";
            }
            else if (Group == "CommunityActivityIndex" && url.ToLower().Contains("/admin/communityactivity/index"))
            {
                active = "active";
            }
            else if (Group == "DistanceActivityIndex" && url.ToLower().Contains("/admin/distanceactivity/index"))
            {
                active = "active";
            }
            else if (Group == "StepActivityIndex" && url.ToLower().Contains("/admin/stepactivity/index"))
            {
                active = "active";
            }
            else if (Group == "IgoldActivityIndex" && url.ToLower().Contains("/admin/igoldactivity/index"))
            {
                active = "active";
            }
            else if (Group == "JourneyActivityIndex" && url.ToLower().Contains("/admin/journeyactivity/index"))
            {
                active = "active";
            }
            else if (Group == "GiftActivityIndex" && url.ToLower().Contains("/admin/giftactivity/index"))
            {
                active = "active";
            }



            // ADs
            if (Group == "Ads" && (url.ToLower().Contains("/admin/adstype/index") ||
                                       url.ToLower().Contains("/admin/adstype/add") ||
                                       url.ToLower().Contains("/admin/adstype/update")))
            {
                active = "active";
            }
            else if (Group == "AdsTypeIndex" && url.ToLower().Contains("/admin/adstype/index"))
            {
                active = "active";
            }
            else if (Group == "AdsTypeIndex" && url.ToLower().Contains("/admin/adstype/add"))
            {
                active = "active";
            }
            else if (Group == "AdsTypeIndex" && url.ToLower().Contains("/admin/adstype/update"))
            {
                active = "active";
            }
            return active;
        }
    }
}
