﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Utilities
{
 public class ConstantsHandler
    {
        public static string UserGift = "UserGift";
        public static string MainCore = "MainCore";
        public static string Account = "Account";
        public static string GiftBox = "GiftBox";
        public static string Common = "Common";
        public static string SystemGiftType = "SystemGiftType";
        public static string SimilarGiftType = "SimilarGiftType";
        public static string GiftCreateCondType = "GiftCreateCondType";
        public static string AdminCP = "AdminCP";
        public static string ClientConfig = "ClientConfig";

    }
}
