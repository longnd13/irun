﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Utilities
{
  public  class EnumHandler
    {
        public enum LoginProviderType
        {
            ME = 0,
            FACEBOOK = 1,
            GOOGLE = 2,
            TWEETER = 3,
            ZALO = 4
        }

        public enum Config
        {
            Step_to_distance = 1,
            Distance_to_gold = 2,
            Distance_from_step_to_gold = 3,
            GiftBox = 4,
            OTPAccount = 5,
          
        }

        public enum ConfigTypeGiftBox
        {
            Default = 1,         
        }

        public enum ConfigActionGoldByUser
        {
            AdminPlus = 1,
            AdminMinus = 2,
        }
    }
}
