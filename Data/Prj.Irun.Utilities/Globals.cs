﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Utilities
{
    public class Globals
    {
        public static string AlertMessage(string message, bool result)
        {
            if (result.Equals(true))
            {
                return "<span style='color:Green;font-weight:bold'>" + message + "</span>";
            }
            else
            {
                return "<span style='color:REd;font-weight:bold'>" + message + "</span>";
            }
        }

        public static Guid RandomGuid()
        {
            return Guid.NewGuid();
        }
        // Lấy IP
        public static string GetIP()
        {
            string sIP = "";
            if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] == null)
            {
                sIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            else
            {
                sIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            return sIP;
        }
        public static string MD5FromString(string inputString)
        {
            return BitConverter.ToString(System.Security.Cryptography.MD5.Create().ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(inputString))).Replace("-", string.Empty);
        }

        public static decimal Chia_lay_so_du(decimal a, decimal b)
        {
            decimal result = 0;
            result = a % b;

            return result;
        }

        public static decimal Chia_lay_so_nguyen(decimal a, decimal b)
        {
            return a / b;
        }

        public static string RandomCode(int size)
        {
            Random _rng = new Random();
            string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToLower();
            char[] buffer = new char[size];
            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[_rng.Next(_chars.Length)];
            }
            return new string(buffer);
        }


        public static ResultCaculatorSteps caculatorSteps(decimal distance, decimal value)
        {

            
            var result = new ResultCaculatorSteps();

            result.gift_from_distance = distance / value;
            result.gift_receive = Protector.Int(result.gift_from_distance.ToString().Split('.')[0]);
            result.distance_surplus = distance % value;
            return result;
        }

        public class ResultCaculatorSteps
        {
            public decimal gift_from_distance { get; set; }
            public int gift_receive { get; set; }
            public decimal distance_surplus { get; set; }
        }
    }
}
