﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Utilities
{
    public class RSAHandler
    {
        public class RSAKey
        {
            public static string publicKey = "<RSAKeyValue><Modulus>2drbTqGwW2ZbOVeGM344qTzuUgkuDdo5ENS1uBuoH1JsecUtKeraoMct6J2sexLBY9Sx8YlmGlNQJu9j+X9BBF3MfwaEIhe4wmM2yPAKurDk3J12ghU/w643SXYQWv2dapuWOAIree9sS7JcniQPeNp8TsbLReRusp2eEY4ciQoDKavUuYridU6/2UTnBH9vwCReMjPwsxkcyWjQSi6w2AmzI8eShpm2QSaH/08F/ctlgObVDNiGtYmtzOi0Q18odvN2dn+Fe9GzKUGNxDiqIqnsMNPovd0VYw1sok/D/sLf4TJA+SeNU0XL13+SKJNdk5S1paYro89/eQKXfJC4qQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            public static string privateKey = "<RSAKeyValue><Modulus>2drbTqGwW2ZbOVeGM344qTzuUgkuDdo5ENS1uBuoH1JsecUtKeraoMct6J2sexLBY9Sx8YlmGlNQJu9j+X9BBF3MfwaEIhe4wmM2yPAKurDk3J12ghU/w643SXYQWv2dapuWOAIree9sS7JcniQPeNp8TsbLReRusp2eEY4ciQoDKavUuYridU6/2UTnBH9vwCReMjPwsxkcyWjQSi6w2AmzI8eShpm2QSaH/08F/ctlgObVDNiGtYmtzOi0Q18odvN2dn+Fe9GzKUGNxDiqIqnsMNPovd0VYw1sok/D/sLf4TJA+SeNU0XL13+SKJNdk5S1paYro89/eQKXfJC4qQ==</Modulus><Exponent>AQAB</Exponent><P>8QKx+AbglT9aIKbJ56X/DU7vmhj4bV8b3i7BsW1+Woz2P0VQZRVhhNrs9L8MZxP9J3bm5Oh+7KSDS0MmoEhVwq7R+iY+JbpbQfxEcJbi+QQGfO9No36FsCQ9X2xivvEv/WJV/jhgLl8qAoPoSsJ7Bjxvfd/7LqxKs+60s8EA5qc=</P><Q>52d74ztDR5q1uFEapUGw98ZzeIdS4duY+RmqB78A3VAFWZU8jzHxZ72SUrKMo3DMY74nIoiiIk8HjGU/KG1Uzl2ZBrVGlNMzHzApKfORZlEWEqiKSwFRwNH92HJi9sheEH8zjUfk5E6q2eKLPj+2mu/fVFXdnA3ffuWSBb6SoC8=</Q><DP>GlhtE+vMiQQP2hnhYE0pAE9sqa+pGGuZJESAL+EEOunF0UzGaQZZTqiP1oE73di1h6bbvpIbrcqd/5ArnMHLfiiWE34ytPUWHLRy1svnq8jg3K9cEU83VfXgL5otgSnkURJ6y/tlqCwG75dzwRhZUmkw1tWxI3z5/YlOf4fNjWk=</DP><DQ>zi0N3PA4idv2fTsGGqflHV7cE0+/MS2PpqKPNEeiZY2etqqZQpylEmF9teEQHa+q7FPIWqSSkS8I8rZjB4oiPZx3c9qgAIoMyoUGqDI0Io2stQMl9J5AUSTIn8mLE4aDCP/M1txvvFu7LlcC6XurH3EJ8wJuaVSTBWVWLXgW/FE=</DQ><InverseQ>lN1RK7rxBfPZEG2psJGKjGZMzNTMNjhSDWi/4MZ0Z/3nBFmRe3g8hmQkCfTZswjce2Wm7f5MNMWgR60UX3a6yN3JEmSFeJ5tkoRsG+1VoHl69mx0Yf1GyC6I5nmZVJ1G6f3gIfqhZVEY02DrKyTa3XuSmh7N30YaGr8dFDzu5uY=</InverseQ><D>B+vkU4yfjANBx+j90TuCteyoQcVhrlFEB++kF+ixiUoG9bqMtNx8TKJRcgbhsavx6j5epgi39LcY0AGeYgEVjSIYAHptLYwtRKw03sOTsEwkVj1HhN1e7y+ThRFM8PIN2kWEBc9WY12oU2xN39Yx1dQj9v1VgNoYiFgg2kSPlw/79fCPtJ7DXmzAGmvVevyzxTe4Frsdu/HxcQpC3+p0fLR7kjQWcviQ9pf2SfDXDwRZOq7McyANs7kfcj5scD+jzdTead86p8U84vOWAYC4s5azSkDHTaAI5YgTMRUBBdUw2Zn510Zs7T0ZB8RHROUC48Jvza3rGzPOgKLwJHXUbQ==</D></RSAKeyValue>";


        }
        public sealed class RSACryptography
        {

            #region Private Fields

            private const int KEY_SIZE = 2048; // The size of the RSA key to use in bits.
            private bool fOAEP = false;
            private RSACryptoServiceProvider rsaProvider = null;

            #endregion

            #region Constructors

            /// <summary>
            /// Initializes a new instance of the System.Security.Cryptography.RSACryptoServiceProvider
            /// class with the predefined key size and parameters.
            /// </summary>
            public RSACryptography()
            {
            }

            #endregion

            #region Private Methods

            /// <summary>
            /// Initializes a new instance of the System.Security.Cryptography.CspParameters class.
            /// </summary>
            /// <returns>An instance of the System.Security.Cryptography.CspParameters class.</returns>
            private CspParameters GetCspParameters()
            {
                // Create a new key pair on target CSP
                CspParameters cspParams = new CspParameters();
                cspParams.ProviderType = 1; // PROV_RSA_FULL 
                // cspParams.ProviderName; // CSP name
                // cspParams.Flags = CspProviderFlags.UseArchivableKey;
                cspParams.KeyNumber = (int)KeyNumber.Exchange;

                return cspParams;
            }

            /// <summary>  
            /// Gets the maximum data length for a given key  
            /// </summary>         
            /// <param name="keySize">The RSA key length  
            /// <returns>The maximum allowable data length</returns>  
            public int GetMaxDataLength()
            {
                if (fOAEP)
                    return ((KEY_SIZE - 384) / 8) + 7;
                return ((KEY_SIZE - 384) / 8) + 37;
            }

            /// <summary>  
            /// Checks if the given key size if valid  
            /// </summary>         
            /// <param name="keySize">The RSA key length  
            /// <returns>True if valid; false otherwise</returns>  
            public static bool IsKeySizeValid()
            {
                return KEY_SIZE >= 384 &&
                       KEY_SIZE <= 16384 &&
                       KEY_SIZE % 8 == 0;
            }

            #endregion

            #region Public Methods

            /// <summary>
            /// Generate a new RSA key pair.
            /// </summary>
            /// <param name="publicKey">An XML string containing ONLY THE PUBLIC RSA KEY.</param>
            /// <param name="privateKey">An XML string containing a PUBLIC AND PRIVATE RSA KEY.</param>
            public void GenerateKeys(out string publicKey, out string privateKey)
            {
                try
                {
                    CspParameters cspParams = GetCspParameters();
                    cspParams.Flags = CspProviderFlags.UseArchivableKey;

                    rsaProvider = new RSACryptoServiceProvider(KEY_SIZE, cspParams);

                    // Export public key
                    publicKey = rsaProvider.ToXmlString(false);

                    // Export private/public key pair 
                    privateKey = rsaProvider.ToXmlString(true);
                }
                catch (Exception ex)
                {
                    // Any errors? Show them
                    throw new Exception("Exception generating a new RSA key pair! More info: " + ex.Message);
                }
                finally
                {
                    // Do some clean up if needed
                }

            } // GenerateKeys method

            /// <summary>
            /// Encrypts data with the System.Security.Cryptography.RSA algorithm.
            /// </summary>
            /// <param name="publicKey">An XML string containing the public RSA key.</param>
            /// <param name="plainText">The data to be encrypted.</param>
            /// <returns>The encrypted data.</returns>
            public string Encrypt(string publicKey, string plainText)
            {
                if (string.IsNullOrWhiteSpace(plainText))
                    throw new ArgumentException("Data are empty");

                int maxLength = GetMaxDataLength();
                if (Encoding.Unicode.GetBytes(plainText).Length > maxLength)
                    throw new ArgumentException("Maximum data length is " + maxLength / 2);

                if (!IsKeySizeValid())
                    throw new ArgumentException("Key size is not valid");

                if (string.IsNullOrWhiteSpace(publicKey))
                    throw new ArgumentException("Key is null or empty");

                byte[] plainBytes = null;
                byte[] encryptedBytes = null;
                string encryptedText = "";

                try
                {
                    CspParameters cspParams = GetCspParameters();
                    cspParams.Flags = CspProviderFlags.NoFlags;

                    rsaProvider = new RSACryptoServiceProvider(KEY_SIZE, cspParams);

                    // [1] Import public key
                    rsaProvider.FromXmlString(publicKey);

                    // [2] Get plain bytes from plain text
                    plainBytes = Encoding.Unicode.GetBytes(plainText);

                    // Encrypt plain bytes
                    encryptedBytes = rsaProvider.Encrypt(plainBytes, false);

                    // Get encrypted text from encrypted bytes
                    // encryptedText = Encoding.Unicode.GetString(encryptedBytes); => NOT WORKING
                    encryptedText = Convert.ToBase64String(encryptedBytes);
                }
                catch (Exception ex)
                {
                    // Any errors? Show them
                    throw new Exception("Exception encrypting file! More info: " + ex.Message);
                }
                finally
                {
                    // Do some clean up if needed
                }

                return encryptedText;

            } // Encrypt method

            /// <summary>
            /// Decrypts data with the System.Security.Cryptography.RSA algorithm.
            /// </summary>
            /// <param name="privateKey">An XML string containing a public and private RSA key.</param>
            /// <param name="encryptedText">The data to be decrypted.</param>
            /// <returns>The decrypted data, which is the original plain text before encryption.</returns>
            public string Decrypt(string privateKey, string encryptedText)
            {
                if (string.IsNullOrWhiteSpace(encryptedText))
                    throw new ArgumentException("Data are empty");

                if (!IsKeySizeValid())
                    throw new ArgumentException("Key size is not valid");

                if (string.IsNullOrWhiteSpace(privateKey))
                    throw new ArgumentException("Key is null or empty");

                byte[] encryptedBytes = null;
                byte[] plainBytes = null;
                string plainText = "";

                try
                {
                    CspParameters cspParams = GetCspParameters();
                    cspParams.Flags = CspProviderFlags.NoFlags;

                    rsaProvider = new RSACryptoServiceProvider(KEY_SIZE, cspParams);

                    // [1] Import private/public key pair
                    rsaProvider.FromXmlString(privateKey);

                    // [2] Get encrypted bytes from encrypted text
                    // encryptedBytes = Encoding.Unicode.GetBytes(encryptedText); => NOT WORKING
                    encryptedBytes = Convert.FromBase64String(encryptedText);

                    // Decrypt encrypted bytes
                    plainBytes = rsaProvider.Decrypt(encryptedBytes, false);

                    // Get decrypted text from decrypted bytes
                    plainText = Encoding.Unicode.GetString(plainBytes);
                }
                catch (Exception ex)
                {
                    // Any errors? Show them
                    throw new Exception("Exception decrypting file! More info: " + ex.Message);
                }
                finally
                {
                    // Do some clean up if needed
                }

                return plainText;

            } // Decrypt method

            #endregion

        }
    }
}
