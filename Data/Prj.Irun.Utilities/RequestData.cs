﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Utilities
{
    public class RequestData
    {
        public static string GETRequest(string requestURL)
        {
            string resultOutput = string.Empty;
            string htmlResult = "";
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(requestURL);
            myRequest.Method = "GET";
            myRequest.KeepAlive = true;
            myRequest.ContentType = "application/json; charset=utf-8";
            myRequest.Accept = "application/json";
            myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; rv:2.0) Gecko/20100101 Firefox/4.0";
            myRequest.Headers.Add("Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.7");
            myRequest.Headers.Add("Keep-Alive:15");
            myRequest.Proxy = null;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)myRequest.GetResponse())
                {
                    htmlResult = string.Empty;

                    using (BufferedStream buffer = new BufferedStream(response.GetResponseStream()))
                    {
                        using (StreamReader readStream = new StreamReader(buffer, Encoding.UTF8))
                        {
                            resultOutput = readStream.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                htmlResult = ex.ToString();
            }
            //finally
            //{
            //    htmlResult = resultOutput;
            //}
            return resultOutput;
        }
        public static string POSTRequest(string requestURL, string postData)
        {
            try
            {
                // get the response 
                var request = (HttpWebRequest)WebRequest.Create(requestURL);
                request.ContentType = "application/json; charset=utf-8";
                request.Method = "POST";
                //  request.Accept = "JSON";
                using (Stream requestStream = request.GetRequestStream())
                {
                    byte[] postDatabytes = Encoding.UTF8.GetBytes(postData);
                    requestStream.Write(postDatabytes, 0, postDatabytes.Length);
                }
                var webResponse = request.GetResponse();
                if (webResponse == null)
                {
                    return string.Empty;
                    //return "Unable to connect to the remote server";
                }
                var sr = new StreamReader(webResponse.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch (Exception ex)
            {

                return ex.ToString();
            }
        }
    }
}
