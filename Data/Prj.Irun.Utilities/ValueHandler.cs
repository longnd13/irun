﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj.Irun.Utilities
{
  public  class ValueHandler
    {
        public class GiftRewardTypeValue
        {
            public static string IGOLD = "detgh";
            public static string QUANG_CAO = "frtyj";
            public static string BASIC_GIFT = "1e45t";
            public static string SAVING_POINT = "vfg67";
        }

        public class GiftCreateCondTypeValue
        {
            public static string MAXIMUM_DAILY_GIFT = "8ui9o";
         
        }

        public class UserGiftStatusValue
        {
            public static string CheckingOpenCond = "00001";
            public static string WaitingToOpen = "00002";
            public static string Opened = "00003";
            public static string Destroyed = "00004";

        }
        public class SystemGiftTypeValue
        {

            public static string BASIC_GIFT = "fgtyh";
            public static string SAVING_POINT_GIFT = "r567y";
            public static string DAILY_GIFT = "hu890";
            public static string CHALLENGE_GIFT = "21dfr";

        }
    }
}
