﻿using Prj.Irun.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
   
    public class AccountController : BaseController
    {    
        private readonly ItestService _testService;
        public AccountController(ItestService testService)
        {
            _testService = testService;
        }
        // GET: Admin/Account
        public ActionResult Index()
        {

         var a =  _testService.getdata();
            return View();
        }
    }
}