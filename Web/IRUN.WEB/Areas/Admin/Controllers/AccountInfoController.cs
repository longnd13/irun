﻿using Prj.Irun.BusinessLogic.Functions;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    //[Authorize]
    public class AccountInfoController : BaseController
    {
        private readonly IcpAccountInfoService _accountInfoService;
        public AccountInfoController(IcpAccountInfoService accountInfoService)
        {
            _accountInfoService = accountInfoService;
        }
        // GET: Admin/AccountInfo
        public ActionResult Index(int? Page, string GoldSort, string StepSort, string DistanceSort, string Flags)
        {
            try
            {
                var url = this.Request.UrlReferrer;
                var model = new AccountInfo_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                //var list = _accountInfoService.GetAll(model);

                var list = _accountInfoService.GetAllBySort(model, GoldSort, StepSort, DistanceSort);

                long lCount = list.data.Count;
                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;

                if (url != null && !string.IsNullOrEmpty(url.Query) && Flags == "0")
                {
                    if (this.Request.Url == this.Request.UrlReferrer)
                    {
                        if (!string.IsNullOrEmpty(GoldSort))
                        {
                            return Redirect("Index?GoldSort=Asc&Flags=1");
                        }
                        if (!string.IsNullOrEmpty(StepSort))
                        {
                            return Redirect("Index?StepSort=Asc&Flags=1");
                        }
                        if (!string.IsNullOrEmpty(DistanceSort))
                        {
                            return Redirect("Index?DistanceSort=Asc&Flags=1");
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(int? Page, string username, string fullname)
        {
            try
            {
                var model = new AccountInfo_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _accountInfoService.SearchByUsername(model, username, fullname);
                long lCount = list.data.Count;
                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var result = _accountInfoService.Delete(Protector.Int(id));
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(int id)
        {
            try
            {
                var msg = _accountInfoService.GetByID(id);

                return View(msg.data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Update(FormCollection fc, AccountInfoModel model)
        {
            try
            {
                model.ModifiedBy = Environment.UserName;
                model.ModifiedDate = Convert.ToDateTime(DateTime.Now.ToLongTimeString());
                var msg = _accountInfoService.Updated(model);

                if (msg.success.Equals(true))
                {
                    return RedirectToAction("/Index");
                }
                else
                {
                    ViewBag.message = msg.message;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        public ActionResult Add()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Add(FormCollection fc, AccountInfoModel model)
        {
            try
            {

                model.CreatedBy = UserID();
                model.CreatedDate = DateHandler.DateNow();
                var data_response = _accountInfoService.Add(model);
                ViewBag.message = Globals.AlertMessage(data_response.message, data_response.success);
                if (data_response.success.Equals(true))
                {
                    ModelState.Clear();
                    return View(new AccountInfoModel());
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }
    }
}