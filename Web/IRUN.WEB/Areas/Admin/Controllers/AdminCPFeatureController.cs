﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class AdminCPFeatureController : BaseController
    {
        private readonly IAdminCPFeatureService _accountFeatureService;
        public AdminCPFeatureController(IAdminCPFeatureService accountFeatureService)
        {
            _accountFeatureService = accountFeatureService;
        }

        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new AdminCPFeature_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _accountFeatureService.GetAll(model);
                long lCount = list.data.Count;
                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var getObj = _accountFeatureService.GetByID(id);
                if (getObj.data.ParentId == 0)
                {
                    var result = _accountFeatureService.Delete(Protector.Int(id));
                    if (result.success.Equals(true))
                    {
                        var nodeResult = _accountFeatureService.DeleteNode(Protector.Int(getObj.data.Id));
                        if (nodeResult.success.Equals(true))
                        {
                            msg = "OK";
                        }
                        else
                        {
                            msg = "Oop! Something wrong, please try again...";
                        }
                    }
                }
                else
                {
                    var result = _accountFeatureService.Delete(Protector.Int(id));
                    if (result.success.Equals(true))
                    {
                        msg = "OK";
                    }
                    else
                    {
                        msg = "Oop! Something wrong, please try again...";
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(int id)
        {
            try
            {
                var msg = _accountFeatureService.GetByID(id);
                #region ParentID
                var model = new AdminCPFeature_DataModel();
                model.PageIndex = Protector.Int(1);
                model.PageSize = 9999;

                var list = _accountFeatureService.GetAll(model);

                var rootObj = _accountFeatureService.GetByParentID(model, 0);

                for (int i = 0; i < rootObj.data.Count; i++)
                {
                    model.List.Add(rootObj.data.List[i]);
                    //var root = _accountFeatureService.GetByParentID(model, int.Parse(rootObj.data.List[i].Id.ToString()));
                    //for (int j = 0; j < root.data.Count; j++)
                    //{
                    //    var nodeObj = _accountFeatureService.GetNodeByParentID(root.data.List[j].ParentId, root.data.List[j].Id);
                    //    model.List.Add(nodeObj.data);
                    //}
                }
                ViewBag.rootData = model.List;
                #endregion
                return View(msg.data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Update(FormCollection fc, AdminCPFeatureModel model)
        {
            try
            {
                model.ModifiedBy = Environment.UserName;
                model.ModifiedDate = Convert.ToDateTime(DateTime.Now.ToLongTimeString());
                var msg = _accountFeatureService.Updated(model);
                if (msg.success.Equals(true))
                {
                    return RedirectToAction("/Index");
                }
                else
                {
                    ViewBag.message = msg.message;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }


        public ActionResult Add()
        {
            try
            {
                #region ParentID
                var model = new AdminCPFeature_DataModel();
                model.PageIndex = Protector.Int(1);
                model.PageSize = 9999;

                var list = _accountFeatureService.GetAll(model);

                var rootObj = _accountFeatureService.GetByParentID(model, 0);

                for (int i = 0; i < rootObj.data.Count; i++)
                {
                    model.List.Add(rootObj.data.List[i]);
                    //var root = _accountFeatureService.GetByParentID(model, int.Parse(rootObj.data.List[i].Id.ToString()));
                    //for (int j = 0; j < root.data.Count; j++)
                    //{
                    //    var nodeObj = _accountFeatureService.GetNodeByParentID(root.data.List[j].ParentId, root.data.List[j].Id);
                    //    model.List.Add(nodeObj.data);
                    //}
                }
                ViewBag.rootData = model.List;
                #endregion
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Add(FormCollection fc, AdminCPFeatureModel model)
        {
            try
            {
                model.CreatedBy = Environment.UserName;
                model.DeletedBy = Environment.UserName;
                model.CreatedDate = Convert.ToDateTime(DateTime.Now.ToLongTimeString());
                model.Code = Globals.RandomCode(5);
                var data_response = _accountFeatureService.Add(model);

                return RedirectToAction("/Index");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }
    }
}