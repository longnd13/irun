﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class AdminCPPermissionController : BaseController
    {
        private readonly IcpAdminCPPermissionService _adminCPPermissionService;
        private readonly IAdminCPFeatureService _accountFeatureService;
        private readonly IcpAdminCpService _adminCpService;
        public AdminCPPermissionController(IcpAdminCPPermissionService adminCPPermissionService, IAdminCPFeatureService accountFeatureService, IcpAdminCpService adminCpService)
        {
            _adminCPPermissionService = adminCPPermissionService;
            _accountFeatureService = accountFeatureService;
            _adminCpService = adminCpService;
        }

        public ActionResult Index()
        {
            try
            {
                var model = new AdminCPPermission_DataModel();
                model.PageIndex = 1;
                model.PageSize = 9999;

                #region AdminCP
                var modelAdminCP = new AdminCp_DataModel();
                modelAdminCP.PageIndex = 1;
                modelAdminCP.PageSize = 9999;

                var listAdminCP = _adminCpService.GetAll(modelAdminCP);
                ViewBag.listServicesAdminCP = listAdminCP.data.List;
                #endregion

                #region AdminCPFeature
                var modelAdminCPFeature = new AdminCPFeature_DataModel();
                modelAdminCPFeature.PageIndex = 1;
                modelAdminCPFeature.PageSize = 9999;

                var listAdminCPFeature = _accountFeatureService.GetByParentID(modelAdminCPFeature, 0);
                ViewBag.listServicesAdminCPFeature = listAdminCPFeature.data.List;
                #endregion

                string userName = Request.Form["UserName"];
                string type = Request.Form["type"];
                if (userName != null && type != null)
                {
                    var adminCpModel = new AdminCPPermissionModel();
                    string[] typeSplit = type.ToString().Split(',');
                    var result = _adminCPPermissionService.Delete(userName);
                    if (result.success.Equals(true))
                    {
                        foreach (var item in typeSplit)
                        {
                            adminCpModel.UserName = userName;
                            adminCpModel.FeatureCode = item;
                            adminCpModel.CreatedDate = Convert.ToDateTime(DateTime.Now.ToLongTimeString());
                            var data_response = _adminCPPermissionService.Add(adminCpModel);
                        }
                    }                   
                }

                var list = _adminCPPermissionService.GetAll(model);
                long lCount = list.data.Count;

                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public ActionResult GetFeatureByGroupId(int groupId)
        {
            if (groupId > 0)
            {
                var list = _accountFeatureService.GetFeatureByGroupId(groupId);

                var model = new AdminCPPermission_DataModel();
                model.PageIndex = 1;
                model.PageSize = 9999;

                var listData = _adminCPPermissionService.GetAll(model);

                ViewBag.listData = listData.data.List;

                return PartialView("_ListFeatureByGroup", list);
            }
            else
            {
                return PartialView("_ListFeatureByGroup", null);
            }
        }

        public JsonResult GetPermission(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                MessageModel msg = new MessageModel();
                msg = _adminCPPermissionService.GetPermission(userName);
                return Json(msg.message, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}