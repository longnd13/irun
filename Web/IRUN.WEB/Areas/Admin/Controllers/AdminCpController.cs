﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    [Authorize]
    public class AdminCpController : BaseController
    {
        private readonly IcpAdminCpService _adminCpService;
        public AdminCpController(IcpAdminCpService adminCpService)
        {
            _adminCpService = adminCpService;
        }

        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new AdminCp_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _adminCpService.GetAll(model);
                long lCount = list.data.Count;
                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var result = _adminCpService.Delete(Protector.Int(id));
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(int id)
        {
            try
            {
                var msg = _adminCpService.GetByID(id);
                return View(msg.data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Update(FormCollection fc, AdminCpModel model)
        {
            try
            {
                model.CreatedBy = Environment.UserName;
                var msg = _adminCpService.Updated(model);
                if (msg.success.Equals(true))
                {
                    return RedirectToAction("/Index");
                }
                else
                {
                    ViewBag.message = msg.message;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }


        public ActionResult CreatedAdmin()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult CreatedAdmin(FormCollection fc, AdminCpModel model)
        {
            try
            {
                model.CreatedBy = UserID();
                model.CreatedDate = DateHandler.DateNow();
                var data_response = _adminCpService.Add(model);
                ViewBag.message = Globals.AlertMessage("Created User: " + model.UserName + " Successful.", data_response.success);
                if (data_response.success.Equals(true))
                {
                    ModelState.Clear();
                    return View(new AdminCpModel());
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = Globals.AlertMessage(ex.ToString(), false);
            }
            return View();
        }

        public ActionResult ChangePassword(int id)
        {
            try
            {
                var msg = _adminCpService.GetByID(id);
                return View(msg.data);
            }
            catch (Exception ex)
            {
                ViewBag.message = Globals.AlertMessage(ex.ToString(), false);
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult ChangePassword(FormCollection fc, AdminCpModel model)
        {
            try
            {
                var checkPwd = Globals.MD5FromString(model.OldPwd);                
                if (model.Password == checkPwd)
                {
                    var newPwd = Globals.MD5FromString(model.NewPwd);
                    model.Password = newPwd;
                    var msg = _adminCpService.Updated(model);
                    if (msg.success.Equals(true))
                    {
                        return RedirectToAction("/Index");
                    }
                    else
                    {
                        ViewBag.message = msg.message;
                    }
                }
                else
                {
                    ViewBag.message = "Invalid password, please try again.";
                }

            }
            catch (Exception ex)
            {
                ViewBag.message = Globals.AlertMessage(ex.ToString(), false);
            }
            return View();
        }
    }
}