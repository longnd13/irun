﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class AdsActivityController : BaseController
    {
        private readonly IcpAdsActivityService _adsActivityService;
        private readonly IcpAdsTypeService _adsTypeService;
        public AdsActivityController(IcpAdsActivityService adsActivityService, IcpAdsTypeService adsTypeService)
        {
            _adsActivityService = adsActivityService;
            _adsTypeService = adsTypeService;
        }
        // GET: Admin/AdsActivity
        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new AdsActivity_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _adsActivityService.GetAll(model);
                long lCount = list.data.Count;

                #region Load Dropdown
                var dataModel = new AdsType_DataModel();
                dataModel.PageIndex = Protector.Int(Page, 1);
                dataModel.PageSize = 9999;

                var listData = _adsTypeService.GetAll(dataModel);
                ViewBag.ListDropdown = listData.data.List;
                #endregion

                #region Load data
                var adminData = new AdsType_DataModel();
                for (int i = 0; i < list.data.Count; i++)
                {
                    var adminCp = _adsTypeService.GetByID(list.data.List[i].AdsCodeID); //Edit lại
                    adminData.List.Add(adminCp.data);
                }
                ViewBag.listAdminData = adminData.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(int? Page, string AdsCodeID)
        {
            try
            {
                var model = new AdsActivity_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _adsActivityService.SearchByAdsCodeID(model, AdsCodeID);
                long lCount = list.data.Count;

                #region Load Dropdown
                var dataModel = new AdsType_DataModel();
                dataModel.PageIndex = Protector.Int(Page, 1);
                dataModel.PageSize = 9999;

                var listData = _adsTypeService.GetAll(dataModel);
                ViewBag.ListDropdown = listData.data.List;
                #endregion

                #region Load data
                var adminData = new AdsType_DataModel();
                for (int i = 0; i < list.data.Count; i++)
                {
                    var adminCp = _adsTypeService.GetByID(list.data.List[i].AdsCodeID); //Edit lại
                    adminData.List.Add(adminCp.data);
                }
                ViewBag.listAdminData = adminData.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(string adsCodeID)
        {
            string msg = "";
            try
            {
                var result = _adsActivityService.Delete(adsCodeID);
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
    }
}