﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.Utilities;

namespace IRUN.WEB.Areas.Admin.Controllers
{

    public class AuthenticateController : BaseController
    {
        private readonly IcpAdminCpService _adminCpService;
        public AuthenticateController(IcpAdminCpService adminCpService)
        {
            _adminCpService = adminCpService;
        }

        // GET: Admin/Authenticate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            if (UserID() != null)
            {
                return RedirectToAction("Index", "DashBoard", new { area = "Admin" });
            }
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Login(AuthenticateModel.LoginModel model)
        {
            var result = _adminCpService.Authenticate(model);
            if (result.success == true)
            {
                FormsAuthentication.SetAuthCookie(model.UserID, false);
                string sReturnUrl = Protector.String(Request.QueryString["ReturnUrl"]);
                if (sReturnUrl == "")
                {
                    return RedirectToAction("Index", "DashBoard", new { area = "Admin" });
                }
                return Redirect(sReturnUrl);
            }
            else
            {
                ViewBag.message = Globals.AlertMessage("Tài khoản hoặc mật khẩu không đúng.", result.success);
                return View();
            }
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Authenticate", new { area = "Admin" });
        }
    }
}