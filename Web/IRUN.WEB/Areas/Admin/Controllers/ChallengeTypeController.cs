﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class ChallengeTypeController : BaseController
    {
        private readonly IcpChallengeTypeService _challengeTypeService;
        public ChallengeTypeController(IcpChallengeTypeService challengeTypeService)
        {
            _challengeTypeService = challengeTypeService;
        }

        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new ChallengeType_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _challengeTypeService.GetAll(model);
                long lCount = list.data.Count;
                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var result = _challengeTypeService.Delete(Protector.Int(id));
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(int id)
        {
            try
            {
                var msg = _challengeTypeService.GetByID(id);

                return View(msg.data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Update(FormCollection fc, ChallengeTypeModel model)
        {
            try
            {
                var msg = _challengeTypeService.Updated(model);

                if (msg.success.Equals(true))
                {
                    return RedirectToAction("/Index");
                }
                else
                {
                    ViewBag.message = msg.message;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }


        public ActionResult Add()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Add(FormCollection fc, ChallengeTypeModel model)
        {
            try
            {
                var data_response = _challengeTypeService.Add(model);

                return RedirectToAction("/Index");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }
    }
}