﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class GiftComponentTypeController : BaseController
    {
        private readonly IcpGiftComponentTypeService _giftComponentTypeService;
        private readonly IcpGiftRewardTypeService _giftRewardTypeService;
        public GiftComponentTypeController(IcpGiftComponentTypeService GiftComponentTypeService, IcpGiftRewardTypeService giftRewardTypeService)
        {
            _giftComponentTypeService = GiftComponentTypeService;
            _giftRewardTypeService = giftRewardTypeService;
        }

        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new GiftComponentType_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _giftComponentTypeService.GetAll(model);
                long lCount = list.data.Count;

                var rewardModel = new GiftRewardType_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var reward = _giftRewardTypeService.GetByID(list.data.List[i].GiftRewardTypeCodeID.ToString());
                    rewardModel.List.Add(reward.data);
                }
                ViewBag.listReward = rewardModel.List;

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(string CodeID)
        {
            string msg = "";
            try
            {
                var result = _giftComponentTypeService.Delete(CodeID);
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(string CodeID)
        {
            try
            {
                var model = new GiftRewardType_DataModel();
                model.PageIndex = 1;
                model.PageSize = 9999;

                var list = _giftRewardTypeService.GetAll(model);

                ViewBag.listServices = list.data.List;

                var msg = _giftComponentTypeService.GetByID(CodeID);

                return View(msg.data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Update(FormCollection fc, GiftComponentTypeModel model)
        {
            try
            {
                var msg = _giftComponentTypeService.Updated(model);

                if (msg.success.Equals(true))
                {
                    return RedirectToAction("/Index");
                }
                else
                {
                    ViewBag.message = msg.message;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }


        public ActionResult Add()
        {
            try
            {
                var model = new GiftRewardType_DataModel();
                model.PageIndex = 1;
                model.PageSize = 9999;

                var list = _giftRewardTypeService.GetAll(model);

                ViewBag.listServices = list.data.List;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Add(FormCollection fc, GiftComponentTypeModel model)
        {
            try
            {
                var data_response = _giftComponentTypeService.Add(model);

                return RedirectToAction("/Index");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }
    }
}