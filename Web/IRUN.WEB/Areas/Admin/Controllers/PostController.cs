﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class PostController : BaseController
    {
        private readonly IcpPostService _postService;
        private readonly IcpPostPicService _postPicService;
        private readonly IcpPostVideoService _postVideoService;
        private readonly IcpPostHashtagService _postHashtagService;
        private readonly IcpHashtagService _hashtagService;
        private readonly IcpPostLikeService _postLikeService;
        public PostController(IcpPostService postService, IcpPostPicService postPicService, IcpPostVideoService postVideoService, IcpPostHashtagService postHashtagService, IcpHashtagService hashtagService, IcpPostLikeService postLikeService)
        {
            _postService = postService;
            _postPicService = postPicService;
            _postVideoService = postVideoService;
            _postHashtagService = postHashtagService;
            _hashtagService = hashtagService;
            _postLikeService = postLikeService;
        }
        // GET: Admin/Post
        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new Post_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _postService.GetAll(model);
                long lCount = list.data.Count;

                #region Loop PostPic By ID
                var PostPicLoopModel = new PostPic_DataModel();
                PostPicLoopModel.PageIndex = 1;
                PostPicLoopModel.PageSize = 9999;

                for (int i = 0; i < list.data.Count; i++)
                {
                    var PostPicLoop = _postPicService.SearchById(PostPicLoopModel, Protector.Int(list.data.List[i].Id));
                    string listImg = string.Empty;
                    foreach (var item in PostPicLoop.data.List)
                    {
                        if (listImg == null || listImg == "")
                        {
                            listImg = item.LinkImages;
                        }
                        else
                        {
                            listImg = listImg + ", " + item.LinkImages;
                        }
                    }
                    foreach (var item in PostPicLoop.data.List)
                    {
                        item.LinkImagesList = listImg;
                        PostPicLoopModel.List.Add(item);
                    }
                    listImg = string.Empty;
                }
                ViewBag.ListFilterPostPic = PostPicLoopModel.List;
                #endregion

                #region Loop PostVideo By ID
                var PostVideoLoopModel = new PostVideo_DataModel();
                PostVideoLoopModel.PageIndex = 1;
                PostVideoLoopModel.PageSize = 9999;

                for (int i = 0; i < list.data.Count; i++)
                {
                    var PostVideoLoop = _postVideoService.SearchById(PostVideoLoopModel, Protector.Int(list.data.List[i].Id));
                    string listVideo = string.Empty;
                    foreach (var item in PostVideoLoop.data.List)
                    {
                        if (listVideo == null || listVideo == "")
                        {
                            listVideo = item.LinkImages;
                        }
                        else
                        {
                            listVideo = listVideo + ", " + item.LinkImages;
                        }
                    }
                    foreach (var item in PostVideoLoop.data.List)
                    {
                        item.LinkVideosList = listVideo;
                        PostVideoLoopModel.List.Add(item);
                    }
                    listVideo = string.Empty;
                }
                ViewBag.ListFilterPostVideo = PostVideoLoopModel.List;
                #endregion

                #region Loop PostHashtag By ID
                var PostHashtagLoopModel = new PostHashtag_DataModel();
                PostHashtagLoopModel.PageIndex = 1;
                PostHashtagLoopModel.PageSize = 9999;

                var HashtagLoopModel = new Hashtag_DataModel();
                HashtagLoopModel.PageIndex = 1;
                HashtagLoopModel.PageSize = 9999;

                for (int i = 0; i < list.data.Count; i++)
                {
                    var PostHashtagLoop = _postHashtagService.GetByID(Protector.Int(list.data.List[i].Id));
                    var HashtagLoop = _hashtagService.GetByID(Protector.Int(PostHashtagLoop.data.HashtagId));
                    HashtagLoopModel.List.Add(HashtagLoop.data);
                }
                ViewBag.ListFilterHashtag = HashtagLoopModel.List;
                #endregion

                #region Loop PostLike By ID
                var PostLikeLoopModel = new PostLike_DataModel();
                PostLikeLoopModel.PageIndex = 1;
                PostLikeLoopModel.PageSize = 9999;

                for (int i = 0; i < list.data.Count; i++)
                {
                    var PostLikeLoop = _postLikeService.GetByID(Protector.Int(list.data.List[i].Id));
                    PostLikeLoopModel.List.Add(PostLikeLoop.data);
                }
                ViewBag.ListFilterPostLike = PostLikeLoopModel.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(int? Page, string UserID)
        {
            try
            {
                var model = new Post_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _postService.SearchByUsername(model, UserID);
                long lCount = list.data.Count;

                #region Loop PostPic By ID
                var PostPicLoopModel = new PostPic_DataModel();
                PostPicLoopModel.PageIndex = 1;
                PostPicLoopModel.PageSize = 9999;

                for (int i = 0; i < list.data.Count; i++)
                {
                    var PostPicLoop = _postPicService.SearchById(PostPicLoopModel, Protector.Int(list.data.List[i].Id));
                    string listImg = string.Empty;
                    foreach (var item in PostPicLoop.data.List)
                    {
                        if (listImg == null || listImg == "")
                        {
                            listImg = item.LinkImages;
                        }
                        else
                        {
                            listImg = listImg + ", " + item.LinkImages;
                        }
                    }
                    foreach (var item in PostPicLoop.data.List)
                    {
                        item.LinkImagesList = listImg;
                        PostPicLoopModel.List.Add(item);
                    }
                    listImg = string.Empty;
                }
                ViewBag.ListFilterPostPic = PostPicLoopModel.List;
                #endregion

                #region Loop PostVideo By ID
                var PostVideoLoopModel = new PostVideo_DataModel();
                PostVideoLoopModel.PageIndex = 1;
                PostVideoLoopModel.PageSize = 9999;

                for (int i = 0; i < list.data.Count; i++)
                {
                    var PostVideoLoop = _postVideoService.SearchById(PostVideoLoopModel, Protector.Int(list.data.List[i].Id));
                    string listVideo = string.Empty;
                    foreach (var item in PostVideoLoop.data.List)
                    {
                        if (listVideo == null || listVideo == "")
                        {
                            listVideo = item.LinkImages;
                        }
                        else
                        {
                            listVideo = listVideo + ", " + item.LinkImages;
                        }
                    }
                    foreach (var item in PostVideoLoop.data.List)
                    {
                        item.LinkVideosList = listVideo;
                        PostVideoLoopModel.List.Add(item);
                    }
                    listVideo = string.Empty;
                }
                ViewBag.ListFilterPostVideo = PostVideoLoopModel.List;
                #endregion

                #region Loop PostHashtag By ID
                var PostHashtagLoopModel = new PostHashtag_DataModel();
                PostHashtagLoopModel.PageIndex = 1;
                PostHashtagLoopModel.PageSize = 9999;

                var HashtagLoopModel = new Hashtag_DataModel();
                HashtagLoopModel.PageIndex = 1;
                HashtagLoopModel.PageSize = 9999;

                for (int i = 0; i < list.data.Count; i++)
                {
                    var PostHashtagLoop = _postHashtagService.GetByID(Protector.Int(list.data.List[i].Id));
                    var HashtagLoop = _hashtagService.GetByID(Protector.Int(PostHashtagLoop.data.HashtagId));
                    HashtagLoopModel.List.Add(HashtagLoop.data);
                }
                ViewBag.ListFilterHashtag = HashtagLoopModel.List;
                #endregion

                #region Loop PostLike By ID
                var PostLikeLoopModel = new PostLike_DataModel();
                PostLikeLoopModel.PageIndex = 1;
                PostLikeLoopModel.PageSize = 9999;

                for (int i = 0; i < list.data.Count; i++)
                {
                    var PostLikeLoop = _postLikeService.GetByID(Protector.Int(list.data.List[i].Id));
                    PostLikeLoopModel.List.Add(PostLikeLoop.data);
                }
                ViewBag.ListFilterPostLike = PostLikeLoopModel.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var result = _postService.ChangeStatus(Protector.Int(id));
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
    }
}