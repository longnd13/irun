﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class RewardUserGiftController : BaseController
    {
        private readonly IcpRewardUserGiftService _rewardUserGiftService;
        private readonly IcpGiftRewardTypeService _giftRewardTypeService;
        private readonly IcpUserGiftService _userGiftService;
        public RewardUserGiftController(IcpRewardUserGiftService rewardUserGiftService, IcpGiftRewardTypeService giftRewardTypeService, IcpUserGiftService userGiftService)
        {
            _rewardUserGiftService = rewardUserGiftService;
            _giftRewardTypeService = giftRewardTypeService;
            _userGiftService = userGiftService;
        }
        // GET: Admin/RewardUserGift
        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new RewardUserGift_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _rewardUserGiftService.GetAll(model);
                long lCount = list.data.Count;

                #region dropdownlist GiftRewardType
                var giftRewardTypeModel = new GiftRewardType_DataModel();
                giftRewardTypeModel.PageIndex = Protector.Int(Page, 1);
                giftRewardTypeModel.PageSize = 9999;

                var giftRewardType = _giftRewardTypeService.GetAll(giftRewardTypeModel);
                ViewBag.listGiftRewardType = giftRewardType.data.List;
                #endregion

                #region dropdownlist UserGift
                var userGiftModel = new UserGift_DataModel();
                userGiftModel.PageIndex = Protector.Int(Page, 1);
                userGiftModel.PageSize = 9999;

                var userGift = _userGiftService.GetAll(userGiftModel);
                ViewBag.listUserGift = userGift.data.List;
                #endregion

                #region Loop GiftRewardType By ID
                var giftRewardTypeLoopModel = new GiftRewardType_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var giftRewardTypeLoop = _giftRewardTypeService.GetByID(list.data.List[i].GiftRewardTypeCodeID);
                    giftRewardTypeLoopModel.List.Add(giftRewardTypeLoop.data);
                }
                ViewBag.ListFilterGiftRewardType = giftRewardTypeLoopModel.List;
                #endregion

                #region Loop UserGift By ID
                var userGiftLoopModel = new UserGift_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var userGiftLoop = _userGiftService.GetByID(Protector.Int(list.data.List[i].UserGiftID));
                    userGiftLoopModel.List.Add(userGiftLoop.data);
                }
                ViewBag.ListFilterUserGift = userGiftLoopModel.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(int? Page, int userGiftID, string GiftRewardTypeCodeID)
        {
            try
            {
                var model = new RewardUserGift_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _rewardUserGiftService.Search(model, userGiftID, GiftRewardTypeCodeID);
                long lCount = list.data.Count;

                #region dropdownlist GiftRewardType
                var giftRewardTypeModel = new GiftRewardType_DataModel();
                giftRewardTypeModel.PageIndex = Protector.Int(Page, 1);
                giftRewardTypeModel.PageSize = 9999;

                var giftRewardType = _giftRewardTypeService.GetAll(giftRewardTypeModel);
                ViewBag.listGiftRewardType = giftRewardType.data.List;
                #endregion

                #region dropdownlist UserGift
                var userGiftModel = new UserGift_DataModel();
                userGiftModel.PageIndex = Protector.Int(Page, 1);
                userGiftModel.PageSize = 9999;

                var userGift = _userGiftService.GetAll(userGiftModel);
                ViewBag.listUserGift = userGift.data.List;
                #endregion

                #region Loop GiftRewardType By ID
                var giftRewardTypeLoopModel = new GiftRewardType_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var giftRewardTypeLoop = _giftRewardTypeService.GetByID(list.data.List[i].GiftRewardTypeCodeID);
                    giftRewardTypeLoopModel.List.Add(giftRewardTypeLoop.data);
                }
                ViewBag.ListFilterGiftRewardType = giftRewardTypeLoopModel.List;
                #endregion

                #region Loop UserGift By ID
                var userGiftLoopModel = new UserGift_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var userGiftLoop = _userGiftService.GetByID(Protector.Int(list.data.List[i].UserGiftID));
                    userGiftLoopModel.List.Add(userGiftLoop.data);
                }
                ViewBag.ListFilterUserGift = userGiftLoopModel.List;
                #endregion
                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var result = _rewardUserGiftService.Delete(Protector.Int(id));
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
    }
}