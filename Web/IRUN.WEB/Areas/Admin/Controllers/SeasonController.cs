﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class SeasonController : BaseController
    {
        private readonly IcpSeasonService _seasonService;
        public SeasonController(IcpSeasonService seasonService)
        {
            _seasonService = seasonService;
        }

        // GET: Admin/Season
        public ActionResult Index()
        {
            try
            {
                var model = new Season_DataModel();
                model.PageIndex = 1;
                model.PageSize = int.MaxValue;
                var list = _seasonService.GetAll(model);
                long lCount = list.data.Count;
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var result = _seasonService.Delete(Protector.Int(id));
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(int id)
        {
            try
            {
                var msg = _seasonService.GetByID(id);

                return View(msg.data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Update(FormCollection fc, SeasonModel model)
        {
            try
            {
                model.ModifiedBy = Environment.UserName;
                model.ModifiedDate = Convert.ToDateTime(DateTime.Now.ToLongTimeString());
                var msg = _seasonService.Updated(model);

                if (msg.success.Equals(true))
                {
                    return RedirectToAction("/Index");
                }
                else
                {
                    ViewBag.message = msg.message;
                }

                return View(msg.data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }


        public ActionResult Add()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Add(FormCollection fc, SeasonModel model)
        {
            try
            {
                model.CreatedBy = Environment.UserName;
                model.CreatedDate = Convert.ToDateTime(DateTime.Now.ToLongTimeString());
                var data_response = _seasonService.Add(model);

                return RedirectToAction("/Index");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }
    }
}