﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class SimilarGiftTypeController : BaseController
    {
        private readonly IcpSimilarGiftTypeService _similarGiftTypeService;
        private readonly IcpSystemGiftTypeService _systemGiftTypeService;
        private readonly IcpGiftOpenCondTypeService _giftOpenCondTypeService;
        private readonly IcpGiftComponentTypeService _giftComponentTypeService;
        private readonly IcpSimilarGiftTypeGiftComponentTypeService _similarGiftTypeGiftComponentTypeService;
        public SimilarGiftTypeController(IcpSimilarGiftTypeService similarGiftTypeService, IcpSystemGiftTypeService systemGiftTypeService, IcpGiftOpenCondTypeService giftOpenCondTypeService, IcpGiftComponentTypeService giftComponentTypeService, IcpSimilarGiftTypeGiftComponentTypeService similarGiftTypeGiftComponentTypeService)
        {
            _similarGiftTypeService = similarGiftTypeService;
            _systemGiftTypeService = systemGiftTypeService;
            _giftOpenCondTypeService = giftOpenCondTypeService;
            _giftComponentTypeService = giftComponentTypeService;
            _similarGiftTypeGiftComponentTypeService = similarGiftTypeGiftComponentTypeService;
        }

        public ActionResult Index(int? Page)
        {
            try
            {
                string request = Request.Form["submit"];
                string requestType = Request.Form["type"];
                string checkRecord = Request.Form["SystemGiftTypeCodeID"];

                var model = new SimilarGiftType_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _similarGiftTypeService.GetAll(model);

                #region dropdownData
                var systemGiftTypeModel = new SystemGiftType_DataModel();
                systemGiftTypeModel.PageIndex = 1;
                systemGiftTypeModel.PageSize = 9999;

                var listsystemGift = _systemGiftTypeService.GetAll(systemGiftTypeModel);

                ViewBag.listSystemGift = listsystemGift.data.List;
                #endregion

                #region Load data
                var systemGiftTypeData = new SystemGiftType_DataModel();
                for (int i = 0; i < list.data.Count; i++)
                {
                    var systemGiftType = _systemGiftTypeService.GetByCodeId(Protector.String(list.data.List[i].SystemGiftTypeCodeID));
                    systemGiftTypeData.List.Add(systemGiftType.data);
                }
                ViewBag.listSystemGiftTypeData = systemGiftTypeData.List;
                #endregion

                if (request == "Save")
                {
                    if (checkRecord == "0")
                    {
                        ViewBag.message = Globals.AlertMessage("Please select SystemGiftTypeCode", false);
                    }
                    else
                    {
                        if (requestType != null)
                        {
                            var updateList = _similarGiftTypeService.UpdateAll();
                            if (updateList.success.Equals(true))
                            {
                                var obj = new SimilarGiftTypeModel();
                                string[] typeSplit = requestType.Split(',');
                                int sum = 0;
                                for (int j = 0; j < typeSplit.Length; j++)
                                {
                                    sum += int.Parse(typeSplit[j]);
                                    if (sum > 100)
                                    {
                                        return RedirectToAction("/Index");
                                    }
                                }
                                for (int i = 0; i < typeSplit.Length; i++)
                                {
                                    var getObj = _similarGiftTypeService.GetByCreatedRate(Protector.Int(typeSplit[i]), Protector.String(checkRecord));
                                    getObj.data.IsSelected = true;
                                    var updateObj = _similarGiftTypeService.Updated(getObj.data);
                                    ViewBag.message = Globals.AlertMessage("Update successful.", true);
                                }
                            }
                            list = _similarGiftTypeService.GetAll(model);
                        }
                        long lCount = list.data.Count;

                        #region Paging
                        if (lCount > 0)
                        {
                            if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                            {
                                ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                            }
                            else
                            {
                                ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                            }
                        }
                        #endregion
                        ViewBag.Count = list.data.Count;
                        ViewBag.listData = list.data.List;
                    }
                }
                else if (request == "Update")
                {
                    string record = Request.Form["recordUpdate"];

                    string mess = string.Empty;
                    int count = 0;

                    if (!string.IsNullOrEmpty(record))
                    {
                        string[] recordSplit = record.Split(',');

                        if (checkRecord == "0")
                        {
                            var listRecord = _similarGiftTypeService.GetAllSystemGiftType();
                            for (int i = 0; i < listRecord.Count; i++)
                            {
                                if (recordSplit[i] == "")
                                {
                                    recordSplit[i] = "1";
                                }
                                if (int.Parse(recordSplit[i]) > 100)
                                {
                                    recordSplit[i] = "100";
                                }
                                if (listRecord[i].CreatedRate != int.Parse(recordSplit[i]))
                                {
                                    var updateRecord = _similarGiftTypeService.UpdateCreateRate(Protector.String(listRecord[i].CodeID), Protector.Int(recordSplit[i]));
                                    if (updateRecord.success.Equals(true))
                                    {
                                        mess += "=> From " + listRecord[i].CreatedRate + " To " + int.Parse(recordSplit[i]) + ".<br/>";
                                        count++;
                                    }
                                    else
                                    {
                                        mess += "Failed data at " + listRecord[i].CreatedRate + " to " + int.Parse(recordSplit[i]) + ".<br/>";
                                    }
                                }
                            }
                        }
                        else
                        {
                            var obj = _similarGiftTypeService.SearchBySystemGiftType(checkRecord);
                            for (int i = 0; i < obj.Count; i++)
                            {
                                if (recordSplit[i] == "")
                                {
                                    recordSplit[i] = "1";
                                }
                                if (int.Parse(recordSplit[i]) > 100)
                                {
                                    recordSplit[i] = "100";
                                }
                                if (obj[i].CreatedRate != int.Parse(recordSplit[i]))
                                {
                                    var updateRecord = _similarGiftTypeService.UpdateCreateRate(Protector.String(obj[i].CodeID), Protector.Int(recordSplit[i]));
                                    if (updateRecord.success.Equals(true))
                                    {
                                        mess += "=> From " + obj[i].CreatedRate + " To " + int.Parse(recordSplit[i]) + ".<br/>";
                                        count++;
                                    }
                                    else
                                    {
                                        mess += "Failed data at " + obj[i].CreatedRate + " to " + int.Parse(recordSplit[i]) + ".<br/>";
                                    }
                                }
                            }
                        }

                        if (count != 0)
                        {
                            if (count == 1)
                            {
                                ViewBag.message = Globals.AlertMessage(count + " row was changed." + "<br/>" + mess, true);
                            }
                            else
                            {
                                ViewBag.message = Globals.AlertMessage(count + " rows was changed." + "<br/>" + mess, true);
                            }
                        }
                    }
                }

                var listData = _similarGiftTypeService.GetAllSystemGiftType();

                #region Load data
                for (int i = 0; i < listData.Count; i++)
                {
                    var systemGiftType = _systemGiftTypeService.GetByCodeId(Protector.String(listData[i].SystemGiftTypeCodeID));
                    var giftOpenCondType = _giftOpenCondTypeService.GetByCodeId(Protector.String(listData[i].GiftOpenCondTypeCodeID));
                    listData[i].SystemGiftTypeCode = systemGiftType.data.Name;
                    listData[i].GiftOpenCondTypeCode = giftOpenCondType.data.Name;
                }
                #endregion
                ViewBag.PartialList = listData;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public ActionResult getSimilarGiftType(string SystemGiftTypeCodeID)
        {
            if (SystemGiftTypeCodeID != "0")
            {
                var listData = _similarGiftTypeService.SearchBySystemGiftType(SystemGiftTypeCodeID);

                #region Load data
                for (int i = 0; i < listData.Count; i++)
                {
                    var systemGiftType = _systemGiftTypeService.GetByCodeId(Protector.String(listData[i].SystemGiftTypeCodeID));
                    var giftOpenCondType = _giftOpenCondTypeService.GetByCodeId(Protector.String(listData[i].GiftOpenCondTypeCodeID));
                    listData[i].SystemGiftTypeCode = systemGiftType.data.Name;
                    listData[i].GiftOpenCondTypeCode = giftOpenCondType.data.Name;
                }
                #endregion

                return PartialView("_ListSimilarGiftType", listData);
            }
            else
            {
                var listData = _similarGiftTypeService.GetAllSystemGiftType();

                #region Load data
                for (int i = 0; i < listData.Count; i++)
                {
                    var systemGiftType = _systemGiftTypeService.GetByCodeId(Protector.String(listData[i].SystemGiftTypeCodeID));
                    var giftOpenCondType = _giftOpenCondTypeService.GetByCodeId(Protector.String(listData[i].GiftOpenCondTypeCodeID));
                    listData[i].SystemGiftTypeCode = systemGiftType.data.Name;
                    listData[i].GiftOpenCondTypeCode = giftOpenCondType.data.Name;
                }
                #endregion

                return PartialView("_ListSimilarGiftType", listData);
            }
        }

        public JsonResult Delete(string CodeID)
        {
            string msg = "";
            try
            {
                var result = _similarGiftTypeService.Delete(CodeID);
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(string CodeID)
        {
            try
            {
                Uri postbackUrl = Request.UrlReferrer;
                if (postbackUrl != null)
                {
                    var model = new SimilarGiftTypeModel();
                    var msg = _similarGiftTypeService.GetByID(CodeID);

                    #region dropdownData SystemGiftType
                    var modelSystemGiftType = new SystemGiftType_DataModel();
                    modelSystemGiftType.PageIndex = 1;
                    modelSystemGiftType.PageSize = 9999;

                    var listSystemGiftType = _systemGiftTypeService.GetAll(modelSystemGiftType);

                    ViewBag.listSystemGiftType = listSystemGiftType.data.List;
                    #endregion

                    #region dropdownData GiftOpenCondType
                    var modelGiftOpenCondType = new GiftOpenCondType_DataModel();
                    modelGiftOpenCondType.PageIndex = 1;
                    modelGiftOpenCondType.PageSize = 9999;

                    var listGiftOpenCondType = _giftOpenCondTypeService.GetAll(modelGiftOpenCondType);

                    ViewBag.listGiftOpenCondType = listGiftOpenCondType.data.List;
                    #endregion

                    #region GiftComponentType
                    var modelGiftComponentType = new GiftComponentType_DataModel();
                    modelGiftComponentType.PageIndex = 1;
                    modelGiftComponentType.PageSize = 9999;

                    var listGiftComponentType = _giftComponentTypeService.GetAll(modelGiftComponentType);
                    var multipleList = _giftComponentTypeService.MultipleSelectedGift(modelGiftComponentType, CodeID);

                    ViewBag.listGiftComponentType = listGiftComponentType.data.List;
                    List<string> selectedService = new List<string>();
                    foreach (var item in multipleList.data.List)
                    {
                        selectedService.Add(Protector.String(item.CodeID));
                    }
                    #endregion
                    model = msg.data;
                    model.GiftComponentType = selectedService;
                    return View(model);
                }
                else
                {
                    return RedirectToAction("/Index");
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Update(FormCollection fc, SimilarGiftTypeModel model)
        {
            try
            {
                var msg = _similarGiftTypeService.Updated(model);

                if (msg.success.Equals(true))
                {
                    var removeOldValue = _similarGiftTypeGiftComponentTypeService.DeleteBySimilarGiftId(model.CodeID);
                    var similarModel = new SimilarGiftTypeGiftComponentTypeModel();
                    if (model.GiftComponentType != null && model.GiftComponentType.Count > 0)
                    {
                        foreach (var item in model.GiftComponentType)
                        {
                            similarModel.SimilarGiftTypeCodeID = model.CodeID;
                            similarModel.GiftComponentTypeCodeID = Protector.String(item);
                            var response = _similarGiftTypeGiftComponentTypeService.Add(similarModel);
                        }
                    }
                    return RedirectToAction("/Index");
                }
                else
                {
                    ViewBag.message = msg.message;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }


        public ActionResult Add()
        {
            try
            {
                #region dropdownData SystemGiftType
                var modelSystemGiftType = new SystemGiftType_DataModel();
                modelSystemGiftType.PageIndex = 1;
                modelSystemGiftType.PageSize = 9999;

                var listSystemGiftType = _systemGiftTypeService.GetAll(modelSystemGiftType);

                ViewBag.listSystemGiftType = listSystemGiftType.data.List;
                #endregion

                #region dropdownData GiftOpenCondType
                var modelGiftOpenCondType = new GiftOpenCondType_DataModel();
                modelGiftOpenCondType.PageIndex = 1;
                modelGiftOpenCondType.PageSize = 9999;

                var listGiftOpenCondType = _giftOpenCondTypeService.GetAll(modelGiftOpenCondType);

                ViewBag.listGiftOpenCondType = listGiftOpenCondType.data.List;
                #endregion

                #region GiftComponentType
                var modelGiftComponentType = new GiftComponentType_DataModel();
                modelGiftComponentType.PageIndex = 1;
                modelGiftComponentType.PageSize = 9999;

                var listGiftComponentType = _giftComponentTypeService.GetAll(modelGiftComponentType);

                ViewBag.listGiftComponentType = listGiftComponentType.data.List;
                #endregion
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Add(FormCollection fc, SimilarGiftTypeModel model)
        {
            try
            {
                model.IsSelected = false;
                model.CodeID = Globals.RandomCode(5);
                var data_response = _similarGiftTypeService.Add(model);

                var similarModel = new SimilarGiftTypeGiftComponentTypeModel();
                if (model.GiftComponentType != null && model.GiftComponentType.Count > 0)
                {
                    foreach (var item in model.GiftComponentType)
                    {
                        similarModel.SimilarGiftTypeCodeID = data_response.data.CodeID;
                        similarModel.GiftComponentTypeCodeID = Protector.String(item);
                        var response = _similarGiftTypeGiftComponentTypeService.Add(similarModel);
                    }
                }

                return RedirectToAction("/Index");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }
    }
}