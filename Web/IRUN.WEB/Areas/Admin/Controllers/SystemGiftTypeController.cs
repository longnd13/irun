﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class SystemGiftTypeController : BaseController
    {
        private readonly IcpSystemGiftTypeService _systemGiftTypeService;
        private readonly IcpGiftCreateCondTypeService _giftCreateCondTypeService;
        public SystemGiftTypeController(IcpSystemGiftTypeService SystemGiftTypeService, IcpGiftCreateCondTypeService giftCreateCondTypeService)
        {
            _systemGiftTypeService = SystemGiftTypeService;
            _giftCreateCondTypeService = giftCreateCondTypeService;
        }

        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new SystemGiftType_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;
                var giftModel = new GiftCreateCondType_DataModel();
                giftModel.PageIndex = 1;
                giftModel.PageSize = 9999;

                var list = _systemGiftTypeService.GetAll(model);
                long lCount = list.data.Count;

                
                foreach (var item in list.data.List)
                {
                    var multipleList = _giftCreateCondTypeService.GetByID(item.GiftCreateCondTypeCodeID);
                    giftModel.List.Add(multipleList.data);
                }
                ViewBag.listGiftCreateCondType = giftModel.List;
                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(string CodeID)
        {
            string msg = "";
            try
            {
                var result = _systemGiftTypeService.Delete(CodeID);
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(string CodeID)
        {
            try
            {
                var model = new SystemGiftTypeModel();

                var msg = _systemGiftTypeService.GetByCodeId(CodeID);
                var giftModel = new GiftCreateCondType_DataModel();
                giftModel.PageIndex = 1;
                giftModel.PageSize = 9999;

                var list = _giftCreateCondTypeService.GetAll(giftModel);

                ViewBag.listServices = list.data.List;

                var multipleList = _giftCreateCondTypeService.GetAll(giftModel);

                ViewBag.listGiftCreateCondType = multipleList.data.List;
                return View(msg.data);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Update(FormCollection fc, SystemGiftTypeModel model)
        {
            try
            {
                var msg = _systemGiftTypeService.Updated(model);

                if (msg.success.Equals(true))
                {
                    return RedirectToAction("/Index");
                }
                else
                {
                    ViewBag.message = msg.message;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }


        public ActionResult Add()
        {
            try
            {
                var model = new GiftCreateCondType_DataModel();
                model.PageIndex = 1;
                model.PageSize = 9999;

                var list = _giftCreateCondTypeService.GetAll(model);

                ViewBag.listGiftCreateCondType = list.data.List;

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Add(FormCollection fc, SystemGiftTypeModel model)
        {
            try
            {
                var data_response = _systemGiftTypeService.Add(model);

                var similarModel = new SystemGiftTypeGiftCreateCondTypeModel();

                return RedirectToAction("/Index");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View();
        }
    }
}