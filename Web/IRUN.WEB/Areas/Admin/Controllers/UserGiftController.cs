﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class UserGiftController : BaseController
    {
        private readonly IcpUserGiftService _userGiftService;
        private readonly IcpSystemGiftTypeService _systemGiftTypeService;
        private readonly IcpSimilarGiftTypeService _similarGiftTypeService;
        public UserGiftController(IcpUserGiftService UserGiftService, IcpSystemGiftTypeService systemGiftTypeService, IcpSimilarGiftTypeService similarGiftTypeService)
        {
            _userGiftService = UserGiftService;
            _systemGiftTypeService = systemGiftTypeService;
            _similarGiftTypeService = similarGiftTypeService;
        }
        // GET: Admin/UserGift
        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new UserGift_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _userGiftService.GetAll(model);
                long lCount = list.data.Count;

                #region load dropdownlist System Gift
                var systemGiftModel = new SystemGiftType_DataModel();
                systemGiftModel.PageIndex = Protector.Int(Page, 1);
                systemGiftModel.PageSize = 9999;

                var systemGift = _systemGiftTypeService.GetAll(systemGiftModel);
                ViewBag.listSystemGift = systemGift.data.List;
                #endregion

                #region load dropdownlist Similar Gift
                var similarModel = new SimilarGiftType_DataModel();
                similarModel.PageIndex = Protector.Int(Page, 1);
                similarModel.PageSize = 9999;

                var similar = _similarGiftTypeService.GetAll(similarModel);
                ViewBag.listSimilar = similar.data.List;
                #endregion

                #region Loop System Gift Type By ID
                var systemGiftFilterModel = new SystemGiftType_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var rewardSystemGift = _systemGiftTypeService.GetByCodeId(list.data.List[i].SystemGiftTypeCodeID);
                    systemGiftFilterModel.List.Add(rewardSystemGift.data);
                }
                ViewBag.ListFilterSystemGift = systemGiftFilterModel.List;
                #endregion

                #region Loop Similar Gift Type By ID
                var similarGiftFilterModel = new SimilarGiftType_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var rewardSimilar = _similarGiftTypeService.GetByID(list.data.List[i].SimilarGiftTypeID.Trim());
                    similarGiftFilterModel.List.Add(rewardSimilar.data);
                }
                ViewBag.ListFilterSimilarGift = similarGiftFilterModel.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(int? Page, string SystemGiftTypeCodeID, string SimilarGiftTypeID)
        {
            try
            {
                var model = new UserGift_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _userGiftService.Search(model, SystemGiftTypeCodeID, SimilarGiftTypeID);
                long lCount = list.data.Count;

                #region load dropdownlist System Gift
                var systemGiftModel = new SystemGiftType_DataModel();
                systemGiftModel.PageIndex = Protector.Int(Page, 1);
                systemGiftModel.PageSize = 9999;

                var systemGift = _systemGiftTypeService.GetAll(systemGiftModel);
                ViewBag.listSystemGift = systemGift.data.List;
                #endregion

                #region load dropdownlist Similar Gift
                var similarModel = new SimilarGiftType_DataModel();
                similarModel.PageIndex = Protector.Int(Page, 1);
                similarModel.PageSize = 9999;

                var similar = _similarGiftTypeService.GetAll(similarModel);
                ViewBag.listSimilar = similar.data.List;
                #endregion

                #region Loop System Gift Type By ID
                var systemGiftFilterModel = new SystemGiftType_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var rewardSystemGift = _systemGiftTypeService.GetByCodeId(Protector.String(list.data.List[i].SystemGiftTypeCodeID));
                    systemGiftFilterModel.List.Add(rewardSystemGift.data);
                }
                ViewBag.ListFilterSystemGift = systemGiftFilterModel.List;
                #endregion

                #region Loop Similar Gift Type By ID
                var similarGiftFilterModel = new SimilarGiftType_DataModel();

                for (int i = 0; i < list.data.Count; i++)
                {
                    var rewardSimilar = _similarGiftTypeService.GetByID(list.data.List[i].SimilarGiftTypeID);
                    similarGiftFilterModel.List.Add(rewardSimilar.data);
                }
                ViewBag.ListFilterSimilarGift = similarGiftFilterModel.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var result = _userGiftService.Delete(Protector.Int(id));
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
    }
}