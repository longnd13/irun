﻿using Prj.Irun.BusinessLogic.Interfaces;
using Prj.Irun.BusinessLogic.Models;
using Prj.Irun.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRUN.WEB.Areas.Admin.Controllers
{
    public class UserRewardItemActivityController : BaseController
    {
        private readonly IcpUserRewardItemActivityService _userRewardItemActivityService;
        private readonly IcpRewardItemService _rewardItemService;
        private readonly IcpUserRewardItemTypeService _userRewardItemTypeService;
        public UserRewardItemActivityController(IcpUserRewardItemActivityService userRewardItemActivityService, IcpRewardItemService rewardItemService, IcpUserRewardItemTypeService userRewardItemTypeService)
        {
            _userRewardItemActivityService = userRewardItemActivityService;
            _rewardItemService = rewardItemService;
            _userRewardItemTypeService = userRewardItemTypeService;
        }
        // GET: Admin/UserRewardItemActivity
        public ActionResult Index(int? Page)
        {
            try
            {
                var model = new UserRewardItemActivity_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _userRewardItemActivityService.GetAll(model);
                long lCount = list.data.Count;

                #region data Reward Item
                var rewardItemData = new RewardItem_DataModel();
                for (int i = 0; i < list.data.Count; i++)
                {
                    var rewardItem = _rewardItemService.GetByID(Protector.Int(list.data.List[i].Id));
                    rewardItemData.List.Add(rewardItem.data);
                }
                ViewBag.listRewardItemData = rewardItemData.List;
                #endregion

                #region data User Reward Item Type
                var userRewardItemTypeData = new UserRewardItemType_DataModel();
                for (int i = 0; i < list.data.Count; i++)
                {
                    var userRewardItemType = _userRewardItemTypeService.GetByID(Protector.Int(list.data.List[i].Id));
                    userRewardItemTypeData.List.Add(userRewardItemType.data);
                }
                ViewBag.listUserRewardItemTypeData = userRewardItemTypeData.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(int? Page, string userID)
        {
            try
            {
                var model = new UserRewardItemActivity_DataModel();
                model.PageIndex = Protector.Int(Page, 1);
                model.PageSize = 50;

                var list = _userRewardItemActivityService.SearchByUsername(model, userID);
                long lCount = list.data.Count;

                #region data Reward Item
                var rewardItemData = new RewardItem_DataModel();
                for (int i = 0; i < list.data.Count; i++)
                {
                    var rewardItem = _rewardItemService.GetByID(Protector.Int(list.data.List[i].Id));
                    rewardItemData.List.Add(rewardItem.data);
                }
                ViewBag.listRewardItemData = rewardItemData.List;
                #endregion

                #region data User Reward Item Type
                var userRewardItemTypeData = new UserRewardItemType_DataModel();
                for (int i = 0; i < list.data.Count; i++)
                {
                    var userRewardItemType = _userRewardItemTypeService.GetByID(Protector.Int(list.data.List[i].Id));
                    userRewardItemTypeData.List.Add(userRewardItemType.data);
                }
                ViewBag.listUserRewardItemTypeData = userRewardItemTypeData.List;
                #endregion

                #region Paging
                if (lCount > 0)
                {
                    if (PagingHelper.GetUrl(this.Request.Url.PathAndQuery) == this.Request.Url.AbsolutePath)
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(this.Request.Url.AbsolutePath, lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 1);
                    }
                    else
                    {
                        ViewData["Page"] = PagingHelper.PagingAdmin(PagingHelper.GetUrl(this.Request.Url.PathAndQuery), lCount, ProtectorHelper.Int(model.PageSize), ProtectorHelper.Int(model.PageIndex), 2);
                    }
                }
                #endregion
                ViewBag.Count = list.data.Count;
                ViewBag.listData = list.data.List;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public JsonResult Delete(int id)
        {
            string msg = "";
            try
            {
                var result = _userRewardItemActivityService.Delete(Protector.Int(id));
                if (result.success.Equals(true))
                {
                    msg = "OK";
                }
                else
                {
                    msg = "Oop! Something wrong, please try again...";
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
    }
}